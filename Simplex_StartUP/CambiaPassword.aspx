<%@ Page Language="C#" MasterPageFile="~/MasterDESKTOP.Master" AutoEventWireup="true" CodeBehind="CambiaPassword.aspx.cs" Inherits="SIMPLEX_STARTUP.CambiaPassword" Title="Cambia Password" %>
<%@ Register Assembly="WebSimplex" Namespace="WebSimplex" TagPrefix="WS1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="MainFormSection">

 <WS1:WebSimplexMessageBox ID="R_MessageBox" runat="server" 
        AlertText="Attenzione, vedi il riquadro degli errori." 
        OkAlertButtonCssStyle="AlertButton" 
        AlertImage="Immagini/Omino_Errore.png" 
        OkMessageImage="Immagini/Megafono_tondoviola.png" 
        OkMessageText="Password sostituita con successo. Prego premere OK e rieffettuare l'autenticazione per accedere al sistema."
        OkMessageButtonCssStyle="OkButton" 
        UndoMessageButtonCssStyle="UndoButton" CssOkMsgClass="OkMsg" />

<table class="FRM">
    <tr class="FRM">
    <td class="FRM_COL1"></td>
    <td class="FRM_COL2"></td>
    <td class="FRM_COL3"></td>
    </tr>
    <tr class="FRM">
        <td class="FRM_COL1"></td>
        <td class="FRM_COL2">
        <!-- MessageBox -->
        <asp:Panel ID="Panel_MessageBox" runat="server" CssClass="MessageBox">
           <asp:Label  CssClass="MessageText" ID="L_Message" runat="server" Text="Autenticazione fallita."></asp:Label><br />
           <asp:Label  CssClass="SubMessageText" ID="L_SubMessage" runat="server" Text="Le due password immesse non coincidono."></asp:Label><br /><br /><br />
            <asp:Button ID="R_Annulla" runat="server" Text="Annulla" 
                onclick="R_Annulla_Click" />
            &nbsp;
            &nbsp;
            &nbsp;
            <!--<asp:Button ID="R_Conferma" runat="server" Text="Conferma" /> -->
        </asp:Panel>
        <!-- END MessageBox -->
        </td>
    <td class="FRM_COL3"></td>
    </tr>

<!-- PUT YOUR CONTROLS FROM HERE -->
<!-- PUT YOUR CONTROLS TO HERE -->
    <tr class="FRM">
        <td class="FRM_COL1">
            <asp:Label ID="Label1" runat="server" AssociatedControlID="R_Pwd1" 
                CssClass="FRM_lbl" Text="Scegliere Password"></asp:Label>
        </td>
        <td class="FRM_COL2">
            <asp:TextBox ID="R_Pwd1" runat="server" CssClass="FRM_txt" Width="300px" 
                TextMode="Password"></asp:TextBox>
        </td>
    <td class="FRM_COL3">&nbsp;</td>
    </tr>

    <tr class="FRM">
        <td class="FRM_COL1">
            <asp:Label ID="L_Password2" runat="server" AssociatedControlID="R_Pwd2" 
                CssClass="FRM_lbl" Text="Ripetere Password"></asp:Label>
        </td>
        <td class="FRM_COL2">
            <asp:TextBox ID="R_Pwd2" runat="server" CssClass="FRM_txt" 
                TextMode="Password" Width="299px"></asp:TextBox>
        </td>
    <td class="FRM_COL3">&nbsp;</td>
    </tr>

    <tr class="FRM">
        <td class="FRM_COL1">&nbsp;</td>
        <td class="FRM_COL2">
            <asp:Button ID="R_Login" runat="server" Text="Cambia" onclick="R_Login_Click" />
        </td>
    <td class="FRM_COL3">&nbsp;</td>
    </tr>

</table>

</div>
</asp:Content>
