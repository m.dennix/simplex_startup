<%@ Page Language="C#" MasterPageFile="~/MasterDESKTOP.Master" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="SIMPLEX_STARTUP.Login" Title="Autenticazione" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="MainFormSection">

<table class="FRM">
    <tr class="FRM">
    <td class="FRM_COL1"></td>
    <td class="FRM_COL2"></td>
    <td class="FRM_COL3"></td>
    </tr>
    <tr class="FRM">
        <td class="FRM_COL1"></td>
        <td class="FRM_COL2">
        <!-- MessageBox -->
        <asp:Panel ID="Panel_MessageBox" runat="server" CssClass="MessageBox">
           <asp:Label  CssClass="MessageText" ID="L_Message" runat="server" Text="Autenticazione fallita."></asp:Label><br />
           <asp:Label  CssClass="SubMessageText" ID="L_SubMessage" runat="server" Text="Username e/o password errate."></asp:Label><br /><br /><br />
            <asp:Button ID="R_Annulla" runat="server" Text="Annulla" 
                onclick="R_Annulla_Click" />
            &nbsp;
            &nbsp;
            &nbsp;
            <!--<asp:Button ID="R_Conferma" runat="server" Text="Conferma" /> -->
        </asp:Panel>
        <!-- END MessageBox -->
        </td>
    <td class="FRM_COL3"></td>
    </tr>

<!-- PUT YOUR CONTROLS FROM HERE -->
<!-- PUT YOUR CONTROLS TO HERE -->
    <tr class="FRM">
        <td class="FRM_COL1">
            <asp:Label ID="Label1" runat="server" AssociatedControlID="R_UserName" 
                CssClass="FRM_lbl" Text="Nome Utente"></asp:Label>
        </td>
        <td class="FRM_COL2">
            <asp:TextBox ID="R_UserName" runat="server" CssClass="FRM_txt" Width="300px"></asp:TextBox>
        </td>
    <td class="FRM_COL3">&nbsp;</td>
    </tr>

    <tr class="FRM">
        <td class="FRM_COL1">
            <asp:Label ID="L_Password" runat="server" AssociatedControlID="R_Password" 
                CssClass="FRM_lbl" Text="Password"></asp:Label>
        </td>
        <td class="FRM_COL2">
            <asp:TextBox ID="R_Password" runat="server" CssClass="FRM_txt" 
                TextMode="Password" Width="299px"></asp:TextBox>
        </td>
    <td class="FRM_COL3">&nbsp;</td>
    </tr>

    <tr class="FRM">
        <td class="FRM_COL1">&nbsp;</td>
        <td class="FRM_COL2">
            <asp:Button ID="R_Login" runat="server" Text="Accedi" onclick="R_Login_Click" />
        </td>
    <td class="FRM_COL3">&nbsp;</td>
    </tr>

</table>

</div>
</asp:Content>
