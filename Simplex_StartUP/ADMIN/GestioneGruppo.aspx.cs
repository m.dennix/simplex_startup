using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;

namespace SIMPLEX_STARTUP
{
    /// <summary>
    /// Classe TEMPLATE per la creazione di una maschera che mostra una lista di oggetti.
    /// </summary>
    public partial class GestioneGruppo : GenericSpxForm
    {
        /// Stringa della query
        /// </summary>
        String MySql = null;
        /// <summary>
        /// Stringa del filtro
        /// </summary>
        String MyFilter = null;



        /// <summary>
        /// <p>Caricamento della maschera.</p>
        /// <ul>
        /// <li>Invoca preloadEnvironment()</li>
        /// <li>esegue la query di popolamento</li>
        /// </ul>
        /// <pre>
        /// @MdN
        /// ----
        /// Crtd: 30/01/2015
        /// Mdfd: 26/06/2015
        /// </pre>
        /// </summary>
        /// <param name="sender">oggetto che ha invocato l'evento</param>
        /// <param name="e">parametri</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            preloadEnvironment();

            //Traccio l'ingresso nel log
            writeMessageToLog(MyConn, "Form_Load()", MySbc.ToString(), ((UNIF_User)MyApp.CurrentUser).UID.ToString(), Session.SessionID);

        }

        /// <summary>
        /// <p>
        /// Imposta gli attributi necessari al funzionamento.
        /// Si attende in Session["APPLICATION"] l'oggetto GeneralApplication corrente.
        /// Se non è presente, lancia un eccezione del tipo generalApplication TracedMessage.
        /// </p>
        /// <p>
        /// Imposta in sessione l'ID dell'oggetto selezionato. La variabile di sessione ha come 
        /// stringa identificatrice il nome della tabella di riferimento dell'oggetto tutto in maiuscolo.
        /// </p>
        /// <p>Esempio: </p>
        /// <p>
        /// se la tabella è 'utente' il nome della variabile di sessione è 'UTENTE'.
        /// </p>
        /// 
        /// <p>Valorizza i seguenti attributi derivati da GenericSpxForm:
        /// </p>
        /// <ul>
        /// <li>MyApp               :   APPLICAZIONE CORRENTE;</li>
        /// <li>MyConn              :   CONNESSIONE DI DEFAULT;</li>
        /// <li>MyMainMenu          :   CONTROLLO MENU SINISTRO;</li>
        /// <li>MySbc               :   CONTROLLO BREADCRUMBS;</li>
        /// <li>MyAspxFileName      :   NOME DELLA PAGINA WEB/WEB FORM.</li>
        /// </ul>
        /// <p>
        /// Valorizza i suoi attributi caratteristici che sono:
        /// </p><p>
        /// <ul>
        /// <li>MyBackURL           :   URL DELLA PAGINA CHIAMANTE.</li>
        /// </ul>
        /// </p>
        /// </summary>
        protected override void preloadEnvironment()
        {
            MyFunzioneApplicativa = "Amministrazione";

            base.preloadEnvironment();
            #region [ADD YOUR CODE HERE]

            MyName = "Gestione Gruppi";
            Title = MyName;

            MyAspxFileName = "~/SIMPLEX_STARTUP_ADMIN/" + MyAspxFileName; //@MdN: 23/07/2015

            AddToSimplexBreadCrumbs();
            // Gestione TBL_UNDER_FORM
            TBL_UNDER_FORM = (simplex_ORM.SQLSrv.SQLTable)Session["GRUPPO"];
            if (TBL_UNDER_FORM == null)
                TBL_UNDER_FORM = simplex_ORM.SQLSrv.SQLTable.createSQLTable(MyConn, "ADMN_gruppo");
            R_Nuovo.Text = "Salva";

            #endregion
        }

        protected void R_Lista_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            Int32 _row = 0;
            Int32 _key = 0;

            // indentificazione della riga selezionata
            if (e.CommandName.CompareTo("Edit") != 0)
                return;

            // identificare la riga che ha scatenato l'evento: ASP.NET non lo fa in automatico
            // N.B. CommandArgument è 0-BASED!!!!
            _row = Int32.Parse(e.CommandArgument.ToString());

            // identificare il valore della chiave associata alla riga
            _key = Int32.Parse(((GridView)e.CommandSource).Rows[_row].Cells[0].Text);

            // Creare l'oggetto SQLTable e caricarvi il record identificato da l_key
            try
            {
                if (MyConn.State == ConnectionState.Closed)
                    MyConn.Open();
                TBL_UNDER_FORM = simplex_ORM.SQLSrv.SQLTable.createSQLTable(MyConn, "ADMN_gruppo");
                TBL_UNDER_FORM.setValue("ID", _key.ToString());
                if (MyConn.State == ConnectionState.Closed)
                    MyConn.Open();
                TBL_UNDER_FORM.load();
                MyConn.Close();
            }
            catch (simplex_ORM.Spx_ORMMessage SORMM)
            {
                MyErrors.Add(SORMM);
                showErrors();
                return;
            }

            // Immettere l'oggetto in sessione nella variabile GRUPPO
            Session["GRUPPO"] = TBL_UNDER_FORM;

            //@MdN 23/07/2015
            if (TBL_UNDER_FORM.getValue("ID").ToString() != null)
                writeMessageToLog(MyConn, "R_Lista_RowCommand(): ID=" + TBL_UNDER_FORM.getValue("ID").ToString(), MySbc.ToString(), ((UNIF_User)MyApp.CurrentUser).UID.ToString(), Session.SessionID);


            // Passa il controllo alla pagina medesima
            Response.Redirect("GestioneGruppo.aspx");
        }
     
        protected void R_Nuovo_Click(object sender, EventArgs e)
        {
            if (TBL_UNDER_FORM == null)
            {
                simplex_ORM.Spx_ORMMessage ORMM = new simplex_ORM.Spx_ORMMessage("GestioneGruppo.R_Nuovo_Click(): Oggetto TBL_UNDER_FORM non istanziato.<br>Contattare l'informatica.",1003);
                MyErrors.Add(ORMM);
                ORMM.traceLog(MyConn, ((UNIF_User)MyApp.CurrentUser).UID.ToString(), Session.SessionID);
                return;
            }
            TBL_UNDER_FORM.setValue("USR", ((UNIF_User)MyApp.CurrentUser).UID.ToString());
            try
            {
                this.modify(TBL_UNDER_FORM, "ContentPlaceHolder1", "System.Web.UI.WebControls.ContentPlaceHolder");
            }
            catch (simplex_ORM.Spx_ORMMessage ORMM)
            {
                MyErrors.Add(ORMM);
                ORMM.traceLog(MyConn, ((UNIF_User)MyApp.CurrentUser).UID.ToString(), Session.SessionID);
            }
            this.R_Lista.DataBind();
            showErrors();

            //@MdN 23/07/2015
            if (TBL_UNDER_FORM.getValue("ID").ToString() != null)
                writeMessageToLog(MyConn, "R_Nuovo_Click(): ID=" + TBL_UNDER_FORM.getValue("ID").ToString(), MySbc.ToString(), ((UNIF_User)MyApp.CurrentUser).UID.ToString(), Session.SessionID);

        }

        protected void R_Elimina_Click(object sender, EventArgs e)
        {
            TBL_UNDER_FORM.setValue("DATA_DEL", simplex_ORM.Column.DateToSQLDateString(DateTime.Now));
            this.R_Nuovo_Click(sender, e);
                        
            TBL_UNDER_FORM = simplex_ORM.SQLSrv.SQLTable.createSQLTable(MyConn,"ADMN_gruppo");
            Session["GRUPPO"] = TBL_UNDER_FORM;
           
            this.ID.Text = "";
            this.DENOMINAZIONE.Text = "";
            this.NOTE.Text = "";
        }

        protected void R_Indietro_Click(object sender, EventArgs e)
        {
            // @MdN 23/07/2015
            String _callingpage = MySbc.PreviousLink;
            Response.Redirect(_callingpage);

            //OBSOLETO 23/07/2015
            //if ((String)Session["CALLING-PAGE"] != null && ((String)Session["CALLING-PAGE"]).CompareTo(MyAspxFileName) != 0)
            //{
            //    String _callingpage = (String)Session["CALLING-PAGE"];
            //    Session["CALLING-PAGE"] = null;
            //    Response.Redirect(_callingpage);
            //}
            //OBSOLETO 23/07/2015
        }

        /// <summary>
        /// <p>
        /// Crea una nuova istanza dell'oggetto Gruppo.
        /// </p>
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 28/07/2015
        /// </pre>
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void R_Crea_Click(object sender, EventArgs e)
        {
            TBL_UNDER_FORM = simplex_ORM.SQLSrv.SQLTable.createSQLTable(MyConn, "ADMN_gruppo");
            Session["GRUPPO"] = TBL_UNDER_FORM;

            this.ID.Text = "";
            this.DENOMINAZIONE.Text = "";
            this.NOTE.Text = "";

        }//fine
    }
}
