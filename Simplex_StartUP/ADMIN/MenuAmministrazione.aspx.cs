using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;

namespace SIMPLEX_STARTUP
{
    public partial class MenuAmministrazione : GenericSpxForm
    {
        #region ATTRIBUTI
        private UNIF_User MyUser;
        /// <summary>
        /// <p>
        /// Numero di colonne (icona, descrizione) su cui visualizzare il menu.
        /// </p>
        /// <pre>
        /// @MdN
        /// ----
        /// Crtd: 23/03/2015
        /// </pre>
        /// </summary>
        protected int MyColonne = 1;

        /// <summary>
        /// Funzione applicativa o programma di cui su vuole estrarre il menu.
        /// Il valore (intero) consente di specificare una funzione applicativa 
        /// (Esempio la stessa web application "Risorse Umane" può presentare 
        /// due funzioni applicative distinte come se fossero due programmi diversi: 
        /// Gestione Giuridica e Gestione Economica.
        /// Se il valore è -1, si estraggono tutte le voci del menù e quindi tutte le
        /// funzioni applicative.
        /// </summary>
        protected int FunzioneApplicativa = -1;
        /// <summary>
        /// Livello delle voci di menu da visualizzare
        /// </summary>
        protected int Livello = 0;

        #endregion

        #region GESTORI

        protected override void Page_Load(object sender, EventArgs e)
        {
            preloadEnvironment();
            FunzioneApplicativa = ((UNIF_Application)MyApp).getIDByName("Amministrazione"); 
            Livello = (int)((UNIF_Application)MyApp).lookForMenuVoiceID("Amministrazione");
            designMenu(Livello, FunzioneApplicativa);
            if (IsPostBack == false)
                writeMessageToLog(MyConn, "Form_Load()", MySbc.ToString(), ((UNIF_User)MyApp.CurrentUser).UID.ToString(), Session.SessionID);
            Session["CALLING-PAGE"] = "~/SIMPLEX_STARTUP_ADMIN/" + MyAspxFileName;
        }

        /// <summary>
        /// Gestore dell'evento Click sul menu
        /// </summary>
        /// <param name="p_Obj">Oggetto Sender</param>
        /// <param name="e">Argomenti</param>
        public void SimplexMenu_Click(Object p_Obj, EventArgs e)
        {

        }

        #endregion

        #region METDI PROTETTI
        /// <summary>
        /// <p>
        /// Disegna la struttura del menu.
        /// </p>
        /// <pre>
        /// @MdN
        /// ----
        /// Crtd: 23/03/2015
        /// </pre>
        /// </summary>
        /// <param name="p_level">identificativo del livello del menu</param>
        /// <param name="p_FunzApp">identificativo della funzione applicativa (-1 tutte)</param>
        /// <remarks>Il numero di colonne su cui visualizzare il menu èscritto nell'attributo protetto MyColonne.</remarks>
        protected virtual void designMenu(int p_level, int p_FunzApp)
        {
            designMenu(p_level, p_FunzApp, MyColonne);
        }

        /// <summary>
        /// Aggiunge la pagina corrente alla catena delle briciole di pane (SimplexBreadCrumbs)
        /// della maschera corrente.
        /// @MdN
        /// ----
        /// Crtd: 18/01/2015
        /// Mdfd: 25/01/2015
        /// Mdfd: 17/05/2016
        /// </summary>
        /// <remarks>La proprietà MySbc DEVE essere valorizzata.</remarks>
        protected override void AddToSimplexBreadCrumbs()
        {
            int _lvl;
            // controllo
            if (MySbc == null)
                return;
            // AGGIUNTA DI UNA VOCE NELLA CATENA BREADCRUMBS
            if (MyMainMenu != null)
            {
                // Se esiste un menu mi pongo il problema della coerenza tra il menu 
                // e la catena di BreadCrumbs.

                _lvl = MasterPCM.getMenuVoiceLevel(MyConn, MyName);

                // Eccezione: la pagina di indice 0 è sempre la root
                if (_lvl == 0)
                {
                    MySbc.InsertAndBreak(MyName, MyAspxFileName, _lvl);         //@MdN 17/05/2016
                    return;
                }

                if (_lvl == -1)
                    MySbc.Add(MyName, MyAspxFileName);
                else
                {
                    if (_lvl > MySbc.Count)
                        MySbc.Add(MyName, MyAspxFileName);                      //@MdN 25/01/2015
                    else
                        MySbc.InsertAndBreak(MyName, MyAspxFileName, _lvl);     //@MdN 25/01/2015
                }
            }
            else
            {
                // Se il menu non esiste non mi pongo problemi ed accodo all'ultimo anello della catena.
                if (MySbc != null)
                    MySbc.Add(MyName, MyAspxFileName);
            }
        }//fine


        /// <summary>
        /// <p>
        /// Disegna la struttura del menu.
        /// </p>
        /// <pre>
        /// @MdN
        /// ----
        /// Crtd: ?????
        /// Mdfd: @MdN 23/03/2015;
        /// </pre>
        /// </summary>
        /// <param name="p_level">identificativo del livello del menu</param>
        /// <param name="p_FunzApp">identificativo della funzione applicativa (-1 tutte)</param>
        /// <param name="p_numCols">Numero di colonne su cui visualizzare il menu.</param>
        /// <remarks>Il numero di colonne su cui visualizzare il menu modifica l'attributo protetto MyColonne.</remarks>
        protected virtual void designMenu(int p_level, int p_FunzApp, int p_numCols)
        {
            System.Data.Odbc.OdbcCommand _cmd = null;
            System.Data.Odbc.OdbcDataReader _rst = null;

            // Controlli
            if (MyConn == null)
                return;

            // Imposta la proprietà MyColonne. @MdN 23/03/2015
            if (p_numCols == 0)
                MyColonne = 1;
            else
                MyColonne = p_numCols;

            //Composizione della stringa di interrogazione del database
            System.Text.StringBuilder _sql = new System.Text.StringBuilder("SELECT ");
            _sql.Append("UM.ID");
            _sql.Append(", UM.DESCRIZIONE");
            _sql.Append(", ISNULL(UM.PADRE,0) AS PADRE");
            _sql.Append(", UM.LINK");
            _sql.Append(", UM.IMGURL");
            _sql.Append(", UM.NOTE ");
            _sql.Append("FROM UTILS_MENU UM ");
            _sql.Append("WHERE ISNULL(UM.PADRE,0)=").Append(p_level);
            _sql.Append(" AND ID <> PADRE ");
            if (FunzioneApplicativa > -1)
                _sql.Append(" AND FUNZIONE_APPLICATIVA= ").Append(p_FunzApp);
            _sql.Append(" ORDER BY PADRE, ID");

            _cmd = MyConn.CreateCommand();
            _cmd.CommandText = _sql.ToString();
            if (MyConn.State == ConnectionState.Closed)
                MyConn.Open();
            _rst = _cmd.ExecuteReader();
            if (_rst.HasRows == false)
            {
                MyConn.Close();
                _rst.Dispose();
                return;
            }
            // AGGIUNTA DEI CONTROLLI ALLA FORM
            Literal _ltrl = null;
            Image _Img = null;
            LinkButton _lnkb = null;
            int _cols = 0;

            _ltrl = new Literal();
            _ltrl.Text = "<table class=\"SimplexMenuTable\">";
            Master.FindControl("form1").Controls.Add(_ltrl);
            bool _even = true;
            while (_rst.Read())
            {
                if (_cols == 0)
                {
                    _ltrl = new Literal();
                    if (_even == true)
                        _ltrl.Text = "<tr class=\"SpxOddMenuRow\">";
                    else
                        _ltrl.Text = "<tr class=\"SpxEvenMenuRow\">";
                    Master.FindControl("form1").Controls.Add(_ltrl);
                }

                _ltrl = new Literal();
                _ltrl.Text = "<td class=\"SpxMenuCell1\">";
                Master.FindControl("form1").Controls.Add(_ltrl);

                //Inserire l'immagine, se esiste
                _ltrl = new Literal();
                if (_rst.IsDBNull(4) == false)
                {
                    _Img = new Image();
                    _Img.CssClass = "SpxMenuImage";
                    _Img.ImageUrl = _rst.GetString(4);
                    _Img.AlternateText = _rst.GetString(1);
                    Master.FindControl("form1").Controls.Add(_Img);
                }
                _ltrl = new Literal();
                _ltrl.Text = "</td><td class=\"SpxMenuCell2\">";
                Master.FindControl("form1").Controls.Add(_ltrl);


                //Inserire il Link
                if (_rst.IsDBNull(1) == false && _rst.IsDBNull(3) == false)
                {
                    _lnkb = new LinkButton();
                    _lnkb.Text = _rst.GetString(1);
                    _lnkb.ID = "R_LinkButton_" + _rst.GetInt32(0).ToString();
                    _lnkb.Click += SimplexMenu_Click;
                    _lnkb.PostBackUrl = _rst.GetString(3);
                    _lnkb.CssClass = "SpxMenuLinkB";
                    _lnkb.Text = _rst.GetString(1);
                    Master.FindControl("form1").Controls.Add(_lnkb);
                }
                _ltrl = new Literal();
                _ltrl.Text = "</td><td class=\"SpxMenuCell3\">";
                Master.FindControl("form1").Controls.Add(_ltrl);

                //Inserire eventuali note
                if (_rst.IsDBNull(5) == false)
                {
                    _ltrl = new Literal();
                    _ltrl.Text = _rst.GetString(5);
                    Master.FindControl("form1").Controls.Add(_ltrl);
                }

                _ltrl = new Literal();
                _ltrl.Text = "</td>";
                Master.FindControl("form1").Controls.Add(_ltrl);

                if (_cols == (MyColonne - 1))
                {
                    _ltrl = new Literal();
                    _ltrl.Text = "</tr>";
                    Master.FindControl("form1").Controls.Add(_ltrl);
                    _even = _even ^ true;
                }
                //_even = _even ^ true;
                _cols = (_cols < (MyColonne - 1)) ? (_cols + 1) : 0;

            }//FINE DEL CICLO
            // chiusura della riga
            if (_cols != 0)
            {
                while (_cols < (MyColonne))
                {
                    _ltrl = new Literal();
                    _ltrl.Text = "<td class=\"SpxMenuCell1\"></td><td class=\"SpxMenuCell2\"></td><td class=\"SpxMenuCell3\"></td>";
                    Master.FindControl("form1").Controls.Add(_ltrl);
                    _cols++;
                }
                _ltrl = new Literal();
                _ltrl.Text = "</tr>";
                Master.FindControl("form1").Controls.Add(_ltrl);
            }
            // chiusura della tabella
            _ltrl = new Literal();
            _ltrl.Text = "</table>";
            Master.FindControl("form1").Controls.Add(_ltrl);

            // FINE DELLA COMPOSIZIONE DELLA TABELLA
            _rst.Close();
            _cmd.Dispose();
            MyConn.Close();
        }

        protected override void preloadEnvironment()
        {
            base.preloadEnvironment();
            MyFunzioneApplicativa = "Amministrazione";

            //@MdN 25/06/2015
            /* Prelevo dalla sessione l'applicazione */
            if (IsPostBack == false)
            {
                MyApp = (UNIF_Application)Session["APPLICATION"];
                try
                {
                    // @MdN 22/07/2015: old - obsoleta //FunzioneApplicativa = int.Parse(ConfigurationSettings.AppSettings[MyFunzioneApplicativa]);
                    FunzioneApplicativa = ((UNIF_Application)MyApp).getIDByName(MyFunzioneApplicativa);
                }
                catch (Exception EE)
                {
                    Session["SpxErrorMessage"] = "Impossibile trovare la chiave 'Amminsitrazione' nel file Web.config";
                    Response.Redirect("~/GenericSpxErrorForm.aspx");
                }

                if (MyApp == null)
                {
                    MyApp = new UNIF_Application();
                    //1) Caricare la connessione ODBC
                    MyApp.loadConnections("OdbcConfig");

                    MyUser = (UNIF_User)Session["UNIF_User"];
                    Session["UNIF_User"] = null;
                    if (MyUser == null)
                        Response.Redirect("~/GenericSpxErrorForm.aspx");
                    MyApp.CurrentUser = MyUser;

                }
                //CAMBIO FUNZIONE APPLICATIVA
                if (((UNIF_Application)MyApp).ID != FunzioneApplicativa)
                {
                    ((UNIF_Application)MyApp).load(FunzioneApplicativa);
                    Session.Add("APPLICATION", MyApp); //Session["APPLICATION"] = MyApp;
                }
            }
            //@MdN 25/06/2015

            Livello = ((UNIF_Application)MyApp).ID;
            MyName = "Amministrazione";
            MyAspxFileName = "~/SIMPLEX_STARTUP_ADMIN/" + MyAspxFileName;
            AddToSimplexBreadCrumbs();
        }
        #endregion
    }
}
