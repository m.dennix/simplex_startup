<%@ Page Language="C#" MasterPageFile="~/MasterPCM.Master" AutoEventWireup="true" CodeBehind="DettagliUtente.aspx.cs" Inherits="SIMPLEX_STARTUP.DettagliUtente" Title="Dettagli Utente" %>
<%@ Register Assembly="WebSimplex" Namespace="WebSimplex"  TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    </asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MenuPlaceHolder" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="MainFormSection">
  <table class="FRM">
    
      <tr class="FRM">
        <td class="FRM_COL1"></td>
        <td class="FRM_COL2">
        <!-- MessageBox -->
        <!--

        -->
        <!-- END MessageBox -->
        </td>
        <td class="FRM_COL3"></td>
      </tr>
    
    <tr class="FRM">
        <td class="FRM_COL1">
            <asp:Label ID="L_ID" runat="server" AssociatedControlID="ID" Text="ID"></asp:Label>
        </td>
        <td class="FRM_COL2">
            <asp:TextBox ID="ID" runat="server" ReadOnly="True"></asp:TextBox>
        </td>
        <td class="FRM_COL3">
            <asp:Button ID="R_Back" runat="server" Text="Indietro" onclick="R_Back_Click" />
        </td>
    </tr>
    <tr class="FRM">
        <td class="FRM_COL1">
            <asp:Label ID="L_Username" runat="server" AssociatedControlID="Username" 
                Text="Username"></asp:Label>
        </td>
        <td class="FRM_COL2">
            <asp:TextBox ID="Username" runat="server"></asp:TextBox>
        </td>
        <td class="FRM_COL3">&nbsp;</td>
    </tr>
    <tr class="FRM">
        <td class="FRM_COL1">
            <asp:Label ID="L_EMail" runat="server" Text="EMail" AssociatedControlID="EMail"></asp:Label>
        </td>
        <td class="FRM_COL2" style="margin-left: 40px">
            <asp:TextBox ID="EMail" runat="server" Width="248px"></asp:TextBox>
        </td>
        <td class="FRM_COL3">&nbsp;</td>
    </tr>
    <tr class="FRM">
        <td class="FRM_COL1">
            <asp:Label ID="L_Telefoni" runat="server" Text="Telefoni" 
                AssociatedControlID="Telefoni"></asp:Label>
        </td>
        <td class="FRM_COL2">
            <asp:TextBox ID="Telefoni" runat="server" Width="247px"></asp:TextBox>
        </td>
        <td class="FRM_COL3">&nbsp;</td>
    </tr>
    <tr class="FRM">
        <td class="FRM_COL1">
            <asp:Label ID="L_Nome" runat="server" Text="Nome" AssociatedControlID="Nome"></asp:Label>
        </td>
        <td class="FRM_COL2">
            <asp:TextBox ID="Nome" runat="server"></asp:TextBox>
        </td>
        <td class="FRM_COL3">&nbsp;</td>
    </tr>
    <tr class="FRM">
        <td class="FRM_COL1">
            <asp:Label ID="L_Cognome" runat="server" Text="Cognome" 
                AssociatedControlID="Cognome"></asp:Label>
        </td>
        <td class="FRM_COL2">
            <asp:TextBox ID="Cognome" runat="server"></asp:TextBox>
        </td>
        <td class="FRM_COL3">&nbsp;</td>
    </tr>
    <tr class="FRM">
        <td class="FRM_COL1">
            <asp:Label ID="L_Sesso" runat="server" Text="Sesso" AssociatedControlID="Sesso"></asp:Label>
        </td>
        <td class="FRM_COL2">
            <asp:DropDownList ID="Sesso" runat="server" Height="18px" Width="56px">
                <asp:ListItem></asp:ListItem>
                <asp:ListItem Value="M">Maschio</asp:ListItem>
                <asp:ListItem Value="F">Femmina</asp:ListItem>
            </asp:DropDownList>
        </td>
        <td class="FRM_COL3">&nbsp;</td>
    </tr>
    <tr class="FRM">
        <td class="FRM_COL1">
            <asp:Label ID="L_locale" runat="server" Text="Locale" 
                AssociatedControlID="locale"></asp:Label>
        </td>
        <td class="FRM_COL2">
            <asp:CheckBox ID="locale" runat="server" />
        </td>
        <td class="FRM_COL3">&nbsp;</td>
    </tr>
    <tr class="FRM">
        <td class="FRM_COL1">
            &nbsp;</td>
        <td class="FRM_COL2">
            <asp:Button ID="R_Inserisci" runat="server" onclick="R_Inserisci_Click" 
                Text="Inserisci" />
        </td>
        <td class="FRM_COL3">
            <asp:Button ID="R_Elimina" runat="server" Text="Elimina" 
                onclick="R_Elimina_Click" />
        </td>
    </tr>
    </table>
 </div>
 <div id="TabSection">
        <uc1:WebSimplexTabs ID="R_SpxTabs" runat="server" Name="R_SpxTabs" 
            DefaultTabSelected="0" />
 </div>
 <div id="SubFormSection">
     <asp:MultiView ID="R_MultiView" runat="server">
         <asp:View ID="R_View0" runat="server">
             <asp:Label ID="L_GruppiDisponibili" runat="server" Text="Gruppi Disponibili: "></asp:Label>
             <asp:DropDownList ID="R_GruppiDisponibili" runat="server" AutoPostBack="True" 
                 DataSourceID="SqlDataSource1_R_GruppiDisponibili" DataTextField="DENOMINAZIONE" 
                 DataValueField="ID" 
                 onselectedindexchanged="R_GruppiDisponibili_SelectedIndexChanged">
             </asp:DropDownList>
             <asp:SqlDataSource ID="SqlDataSource1_R_GruppiDisponibili" runat="server" 
                 ConnectionString="<%$ ConnectionStrings:SIMPLEX_STARTUP_DBConnectionStringForForms %>" 
                 SelectCommand="SELECT [ID], [DENOMINAZIONE] FROM [ADMN_gruppo] WHERE ([DATA_DEL] IS NULL)">
             </asp:SqlDataSource>
             &nbsp;<asp:Button ID="R_Aggiungi" runat="server" Text="Aggiungi" 
                 onclick="R_Aggiungi_Click" />
             <br />
             <asp:GridView ID="R_ListaGruppi" runat="server" AutoGenerateColumns="False" 
                 DataKeyNames="ID" 
                 DataSourceID="SqlDataSource1_ListaGruppiAppartenenzaUtente" 
                 onrowcommand="R_ListaGruppi_RowCommand">
                 <Columns>
                     <asp:BoundField DataField="ID" HeaderText="ID" InsertVisible="False" 
                         ReadOnly="True" SortExpression="ID" />
                     <asp:BoundField DataField="DENOMINAZIONE" HeaderText="DENOMINAZIONE" 
                         SortExpression="DENOMINAZIONE" />
                     <asp:BoundField DataField="NOTE" HeaderText="NOTE" SortExpression="NOTE" />
                     <asp:ButtonField ButtonType="Button" CommandName="Edit" Text="Dissocia" />
                 </Columns>
             </asp:GridView>
             <asp:SqlDataSource ID="SqlDataSource1_ListaGruppiAppartenenzaUtente" 
                 runat="server" 
                 ConnectionString="<%$ ConnectionStrings:SIMPLEX_STARTUP_DBConnectionStringForForms %>" 
                 SelectCommand="SELECT [ID], [DENOMINAZIONE], [NOTE] FROM [ADMN_gruppo] WHERE ([DATA_DEL] IS NULL)">
             </asp:SqlDataSource>
         </asp:View>
     </asp:MultiView>
 </div>
</asp:Content>
