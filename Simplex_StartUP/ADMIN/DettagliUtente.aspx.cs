using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using GeneralApplication;
using simplex_FORM;
using WebSimplex;

namespace SIMPLEX_STARTUP
{
    /// <summary>
    /// Derivata da SimplexForm.
    /// Ha attributi protetti necessari per il funzionamento dell'applicazione.
    /// Esegue Override dei seguenti metodi:
    /// ------
    /// @MdN 
    /// Crtd: 06/12/2014
    /// Mdfd: 12/08/2015 - ristrutturazione: la form deriva direttamente da SimplexForm e non più da GenericSpxForm
    /// Mdfd: 27/05/2016 - ulteriore ristrutturazione.
    /// Mdfd: 10/06/2016 - Eliminata AddSimplexToBreadCrumbs(); eliminato My MyAspxFileName; Modificata preloadEnvvirnoment().
    /// </summary>
    public partial class DettagliUtente :   GenericSpxForm //simplex_FORM.SimplexForm //12/08/2015
    {
        #region ATTRIBUTI
        /// <summary>
        /// Oggetto applicazione corrente.
        /// </summary>
        protected new GeneralApplication.GeneralApplication MyApp = null;
        /// <summary>
        /// Oggetto connessione corrente al database SIMPLEX_STARTUP
        /// DEVE essere sempre chiusa.
        /// </summary>
        protected new System.Data.Odbc.OdbcConnection MyConn = null;
        /// <summary>
        /// Nome della pagina come visualizzata nel menu.
        /// </summary>
        protected new String MyName = null;
        /// <summary>
        /// Riferimento all' oggetto di classe WebSimplexBreadCrumbs.
        /// </summary>
        protected new WebSimplexBreadCrumbs MySbc = null;

        /// <summary>
        /// Nome del file della pagina corrente. Viene valorizzato automaticamente 
        /// dal metodo preloadEnvironment().
        /// </summary>
        //protected new String MyAspxFileName = null; //<deleted @MdN: 10/06/2016>
        
        /// <summary>
        /// URL della pagina chiamante.
        /// </summary>
        protected new String MyBackURL = null;
        /// <summary>
        /// Indica se il riquadro dei messaggi deve essere visibile o meno.
        /// Per default NON è visibile.
        /// </summary>
        protected new Boolean MyMessageBoxVisible = false;
        /// <summary>
        /// Stringa che identifica la funzione applicativa di appartenenza.
        /// <pre>
        /// @MdN
        /// ----
        /// Crtd: 12/08/2015
        /// </pre>
        /// </summary>
        protected new String MyFunzioneApplicativa = null;        
        /// <summary>
        /// Menu verticale associato alla maschera
        /// </summary>
        protected new System.Web.UI.WebControls.Menu MyMainMenu = null;
        #endregion

        #region METODI PROTETTI

            /// <summary>
            /// Imposta gli attributi necessari al funzionamento.
            /// Si attende in Session["APPLICATION"] l'oggetto GeneralApplication corrente.
            /// Se non è presente, lancia un eccezione del tipo generalApplication TracedMessage.
            /// Valorizza:
            /// MyApp               :   APPLICAZIONE CORRENTE;
            /// MyConn              :   CONNESSIONE DI DEFAULT;
            /// MyMainMenu          :   CONTROLLO MENU SINISTRO;
            /// MySbc               :   CONTROLLO BREADCRUMBS;
            /// MyAspxFileName      :   NOME DELLA PAGINA WEB/WEB FORM.
            /// ------
            /// @MdN
            /// Crtd: 06/12/2014
            /// </summary>
            protected override void preloadEnvironment()
            {
                MyFunzioneApplicativa = "Amministrazione";
                base.preloadEnvironment();
                #region [SCRIVERE QUI IL PROPRIO CODICE]

                MyApp = (GeneralApplication.GeneralApplication)Session["APPLICATION"];
                if (MyApp != null)
                    MyConn = (System.Data.Odbc.OdbcConnection)MyApp.DefaultConnection;
                else
                {
                    throw new TracedMessage("Istanza di applicazione inesistente", "GTELForm", "preloadEnvironment()");
                }

                // valorizzazione del riferimento al menu principale della maschera
                try
                {
                    MyMainMenu = (Menu)this.Master.FindControl("R_MainMenu");
                }
                catch (Exception E)
                {
                    MyMainMenu = null;
                }

                // valorizzazione delle briciole di pane
                try
                {
                    MySbc = (WebSimplexBreadCrumbs)this.Master.FindControl("R_BreadCrumbs");
                }
                catch (Exception E)
                {
                    MySbc = null;
                }

                if (this.PreviousPage != null)
                    MyBackURL = this.PreviousPage.AppRelativeVirtualPath.Substring(this.PreviousPage.AppRelativeVirtualPath.LastIndexOf('/') + 1);
                else
                    MyBackURL = (String)Session["CALLING-PAGE"];

                MyAspxFileName = this.Page.AppRelativeVirtualPath.Substring(this.Page.AppRelativeVirtualPath.LastIndexOf("/") + 1);


                TBL_UNDER_FORM = (simplex_ORM.SQLSrv.SQLTable)Session["UTENTE"];
                
                // VERIFICA che l'oggetto in sessione sia del tipo corretto
                if(TBL_UNDER_FORM!=null && TBL_UNDER_FORM.Name.CompareTo("ADMN_utente")!=0)
                    TBL_UNDER_FORM=null;
                
                // Creazione di un nuovo oggetto
                if(TBL_UNDER_FORM==null)
                    TBL_UNDER_FORM = simplex_ORM.SQLSrv.SQLTable.createSQLTable(MyConn, "ADMN_utente");
               
                // Visualizzazione dei TAB SOLO se l'oggetto è valorizzato
                if (TBL_UNDER_FORM.getValue("ID") == null)
                    this.R_MultiView.Visible=false;
                else
                    this.R_MultiView.Visible= true;
             
                //Associazione del MultiView al TAB
                R_SpxTabs.AssociatedMultiView = R_MultiView;

                //Aggiunta delle tabs
                this.R_SpxTabs.Add("Gruppi");

                // <removed @MdN: 10/06/2016>
                ////Aggiunta ai breadcrumbs
                //if (TBL_UNDER_FORM.getValue("ID") == null)
                //    MyName = "Nuovo Utente";
                //else
                //    MyName = "Dettagli Utente";
                // <removed @MdN: 10/06/2016>

                MyAspxFileName = "~/SIMPLEX_STARTUP_ADMIN/" + MyAspxFileName;
                AddToSimplexBreadCrumbs();

                //Impostazione della GridViewList e della DropDownList
                if (TBL_UNDER_FORM.getValue("ID") != null)
                {
                    SqlDataSource1_ListaGruppiAppartenenzaUtente.SelectCommand = "SELECT ID, DENOMINAZIONE, NOTE FROM ADMN_gruppo WHERE DATA_DEL IS NULL AND ID IN (SELECT GRUPPO FROM ADMN_utentegruppo WHERE UTENTE=" + TBL_UNDER_FORM.getValue("ID") + ")";
                    R_ListaGruppi.DataBind();
                    if (IsPostBack == false)
                    {
                        SqlDataSource1_R_GruppiDisponibili.SelectCommand = "SELECT ID, DENOMINAZIONE FROM ADMN_gruppo WHERE DATA_DEL IS NULL AND ID NOT IN (SELECT GRUPPO FROM ADMN_utentegruppo WHERE UTENTE=" + TBL_UNDER_FORM.getValue("ID") + ")";
                        impostaVoceNullaDDL();
                        //R_GruppiDisponibili.DataBind();
                    }
                }
                else
                {
                    this.R_GruppiDisponibili.Visible = false;
                }

                // RENDE VISIBILE IL MSGBOX
                if (Session["DettagliUtente.Panel_MessageBox.Visible"] != null)
                {
                    MyMessageBoxVisible = (Boolean)Session["DettagliUtente.Panel_MessageBox.Visible"];
                    Session["DettagliUtente.Panel_MessageBox.Visible"] = false;
                }
                else
                {
                    MyMessageBoxVisible = false;
                }

                #endregion
            }//fine preloadEnvironment


        /// <summary>
        /// Imposta una voce di default 'seleziona' in testa alle Drop Down List (DDL) ed esegue il DataBinding.
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 15/03/2015
        /// </pre>
        /// </summary>
        protected void impostaVoceNullaDDL()    
        {                
            ListItem _li = new ListItem("Seleziona", "0");
            R_GruppiDisponibili.Items.Clear();
            R_GruppiDisponibili.Items.Add(_li);
            R_GruppiDisponibili.AppendDataBoundItems = true;
            R_GruppiDisponibili.DataBind();
            R_GruppiDisponibili.SelectedIndex = 0;
            Session["R_GruppiDisponibili"] = "0";
        }

        /// <summary>
        /// <p>EN: Show the errors occurred in the last operation. At the end, clears the list of errors. 
        /// </p>
        /// <p>IT: Mostra gli errori verificatisi nell'ultima operazione. Al termine, cancella la lista degli errori.
        /// </p>
        /// </summary>
        protected void showErrors()
        {
            if (MyErrors.Count > 0)
            {
                Boolean l_pari = false;
                Literal l_ErrArea = null;
                System.Text.StringBuilder l_ErrString = new System.Text.StringBuilder("<table class=\"ErrTab\">");
                l_ErrArea = (Literal)this.Master.FindControl("ListOfErrors");

                foreach (simplex_ORM.Spx_ORMMessage ORMM in MyErrors)
                {
                    if (l_pari == true)
                        l_ErrString.Append("<tr class=\"ErrRowPari\">");
                    else
                        l_ErrString.Append("<tr class=\"ErrRowDisPari\">");
                    l_pari = !l_pari;
                    l_ErrString.Append("<td class=\"ErrCodCol\">");
                    l_ErrString.Append(ORMM.MessageCode.ToString());
                    l_ErrString.Append("</td><td class=\"ErrDescCol\">");
                    l_ErrString.Append(ORMM.Message);
                    l_ErrString.Append("</td></tr>");
                }
                l_ErrString.Append("</table>");
                l_ErrArea.Text = l_ErrString.ToString();
                MyErrors.Clear(); //<-- ATTENZIONE
            }
        }//fine
        #endregion

        #region GESTORI

        protected void R_ListaGruppi_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            Int32 _row = 0;
            Int32 _key = 0;
            System.Data.Odbc.OdbcCommand _cmd;

            // indentificazione della riga selezionata
            if (e.CommandName.CompareTo("Edit") != 0)
                return;

            // identificare la riga che ha scatenato l'evento: ASP.NET non lo fa in automatico
            // N.B. CommandArgument è 0-BASED!!!!
            _row = Int32.Parse(e.CommandArgument.ToString());

            // identificare il valore della chiave associata alla riga
            _key = Int32.Parse(((GridView)e.CommandSource).Rows[_row].Cells[0].Text);

            // Cancellazione del record
            System.Text.StringBuilder _sql = new System.Text.StringBuilder("DELETE FROM ADMN_utentegruppo WHERE UTENTE = ").Append(TBL_UNDER_FORM.getValue("ID")).Append(" AND GRUPPO=").Append(_key.ToString());
            try
            {
                if (MyConn.State == ConnectionState.Closed)
                    MyConn.Open();
                _cmd = MyConn.CreateCommand();
                _cmd.CommandText = _sql.ToString();
                _cmd.ExecuteNonQuery();
                MyConn.Close();
            }
            catch (Exception E)
            {
                simplex_ORM.Spx_ORMMessage ORMM = new simplex_ORM.Spx_ORMMessage(E);
                ORMM.MessageField = MySbc.ToString() + "R_ListaGruppi_RowCommand()";
                ORMM.Message = "While Executing the query [" + _sql.ToString() + "]: " + E.Message;
                ORMM.traceLog(MyConn, ((UNIF_User)MyApp.CurrentUser).UID.ToString(), Session.SessionID);
                MyErrors.Add(ORMM);
                showErrors();
            }
            // Aggiornamento controlli
            // ...
            SqlDataSource1_R_GruppiDisponibili.SelectCommand = "SELECT ID, DENOMINAZIONE FROM ADMN_gruppo WHERE DATA_DEL IS NULL AND ID NOT IN (SELECT GRUPPO FROM ADMN_utentegruppo WHERE UTENTE=" + TBL_UNDER_FORM.getValue("ID") + ")";
            impostaVoceNullaDDL();
            //R_GruppiDisponibili.DataBind();
            R_ListaGruppi.DataBind();

            // Tracciamento nel log
            writeMessageToLog("Cancellazione ADMN_utenteruppo(" + TBL_UNDER_FORM.getValue("ID") + ", " + _key.ToString() + ")", MySbc.ToString(), ((UNIF_User)MyApp.CurrentUser).UID.ToString(), Session.SessionID);

        }


        /// <summary>
        /// Inserisce o modifica un'oggetto di tipo utente
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void R_Inserisci_Click(object sender, EventArgs e)
        {
            try
            {
                modify();
            }
            catch (simplex_ORM.Spx_ORMMessage SORMM)
            {
                MyErrors.Add(SORMM);
                showErrors();
                return;
            }

            MyErrors.Add(new simplex_ORM.Spx_ORMMessage("Operazione avvenuta con successo."));
            showErrors();
            //@MdN 23/07/2015
            writeMessageToLog(MyConn, "Inserito Utente ID=" + TBL_UNDER_FORM.getValue("ID").ToString(), MySbc.ToString(), ((UNIF_User)MyApp.CurrentUser).UID.ToString(), Session.SessionID);
        }


        protected void Page_Load(object sender, EventArgs e)
            {
                Boolean _isVisible = false;

                preloadEnvironment();

                if (TBL_UNDER_FORM.getValue("ID") != null)
                    _isVisible = true;

                this.R_Aggiungi.Visible = _isVisible;
                this.R_GruppiDisponibili.Visible = _isVisible;
                this.R_ListaGruppi.Visible = _isVisible;
                this.R_MultiView.ActiveViewIndex = 0;

                //Scrittura del log ed impostazione delle DDL
                if (IsPostBack == false)
                {
                    writeMessageToLog(MyConn, "Form_Load()", MySbc.ToString(), ((UNIF_User)MyApp.CurrentUser).UID.ToString(), Session.SessionID);
                    impostaVoceNullaDDL();
                }

                //@MdN 23/07/2015
                if (IsPostBack == false)
                {
                    if (TBL_UNDER_FORM.getValue("ID") != null)
                        writeMessageToLog(MyConn, "Loaded UNIF_User ID=" + TBL_UNDER_FORM.getValue("ID").ToString(), MySbc.ToString(), ((UNIF_User)MyApp.CurrentUser).UID.ToString(), Session.SessionID);
                    else
                        writeMessageToLog(MyConn, "New UNIF_User", MySbc.ToString(), ((UNIF_User)MyApp.CurrentUser).UID.ToString(), Session.SessionID);
                }

                if (MyErrors.Count > 0)
                    showErrors();
                else
                    ((Literal)this.Master.FindControl("ListOfErrors")).Text = "";
            }

        protected void R_Elimina_Click(object sender, EventArgs e)
        {
            if (TBL_UNDER_FORM == null)
                return;
            if (TBL_UNDER_FORM.getValue("ID") == null)
                return;
            //prima fase: impostazione della variabile di sessione
            if (MyMessageBoxVisible == false)
            {
                Session["DettagliUtente.Panel_MessageBox.Visible"] = true;
                Session["DettagliUtente.R_Conferma.Click"] = "R_Elimina_Click";
                MyMessageBoxVisible = true;
                //Panel_MessageBox.Visible = MyMessageBoxVisible;
                return;
            }

            Session["DettagliUtente.Panel_MessageBox.Visible"] = false;
            Session["DettagliUtente.R_Conferma.Click"] = null;
            MyMessageBoxVisible = false;
            //Panel_MessageBox.Visible = MyMessageBoxVisible;
            if (Int32.Parse(TBL_UNDER_FORM.getValue("ID")) > 0)
            {
                TBL_UNDER_FORM.setValue("Data_oscuramento", simplex_ORM.Column.DateToSQLDateString(DateTime.Now));
                modify(TBL_UNDER_FORM, "ContentPlaceHolder1", "System.Web.UI.WebControls.ContentPlaceHolder");
                writeMessageToLog("DettagliUtente.R_Elimina_Click(ID=" + TBL_UNDER_FORM.getValue("ID") + ")", MySbc.ToString(), ((UNIF_User)MyApp.CurrentUser).UID.ToString(), Session.SessionID);
                Response.Redirect(MyBackURL);
            }
        }

        /// <summary>
        /// tasto per tornare alla pagina precedente
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void R_Back_Click(object sender, EventArgs e)
        {
            Response.Redirect(MyBackURL);
        }

        protected void R_Aggiungi_Click(object sender, EventArgs e)
        {
            String _gruppo;
            try
            {
                if (MyConn.State == ConnectionState.Closed)
                    MyConn.Open();
                simplex_ORM.SQLSrv.SQLTable _utentegruppo = simplex_ORM.SQLSrv.SQLTable.createSQLTable(MyConn, "ADMN_utentegruppo");
                _utentegruppo.setValue("UTENTE", TBL_UNDER_FORM.getValue("ID"));
                _gruppo = (String)Session["R_GruppiDisponibili"];
                //Session["R_GruppiDisponibili"] = null;
                if (_gruppo == null || _gruppo.CompareTo("0") == 0)
                {
                    throw new simplex_ORM.Spx_ORMMessage("DettagliUtente: selezionare un gruppo da associare");
                }
                _utentegruppo.setValue("GRUPPO", _gruppo);
                if (_utentegruppo.CurrentConnection.State == ConnectionState.Closed)
                    _utentegruppo.CurrentConnection.Open();                                         //@MdN: 25/06/2015
                _utentegruppo.save(true);
                _utentegruppo.CurrentConnection.Close();                                            //@MdN: 25/06/2015
            }
            catch (simplex_ORM.Spx_ORMMessage ORMM)
            {
                MyErrors.Add(ORMM);
                ORMM.traceLog(MyConn, ((UNIF_User)MyApp.CurrentUser).UID.ToString(), Session.SessionID);
                showErrors();
            }
            // Refresh
            SqlDataSource1_R_GruppiDisponibili.SelectCommand = "SELECT ID, DENOMINAZIONE FROM ADMN_gruppo WHERE DATA_DEL IS NULL AND ID NOT IN (SELECT GRUPPO FROM ADMN_utentegruppo WHERE UTENTE=" + TBL_UNDER_FORM.getValue("ID") + ")";
            impostaVoceNullaDDL();
            //R_GruppiDisponibili.DataBind();
            R_ListaGruppi.DataBind();
        }

        protected void R_GruppiDisponibili_SelectedIndexChanged(object sender, EventArgs e)
        {
            Session["R_GruppiDisponibili"] = R_GruppiDisponibili.SelectedValue;
            //R_GruppiDisponibili.SelectedIndex = 0;
        }
        #endregion

        #region PROPRIETA'
        public String FormName
            {
                get
                {
                    return MyName;
                }
                set
                {
                    if (value == null)
                        MyName = "### DA DEFINIRE";
                    else
                        MyName = value;
                }
            }



        #endregion

        #region METODI PUBBLICI
            /// <summary>
            /// Modifica l'oggetto corrente con il contenuto della FORM e salva.
            /// </summary>
            public override void modify()
            {
                //Controlli
                if (MyConn == null)
                    throw new simplex_ORM.Spx_ORMMessage("DettagliUtente.modify(): connessione corrente non istanziata",1006);
                if (MyConn.State == ConnectionState.Closed)
                    MyConn.Open();
                if (TBL_UNDER_FORM == null)
                    TBL_UNDER_FORM = simplex_ORM.SQLSrv.SQLTable.createSQLTable(MyConn, "ADMN_utente");

                //Informazioni di controllo
                TBL_UNDER_FORM.setValue("utente",((UNIF_User)MyApp.CurrentUser).UID.ToString());
                TBL_UNDER_FORM.setValue("Data_Sys", simplex_ORM.Column.DateToSQLDateString(DateTime.Now));
                //Inserimento dei valori della form negli attributi dell'oggetto e salvataggio
                base.modify(TBL_UNDER_FORM, "ContentPlaceHolder1", "System.Web.UI.WebControls.ContentPlaceHolder");
                MyConn.Close();
            }
        #endregion
    }
}
