using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;

namespace SIMPLEX_STARTUP
{
    /// <summary>
    /// Classe TEMPLATE per la creazione di una maschera che mostra una lista di oggetti.
    /// </summary>
    public partial class ListaApplicazioni : GenericSpxForm
    {
        /// <summary>
        /// URL della pagina chiamante.
        /// </summary>
        String MyBackURL = null;
        /// <summary>
        /// Stringa della query
        /// </summary>
        String MySql = null;
        /// <summary>
        /// Stringa del filtro
        /// </summary>
        String MyFilter = null;



        /// <summary>
        /// <p>Caricamento della maschera.</p>
        /// <ul>
        /// <li>Invoca preloadEnvironment()</li>
        /// <li>esegue la query di popolamento</li>
        /// </ul>
        /// <pre>
        /// @MdN
        /// ----
        /// Crtd: 30/01/2015
        /// </pre>
        /// </summary>
        /// <param name="sender">oggetto che ha invocato l'evento</param>
        /// <param name="e">parametri</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            preloadEnvironment();

            //Traccio l'ingresso nel log
            writeMessageToLog(MyConn, "Form_Load()", MySbc.ToString(), ((UNIF_User)MyApp.CurrentUser).UID.ToString(), Session.SessionID);

        }

        /// <summary>
        /// <p>
        /// Imposta gli attributi necessari al funzionamento.
        /// Si attende in Session["APPLICATION"] l'oggetto GeneralApplication corrente.
        /// Se non è presente, lancia un eccezione del tipo generalApplication TracedMessage.
        /// </p>
        /// <p>
        /// Imposta in sessione l'ID dell'oggetto selezionato. La variabile di sessione ha come 
        /// stringa identificatrice il nome della tabella di riferimento dell'oggetto tutto in maiuscolo.
        /// </p>
        /// <p>Esempio: </p>
        /// <p>
        /// se la tabella è 'utente' il nome della variabile di sessione è 'UTENTE'.
        /// </p>
        /// 
        /// <p>Valorizza i seguenti attributi derivati da GenericSpxForm:
        /// </p>
        /// <ul>
        /// <li>MyApp               :   APPLICAZIONE CORRENTE;</li>
        /// <li>MyConn              :   CONNESSIONE DI DEFAULT;</li>
        /// <li>MyMainMenu          :   CONTROLLO MENU SINISTRO;</li>
        /// <li>MySbc               :   CONTROLLO BREADCRUMBS;</li>
        /// <li>MyAspxFileName      :   NOME DELLA PAGINA WEB/WEB FORM.</li>
        /// </ul>
        /// <p>
        /// Valorizza i suoi attributi caratteristici che sono:
        /// </p><p>
        /// <ul>
        /// <li>MyBackURL           :   URL DELLA PAGINA CHIAMANTE.</li>
        /// </ul>
        /// </p>
        /// </summary>
        protected override void preloadEnvironment()
        {
            MyFunzioneApplicativa = "Amministrazione";

            base.preloadEnvironment();
            MyName = this.Title;  //"Funzioni Applicative";
            Title = MyName;
            MyAspxFileName = "~/SIMPLEX_STARTUP_ADMIN/" + MyAspxFileName;
            AddToSimplexBreadCrumbs();
        }

        /// <summary>
        /// <p>
        /// 1) Pulisce il contenuto della Session["FUNZIONE_APPLICATIVA"]
        /// 2) Passa il controllo alla maschera DettagliApplicazioni.aspx
        /// </p>
        /// 
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 01/03/2015
        /// </pre>
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void R_Nuovo_Click(object sender, EventArgs e)
        {
            Session["FUNZIONE_APPLICATIVA"] = null;
            Response.Redirect("DettagliApplicazioni.aspx");
        }

        /// <summary>
        /// Seleziona un oggetto e lo porta in dettaglio.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void R_Lista_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            Int32 _row = 0;
            Int32 _key = 0;

            // indentificazione della riga selezionata
            if (e.CommandName.CompareTo("Edit") != 0)
                return;

            // identificare la riga che ha scatenato l'evento: ASP.NET non lo fa in automatico
            // N.B. CommandArgument è 0-BASED!!!!
            _row = Int32.Parse(e.CommandArgument.ToString());

            // identificare il valore della chiave associata alla riga
            _key = Int32.Parse(((GridView)e.CommandSource).Rows[_row].Cells[0].Text);

            // Creare l'oggetto SQLTable e caricarvi il record identificato da l_key
            try
            {
                if (MyConn.State == ConnectionState.Closed)
                    MyConn.Open();
                TBL_UNDER_FORM = simplex_ORM.SQLSrv.SQLTable.createSQLTable(MyConn, "ADMN_FunzioneApplicativa");
                TBL_UNDER_FORM.setValue("ID", _key.ToString());
                if (MyConn.State == ConnectionState.Closed)
                    MyConn.Open();
                TBL_UNDER_FORM.load();
                MyConn.Close();
            }
            catch (simplex_ORM.Spx_ORMMessage SORMM)
            {
                SORMM.traceLog(MyConn, ((UNIF_User)MyApp.CurrentUser).UID.ToString(), Session.SessionID);
                MyErrors.Add(SORMM);
                showErrors();
                return;
            }

            // Immettere l'oggetto in sessione nella variabile UTENTE
            Session["FUNZIONE_APPLICATIVA"] = TBL_UNDER_FORM;

            //scrittura nel log
            writeMessageToLog("R_Lista_RowCommand(ID=" + TBL_UNDER_FORM.getValue("ID") + ")", MySbc.ToString(), ((UNIF_User)MyApp.CurrentUser).UID.ToString(), Session.SessionID);

            //Imposta la variabile di sessione per il BACK
            Session["CALLING-PAGE"] = MyAspxFileName;

            // Passa il controllo alla pagina dei dettagli
            Response.Redirect("DettagliApplicazioni.aspx");

        }//FINE
    }
}
