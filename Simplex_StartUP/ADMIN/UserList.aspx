<%@ Page Language="C#" MasterPageFile="~/MasterPCM.Master" AutoEventWireup="true" CodeBehind="UserList.aspx.cs" Inherits="SIMPLEX_STARTUP.UserList
" Title="Lista Utenti" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MenuPlaceHolder" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
 <div id="MainFormSection">
  <table class="FRM">
  
      <tr class="FRM_ROW">
        <td class="FRM_COL1"></td>
        <td class="FRM_COL2"></td>
        <td class="FRM_COL3">
            <asp:Button ID="R_Indietro" runat="server" Text="Indietro" 
                onclick="R_Indietro_Click" />
        </td>
    </tr>
    
    <tr class="FRM_ROW">
        <td class="FRM_COL1">
            <asp:Label ID="L_Cognome" runat="server" AssociatedControlID="R_Cognome" 
                CssClass="FRM_lbl" Text="Cognome"></asp:Label>
        </td>
        <td class="FRM_COL2">
            <asp:TextBox ID="R_Cognome" runat="server" CssClass="FRM_txt" Width="268px"></asp:TextBox>
        </td>
        <td class="FRM_COL3"></td>
    </tr>
    
    <tr class="FRM_ROW">
        <td class="FRM_COL1"></td>
        <td class="FRM_COL2"></td>
        <td class="FRM_COL3"></td>
    </tr>
  
    <tr class="FRM">
        <td class="FRM_COL1">
            <asp:Label ID="Label1" runat="server" Text="Azioni" CssClass="FRM_lbl"></asp:Label>
        </td>
        <td class="FRM_COL2">
            <asp:Button ID="R_Cerca" runat="server" onclick="R_Cerca_Click" Text="Cerca" />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <asp:Button ID="R_NuovaRicerca" runat="server" onclick="R_NuovaRicerca_Click" 
                Text="Nuova Ricerca" />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <asp:Button ID="Button1" runat="server" Text="Nuovo" onclick="R_Nuovo_Click" />
        &nbsp;&nbsp;&nbsp;
        </td>
        <td class="FRM_COL3"></td>
    </tr>    
    </table>
 </div>
  <div id="SubFormSection">
      <asp:GridView ID="R_Lista" runat="server" AllowPaging="True" 
          AllowSorting="True" AutoGenerateColumns="False" DataKeyNames="ID" 
          DataSourceID="SqlDataSource_Utenti" onrowcommand="R_Lista_RowCommand">
          <Columns>
              <asp:BoundField DataField="ID" HeaderText="ID" InsertVisible="False" 
                  ReadOnly="True" SortExpression="ID" />
              <asp:BoundField DataField="Username" HeaderText="Username" 
                  SortExpression="Username" />
              <asp:BoundField DataField="Nome" HeaderText="Nome" SortExpression="Nome" />
              <asp:BoundField DataField="Cognome" HeaderText="Cognome" 
                  SortExpression="Cognome" />
              <asp:CheckBoxField DataField="locale" HeaderText="locale" 
                  SortExpression="locale" />
              <asp:BoundField DataField="EMail" HeaderText="EMail" SortExpression="EMail" />
              <asp:ButtonField ButtonType="Button" CommandName="Edit" HeaderText="Comando" 
                  ShowHeader="True" Text="Seleziona" />
          </Columns>
      </asp:GridView>
      <asp:SqlDataSource ID="SqlDataSource_Utenti" runat="server" 
          ConnectionString="<%$ ConnectionStrings:SIMPLEX_STARTUP_DBConnectionStringForForms %>" 
          SelectCommand="SELECT [ID], [Username], [Nome], [Cognome], [locale], [EMail] FROM [ADMN_utente] WHERE ([Data_oscuramento] IS NULL)">
      </asp:SqlDataSource>
      <asp:SqlDataSource ID="SqlDataSource1" runat="server"></asp:SqlDataSource>
 </div>
</asp:Content>
