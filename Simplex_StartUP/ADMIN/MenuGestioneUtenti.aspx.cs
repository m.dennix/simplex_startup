using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;

namespace SIMPLEX_STARTUP
{
    public partial class MenuGestioneUtenti : SimplexMenu
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            preloadEnvironment();
            designMenu(Livello, FunzioneApplicativa,2);
            if (IsPostBack == false)
                writeMessageToLog(MyConn, "Form_Load()", MySbc.ToString(), ((UNIF_User)MyApp.CurrentUser).UID.ToString(), Session.SessionID);
            Session["CALLING-PAGE"] = MyAspxFileName;
        }

        protected override void preloadEnvironment()
        {
            MyApp = (GeneralApplication.GeneralApplication)Session["APPLICATION"];
            if (MyApp == null)
                Response.Redirect("~/GenericSpxErrorForm.aspx");

            //MyFunzioneApplicativa = "Amministrazione";
            MyFunzioneApplicativa = ((UNIF_Application)MyApp).Name;
            base.preloadEnvironment();

            Livello = ((UNIF_Application)MyApp).lookForMenuVoiceID("Gestione Utenti") ;
            FunzioneApplicativa = ((UNIF_Application)MyApp).ID;

            MyName = "Gestione Utenti";
            AddToSimplexBreadCrumbs();
        }
    }
}
