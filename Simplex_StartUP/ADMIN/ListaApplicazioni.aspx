<%@ Page Language="C#" MasterPageFile="~/MasterPCM.Master" AutoEventWireup="true" CodeBehind="ListaApplicazioni.aspx.cs" Inherits="SIMPLEX_STARTUP.ListaApplicazioni" Title="Lista Funzioni Applicative" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MenuPlaceHolder" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
 <div id="MainFormSection">
  <table class="FRM">
    <tr class="FRM">
        <td class="FRM_COL1">
            <asp:Label ID="L_Azioni" runat="server" Text="Azioni"></asp:Label>
        </td>
        <td class="FRM_COL2">
            <asp:Button ID="R_Nuovo" runat="server" Text="Nuovo" onclick="R_Nuovo_Click" />
        </td>
        <td class="FRM_COL3"></td>
    </tr>
    </table>
 </div>
  <div id="SubFormSection">
      <asp:GridView ID="R_Lista" runat="server" AllowPaging="True" 
          AllowSorting="True" AutoGenerateColumns="False" DataKeyNames="ID" 
          DataSourceID="SqlDataSource_ListaApplicazioni" 
          onrowcommand="R_Lista_RowCommand">
          <Columns>
              <asp:BoundField DataField="ID" HeaderText="ID" InsertVisible="False" 
                  ReadOnly="True" SortExpression="ID" />
              <asp:BoundField DataField="DENOMINAZIONE" HeaderText="DENOMINAZIONE" 
                  SortExpression="DENOMINAZIONE" />
              <asp:BoundField DataField="NOTE" HeaderText="NOTE" SortExpression="NOTE" />
              <asp:ButtonField ButtonType="Button" CommandName="Edit" Text="Seleziona" />
          </Columns>
      </asp:GridView>
      <asp:SqlDataSource ID="SqlDataSource_ListaApplicazioni" runat="server" 
          ConnectionString="<%$ ConnectionStrings:SIMPLEX_STARTUP_DBConnectionStringForForms %>" 
          SelectCommand="SELECT [ID], [DENOMINAZIONE], [NOTE] FROM [ADMN_FunzioneApplicativa] WHERE ([DATA_DEL] IS NULL)">
      </asp:SqlDataSource>
 </div>
</asp:Content>
