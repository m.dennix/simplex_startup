<%@ Page Language="C#" MasterPageFile="~/MasterPCM.Master" AutoEventWireup="true" CodeBehind="DettagliApplicazioni.aspx.cs" Inherits="SIMPLEX_STARTUP.DettagliApplicazioni" Title="Dettagli Applicazioni" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MenuPlaceHolder" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<!-- PUT YOUR CONTROLS FROM HERE -->
<div id="MainFormSection">
<table class="FRM">
    <tr class="FRM">
    <td class="FRM_COL1"></td>
    <td class="FRM_COL2">
        
        <asp:Panel ID="Panel_MessageBox" runat="server" CssClass="MessageBox">
            <asp:Label ID="L_Message" runat="server" Text="Confermare l'Operazione ?"></asp:Label><br /><br /><br />
            <asp:Button ID="R_Annulla" runat="server" Text="Annulla" 
                onclick="R_Annulla_Click" />
            &nbsp;
            &nbsp;
            &nbsp;
            <asp:Button ID="R_Conferma" runat="server" Text="Conferma" />
        </asp:Panel>
        
        </td>
    <td class="FRM_COL3"></td>
    </tr>
    <tr class="FRM">
    <td class="FRM_COL1">
        <asp:Label ID="L_ID" runat="server" AssociatedControlID="ID" Text="ID"></asp:Label>
        </td>
    <td class="FRM_COL2">
        <asp:TextBox ID="ID" runat="server"></asp:TextBox>
        </td>
    <td class="FRM_COL3">
        <asp:Button ID="R_Indietro" runat="server" Text="Indietro" 
            onclick="R_Indietro_Click" />
        </td>
    </tr>
    <tr class="FRM">
    <td class="FRM_COL1">
        <asp:Label ID="L_DESCRIZIONE" runat="server" AssociatedControlID="DENOMINAZIONE" 
            Text="DESCRIZIONE"></asp:Label>
        </td>
    <td class="FRM_COL2">
        <asp:TextBox ID="DENOMINAZIONE" runat="server" Width="468px"></asp:TextBox>
        </td>
    <td class="FRM_COL3">&nbsp;</td>
    </tr>
    
    <!-- @MdN 26/04/2015 -->
    <tr class="FRM">
        <td class="FRM_COL1">
            <asp:Label ID="L_HOMELNK" runat="server" AssociatedControlID="HOMELNK" 
                Text="Home"></asp:Label>
        </td>
        <td class="FRM_COL2">
            <asp:TextBox ID="HOMELNK" runat="server" Width="466px"></asp:TextBox>
        </td>
        <td class="FRM_COL3"></td>    
    </tr>

    <tr class="FRM">
        <td class="FRM_COL1">
            <asp:Label ID="L_IMGURL" runat="server" AssociatedControlID="IMGURL" 
                Text="Immagine"></asp:Label>
        </td>
        <td class="FRM_COL2">
            <asp:TextBox ID="IMGURL" runat="server" Width="465px"></asp:TextBox>
        </td>
        <td class="FRM_COL3"></td>    
    </tr>

    
    <tr class="FRM">
    <td class="FRM_COL1">
        <asp:Label ID="Label3" runat="server" Text="NOTE"></asp:Label>
        </td>
    <td class="FRM_COL2">
        <asp:TextBox ID="NOTE" runat="server" Rows="5" TextMode="MultiLine" 
            Width="451px"></asp:TextBox>
        </td>
    <td class="FRM_COL3">&nbsp;</td>
    </tr>
    <tr class="FRM">
    <td class="FRM_COL1">&nbsp;</td>
    <td class="FRM_COL2"><fieldset class="GruppoDiCampi">
        <asp:Label ID="L_Avviso" runat="server" 
            Text="Scegli i gruppi che devono avere accesso alla funzione applicativa"></asp:Label>
        <br />
        <asp:DropDownList ID="GRUPPI_DISPONIBILI" runat="server" 
            DataSourceID="SqlDataSource_DDLGruppiDisponibili" DataTextField="DENOMINAZIONE" 
            DataValueField="ID" AutoPostBack="True" 
            onselectedindexchanged="GRUPPI_DISPONIBILI_SelectedIndexChanged">
        </asp:DropDownList>
        <asp:SqlDataSource ID="SqlDataSource_DDLGruppiDisponibili" runat="server" 
            ConnectionString="<%$ ConnectionStrings:SIMPLEX_STARTUP_DBConnectionStringForForms %>" 
            SelectCommand="SELECT [ID], [DENOMINAZIONE] FROM [ADMN_gruppo] WHERE ([DATA_DEL] IS NULL)">
        </asp:SqlDataSource>
&nbsp;<asp:Button ID="R_Aggiungi" runat="server" Text="Aggiungi" 
            onclick="R_Aggiungi_Click" />
        </fieldset>
        </td>
    <td class="FRM_COL3">&nbsp;</td>
    </tr>
    <tr class="FRM">
    <td class="FRM_COL1">
        <asp:Label ID="R_GRUPPI" runat="server" Text="GRUPPI"></asp:Label>
        </td>
    <td class="FRM_COL2">
        <asp:GridView ID="GRUPPI" runat="server" AutoGenerateColumns="False" 
            DataKeyNames="ID" DataSourceID="SqlDataSource_GruppiXFunzioneApplicativa" 
            onrowcommand="GRUPPI_RowCommand">
            <Columns>
                <asp:BoundField DataField="ID" HeaderText="ID" InsertVisible="False" 
                    ReadOnly="True" SortExpression="ID" />
                <asp:BoundField DataField="DENOMINAZIONE" HeaderText="DENOMINAZIONE" 
                    SortExpression="DENOMINAZIONE" />
                <asp:ButtonField ButtonType="Button" CommandName="Edit" HeaderText="Comandi" 
                    ShowHeader="True" Text="Dissocia" />
            </Columns>
        </asp:GridView>
        <asp:SqlDataSource ID="SqlDataSource_GruppiXFunzioneApplicativa" runat="server" 
            ConnectionString="<%$ ConnectionStrings:SIMPLEX_STARTUP_DBConnectionStringForForms %>" 
            SelectCommand="SELECT [ID], [DENOMINAZIONE] FROM [ADMN_gruppo] WHERE ([DATA_DEL] IS NULL)">
        </asp:SqlDataSource>
        </td>
    <td class="FRM_COL3">&nbsp;</td>
    </tr>
    <tr class="FRM">
    <td class="FRM_COL1">
        <asp:Button ID="R_Salva" runat="server" Text="Salva" onclick="R_Salva_Click" />
        </td>
    <td class="FRM_COL2">&nbsp;</td>
    <td class="FRM_COL3">
        <asp:Button ID="R_Elimina" runat="server" Text="Elimina" 
            onclick="R_Elimina_Click" />
        </td>
    </tr>
</table>
</div>
<!-- PUT YOUR CONTROLS TO HERE -->
</asp:Content>
