using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;

namespace SIMPLEX_STARTUP
{
    /// <summary>
    /// Classe TEMPLATE per la creazione di una maschera che mostra una lista di oggetti.
    /// </summary>
    public partial class UserList : GenericSpxForm
    {
        /// <summary>
        /// Stringa della query
        /// </summary>
        String MySql = null;
        /// <summary>
        /// Stringa del filtro
        /// </summary>
        String MyFilter = null;
        /// <summary>
        /// Stringa di ordinamento
        /// </summary>
        String MyOrder = null;

        // <added @MdN: 01/06/2016>
        /// <summary>
        /// Stringa SQL di base
        /// </summary>
        String MySql_Base = "SELECT [ID], [Username], [Nome], [Cognome], [locale], [EMail] FROM [ADMN_utente]";
        /// <summary>
        /// Filtro di base
        /// </summary>
        String MyFilter_Base = "([Data_oscuramento] IS NULL)";
        /// <summary>
        /// Ordinamento di base
        /// </summary>
        String MyOrder_Base = null;
        // </added @MdN: 01/06/2016>

        /// <summary>
        /// <p>Caricamento della maschera.</p>
        /// <ul>
        /// <li>Invoca preloadEnvironment()</li>
        /// <li>esegue la query di popolamento</li>
        /// </ul>
        /// <pre>
        /// @MdN
        /// ----
        /// Crtd: 30/01/2015
        /// </pre>
        /// </summary>
        /// <param name="sender">oggetto che ha invocato l'evento</param>
        /// <param name="e">parametri</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            preloadEnvironment();

            if (MySql != null)
            {
                SqlDataSource_Utenti.SelectCommand = MySql;
                R_Lista.DataBind();
            }

            //Traccio l'ingresso nel log
            writeMessageToLog(MyConn, "Form_Load()", MySbc.ToString(), ((UNIF_User)MyApp.CurrentUser).UID.ToString(), Session.SessionID);
        }

        /// <summary>
        /// <p>
        /// Imposta gli attributi necessari al funzionamento.
        /// Si attende in Session["APPLICATION"] l'oggetto GeneralApplication corrente.
        /// Se non è presente, lancia un eccezione del tipo generalApplication TracedMessage.
        /// </p>
        /// <p>
        /// Imposta in sessione l'ID dell'oggetto selezionato. La variabile di sessione ha come 
        /// stringa identificatrice il nome della tabella di riferimento dell'oggetto tutto in maiuscolo.
        /// </p>
        /// <p>Esempio: </p>
        /// <p>
        /// se la tabella è 'utente' il nome della variabile di sessione è 'UTENTE'.
        /// </p>
        /// 
        /// <p>Valorizza i seguenti attributi derivati da GenericSpxForm:
        /// </p>
        /// <ul>
        /// <li>MyApp               :   APPLICAZIONE CORRENTE;</li>
        /// <li>MyConn              :   CONNESSIONE DI DEFAULT;</li>
        /// <li>MyMainMenu          :   CONTROLLO MENU SINISTRO;</li>
        /// <li>MySbc               :   CONTROLLO BREADCRUMBS;</li>
        /// <li>MyAspxFileName      :   NOME DELLA PAGINA WEB/WEB FORM.</li>
        /// <li>MyBackURL           :   URL DELLA PAGINA CHIAMANTE.</li>
        /// </ul>
        /// <p>
        /// Valorizza i suoi attributi caratteristici che sono:
        /// <ul>
        /// <li></li>
        /// <li>MySql: Query corrente comprensiva di eventuale filtraggio;</li>
        /// <li>MySql_Base: Query di base senza filtraggio.</li>
        /// <li>MyFilter: filtraggio corrente comprensivo di criteri immessi dall'utente.</li>
        /// <li>MyFilter_Base: filtraggio di base</li>
        /// </ul>
        /// 
        /// </p>
        /// 
        /// <p>
        /// <ul>
        /// 
        /// </ul>
        /// </p>
        /// </summary>
        protected override void preloadEnvironment()
        {
            base.preloadEnvironment();
            //MyName = "Lista Utenti";
            //Title = MyName;
            MyName = Title;
            MyAspxFileName = "~/SIMPLEX_STARTUP_ADMIN/" + MyAspxFileName;
            AddToSimplexBreadCrumbs();

            #region inizializzazione delle query
            // Si usa la Session e non la ViewState in modo che, tornando indietro dal dettaglio, si recuperino i risultato della ricerca.
            if (Session["MYFILTER"] != null)
                MyFilter = (String)Session["MYFILTER"];

            if (Session["MYSQL"] != null)
                MySql = (String)Session["MYSQL"];
            else
            {
                MySql = MySql_Base + " WHERE " + MyFilter_Base;
                Session.Add("MYSQL", MySql);
            }            

            #endregion
        }

        /// <summary>
        /// <p>
        /// Gestore dell'evento di selezione di una riga della ListView
        /// </p>
        /// <pre>
        /// @MdN
        /// ----
        /// Crtd: 31/01/2015
        /// </pre>
        /// </summary>
        /// <param name="sender">Oggetto che ha scatenato l'evento</param>
        /// <param name="e">oggetto GridViewCommandEventArgs corrispondente al record che ha fattivamente originato l'evento.</param>
        protected void R_Lista_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            Int32 _row = 0;
            Int32 _key = 0;

            // indentificazione della riga selezionata
            if (e.CommandName.CompareTo("Edit") != 0)
                return;

            // identificare la riga che ha scatenato l'evento: ASP.NET non lo fa in automatico
            // N.B. CommandArgument è 0-BASED!!!!
            _row = Int32.Parse(e.CommandArgument.ToString());

            // identificare il valore della chiave associata alla riga
            _key = Int32.Parse(((GridView)e.CommandSource).Rows[_row].Cells[0].Text);

            // Creare l'oggetto SQLTable e caricarvi il record identificato da l_key
            try
            {
                if (MyConn.State == ConnectionState.Closed)
                    MyConn.Open();
                TBL_UNDER_FORM = simplex_ORM.SQLSrv.SQLTable.createSQLTable(MyConn,"ADMN_utente");
                TBL_UNDER_FORM.setValue("ID", _key.ToString());
                if (MyConn.State == ConnectionState.Closed)
                    MyConn.Open();
                TBL_UNDER_FORM.load();
                MyConn.Close();
            }
            catch (simplex_ORM.Spx_ORMMessage SORMM)
            {
                MyErrors.Add(SORMM);
                showErrors();
                return;
            }

            // Immettere l'oggetto in sessione nella variabile UTENTE
            Session["UTENTE"] = TBL_UNDER_FORM;
            
            // Immettere l'URL corrente nella variabile di sessione CALLING-PAGE
            Session["CALLING-PAGE"] = MyAspxFileName;

            // Passa il controllo alla pagina dei dettagli
            Response.Redirect("DettagliUtente.aspx");

        }
        /// <summary>
        /// Gestore dell'evento click sul tasto "NUOVO"
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: ???
        /// Mdfd: 10/06/2016 pre-imposta la variabile di sessione CALLING-PAGE;
        /// </pre>
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void R_Nuovo_Click(object sender, EventArgs e)
        {
            // Cancella la variabile di sessione
            Session["UTENTE"] = null;

            //<added @MdN: 10/06/2016>
            // pre-imposta la variabile di sessione CALLING-PAGE
            Session["CALLING-PAGE"] = MyAspxFileName;
            //<added @MdN: 10/06/2016>

            // Passa il controllo alla pagina dei dettagli
            Response.Redirect("~/SIMPLEX_STARTUP_ADMIN/DettagliUtente.aspx");
        }

        /// <summary>
        /// Cancella lo stato e riporta al chiamante.
        /// ----
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 01/06/2016
        /// </pre>
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void R_Indietro_Click(object sender, EventArgs e)
        {
            Session["MYSQL"] = null;
            Session["MYFILTER"] = null;

            Response.Redirect(MySbc.PreviousLink);
        }

        /// <summary>
        /// Cancella tutto e ricarica la lista iniziale.
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 01/06/2016
        /// </pre>
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void R_NuovaRicerca_Click(object sender, EventArgs e)
        {
            Session["MYSQL"] = null;
            Session["MYFILTER"] = null;

            R_Cognome.Text = null;

            MySql = MySql_Base + " WHERE " + MyFilter_Base;

            SqlDataSource_Utenti.SelectCommand = MySql;
            R_Lista.DataBind();

        }

        /// <summary>
        /// Scatena una ricerca se la textbox coniene una stringa, altrimenti mostra tutti i record.
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 01/06/2016
        /// Mdfd: 24/06/2016 - correzioni
        /// </pre>
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void R_Cerca_Click(object sender, EventArgs e)
        {
            String _filter = null;
            String[] _keyWords = null;
            Char[] _seps = {' '};

            // * Controllo
            if (this.R_Cognome.Text == null)
                R_NuovaRicerca_Click(sender, e);

            // * Controllo
            if (this.R_Cognome.Text != null && this.R_Cognome.Text.Trim().Length==0)
                R_NuovaRicerca_Click(sender, e);

            _keyWords = this.R_Cognome.Text.Split(_seps);

            foreach(String _s in _keyWords)
            {

                String _subFilter = "(Cognome Like '" + _s.Replace("'", "''") + "%' OR Username Like '%" + _s.Replace("'", "''") + "%')"; //<corr. @MdN 24/06/2016>

                if (_filter != null && _filter.Length != 0)
                    _filter = _filter + " AND " + _subFilter;
                else
                    _filter = _subFilter;
            }

            MySql = MySql_Base + " WHERE " + MyFilter_Base + " AND " + _filter;

            Session.Add("MYSQL", MySql);
            Session.Add("MYFILTER", _filter);

            SqlDataSource_Utenti.SelectCommand = MySql;
            R_Lista.DataBind();

        }//fine
    }
}
