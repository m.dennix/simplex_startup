<%@ Page Language="C#" MasterPageFile="~/MasterPCM.Master" AutoEventWireup="true" CodeBehind="GestioneGruppo.aspx.cs" Inherits="SIMPLEX_STARTUP.GestioneGruppo" Title="Gestione Gruppo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    </asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MenuPlaceHolder" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
 <div id="MainFormSection">
  <table class="FRM">
    <tr class="FRM">
        <td class="FRM_COL1">
            <asp:Label ID="L_ID" runat="server" AssociatedControlID="ID" Text="ID"></asp:Label>
        </td>
        <td class="FRM_COL2">
            <asp:TextBox ID="ID" runat="server" ReadOnly="True" Width="95px"></asp:TextBox>
        </td>
        <td class="FRM_COL3">
            <asp:Button ID="R_Indietro" runat="server" Text="Indietro" 
                onclick="R_Indietro_Click" />
        </td>
    </tr>
    <tr class="FRM">
        <td class="FRM_COL1">
            <asp:Label ID="L_DENOMINAZIONE" runat="server" 
                AssociatedControlID="DENOMINAZIONE" Text="Denominazione"></asp:Label>
        </td>
        <td class="FRM_COL2">
            <asp:TextBox ID="DENOMINAZIONE" runat="server" Width="465px"></asp:TextBox>
        </td>
        <td class="FRM_COL3">&nbsp;</td>
    </tr>
    <tr class="FRM">
        <td class="FRM_COL1">
            <asp:Label ID="L_NOTE" runat="server" AssociatedControlID="NOTE" Text="Note"></asp:Label>
        </td>
        <td class="FRM_COL2">
            <asp:TextBox ID="NOTE" runat="server" Height="65px" TextMode="MultiLine" 
                Width="464px"></asp:TextBox>
        </td>
        <td class="FRM_COL3">
            &nbsp;</td>
    </tr>
    <tr class="FRM">
        <td class="FRM_COL1">
            &nbsp;</td>
        <td class="FRM_COL2">
            &nbsp;</td>
        <td class="FRM_COL3">
            &nbsp;</td>
    </tr>
    <tr class="FRM">
        <td class="FRM_COL1">
            <asp:Label ID="L_Azioni" runat="server" Text="Azioni"></asp:Label>
        </td>
        <td class="FRM_COL2">
            <asp:Button ID="R_Nuovo" runat="server" Text="Nuovo" onclick="R_Nuovo_Click" />
        &nbsp;<asp:Button ID="R_Crea" runat="server" onclick="R_Crea_Click" Text="Crea" />
&nbsp;
        </td>
        <td class="FRM_COL3">
            <asp:Button ID="R_Elimina" runat="server" Text="Elimina" 
                onclick="R_Elimina_Click" />
        </td>
    </tr>
    </table>
 </div>
  <div id="SubFormSection">
      <asp:GridView ID="R_Lista" runat="server" AllowPaging="True" 
          AllowSorting="True" AutoGenerateColumns="False" DataKeyNames="ID" 
          DataSourceID="SqlDataSourceListaGruppo" onrowcommand="R_Lista_RowCommand">
          <Columns>
              <asp:BoundField DataField="ID" HeaderText="ID" InsertVisible="False" 
                  ReadOnly="True" SortExpression="ID" />
              <asp:BoundField DataField="DENOMINAZIONE" HeaderText="DENOMINAZIONE" 
                  SortExpression="DENOMINAZIONE" />
              <asp:BoundField DataField="NOTE" HeaderText="NOTE" SortExpression="NOTE" />
              <asp:ButtonField ButtonType="Button" CommandName="Edit" Text="Seleziona" />
          </Columns>
      </asp:GridView>
      <asp:SqlDataSource ID="SqlDataSourceListaGruppo" runat="server" 
          ConnectionString="<%$ ConnectionStrings:SIMPLEX_STARTUP_DBConnectionStringForForms %>" 
          SelectCommand="SELECT [ID], [DENOMINAZIONE], [NOTE] FROM [ADMN_gruppo] WHERE ([DATA_DEL] IS NULL)">
      </asp:SqlDataSource>
 </div>
</asp:Content>
