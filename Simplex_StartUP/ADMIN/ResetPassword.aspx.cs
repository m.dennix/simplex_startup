using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;

namespace SIMPLEX_STARTUP
{
    /// <summary>
    /// Maschera per il reset delle password delle utenze locali.<br></br>
    /// L'amministratore deve semplicemente selezionare un nominativo ed automaticamente la relativa password 
    /// viene impostata ad un valore casuale.<br></br>
    /// <br></br>
    /// Si ricorda che gli attributi derivati da GenericListSpxForm sono:<br></br>
    /// MySql:                   Stringa della query<br></br>
    /// MyFilter:                Stringa del filtro<br></br>
    /// _selectListaBase:        Query di base della lista<br></br>
    /// _filterListaBase:        Filtraggio di base della lista<br></br>
    /// _orderListBase:          Ordinamento delle righe della lista<br></br>
    /// <pre>
    /// ----
    /// @MdN
    /// Crtd: 06/05/2016
    /// </pre>
    /// </summary>
    public partial class ResetPassword : GenericListSpxForm
    {
        #region ATTRIBUTI
        #endregion
        
        /// <summary>
        /// <p>Caricamento della maschera.</p>
        /// <ul>
        /// <li>Invoca preloadEnvironment()</li>
        /// <li>esegue la query di popolamento</li>
        /// </ul>
        /// <pre>
        /// @MdN
        /// ----
        /// Crtd: 30/01/2015
        /// </pre>
        /// </summary>
        /// <param name="sender">oggetto che ha invocato l'evento</param>
        /// <param name="e">parametri</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            preloadEnvironment();

            #region [ADD YOUR CODE HERE]

            // * Controllo Accesso
            #endregion

            if (IsPostBack == false)
                writeMessageToLog(MyConn, "Form_Load", MySbc.ToString(), MyApp.CurrentUser.UID.ToString(), Session.SessionID);

            if (Session["MY_ERRORS"] != null)
            {
                MyErrors = (System.Collections.Generic.List<simplex_ORM.Spx_ORMMessage>)Session["MY_ERRORS"];
                showErrors();
                Session["MY_ERRORS"] = null;
            }
            
        }

        #region METODI PROTETTI
        /// <summary>
        /// Cancella la maschera e reimposta le variabili di stato utili per la ricerca
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 02/08/2015
        /// </pre>
        /// </summary>
        protected virtual void cancella()
        {
            ViewState["MYSQL"] = null;
            ViewState["MYFILER"] = null;
            ViewState["ORDER"] = null;

            #region TO CHANGE: ASSEGNAZIONI
            MySql = "SELECT * FROM SIMPLEX_STARTUP_VW_CENT_DIPENDENTI";                        //<-- CHANGE HERE
            MyFilter = null;                                                        //<-- CHANGE HERE
            this.SqlDataSource_MYSOURCE.SelectCommand = MySql;       //<-- CHANGE HERE
            #endregion

            #region [ADD YOUR CODE HERE]
            // aggiungere in questo punto la materiale cancellazione dei campi della Form
            #endregion
        }

        #endregion

        #region GESTORI

        /// <summary>
        /// <p>
        /// Imposta gli attributi necessari al funzionamento.
        /// Si attende in Session["APPLICATION"] l'oggetto GeneralApplication corrente.
        /// Se non è presente, lancia un eccezione del tipo generalApplication TracedMessage.
        /// </p>
        /// <p>
        /// Imposta in sessione l'ID dell'oggetto selezionato. La variabile di sessione ha come 
        /// stringa identificatrice il nome della tabella di riferimento dell'oggetto tutto in maiuscolo.
        /// </p>
        /// <p>Esempio: </p>
        /// <p>
        /// se la tabella è 'utente' il nome della variabile di sessione è 'UTENTE'.
        /// </p>
        /// 
        /// <p>Valorizza i seguenti attributi derivati da GenericSpxForm:
        /// </p>
        /// <ul>
        /// <li>MyApp               :   APPLICAZIONE CORRENTE;</li>
        /// <li>MyConn              :   CONNESSIONE DI DEFAULT;</li>
        /// <li>MyMainMenu          :   CONTROLLO MENU SINISTRO;</li>
        /// <li>MySbc               :   CONTROLLO BREADCRUMBS;</li>
        /// <li>MyAspxFileName      :   NOME DELLA PAGINA WEB/WEB FORM.</li>
        /// </ul>
        /// <p>
        /// Valorizza i suoi attributi caratteristici che sono:
        /// </p><p>
        /// <ul>
        /// <li>MyBackURL           :   URL DELLA PAGINA CHIAMANTE.</li>
        /// </ul>
        /// </p>
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: ????
        /// Mdfd: 10/04/2016 09:49 Monterotondo: da Session a ViewState
        /// </pre>
        /// </summary>
        protected override void preloadEnvironment()
        {
            // PARAMETRI DI BASE
            _selectListaBase = "select AU.ID, AU.Username, AU.Email, AU.Cognome, AU.Nome, CASE isnull(API.PWDMD5, 'NULL') WHEN 'NULL' THEN 'ASSENTE' ELSE 'PRESENTE' END AS PASSWORD, CASE FLAG_RESET WHEN 1 THEN 'Sì' ELSE 'No' END as RESET from dbo.ADMN_utente AU LEFT JOIN dbo.ADMN_PASSWORD_INTERNE API ON AU.ID=API.USR_ID";
            _filterListaBase = " WHERE AU.DATA_OSCURAMENTO IS NULL AND AU.LOCALE = 1";
            _orderListBase = " ORDER BY AU.Cognome desc, AU.Nome desc";
            MyDirectory = "SIMPLEX_STARTUP_ADMIN";

            base.preloadEnvironment();

            #region TO CHANGE: ASSEGNAZIONI - Cambiare dove indicato da "CHANGE HERE"
            //if (ViewState["MYSQL"] != null)
            //    MySql = (String)ViewState["MYSQL"];
            //else
            //    MySql = _selectListaBase + _filterListaBase + _orderListBase;

            //if (ViewState["MYFILTER"] != null)
            //    MyFilter = (String)ViewState["MYFILTER"];
            //else
            //    MyFilter = _filterListaBase;

            //if (ViewState["MYORDER"] != null)
            //    MyOrder = _orderListBase;
            //else
            //    MyOrder = (String)ViewState["MYORDER"];

            //// Nome e webcrumbs
            //MyName = this.Title;                                               //<-- CHANGE HERE
            //MyAspxFileName = "~/SIMPLEX_STARTUP_ADMIN/" + MyAspxFileName;              //<-- CHANGE HERE
            //AddToSimplexBreadCrumbs();

            #endregion

            #region [ADD YOUR CODE HERE]

            // Impostazione della query.
            SqlDataSource_MYSOURCE.SelectCommand = MySql;
            R_Lista.DataSource = SqlDataSource_MYSOURCE;
            R_Lista.DataBind();

            #endregion

        }

        protected void R_Indietro_Click(object sender, EventArgs e)
        {
            #region TO CHANGE: ASSEGNAZIONI
            if(MySbc.PreviousLink!=null)                                    //<--TO CHANGE
                Response.Redirect(MySbc.PreviousLink);                      //<--TO CHANGE
            #endregion
        }

        /// <summary>
        /// Imposta la password temporanea ed il flag che obbliga il cambiamento al primo accesso.
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 25/05/2016
        /// Mdfd: 30-31/05/2016
        /// </pre>
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected override void R_Lista_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            int _row = 0;
            int _key = 0;
            String _tempPwd = null;
            String _sql = null;
            String _testo = null;
            String _server = null;
            String _sender = null;
            simplex_ORM.SQLSrv.SQLTable _USR = null;
            System.Data.Odbc.OdbcDataReader _dr=null;
            System.Data.Odbc.OdbcCommand _cmd = null;


            #region [ADD YOUR CODE HERE]
            
            if (e.CommandName.CompareTo("Seleziona") == 0)
            {
                String _newPwd = null;

                // identificare la riga che ha scatenato l'evento: ASP.NET non lo fa in automatico
                // N.B. CommandArgument è 0-BASED!!!!
                _row = Int32.Parse(e.CommandArgument.ToString());
                // identificare il valore della chiave associata alla riga
                _key = (Int32)((GridView)e.CommandSource).DataKeys[_row].Values[0];

                if (TBL_UNDER_FORM == null && MyConn != null)
                    TBL_UNDER_FORM = simplex_ORM.SQLSrv.SQLTable.createSQLTable(MyConn, "ADMN_PASSWORD_INTERNE");

                TBL_UNDER_FORM.setValue("USR_ID", _key.ToString());
                try
                {
                    TBL_UNDER_FORM.load();
                }
                catch (simplex_ORM.Spx_ORMMessage ORMM)
                {
                    if (ORMM.MessageCode == 8)
                    {
                        // TROVATO UN UTENTE INTERNO CHE NON HA ANCORA ASSEGNATA UNA PASSWORD
                        TBL_UNDER_FORM.setValue("USR_ID", _key.ToString());
                        TBL_UNDER_FORM.setValue("USR", MyApp.CurrentUser.UID.ToString());
                        TBL_UNDER_FORM.setValue("USR_UM", MyApp.CurrentUser.UID.ToString());
                    }
                    else
                    {
                        ORMM.Source = "ResetPassword()-->" + ORMM.Source;
                        throw ORMM;
                    }
                }
                _newPwd = ((UNIF_User)MyApp.CurrentUser).getTempPwd();
                TBL_UNDER_FORM.setValue("PWDMD5", ((UNIF_User)MyApp.CurrentUser).getHash_MD5(_newPwd));
                TBL_UNDER_FORM.setValue("FLAG_RESET", "1");
                
                // SALVATAGGIO DELLA PASSWORD
                try
                {
                    TBL_UNDER_FORM.save(true);
                }
                catch (simplex_ORM.Spx_ORMMessage ORMM)
                {
                    ORMM.Source = "ResetPassword()-->" + ORMM.Source;
                    throw ORMM;
                }


                // PRELEVO TUTTE LE INFORMAZIONI SULL'UTENTE
                _USR = simplex_ORM.SQLSrv.SQLTable.createSQLTable(MyConn, "ADMN_Utente");
                _USR.setValue("ID", _key.ToString());
                _USR.load();


                // MESSAGGIO A VIDEO
                simplex_ORM.Spx_ORMMessage _mex = new simplex_ORM.Spx_ORMMessage();
                _mex.Message = "La password temporanea dell'utente " + _USR.getValue("Username") + " è: " + _newPwd + ".";
                if (MyApp != null && MyApp.CurrentUser != null && MyConn != null && Session != null)
                    _mex.traceLog(MyConn, MyApp.CurrentUser.UID.ToString(), Session.SessionID);
                MyErrors.Add(_mex);

                // * * * * * * * * * * * * * * * * * * * * * 
                // * * * * * MESSAGGIO ALL'UTENZA. * * * * *
                // * * * * * * * * * * * * * * * * * * * * * 
                // PRELEVIAMO ALCUNI PARAMETRI DAL FILE DI CONFIGURAZIONE web.config
                _server = System.Configuration.ConfigurationManager.AppSettings["SERVER_NOTIFICHE"];
                _sender = System.Configuration.ConfigurationManager.AppSettings["MITTENTE_NOTIFICHE"];
                if ((_sender != null) && (_server != null))
                {
                    // Testo
                    try
                    {
                        _sql = "SELECT TESTO from ADMN_MESSAGGISTICA WHERE DATA_DEL IS NULL AND TIPO='SET_PWD'";
                        if (MyConn.State == ConnectionState.Closed)
                            MyConn.Open();

                        _cmd = MyConn.CreateCommand();
                        _cmd.CommandText = _sql;

                        _dr = _cmd.ExecuteReader();
                        if (_dr.HasRows == true)
                        {
                            _dr.Read();
                            _testo = _dr.GetString(_dr.GetOrdinal("TESTO"));
                        }

                        _testo = _testo + "\n\n\n" + "Password Temporanea: " + _newPwd;

                        //_USR = simplex_ORM.SQLSrv.SQLTable.createSQLTable(MyConn, "ADMN_Utente");
                        //_USR.setValue("ID", _key.ToString());
                        //_USR.load();

                    }
                    catch (Exception E)
                    {
                        simplex_ORM.Spx_ORMMessage ORMM = new simplex_ORM.Spx_ORMMessage("ResetPassword.aspx: Errore nella query " + _sql + ". Vedi errore successivo.");
                        if (MyApp != null && MyApp.CurrentUser != null && MyConn != null && Session != null)
                            ORMM.traceLog(MyConn, MyApp.CurrentUser.UID.ToString(), Session.SessionID);
                        MyErrors.Add(ORMM);

                        ORMM = new simplex_ORM.Spx_ORMMessage(E.Message);
                        if (MyApp != null && MyApp.CurrentUser != null && MyConn != null && Session != null)
                            ORMM.traceLog(MyConn, MyApp.CurrentUser.UID.ToString(), Session.SessionID);
                        MyErrors.Add(ORMM);

                        _dr.Close();

                        if (MyConn.State == ConnectionState.Open)
                            MyConn.Close();

                        // USCITA FORZATA
                        throw ORMM;
                    }

                    try
                    {
                        ((UNIF_User)MyApp.CurrentUser).sendNotificationTo(_testo, _USR.getValue("EMail"), "PRE.COR.: Reimpostazione della password per l'utenza " + _USR.getValue("Username"), _server);
                    }
                    catch (simplex_ORM.Spx_ORMMessage ORMM)
                    {
                        ORMM.Source = "ResetPassword.R_Lista_RowCommand(): " + ORMM.Source;
                        MyErrors.Add(ORMM);
                        throw ORMM;
                    }
                    // Messaggio all'amministratore e refresh
                    if (MyErrors != null)
                        Session.Add("MY_ERRORS", MyErrors);
                    else
                        Session.Add("MY_ERRORS", null);
                    Response.Redirect(MyAspxFileName);
                } // FINE MESSAGGIO ALL'UTENZA
            }

            #endregion
        }
        #endregion
    }
}
