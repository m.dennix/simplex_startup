using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using GeneralApplication;
using WebSimplex;

namespace SIMPLEX_STARTUP
{
    /// <summary>
    /// Derivata da SimplexForm.
    /// Ha attributi protetti necessari per il funzionamento dell'applicazione.
    /// Esegue Override dei seguenti metodi:
    /// ------
    /// @MdN 
    /// Crtd: 06/12/2014
    /// </summary>
    public partial class DettagliApplicazioni : simplex_FORM.SimplexForm
    {
        /// <summary>
        /// Oggetto applicazione corrente.
        /// </summary>
        protected GeneralApplication.GeneralApplication MyApp = null;
        /// <summary>
        /// Oggetto connessione corrente al database SIMPLEX_STARTUP
        /// DEVE essere sempre chiusa.
        /// </summary>
        protected System.Data.Odbc.OdbcConnection MyConn = null;
        /// <summary>
        /// Nome della pagina come visualizzata nel menu.
        /// </summary>
        protected String MyName = null;
        /// <summary>
        /// Riferimento all' oggetto di classe WebSimplexBreadCrumbs.
        /// </summary>
        protected WebSimplexBreadCrumbs MySbc = null;
        /// <summary>
        /// Nome del file della pagina corrente. Viene valorizzato automaticamente 
        /// dal metodo preloadEnvironment().
        /// </summary>
        protected String MyAspxFileName = null;
        /// <summary>
        /// URL della pagina chiamante.
        /// </summary>
        protected String MyBackURL = null;
        /// <summary>
        /// Indica se il riquadro dei messaggi deve essere visibile o meno.
        /// Per default NON è visibile.
        /// </summary>
        protected Boolean MyMessageBoxVisible = false;

        /// <summary>
        /// Menu verticale associato alla maschera
        /// </summary>
        protected System.Web.UI.WebControls.Menu MyMainMenu = null;


        /// <summary>
        /// Aggiunge la pagina corrente alla catena delle briciole di pane (SimplexBreadCrumbs)
        /// della maschera corrente.
        /// @MdN
        /// ----
        /// Crtd: 18/01/2015
        /// Mdfd: 25/01/2015
        /// </summary>
        /// <remarks>La proprietà MySbc DEVE essere valorizzata.</remarks>
        protected virtual void AddToSimplexBreadCrumbs()
        {
            int _lvl;
            // controllo
            if (MySbc == null)
                return;
            // AGGIUNTA DI UNA VOCE NELLA CATENA BREADCRUMBS
            if (MyMainMenu != null)
            {
                // Se esiste un menu mi pongo il problema della coerenza tra il menu 
                // e la catena di BreadCrumbs.
                _lvl = MasterPCM.getMenuVoiceLevel(MyConn, MyName);
                if (_lvl == -1)
                    MySbc.Add(MyName, MyAspxFileName);
                else
                {
                    if (_lvl > MySbc.Count)
                        MySbc.Add(MyName, MyAspxFileName);                      //@MdN 25/01/2015
                    else
                        MySbc.InsertAndBreak(MyName, MyAspxFileName, _lvl);     //@MdN 25/01/2015
                }
            }
            else
            {
                // Se il menu non esiste non mi pongo problemi ed accodo all'ultimo anello della catena.
                if (MySbc != null)
                    MySbc.Add(MyName, MyAspxFileName);
            }
        }//fine

        /// <summary>
        /// <p>
        /// Imposta gli attributi necessari al funzionamento.
        /// Si attende in Session["APPLICATION"] l'oggetto GeneralApplication corrente.
        /// Se non è presente, lancia un eccezione del tipo generalApplication TracedMessage.
        /// Valorizza:
        /// </p>
        /// <p>
        /// Valorizza i seguenti attributi:
        /// </p>
        /// <ul>
        /// <li>MyApp               :   APPLICAZIONE CORRENTE;</li>
        /// <li>MyConn              :   CONNESSIONE DI DEFAULT;</li>
        /// <li>MyMainMenu          :   CONTROLLO MENU SINISTRO;</li>
        /// <li>MySbc               :   CONTROLLO BREADCRUMBS;</li>
        /// <li>MyAspxFileName      :   NOME DELLA PAGINA WEB/WEB FORM.</li>
        /// <li>MyBackURL           :   URL DELLA PAGINAWEB/WEB FORM CHIAMANTE.</li>
        /// </ul>
        /// <pre>
        /// ------
        /// @MdN
        /// Crtd: 06/12/2014
        /// Mdfd: 30/01/2015
        /// </pre>
        /// </summary>
        protected override void preloadEnvironment()
        {
            //MyFunzioneApplicativa = "Amministrazione";

            base.preloadEnvironment();

            MyApp = (GeneralApplication.GeneralApplication)Session["APPLICATION"];
            if (MyApp != null)
                MyConn = (System.Data.Odbc.OdbcConnection)MyApp.DefaultConnection;
            else
            {
                throw new TracedMessage("Istanza di applicazione inesistente", "GTELForm", "preloadEnvironment()");
            }

            // valorizzazione del riferimento al menu principale della maschera
            try
            {
                MyMainMenu = (Menu)this.Master.FindControl("R_MainMenu");
            }
            catch (Exception E)
            {
                MyMainMenu = null;
            }

            // valorizzazione delle briciole di pane
            try
            {
                MySbc = (WebSimplexBreadCrumbs)this.Master.FindControl("R_BreadCrumbs");
            }
            catch (Exception E)
            {
                MySbc = null;
            }

            if (this.PreviousPage != null)
                MyBackURL = this.PreviousPage.AppRelativeVirtualPath.Substring(this.PreviousPage.AppRelativeVirtualPath.LastIndexOf('/') + 1);
            else
                MyBackURL = (String)Session["CALLING-PAGE"];

            MyAspxFileName = this.Page.AppRelativeVirtualPath.Substring(this.Page.AppRelativeVirtualPath.LastIndexOf("/") + 1);
            //if(IsPostBack==false)
            //    Session["CALLING-PAGE"] = MyAspxFileName;

            ////Scrittura del log
            //if(IsPostBack==false)
            //    writeMessageToLog(MyConn, "Form_Load()", MySbc.ToString(), ((UNIF_User)MyApp.CurrentUser).UID.ToString(), Session.SessionID);

            MyName = "Dettagli Funzione Applicativa";
            Title = MyName;
            MyAspxFileName = "~/SIMPLEX_STARTUP_ADMIN/" + MyAspxFileName;
            AddToSimplexBreadCrumbs();

            // Impostazione dell'oggetto SQLTable associato alla maschera
            if (Session["FUNZIONE_APPLICATIVA"] == null)
            {
                if (MyConn.State == ConnectionState.Closed)
                    MyConn.Open();
                try
                {
                    TBL_UNDER_FORM = simplex_ORM.SQLSrv.SQLTable.createSQLTable(MyConn, "ADMN_FunzioneApplicativa");
                }
                catch (simplex_ORM.Spx_ORMMessage ORMM)
                {
                    ORMM.MessageField = "DettagliApplicazioni.preloadEnvironment()";
                    ORMM.traceLog(MyConn, ((UNIF_User)MyApp.CurrentUser).UID.ToString(),Session.SessionID);
                }                
                MyConn.Close();
            }
            else
                TBL_UNDER_FORM = (simplex_ORM.SQLSrv.SQLTable)Session["FUNZIONE_APPLICATIVA"];

            // Determina la visibilità del Messge Box
            if (Session["DettagliApplicazioni.Panel_MessageBox.Visible"] != null)
                MyMessageBoxVisible = (Boolean)Session["DettagliApplicazioni.Panel_MessageBox.Visible"];
            else
                MyMessageBoxVisible = false;
            Session["DettagliApplicazioni.Panel_MessageBox.Visible"] = null;

        }//fine preloadEnvironment

        protected void Page_Load(object sender, EventArgs e)
        {
            System.Text.StringBuilder _Sql = null;
            preloadEnvironment();
            // Visualizza/Nasconde la parte dei controlli che provvede ad associare i gruppi
            // alla funzione applicativa
            Boolean isVisible = false;
            if (TBL_UNDER_FORM.getValue("ID") != null)
            {
                // Imposta le query dei due controlli a scomparsa.
                // DropDownList
                /* se si omette questa istruzione, ASP.NET resetta i valori selezionati nei controlli */
                isVisible = true;
                if (IsPostBack == false)
                {
                    _Sql = new System.Text.StringBuilder("SELECT ID, DENOMINAZIONE from ADMN_GRUPPO WHERE DATA_DEL IS NULL AND ID NOT IN (SELECT GRUPPO FROM ADMN_FunzioneApplicativaGruppo WHERE FUNZIONEAPPLICATIVA=").Append(TBL_UNDER_FORM.getValue("ID")).Append(")");
                    SqlDataSource_DDLGruppiDisponibili.SelectCommand = _Sql.ToString();
                    this.GRUPPI_DISPONIBILI.DataBind();                    
                }
                _Sql = new System.Text.StringBuilder("SELECT G.ID, G.DENOMINAZIONE FROM ADMN_GRUPPO G INNER JOIN dbo.ADMN_FunzioneApplicativaGruppo AFAG ON AFAG.GRUPPO=G.ID AND AFAG.FUNZIONEAPPLICATIVA =").Append(TBL_UNDER_FORM.getValue("ID")).Append(" WHERE G.DATA_DEL IS NULL");
                SqlDataSource_GruppiXFunzioneApplicativa.SelectCommand = _Sql.ToString();
                this.R_GRUPPI.DataBind();
            }
            else
            {
                _Sql = new System.Text.StringBuilder("SELECT ID, DENOMINAZIONE from ADMN_GRUPPO WHERE DATA_DEL IS NULL");
                SqlDataSource_DDLGruppiDisponibili.SelectCommand = _Sql.ToString();
                this.GRUPPI_DISPONIBILI.DataBind();

                _Sql = new System.Text.StringBuilder("SELECT G.ID, G.DENOMINAZIONE FROM ADMN_GRUPPO G INNER JOIN dbo.ADMN_FunzioneApplicativaGruppo AFAG ON AFAG.GRUPPO=G.ID AND AFAG.FUNZIONEAPPLICATIVA IS NULL");
                SqlDataSource_GruppiXFunzioneApplicativa.SelectCommand = _Sql.ToString();
                this.R_GRUPPI.DataBind();
            }
            if(IsPostBack==false)
                impostaVoceNullaDDL(GRUPPI_DISPONIBILI);            //@MdN 26/04/2015
            R_Aggiungi.Visible = isVisible;
            R_GRUPPI.Visible = isVisible;
            GRUPPI_DISPONIBILI.Visible = isVisible;
            L_Avviso.Visible = isVisible;            

            // Mostra il box dei messaggi
            Panel_MessageBox.Visible = MyMessageBoxVisible;

            // Evento del BOX dei messaggi
            if (Session["DettagliApplicazioni.R_Conferma.Click"] != null)
            {
                if("R_Elimina_Click".CompareTo((String)Session["DettagliApplicazioni.R_Conferma.Click"])==0)
                    R_Conferma.Click += new EventHandler(this.R_Elimina_Click);
            }

            //Scrittura del log
            if (IsPostBack == false)
                writeMessageToLog(MyConn, "Form_Load()", MySbc.ToString(), ((UNIF_User)MyApp.CurrentUser).UID.ToString(), Session.SessionID);
            
        }

        /// <summary>
        /// <p>EN: Show the errors occurred in the last operation. At the end, clears the list of errors. 
        /// </p>
        /// <p>IT: Mostra gli errori verificatisi nell'ultima operazione. Al termine, cancella la lista degli errori.
        /// </p>
        /// </summary>
        protected void showErrors()
        {
            if (MyErrors.Count > 0)
            {
                Boolean l_pari = false;
                Literal l_ErrArea = null;
                System.Text.StringBuilder l_ErrString = new System.Text.StringBuilder("<table class=\"ErrTab\">");
                l_ErrArea = (Literal)this.Master.FindControl("ListOfErrors");

                foreach (simplex_ORM.Spx_ORMMessage ORMM in MyErrors)
                {
                    if (l_pari == true)
                        l_ErrString.Append("<tr class=\"ErrRowPari\">");
                    else
                        l_ErrString.Append("<tr class=\"ErrRowDisPari\">");
                    l_pari = !l_pari;
                    l_ErrString.Append("<td class=\"ErrCodCol\">");
                    l_ErrString.Append(ORMM.MessageCode.ToString());
                    l_ErrString.Append("</td><td class=\"ErrDescCol\">");
                    l_ErrString.Append(ORMM.Message);
                    l_ErrString.Append("</td></tr>");
                }
                l_ErrString.Append("</table>");
                l_ErrArea.Text = l_ErrString.ToString();
                MyErrors.Clear(); //<-- ATTENZIONE
            }
        }//fine

        /// <summary>
        /// Mostra l'oggetto SQLTable che supporta la maschera.
        /// ---
        /// @MdN
        /// Crtd: 01/01/2015
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected virtual void Page_SaveStateComplete(object sender, EventArgs e)
        {
            if (TBL_UNDER_FORM != null)
                this.show(TBL_UNDER_FORM, "ContentPlaceHolder1", "System.Web.UI.WebControls.ContentPlaceHolder");
        }

        /// <summary>
        /// Nome della pagina visualizzato nei menu e nelle briciole di pane.
        /// @MdN
        /// ----
        /// Crtd: 18/01/2015
        /// </summary>
        public String Nome
        {
            get
            {
                return MyName;
            }
            set
            {
                if (value == null)
                    MyName = "### DA DEFINIRE";
                else
                    MyName = value;
            }
        }

        protected void R_Salva_Click(object sender, EventArgs e)
        {
            //VERIFICA L'ESISTENZA DELL'OGGETTO
            if (TBL_UNDER_FORM == null)
            {
                simplex_ORM.Spx_ORMMessage MyErrMsg = new simplex_ORM.Spx_ORMMessage("DattagliApplicazioni.R_Salva_Click(): oggetto TBL_UNDER_FORM non istanziato", 1003);
                MyErrMsg.MessageField = "DattagliApplicazioni.R_Salva_Click()";
                MyErrors.Add(MyErrMsg);
                MyErrMsg.traceLog(MyConn, ((UNIF_User)MyApp.CurrentUser).UID.ToString(), Session.SessionID);
                showErrors();
                return;
            }
            // SALVATAGGIO
            try
            {
                TBL_UNDER_FORM.setValue("USR", ((UNIF_User)MyApp.CurrentUser).UID.ToString());
                modify(TBL_UNDER_FORM, "ContentPlaceHolder1", "System.Web.UI.WebControls.ContentPlaceHolder");
                Session["FUNZIONE_APPLICATIVA"] = TBL_UNDER_FORM;
            }
            catch (simplex_ORM.Spx_ORMMessage ORMM)
            {
                MyErrors.Add(ORMM);
                ORMM.traceLog(MyConn, ((UNIF_User)MyApp.CurrentUser).UID.ToString(), Session.SessionID);                
            }
            showErrors();

            /* visualizzazione dei controlli associativi */
            R_Aggiungi.Visible = true;
            L_Avviso.Visible = true;
            R_GRUPPI.Visible = true;
            GRUPPI_DISPONIBILI.Visible = true;
           
            /* Tracciamento dell'operazione nel log */
            if (IsPostBack == false)
                writeMessageToLog(MyConn, "DattagliApplicazioni.R_Salva_Click()", MySbc.ToString(), ((UNIF_User)MyApp.CurrentUser).UID.ToString(), Session.SessionID);


        }

        /// <summary>
        /// <p>Gestore dell'evento del tasto "Aggiungi"</p>
        /// <p>
        /// Il gestore esegue le seguenti operazioni:
        /// </p>
        /// <ol>
        /// <li>Crea una nuova associazione tra l’entità principale (ad esempio Funzione Applicativa) e l’entità aggregata (ad esempio Gruppo);</li>
        /// <li>Aggiorna la lista di entità aggregate;</li>
        /// <li>Aggiornare la DropDownList delle entità ancora aggregabili.</li>
        /// </ol>
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 07/03/2015
        /// Mdfd: 26/04/2015
        /// </pre>
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void R_Aggiungi_Click(object sender, EventArgs e)
        {
            simplex_ORM.SQLSrv.SQLTable _tbl;
            // controlli //@MdN 26/04/2015
            if (Session["GRUPPI_DISPONIBILI"] == null)
               return;

            // TRACCIA NEL LOG
            writeMessageToLog("R_Aggiungi_Click()", "DettagliApplicazioni", ((UNIF_User)MyApp.CurrentUser).UID.ToString(), Session.SessionID);
            
            //Crea una nuova associazione tra l’entità principale (ad esempio Funzione Applicativa) e l’entità aggregata (ad esempio Gruppo)
            try
            {
                if (MyConn.State == ConnectionState.Closed)
                    MyConn.Open();
                _tbl = simplex_ORM.SQLSrv.SQLTable.createSQLTable(MyConn, "ADMN_FunzioneApplicativaGruppo");
                MyConn.Close();
            }
            catch (simplex_ORM.Spx_ORMMessage ORMM)
            {
                ORMM.traceLog(MyConn, ((UNIF_User)MyApp.CurrentUser).UID.ToString(), Session.SessionID);
                MyErrors.Add(ORMM);
                showErrors();
                return;
            }
            try
            {
                //_tbl.setValue("GRUPPO", this.GRUPPI_DISPONIBILI.SelectedItem.Value.ToString());
                _tbl.setValue("GRUPPO", (String)Session["GRUPPI_DISPONIBILI"].ToString());
                _tbl.setValue("FUNZIONEAPPLICATIVA",TBL_UNDER_FORM.getValue("ID"));
                _tbl.setValue("USR", ((UNIF_User)MyApp.CurrentUser).UID.ToString());
                if (MyConn.State == ConnectionState.Closed)
                    MyConn.Open();
                _tbl.save(true);
                MyConn.Close();
            }
            catch(simplex_ORM.Spx_ORMMessage ORMM)
            {
                ORMM.traceLog(MyConn, ((UNIF_User)MyApp.CurrentUser).UID.ToString(), Session.SessionID);
                MyErrors.Add(ORMM);
                showErrors();
                return;
            }
            // Aggiorna la lista di entità aggregate;
            GRUPPI.DataBind();
            GRUPPI.Visible = true;

            // Aggiorna la DropDownList delle entità ancora aggregabili.
            System.Text.StringBuilder _Sql = new System.Text.StringBuilder("SELECT ID, DENOMINAZIONE from ADMN_GRUPPO WHERE DATA_DEL IS NULL AND ID NOT IN (SELECT GRUPPO FROM ADMN_FunzioneApplicativaGruppo WHERE FUNZIONEAPPLICATIVA=").Append(TBL_UNDER_FORM.getValue("ID")).Append(")");
            SqlDataSource_DDLGruppiDisponibili.SelectCommand = _Sql.ToString();
            this.GRUPPI_DISPONIBILI.DataBind();                    

            // TRACCIA NEL LOG
            writeMessageToLog("R_Aggiungi_Click(): Associati " + _tbl.getValue("FUNZIONEAPPLICATIVA") + " - " + _tbl.getValue("GRUPPO"), "DettagliApplicazioni", ((UNIF_User)MyApp.CurrentUser).UID.ToString(), Session.SessionID);
        }

        protected void GRUPPI_DISPONIBILI_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (GRUPPI_DISPONIBILI.SelectedValue.ToString()!=null && GRUPPI_DISPONIBILI.SelectedValue.ToString().Length > 0)        //@MdN 26/04/2015
                Session["GRUPPI_DISPONIBILI"] = GRUPPI_DISPONIBILI.SelectedValue.ToString();
            else
                Session["GRUPPI_DISPONIBILI"] = null;                                                                               //@MdN 26/04/2015
        }

        /// <summary>
        /// <p>
        /// Gestore del tasto "Dissocia"
        /// </p>
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void GRUPPI_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            Int32 _row = 0;
            Int32 _key = 0;
            System.Data.Odbc.OdbcCommand _cmd;


            // indentificazione della riga selezionata
            if (e.CommandName.CompareTo("Edit") != 0)
                return;

            // identificare la riga che ha scatenato l'evento: ASP.NET non lo fa in automatico
            // N.B. CommandArgument è 0-BASED!!!!
            _row = Int32.Parse(e.CommandArgument.ToString());

            // identificare il valore della chiave associata alla riga
            _key = Int32.Parse(((GridView)e.CommandSource).Rows[_row].Cells[0].Text);

            // Cancellazione del record
            System.Text.StringBuilder _sql = new System.Text.StringBuilder("DELETE FROM ADMN_FunzioneApplicativaGruppo WHERE FUNZIONEAPPLICATIVA = ").Append(TBL_UNDER_FORM.getValue("ID")).Append(" AND GRUPPO=").Append(_key.ToString());
            try
            {
                if (MyConn.State == ConnectionState.Closed)
                    MyConn.Open();
                _cmd = MyConn.CreateCommand();
                _cmd.CommandText = _sql.ToString();
                _cmd.ExecuteNonQuery();
                MyConn.Close();
            }
            catch (Exception E)
            {
                simplex_ORM.Spx_ORMMessage ORMM = new simplex_ORM.Spx_ORMMessage(E);
                ORMM.MessageField = MySbc.ToString() + ".GRUPPI_RowCommand()";
                ORMM.Message = "While Executing the query [" + _sql.ToString() + "]: " + E.Message;
                ORMM.traceLog(MyConn, ((UNIF_User)MyApp.CurrentUser).UID.ToString(), Session.SessionID);
                MyErrors.Add(ORMM);
                showErrors();
            }
            // Aggiornamento controlli
            // ...
            _sql = new System.Text.StringBuilder("SELECT ID, DENOMINAZIONE from ADMN_GRUPPO WHERE DATA_DEL IS NULL AND ID NOT IN (SELECT GRUPPO FROM ADMN_FunzioneApplicativaGruppo WHERE FUNZIONEAPPLICATIVA=").Append(TBL_UNDER_FORM.getValue("ID")).Append(")");
            SqlDataSource_DDLGruppiDisponibili.SelectCommand = _sql.ToString();
            this.GRUPPI_DISPONIBILI.DataBind(); 
            
            // Tracciamento nel log
            writeMessageToLog("Cancellazione ADMN_FunzioneApplicativaGruppo(" + TBL_UNDER_FORM.getValue("ID") + ", " + _key.ToString() + ")", MySbc.ToString(), ((UNIF_User)MyApp.CurrentUser).UID.ToString(), Session.SessionID);
        }

        protected void R_Indietro_Click(object sender, EventArgs e)
        {
            if ((String)Session["CALLING-PAGE"] != null && ((String)Session["CALLING-PAGE"]).CompareTo(MyAspxFileName) != 0)
            {
                String _callingpage = (String)Session["CALLING-PAGE"];
                Session["CALLING-PAGE"] = null;
                Response.Redirect(_callingpage);
            }
        }

        protected void R_Elimina_Click(object sender, EventArgs e)
        {
            if (MyMessageBoxVisible == false)
            {
                Session["DettagliApplicazioni.R_Conferma.Click"] = "R_Elimina_Click";
                MyMessageBoxVisible = true;
                Session["DettagliApplicazioni.Panel_MessageBox.Visible"] = MyMessageBoxVisible;
                Panel_MessageBox.Visible = MyMessageBoxVisible;
                return;
            }
            MyMessageBoxVisible = false;
            Session["DettagliApplicazioni.Panel_MessageBox.Visible"] = MyMessageBoxVisible;
            Panel_MessageBox.Visible = MyMessageBoxVisible;
            Session["DettagliApplicazioni.R_Conferma.Click"] = null;

            TBL_UNDER_FORM.setValue("DATA_DEL", simplex_ORM.Column.DateToSQLDateString(DateTime.Now));
            try
            {
                modify(TBL_UNDER_FORM, "ContentPlaceHolder1", "System.Web.UI.WebControls.ContentPlaceHolder");
            }
            catch(simplex_ORM.Spx_ORMMessage ORMM)
            {
                ORMM.traceLog(MyConn, ((UNIF_User)MyApp.CurrentUser).UID.ToString(), Session.SessionID);
                MyErrors.Add(ORMM);
                showErrors();
                return;
            }
            writeMessageToLog("Cancellazione Funzione Applicativa ID=" + TBL_UNDER_FORM.getValue("ID"), MySbc.ToString(), ((UNIF_User)MyApp.CurrentUser).UID.ToString(), Session.SessionID);
            Response.Redirect(Session["CALLING-PAGE"].ToString());
        }

        protected void R_Annulla_Click(object sender, EventArgs e)
        {
            MyMessageBoxVisible = false;
            Session["DettagliApplicazioni.Panel_MessageBox.Visible"] = MyMessageBoxVisible;
            Panel_MessageBox.Visible = MyMessageBoxVisible;
            Session["DettagliApplicazioni.R_Conferma.Click"] = null;
        }// fine
    }
}
