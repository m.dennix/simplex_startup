using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using System.Collections.Generic;

namespace SIMPLEX_STARTUP
{
    /// <summary>
    /// SimplexMenu
    /// ----
    /// E' una Form che crea dinamicamente un menu a partire da una tabella di questo tipo:
    /// CREATE TABLE [dbo].[UTILS_MENU]
    /// (
    /// [ID] [int] NOT NULL,                                                -- identificatore univoco della voce                            
    /// [DESCRIZIONE] [varchar](24) NOT NULL,                               -- denominazione della voce (appare a video)
    /// [PADRE] [int] NULL,                                                 -- in caso di voce di sottomenu, la voce di menu gerarchicamente superiore
    /// [LINK] [nvarchar](1024) NOT NULL,                                   -- URL della pagina/form a cui reindirizzare
    /// [USR] [int] NOT NULL,                                               -- utente che ha effettuato l'ultima modifica al record/inserimento
    /// [DATA_SYS] [datetime] NOT NULL DEFAULT (getdate()),                 -- data dell'ultima modifica al record/inserimento
    /// [DATA_DEL] [datetime] NULL,                                         -- se diversa da NULL, scartare la voce di menu
    /// [NOTE] [nvarchar](1024) NULL,                                       -- eventuali note esplicative relative alla voce di menu
    /// [IMGURL] [nvarchar](1024) NULL,                                    -- URL di un'immagine/icona che accompagna la voce di menu
    /// [FUNZIONE_APPLICATIVA] [int] NULL,                                  -- codice della funzione applicativa a cui il menu appartiene
    /// )
    /// 
    /// La form può essere chiamata dinamicamente fornendo i parametri della funzione applicativa
    /// e del livello attraverso URL (metodo GET) oppure attraverso le variabili di Sessione.
    /// La form può essere anche chiamata "staticamente" impostando le informazioni attraverso gli
    /// attributi statici di classi opportunamente derivate.
    ///
    /// ----
    /// @MdN
    /// Versione 1.1.0.1 del 22/07/2015
    /// Versione 1.1.0.2 del 30/07/2015
    /// Mdfd: 22/07/2015 - prevista IMGURL invece di IMG_URL
    /// Mdfd: 30/07/2015 - Merano - fattorizzazione del Cambio di funzione applicativa
    /// Versione 1.1.0.3 del 07/07/2016
    /// Mdfd: 07/07/2016 - Introdotte le modifiche necessarie alla realizzazione di un template.
    /// ----
    /// Cose da fare:
    /// 1)  fare in modo che il nome della prima voce del menu non debba necessariamente 
    ///     essere uguale al nome della corrispondente funzione applicativa.
    /// 
    /// </summary>
    public partial class SimplexMenu : GenericSpxForm
    {
        #region ATTRIBUTI
        /// <summary>
        /// <p>
        /// Numero di colonne (icona, descrizione) su cui visualizzare il menu.
        /// </p>
        /// <pre>
        /// @MdN
        /// ----
        /// Crtd: 23/03/2015
        /// </pre>
        /// </summary>
        protected int MyColonne = 1;
        
        
        /// <summary>
        /// Funzione applicativa o programma di cui su vuole estrarre il menu.
        /// Il valore (intero) consente di specificare una funzione applicativa 
        /// (Esempio la stessa web application "Risorse Umane" può presentare 
        /// due funzioni applicative distinte come se fossero due programmi diversi: 
        /// Gestione Giuridica e Gestione Economica.
        /// Se il valore è -1, si estraggono tutte le voci del menù e quindi tutte le
        /// funzioni applicative.
        /// </summary>
        protected int FunzioneApplicativa = -1;
        /// <summary>
        /// Livello delle voci di menu da visualizzare
        /// </summary>
        protected int Livello = 0;
        /// <summary>
        /// Attributo che serve per indicare che questo è un menù di livello superiore al primo.
        /// Per default il menu di tipo SimplexMenu è di primo livello. Settando questa opzione
        /// il menù può essere di livello superiore perchè NON viene forzato il cambio di funzione
        /// applicativa.
        /// <remarks>TODO: Da portare nel template SimplexMenu.</remarks>
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 07/10/2015
        /// Mdfd: 28/10/2015
        /// </pre>
        /// </summary>
        protected Boolean MyCambiaFunzioneApplicativa = false;
        #endregion


        #region GESTORI
        /// <summary>
        /// Gestore dell'evento Form Load().<br></br>
        /// Non viene ereditato dalla classe base.
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: ???
        /// Mdfd: 07/07/2017 - cambiato in metodo virtual.
        /// </pre>
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected virtual new void Page_Load(object sender, EventArgs e)
        {
            preloadEnvironment();
            designMenu(this.Livello, this.FunzioneApplicativa);
        }
        #endregion

        #region METODI PROTETTI
        /// <summary>
        /// Carica lo stato dell'oggetto.
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: ???
        /// Mdfd: 07/07/2016
        /// </pre>
        /// </summary>
        protected override void preloadEnvironment()
        {
            base.preloadEnvironment();

            #region TO CHANGE: CAMBIO FUNZIONE APPLICATIVA
            MyName = this.Title;                            //"toChange"; <modified @MdN: 07/07/2016>
            MyColonne = 1;                                  //<-- change here
            MyFunzioneApplicativa = "toChange";             //<-- change here

            FunzioneApplicativa = ((UNIF_Application)MyApp).getIDByName(MyFunzioneApplicativa);
            Livello = ((UNIF_Application)MyApp).lookForMenuVoiceID(MyName);                     //<-- change here - <modified @MdN: 07/07/2016>

            if (MyBypassVerifyAccess == true)
            {
                if (IsPostBack == false)
                {
                    if (MyApp.Name != null)
                    {
                        if (MyApp.Name.CompareTo(MyFunzioneApplicativa) != 0)
                            ((UNIF_Application)MyApp).load(((UNIF_Application)MyApp).getIDByName(MyFunzioneApplicativa));
                    }
                    else
                    {
                        ((UNIF_Application)MyApp).load(((UNIF_Application)MyApp).getIDByName(MyFunzioneApplicativa));
                    }
                }
            }

            //Breadcrumbs
            // MyAspxFileName = "~/TO_CHANGE/" + MyAspxFileName;       //<-- change here                                      //<-- change here
            AddToSimplexBreadCrumbs();

            #endregion

            #region [WRITE YOUR CODE HERE]
            #endregion
        }
        #endregion

        /// <summary>
        /// <p>
        /// Disegna la struttura del menu.
        /// </p>
        /// <pre>
        /// @MdN
        /// ----
        /// Crtd: 23/03/2015
        /// </pre>
        /// </summary>
        /// <param name="p_level">identificativo del livello del menu</param>
        /// <param name="p_FunzApp">identificativo della funzione applicativa (-1 tutte)</param>
        /// <remarks>Il numero di colonne su cui visualizzare il menu èscritto nell'attributo protetto MyColonne.</remarks>
        protected virtual void designMenu(int p_level, int p_FunzApp)
        {
            designMenu(p_level, p_FunzApp, MyColonne);
        }



        /// <summary>
        /// <p>
        /// Disegna la struttura del menu.
        /// </p>
        /// <pre>
        /// @MdN
        /// ----
        /// Crtd: ?????
        /// Mdfd: @MdN 23/03/2015;
        /// </pre>
        /// </summary>
        /// <param name="p_level">identificativo del livello del menu</param>
        /// <param name="p_FunzApp">identificativo della funzione applicativa (-1 tutte)</param>
        /// <param name="p_numCols">Numero di colonne su cui visualizzare il menu.</param>
        /// <remarks>Il numero di colonne su cui visualizzare il menu modifica l'attributo protetto MyColonne.</remarks>
        protected virtual void designMenu(int p_level, int p_FunzApp, int p_numCols)
        {
            System.Data.Odbc.OdbcCommand _cmd = null;
            System.Data.Odbc.OdbcDataReader _rst = null;

            // Controlli
            if (MyConn == null)
                return;

            // Imposta la proprietà MyColonne. @MdN 23/03/2015
            if (p_numCols == 0)
                MyColonne = 1;
            else
                MyColonne = p_numCols;



            //Composizione della stringa di interrogazione del database
            System.Text.StringBuilder _sql = new System.Text.StringBuilder("SELECT ");
            _sql.Append("UM.ID");
            _sql.Append(", UM.DESCRIZIONE");
            _sql.Append(", ISNULL(UM.PADRE,0) AS PADRE");
            _sql.Append(", UM.LINK");
            _sql.Append(", UM.IMGURL");
            _sql.Append(", UM.NOTE ");
            _sql.Append("FROM UTILS_MENU UM ");
            _sql.Append("WHERE ISNULL(UM.PADRE,0)=").Append(p_level);
            _sql.Append(" AND ID <> PADRE ");
            if (FunzioneApplicativa > -1)
                _sql.Append(" AND FUNZIONE_APPLICATIVA= ").Append(p_FunzApp);
            _sql.Append(" ORDER BY PADRE, ID");

            _cmd = MyConn.CreateCommand();
            _cmd.CommandText = _sql.ToString();
            if (MyConn.State == ConnectionState.Closed)
                MyConn.Open();
            _rst = _cmd.ExecuteReader();
            if (_rst.HasRows == false)
            {
                MyConn.Close();
                _rst.Dispose();
                return;
            }
            // AGGIUNTA DEI CONTROLLI ALLA FORM
            Literal _ltrl = null;
            Image _Img = null;
            LinkButton _lnkb = null;
            int _cols = 0;

            _ltrl = new Literal();
            _ltrl.Text = "<table class=\"SimplexMenuTable\">";
            Master.FindControl("form1").Controls.Add(_ltrl);
            bool _even = true;
            while (_rst.Read())
            {
                if (_cols == 0)
                {
                    _ltrl = new Literal();
                    if (_even == true)
                        _ltrl.Text = "<tr class=\"SpxOddMenuRow\">";
                    else
                        _ltrl.Text = "<tr class=\"SpxEvenMenuRow\">";
                    Master.FindControl("form1").Controls.Add(_ltrl);
                }

                _ltrl = new Literal();
                _ltrl.Text = "<td class=\"SpxMenuCell1\">";
                Master.FindControl("form1").Controls.Add(_ltrl);

                //Inserire l'immagine, se esiste
                _ltrl = new Literal();
                if (_rst.IsDBNull(4) == false)
                {
                    _Img = new Image();
                    _Img.CssClass = "SpxMenuImage";
                    _Img.ImageUrl = _rst.GetString(4);
                    _Img.AlternateText = _rst.GetString(1);
                    Master.FindControl("form1").Controls.Add(_Img);
                }
                _ltrl = new Literal();
                _ltrl.Text = "</td><td class=\"SpxMenuCell2\">";
                Master.FindControl("form1").Controls.Add(_ltrl);


                //Inserire il Link
                if (_rst.IsDBNull(1) == false && _rst.IsDBNull(3) == false)
                {
                    _lnkb = new LinkButton();
                    _lnkb.Text = _rst.GetString(1);
                    _lnkb.ID = "R_LinkButton_" + _rst.GetInt32(0).ToString();
                    _lnkb.Click += SimplexMenu_Click;
                    _lnkb.PostBackUrl = _rst.GetString(3);
                    _lnkb.CssClass = "SpxMenuLinkB";
                    _lnkb.Text = _rst.GetString(1);
                    Master.FindControl("form1").Controls.Add(_lnkb);
                }
                _ltrl = new Literal();
                _ltrl.Text = "</td><td class=\"SpxMenuCell3\">";
                Master.FindControl("form1").Controls.Add(_ltrl);

                //Inserire eventuali note
                if (_rst.IsDBNull(5) == false)
                {
                    _ltrl = new Literal();
                    _ltrl.Text = _rst.GetString(5);
                    Master.FindControl("form1").Controls.Add(_ltrl);
                }

                _ltrl = new Literal();
                _ltrl.Text = "</td>";
                Master.FindControl("form1").Controls.Add(_ltrl);

                if (_cols == (MyColonne - 1))
                {
                    _ltrl = new Literal();
                    _ltrl.Text = "</tr>";
                    Master.FindControl("form1").Controls.Add(_ltrl);
                    _even = _even ^ true;
                }
                //_even = _even ^ true;
                _cols = (_cols < (MyColonne-1)) ? (_cols + 1) : 0;

            }//FINE DEL CICLO
            // chiusura della riga
            if (_cols != 0)
            {
                while (_cols < (MyColonne))
                {
                    _ltrl = new Literal();
                    _ltrl.Text = "<td class=\"SpxMenuCell1\"></td><td class=\"SpxMenuCell2\"></td><td class=\"SpxMenuCell3\"></td>";
                    Master.FindControl("form1").Controls.Add(_ltrl);
                    _cols++;
                }
                _ltrl = new Literal();
                _ltrl.Text = "</tr>";
                Master.FindControl("form1").Controls.Add(_ltrl);
            }
            // chiusura della tabella
            _ltrl = new Literal();
            _ltrl.Text = "</table>";
            Master.FindControl("form1").Controls.Add(_ltrl);

            // FINE DELLA COMPOSIZIONE DELLA TABELLA
            _rst.Close();
            _cmd.Dispose();
            MyConn.Close();
        }


        ///// <summary>
        ///// Disegna la struttura del menu.
        ///// </summary>
        ///// <param name="p_level">identificativo del livello del menu</param>
        ///// <param name="p_FunzApp">identificativo della funzione applicativa (-1 tutte)</param>
        //protected void designMenu(int p_level, int p_FunzApp)
        //{
        //    System.Data.Odbc.OdbcCommand            _cmd = null;
        //    System.Data.Odbc.OdbcDataReader         _rst = null;

        //    // Controlli
        //    if(MyConn==null)
        //        return;
            
        //    //Composizione della stringa di interrogazione del database
        //    System.Text.StringBuilder _sql = new System.Text.StringBuilder("SELECT ");
        //    _sql.Append("UM.ID");
        //    _sql.Append(", UM.DESCRIZIONE");
        //    _sql.Append(", ISNULL(UM.PADRE,0) AS PADRE");
        //    _sql.Append(", UM.LINK");
        //    _sql.Append(", UM.IMG_URL");
        //    _sql.Append(", UM.NOTE ");
        //    _sql.Append("FROM UTILS_MENU UM ");
        //    _sql.Append("WHERE ISNULL(UM.PADRE,0)=").Append(p_level);
        //    _sql.Append(" AND ID <> PADRE ");
        //    if(FunzioneApplicativa>-1)
        //        _sql.Append(" AND FUNZIONE_APPLICATIVA= ").Append(p_FunzApp);
        //    _sql.Append(" ORDER BY PADRE, ID");

        //    _cmd = MyConn.CreateCommand();
        //    _cmd.CommandText = _sql.ToString();
        //    if (MyConn.State == ConnectionState.Closed)
        //        MyConn.Open();
        //    _rst = _cmd.ExecuteReader();
        //    if (_rst.HasRows == false)
        //    {
        //        MyConn.Close();
        //        _rst.Dispose();
        //        return;
        //    }
        //    // AGGIUNTA DEI CONTROLLI ALLA FORM
        //    Literal         _ltrl = null;
        //    Image           _Img = null;
        //    LinkButton      _lnkb=null;

        //    _ltrl = new Literal();
        //    _ltrl.Text="<table class=\"SimplexMenuTable\">";
        //    Master.FindControl("form1").Controls.Add(_ltrl);
        //    bool _even = true;
        //    while (_rst.Read())
        //    {
        //        _ltrl = new Literal();
        //        if(_even==true)
        //            _ltrl.Text="<tr class=\"SpxOddMenuRow\"><td class=\"SpxMenuCell1\">";
        //        else
        //            _ltrl.Text="<tr class=\"SpxEvenMenuRow\"><td class=\"SpxMenuCell1\">";
        //        Master.FindControl("form1").Controls.Add(_ltrl);

        //        //Inserire l'immagine, se esiste
        //        _ltrl = new Literal();
        //        if(_rst.IsDBNull(4)==false)
        //        {
        //            _Img = new Image();
        //            _Img.CssClass="SpxMenuImage";
        //            _Img.ImageUrl = _rst.GetString(4);
        //            _Img.AlternateText = _rst.GetString(1);
        //            Master.FindControl("form1").Controls.Add(_Img);
        //        }
        //        _ltrl = new Literal();
        //        _ltrl.Text = "</td><td class=\"SpxMenuCell2\">";
        //        Master.FindControl("form1").Controls.Add(_ltrl);
                
                
        //        //Inserire il Link
        //        if(_rst.IsDBNull(1)==false && _rst.IsDBNull(3)==false)
        //        {
        //            _lnkb = new LinkButton();
        //            _lnkb.Text = _rst.GetString(1);
        //            _lnkb.ID = "R_LinkButton_" + _rst.GetInt32(0).ToString();
        //            _lnkb.Click += SimplexMenu_Click;
        //            _lnkb.PostBackUrl = _rst.GetString(3);
        //            _lnkb.CssClass = "SpxMenuLinkB";
        //            _lnkb.Text = _rst.GetString(1);
        //            Master.FindControl("form1").Controls.Add(_lnkb);
        //        }
        //        _ltrl = new Literal();
        //        _ltrl.Text = "</td><td class=\"SpxMenuCell3\">";
        //        Master.FindControl("form1").Controls.Add(_ltrl);

        //        //Inserire eventuali note
        //        if(_rst.IsDBNull(5)==false)
        //        {
        //        _ltrl = new Literal();
        //        _ltrl.Text = _rst.GetString(5);
        //        Master.FindControl("form1").Controls.Add(_ltrl);
        //        }
                
        //        _ltrl = new Literal();
        //        _ltrl.Text = "</td></tr>";
        //        Master.FindControl("form1").Controls.Add(_ltrl);
        //        _even = _even ^ true;
        //    }//FINE DEL CICLO
        //    _ltrl = new Literal();
        //    _ltrl.Text="</table>";
        //    Master.FindControl("form1").Controls.Add(_ltrl);
        //    // FINE DELLA COMPOSIZIONE DELLA TABELLA
        //    _rst.Close();
        //    _cmd.Dispose();
        //    MyConn.Close();
        //}

        /// <summary>
        /// Gestore dell'evento Click sul menu
        /// </summary>
        /// <param name="p_Obj">Oggetto Sender</param>
        /// <param name="e">Argomenti</param>
        public void SimplexMenu_Click(Object p_Obj, EventArgs e)
        {

        }
    }
}
