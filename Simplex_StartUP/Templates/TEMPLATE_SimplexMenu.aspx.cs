using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using System.Collections.Generic;

namespace SIMPLEX_STARTUP
{
    /// <summary>
    /// <h1>SimplexMenu</h1><br></br>
    /// <h2>Attributi ereditati dalla classe base:</h2><br></br>
    /// <ul>
    /// <li> protected int <b>MyColonne</b> = 1: <b>Numero di colonne</b> (icona, descrizione) su cui visualizzare il menu.</li>
    /// <li> protected int <b>FunzioneApplicativa</b> = -1:  <b>Funzione applicativa o programma di cui su vuole estrarre il menu.</b>
    ///             Il valore (intero) consente di specificare una funzione applicativa 
    ///             (Esempio la stessa web application "Risorse Umane" può presentare 
    ///             due funzioni applicative distinte come se fossero due programmi diversi: 
    ///             Gestione Giuridica e Gestione Economica.<br></br>
    ///             Se il valore è -1, si estraggono tutte le voci del menù e quindi tutte le
    ///             funzioni applicative.</li>
    /// <li>protected int <b>Livello</b> = 0: Livello delle voci di menu da visualizzare.</li>
    /// <li>protected Boolean <b>MyCambiaFunzioneApplicativa</b> = false:  Attributo che serve per indicare che questo è un menù di livello superiore al primo.
    ///             Per default il menu di tipo SimplexMenu è di primo livello. Settando questa opzione
    ///             il menù può essere di livello superiore perchè NON viene forzato il cambio di funzione
    ///             applicativa.</li>
    /// <li></li>
    /// </ul>
    /// ----
    /// E' una Form che crea dinamicamente un menu a partire da una tabella di questo tipo:<BR></BR>
    /// <pre>
    /// CREATE TABLE [dbo].[UTILS_MENU]
    /// (
    /// [ID] [int] NOT NULL,                                                -- identificatore univoco della voce                            
    /// [DESCRIZIONE] [varchar](24) NOT NULL,                               -- denominazione della voce (appare a video)
    /// [PADRE] [int] NULL,                                                 -- in caso di voce di sottomenu, la voce di menu gerarchicamente superiore
    /// [LINK] [nvarchar](1024) NOT NULL,                                   -- URL della pagina/form a cui reindirizzare
    /// [USR] [int] NOT NULL,                                               -- utente che ha effettuato l'ultima modifica al record/inserimento
    /// [DATA_SYS] [datetime] NOT NULL DEFAULT (getdate()),                 -- data dell'ultima modifica al record/inserimento
    /// [DATA_DEL] [datetime] NULL,                                         -- se diversa da NULL, scartare la voce di menu
    /// [NOTE] [nvarchar](1024) NULL,                                       -- eventuali note esplicative relative alla voce di menu
    /// [IMGURL] [nvarchar](1024) NULL,                                    -- URL di un'immagine/icona che accompagna la voce di menu
    /// [FUNZIONE_APPLICATIVA] [int] NULL,                                  -- codice della funzione applicativa a cui il menu appartiene
    /// )
    /// </pre>
    /// <p>
    /// La form può essere chiamata dinamicamente fornendo i parametri della funzione applicativa
    /// e del livello attraverso URL (metodo GET) oppure attraverso le variabili di Sessione.<br></br>
    /// La form può essere anche chiamata "staticamente" impostando le informazioni attraverso gli
    /// attributi statici di classi opportunamente derivate.
    /// </p>
    /// <pre>
    /// ----
    /// @MdN
    /// Versione 1.1.1 del 22/07/2015
    /// Versione 1.1.2 del 30/07/2015
    /// Mdfd: 22/07/2015 - prevista IMGURL invece di IMG_URL
    /// Mdfd: 30/07/2015 - Merano - fattorizzazione del Cambio di funzione applicativa
    /// Mdfd: 07/07/2016 - creazione del TEMPLATE   
    /// ----
    /// </pre>
    /// 
    /// Cose da fare:<br></br>
    /// 1)  fare in modo che il nome della prima voce del menu non debba necessariamente 
    ///     essere uguale al nome della corrispondente funzione applicativa.<br></br>
    /// 
    /// </summary>
    public partial class TEMPLATE_SimplexMenu : SimplexMenu
    {

        #region ATTRIBUTI PROTETTI
        /// <summary>
        /// <p>
        /// Numero di colonne (icona, descrizione) su cui visualizzare il menu.
        /// </p>
        /// <pre>
        /// @MdN
        /// ----
        /// Crtd: 23/03/2015
        /// </pre>
        /// </summary>
        /// protected int MyColonne = 1;
        /// <summary>
        /// Funzione applicativa o programma di cui su vuole estrarre il menu.
        /// Il valore (intero) consente di specificare una funzione applicativa 
        /// (Esempio la stessa web application "Risorse Umane" può presentare 
        /// due funzioni applicative distinte come se fossero due programmi diversi: 
        /// Gestione Giuridica e Gestione Economica.
        /// Se il valore è -1, si estraggono tutte le voci del menù e quindi tutte le
        /// funzioni applicative.
        /// </summary>
        // protected int FunzioneApplicativa = -1;
        /// <summary>
        /// Livello delle voci di menu da visualizzare
        /// </summary>
        /// protected int Livello = 0;
        /// <summary>
        /// Attributo che serve per indicare che questo è un menù di livello superiore al primo.
        /// Per default il menu di tipo SimplexMenu è di primo livello. Settando questa opzione
        /// il menù può essere di livello superiore perchè NON viene forzato il cambio di funzione
        /// applicativa.
        /// <remarks>TODO: Da portare nel template SimplexMenu.</remarks>
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 07/10/2015
        /// Mdfd: 28/10/2015
        /// </pre>
        /// </summary>
        /// protected Boolean MyCambiaFunzioneApplicativa = false;
        #endregion

        #region GESTORI
        #endregion

        #region METODI PROTETTI
        /// <summary>
        /// Carica lo stato dell'oggetto.
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: ???
        /// Mdfd: 07/07/2016
        /// </pre>
        /// </summary>
        protected override void preloadEnvironment()
        {
            base.preloadEnvironment();

            #region TO CHANGE: CAMBIO FUNZIONE APPLICATIVA
            MyName = this.Title;
            MyColonne = 1;                                  //<-- change possibly here
            MyFunzioneApplicativa = "FUNZAPP";             //<-- change here

            FunzioneApplicativa = ((UNIF_Application)MyApp).getIDByName(MyFunzioneApplicativa);
            Livello = ((UNIF_Application)MyApp).lookForMenuVoiceID(MyName);                     //<-- change here

            if (MyBypassVerifyAccess == true)
            {
                if (IsPostBack == false)
                {
                    if (MyApp.Name != null)
                    {
                        if (MyApp.Name.CompareTo(MyFunzioneApplicativa) != 0)
                            ((UNIF_Application)MyApp).load(((UNIF_Application)MyApp).getIDByName(MyFunzioneApplicativa));
                    }
                    else
                    {
                        ((UNIF_Application)MyApp).load(((UNIF_Application)MyApp).getIDByName(MyFunzioneApplicativa));
                    }
                }
            }

            //Breadcrumbs
            MyAspxFileName = "APPDIR" + MyAspxFileName;       //<-- change here                                      //<-- change here
            AddToSimplexBreadCrumbs();

            #endregion

            #region [WRITE YOUR CODE HERE]
            #endregion
        }
        #endregion
    }
}
