using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using System.Text;
using GeneralApplication;

namespace SIMPLEX_STARTUP
{
    /// <summary>
    /// <h1>TEMPLATE_GenericAssociativeSpxForm</h1>
    /// TEMPLATE basato sulla maschera generica GenericAssociativeSpxForm (v. 1.0.0.2.).<br></br>
    /// Attributi da ridefinire in preloadEnvironment(): <br></br>
    /// - MyControlsXmlFilePath (FACOLTATIVO): Path del file XML contenente la definizione dei controlli da inserire dinamicamente.<br></br>
    /// - MyTableName (OBBLIGATORIO):           Nome della tabella di riferimento della maschera dinamica.<br></br>
    /// - MySelectQuery_base (FACOLTATIVO):     Parte proiettiva della query di selezione della lista dei risultati (GridView): in pratica SELECT FIELD1, FIELD2, ...., FIELDN FROM TABLE <br></br>
    ///                                         senza la clausola WHERE di filtraggio e le condizioni di filtraggio.<br></br>
    ///                                         Se null, la parte proiettiva viene determinata a partire dall'oggetto simplex_ORM.SqlSrv.SqlTable associato alla maschera (vedi preloadEnvironment()).<br></br>
    /// - MyFilterQuery_base (FACOLTATIVO):     Parte selettiva della query di selezione della lista dei risultati (GridView): in pratica tutte le condizioni che seguono la clausola WHERE.<br></br>
    ///                                         Per default è la condizione che selezione solo i record NON oscurati.<br></br>
    /// - MyOrderQuery_base (FACOLTATIVO):      Parte della query di selezione della lista dei risultati che detemina l'ordinamento.<br></br>
    ///                                         Per default è nulla.<br></br>
    /// - MyConnStringName_base (FACOLTATIVO):  Nome della stringa di connessione come impostata nel file di configurazione web.config all'elemento <pre><connectionStrings></pre><br></br>. 
    ///                                         Per default, se MyConnStringName_base è diversa da null: MyConnStringName = MyConnStringName_base, altrimenti cerca di determinarla <br></br>
    ///                                         invocando il metodo guessOLEDBConnectionStringName() <see cref="guessOLEDBConnectionStringName()"/>.<br></br>
    /// Attributi da non definire<br></br>
    /// - MySql<br></br>
    /// - MyFilter<br></br>
    /// - MyOrder<br></br>
    /// - MyCallerName:                         Nome della maschera chiamante. Usato per entrare in sessione e prelevare l'oggetto di classe SQLTable che rappresenta la tabella PRINCIPALE.<br></br>
    ///                                         Vedi Session["MYCALLERNAME"] e Session[Session["MYCALLERNAME"]].<br></br>
    /// - MAIN_UNDER_FORM:                      Oggetto di classe SQLTable che rappresenta la tabella PRINCIPALE.<br></br>
    /// <p>
    /// <h2>GenericAssociativeSpxForm</h2>
    /// Maschera che consente l'associazione di una collezione di oggetti di una classe, detta CLASSE ASSOCIATA,
    /// dall'istanza di una classe detta CLASSE PRINCIPALE.
    /// In pratica si può realizzare una maschera per popolare dati:
    /// <ul>
    /// <li>in associazione 1-N tra la classe PRINCIPALE e la classe ASSOCIATA.</li>
    /// <li>in associazione N-N tra la classe PRINCIPALE ed un'altra classe attraverso la classe ASSOCIATA.</li>
    /// </ul>
    /// 
    /// </p>
    /// <p>
    /// La maschera memorizza l'oggetto della classe ASSOCIATA nell'attributo protetto TBL_UNDER_FORM e 
    /// l'oggetto della classe PRINCIPALE nell'attributo protetto MAIN_UNDER_FORM.
    /// </p>
    /// Derivata da GenericLookupSpxForm:GenericDynamicSpxForm:GenericSpxForm:SimplexForm.
    /// Ha attributi protetti necessari per il funzionamento dell'applicazione.
    /// Esegue Override dei seguenti metodi:
    /// <pre>
    /// - preloadEnvironment()
    /// 
    /// variabili di stato impiegate:
    /// - Session["MyName"]:                            Contiene l'oggetto di classe SQLTable TBL_UNDER_FORM associato alla maschera;
    /// - Session[MyName + "_MYCALLERNAME"]:            Contiene il nome dell'oggetto proveniente dalla della maschera. Una volta usato, lo stato deve essere cancellato.
    /// - Session[Session[MyName + "_MYCALLERNAME"]]:   Contiene l'oggetto di classe SQLTable TBL_UNDER_FORM della maschera chiamante, ossia MAIN_UNDER_FORM.
    ///
    /// - ViewState["MYSQL"]:               Parte proiettiva della query di selezione della lista dei risultati
    /// - ViewState["MYFILTER"]:            Parte selettiva della query di selezione della lista dei risultati
    /// - ViewState["MYORDER"]:             Criterio di ordinamento della query di selezione della lista dei risultati  
    /// - ViewState["MYCONNSTRING"]:        Stringa di connessione per il controllo SqlDataSource della GridView R_Lista
    /// 
    /// ------
    /// @MdN 
    /// Crtd: 10/10/2015
    /// Mdfd: 15/10/2015 - eliminata da drawDynamicForm() la seconda chiamata a designTabs() perchè già invocata nella invocazione base.drawDynamicForm()
    /// Mdfd: 21/06/2016 - Ristrutturazione v 1.0.0.1
    /// Mdfd: 08/07/2016 - Page_Init() divenuta virtual - v. 1.0.0.2.
    /// </pre>
    /// <remarks>TODO: GENERALIZZARE E PORTARE IN WebSimplexFormLibrary.</remarks>
    /// </summary>
    public partial class TEMPLATE_GenericAssociativeSpxForm : GenericAssociativeSpxForm //GenericDynamicSpxForm
    {
        #region ATTRIBUTI PROTETTI DA DEFINIRE (TO CHANGE)
        /// <summary>
        /// Colonna della tabella referente che è chiave per un vincolo di integrità referenziale.
        /// </summary>
        protected String MyFkColumn = null;
        #endregion

        #region METODI PROTETTI
        /// <summary>
        /// <p>
        /// Imposta gli attributi necessari al funzionamento.<br></br>
        /// Si attende in Session["APPLICATION"] l'oggetto GeneralApplication corrente.<br></br>
        /// Se non è presente, lancia un eccezione del tipo generalApplication TracedMessage.<br></br>
        /// Valorizzare:<br></br>
        ///  - MyAspxFileName:  Aggiungere eventualmente la sottodirectory di collocazione del file.
        ///  - MyName:          con title.
        ///  - MyTableName:     Nome della tabella sottesa dalla maschera.
        ///  - MyConnStringName_base:
        ///  - MySelectQuery_base:
        ///  - MyFilterQuery_base:
        ///  - MyOrderQuery_base;
        ///  - MyFkColumn:      Colonna della tabella referente che è chiave per un vincolo di integrità referenziale.
        ///  - MyDirectory:     directory di allocazione del presente file a partire dalla directory corrente (~/).
        /// </p>
        /// <p>
        /// Valorizza i seguenti attributi:
        /// </p>
        /// <ul>
        /// <li>MyApp               :   APPLICAZIONE CORRENTE;</li>
        /// <li>MyConn              :   CONNESSIONE DI DEFAULT;</li>
        /// <li>MyMainMenu          :   CONTROLLO MENU SINISTRO;</li>
        /// <li>MySbc               :   CONTROLLO BREADCRUMBS;</li>
        /// <li>MyAspxFileName      :   NOME DELLA PAGINA WEB/WEB FORM.</li>
        /// <li>MyBackURL           :   URL DELLA PAGINAWEB/WEB FORM CHIAMANTE.</li>
        /// </ul>
        /// <pre>
        /// ------
        /// @MdN
        /// Crtd: 06/12/2014
        /// Mdfd: 30/01/2015
        /// Mdfd: 22/07/2016
        /// </pre>
        /// </summary>
        protected override void preloadEnvironment()
        {
            #region [DEFINIRE MyName, MyTableName, MyConnStringName_base]
            MyName = this.Title;
            MyTableName = "MY_TABLE_NAME";
            MyConnStringName_base = "MY_CONN_STRING";

            if (Session[MyName + "_MYCALLERNAME"] != null)
            {
                MAIN_UNDER_FORM = (simplex_ORM.SQLSrv.SQLTable)Session[(String)Session[MyName + "_MYCALLERNAME"]];
            }
            if (Session[MyName] != null)
            {
                TBL_UNDER_FORM = (simplex_ORM.SQLSrv.SQLTable)Session[MyName];
            }  

            #endregion

            #region [DEFINIRE MySelectQuery_base, MySelectQuery_base, MyOrderQuery_base, MyFkColumn, MyDirectory]

            MyFkColumn = "MY_FK_COLUMN";

            MySelectQuery_base = "MY_SQL_BASE";
            if (MAIN_UNDER_FORM != null && MAIN_UNDER_FORM.getValue("MY_KEY_ID") != null)
                MyFilterQuery_base = "DATA_DEL IS NULL AND " + "MY_SQL_FILTER";
            else
                MyFilterQuery_base = "DATA_DEL IS NULL";

            MyOrderQuery_base = "MY_ORDER_BASE";
            
            MyDirectory = "MY_DIRECTORY";
            #endregion
            
            base.preloadEnvironment();
            if(MyDirectory!=null && MyDirectory.Length>0)
                MyAspxFileName = MyDirectory + MyAspxFileName;            
        }

        /// <summary>
        /// <p>
        /// A partire da una delle colonne del vincolo di integrità referenziale,
        /// il metodo preleva tutte le colonne delle tabelle referente e referenziata che partecipano 
        /// al vincolo e copia i valori delle colonne della tabella referenziata nelle corrispondenti 
        /// colonne della tabella referente!!!
        /// </p>
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 10/10/2015
        /// Tstd: 11/10/2015 - integrità referenziale su chiave costituita di UN SOLO attributo!!!
        /// </pre>
        /// </summary>
        /// <param name="p_fieldName"></param>
        protected override void bindMainObject(String p_fieldName)
        {
            base.bindMainObject(p_fieldName);
        }


        /// <summary>
        /// <p>EN: Show the errors occurred in the last operation. At the end, clears the list of errors. 
        /// </p>
        /// <p>IT: Mostra gli errori verificatisi nell'ultima operazione. Al termine, cancella la lista degli errori.
        /// </p>
        /// </summary>
        protected override void showErrors()
        {
            base.showErrors();
        }//fine

        /// <summary>
        /// Mostra l'oggetto SQLTable che supporta la maschera.
        /// ---
        /// @MdN
        /// Crtd: 01/01/2015
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected override void Page_SaveStateComplete(object sender, EventArgs e)
        {
            base.Page_SaveStateComplete(sender, e);
        }
        #endregion

        #region PROPERTIES PUBBLICHE
        #endregion

        #region GESTORI
        /// <summary>
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: ????
        /// Mdfd: 08/07/2016 - passata a virtual
        /// </pre>
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected virtual void Page_Init(object sender, EventArgs e)
        {

            if (IsPostBack == false)
            {
                System.Web.UI.WebControls.ButtonField _ubtn = new ButtonField();
                _ubtn.ButtonType = ButtonType.Button;
                _ubtn.CommandName = "seleziona";
                _ubtn.Text = "Seleziona";
                _ubtn.Visible = true;
                _ubtn.Initialize(true, R_Lista);
                //R_Lista.EnableViewState = true;
                //R_Lista.RowCommand += new GridViewCommandEventHandler(R_Lista_RowCommand);
                R_Lista.Columns.Add(_ubtn);
            }
            R_Lista.RowCommand += new GridViewCommandEventHandler(R_Lista_RowCommand);
        }

        protected override void R_Lista_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            base.R_Lista_RowCommand(sender, e);
        }

        protected override void Page_Load(object sender, EventArgs e)
        {
            base.Page_Load(sender, e);
        }

        /// <summary>
        /// Gestore dell'evento Click sul tasto "Annulla" della MessageBox
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 08/03/2015
        /// </pre>
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected override void R_Annulla_Click(object sender, EventArgs e)
        {
            base.R_Annulla_Click(sender, e);
        }

        /// <summary>
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: ???
        /// Mdfd: 22/06/2016
        /// </pre>
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected override void R_Salva_Click(object sender, EventArgs e)
        {
            bindMainObject(MyFkColumn);
            base.R_Salva_Click(sender, e);
        }

        protected override void R_Back_Click(object sender, EventArgs e)
        {
            base.R_Back_Click(sender, e);
        }

        protected override void R_Delete_Click(object sender, EventArgs e)
        {
            base.R_Delete_Click(sender, e);

            /////** IL COMANDO AGISCE IN DUE FASI:
            //// * Verifica il controllo sender
            //// * 1) Se il sender è il bottone "R_Delete" -- FASE 1: VISUALIZZA IL MESSAGE BOX E VALORIZZA IL DELEGATO DEL TASTO OK
            //// * 2) Se il sender è il controllo WebSimplexMessageBox "R_MessageBox" -- FASE 2: EFFETTUA L'OPERAZIONE.
            //// * **/
            ////if (sender.GetType().ToString().CompareTo("System.Web.UI.WebControls.Button") == 0)
            ////{
            ////    // FASE 1:
            ////    Session["Evt_OkButton"] = new EventHandler(this.R_Delete_Click);
            ////    this.R_MessageBox.show((int)WebSimplex.WebSimplexMessageBox.BoxType.OkMessage);
            ////    R_MessageBox.Visible = true;
            ////    return;
            ////}
            ////else
            ////{
            ////    //Azione
            ////    #region WRITE YOUR CODE HERE
            ////    //WRITE YOUR CODE FROM HERE
            ////    if (TBL_UNDER_FORM != null && MyApp!=null && MyApp.CurrentUser !=null)
            ////    {
            ////        String _justNow=simplex_ORM.Column.DateToSQLDateString(DateTime.Now);
            ////        TBL_UNDER_FORM.setValue("DATA_DEL", _justNow);
            ////        TBL_UNDER_FORM.setValue("DATA_UM", _justNow);
            ////        TBL_UNDER_FORM.setValue("USR_UM", MyApp.CurrentUser.UID.ToString());
            ////        TBL_UNDER_FORM.save(true);

            ////        //cancello la maschera, pronta per una nuova immissione
            ////        TBL_UNDER_FORM = simplex_ORM.SQLSrv.SQLTable.createSQLTable(MyConn, MyTableName);
            ////        Session[MyName] = TBL_UNDER_FORM;

            ////        // REdirect su me stesso
            ////        HttpContext.Current.Response.Redirect(MyAspxFileName);
            ////    }
            ////    else
            ////    {
            ////        simplex_ORM.Spx_ORMMessage ORMM = new simplex_ORM.Spx_ORMMessage("Impossibile continuare con la cancellazione.");
            ////        if (MyConn != null && MyApp != null && MyApp.CurrentUser != null && Session.SessionID != null)
            ////            ORMM.traceLog(MyConn, MyApp.CurrentUser.UID.ToString(), Session.SessionID);
            ////        MyErrors.Add(ORMM);
            ////        showErrors();
            ////        return;
            ////    }
            ////    //WRITE YOUR CODE TO HERE
            ////    #endregion
            ////}
        }

        protected void R_Nuovo_Click(object sender, EventArgs e)
        {
            base.R_Nuovo_Click(sender, e);
        }
        #endregion
    }
}
