using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;

namespace SIMPLEX_STARTUP
{
    /// <summary>
    /// Classe TEMPLATE per la creazione di una maschera che mostra una lista di oggetti.<br></br>
    /// Fa riferimento alla maschera GenericDetailedListSpxForm.aspx  v.1.0.0.0 del 14/07/2016 (Crtd)
    /// <pre>
    /// ----
    /// @MdN
    /// Crtd: 14/07/2016 v.1.0.0.0
    /// </pre>
    /// </summary>
    public partial class TEMPLATE_GenericDetailedListSpxForm : GenericDetailedListSpxForm
    {

        #region ATTRIBUTI PROTETTI
        #endregion

        #region GESTORI

        protected override void Page_Init(object sender, EventArgs e)
        {
            MyDBConnectionStringName = "<myDBConnectionStringName>";
            base.Page_Init(sender, e);
        }

        /// <summary>
        /// <p>Caricamento della maschera.</p>
        /// <ul>
        /// <li>Invoca preloadEnvironment()</li>
        /// <li>esegue la query di popolamento</li>
        /// </ul>
        /// <pre>
        /// @MdN
        /// ----
        /// Crtd: 30/01/2015
        /// </pre>
        /// </summary>
        /// <param name="sender">oggetto che ha invocato l'evento</param>
        /// <param name="e">parametri</param>
        protected override void Page_Load(object sender, EventArgs e)
        {
            preloadEnvironment();
            base.Page_Load(sender, e);
        }

        protected override void R_Indietro_Click(object sender, EventArgs e)
        {
            base.R_Indietro_Click(sender, e);
        }

        protected override void R_Salva_Click(object sender, EventArgs e)
        {
            base.R_Salva_Click(sender, e);
        }
        #endregion

        #region METODI PROTETTI
        /// <summary>
        /// <p>
        /// Imposta gli attributi necessari al funzionamento.
        /// Si attende in Session["APPLICATION"] l'oggetto GeneralApplication corrente.
        /// Se non è presente, lancia un eccezione del tipo generalApplication TracedMessage.
        /// </p>
        /// <p>
        /// Imposta in sessione l'ID dell'oggetto selezionato. La variabile di sessione ha come 
        /// stringa identificatrice il nome della tabella di riferimento dell'oggetto tutto in maiuscolo.
        /// </p>
        /// <p>Esempio: </p>
        /// <p>
        /// se la tabella è 'utente' il nome della variabile di sessione è 'UTENTE'.
        /// </p>
        /// 
        /// <p>Valorizza i seguenti attributi derivati da GenericSpxForm:
        /// </p>
        /// <ul>
        /// <li>MyApp               :   APPLICAZIONE CORRENTE;</li>
        /// <li>MyConn              :   CONNESSIONE DI DEFAULT;</li>
        /// <li>MyMainMenu          :   CONTROLLO MENU SINISTRO;</li>
        /// <li>MySbc               :   CONTROLLO BREADCRUMBS;</li>
        /// <li>MyAspxFileName      :   NOME DELLA PAGINA WEB/WEB FORM.</li>
        /// </ul>
        /// <p>
        /// Valorizza i suoi attributi caratteristici che sono:
        /// </p><p>
        /// <ul>
        /// <li>MyBackURL           :   URL DELLA PAGINA CHIAMANTE.</li>
        /// </ul>
        /// </p>
        /// </summary>
        protected override void preloadEnvironment()
        {

            #region [INIZIALIZZAZIONE ATTRIBUTI]
            MySql_base = "<mysql>";
            MyOrder_base = "<myorder>";
            MyFilter_base = "<myfilter>";

            TableName ="<mytable>";
            MyDirectory = "<mydirectory>";
            MyDBConnectionStringName = "<myDBConnectionStringName>";
           
            #endregion

            #region [ADD YOUR CODE HERE]
            #endregion

            base.preloadEnvironment();

            if (MyDirectory != null && MyDirectory.CompareTo("<mydirectory>")!=0)
                MyAspxFileName = "~/" + MyDirectory + "/" + MyAspxFileName;
            AddToSimplexBreadCrumbs();
            
            #region [ADD YOUR CODE HERE]
            #endregion

        }
        #endregion
    }
}
