using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using System.Collections.Generic;

namespace SIMPLEX_STARTUP
{
    /// <summary>
    /// SimplexDesktop
    /// ----
    /// E' una Form che crea dinamicamente un menu a partire da una tabella di questo tipo:
    /// 
    /// 
    /// CREATE TABLE dbo.ADMN_FunzioneApplicativa
    ///(
    ///ID						INT					IDENTITY(1,1)       -- identificatore univoco della voce                            
    ///,DENOMINAZIONE			NVARCHAR(128)		NOT NULL            -- denominazione della voce (appare a video)
    ///,NOTE					NVARCHAR(1024)                          -- eventuali note esplicative relative alla voce di menu
    ///,IMGURL					NVARCHAR(1024)                          -- URL di un'immagine/icona che accompagna la voce di menu
    ///,HOMELNK				NVARCHAR(1024)                              -- URL della pagina/form a cui reindirizzare
    ///,USR					INT NOT NULL
    ///,DATA_SYS				DATETIME NOT NULL	DEFAULT GETDATE()
    ///,DATA_DEL				DATETIME NULL
    ///)
    ///GO
    /// 
    /// La form può essere chiamata dinamicamente fornendo i parametri della funzione applicativa
    /// e del livello attraverso URL (metodo GET) oppure attraverso le variabili di Sessione.
    /// La form può essere anche chiamata "staticamente" impostando le informazioni attraverso gli
    /// attributi statici di classi opportunamente derivate.
    ///
    /// </summary>
    public partial class SimplexDesktop : GenericSpxForm
    {
        /// <summary>
        /// Funzione applicativa o programma di cui su vuole estrarre il menu.
        /// Il valore (intero) consente di specificare una funzione applicativa 
        /// (Esempio la stessa web application "Risorse Umane" può presentare 
        /// due funzioni applicative distinte come se fossero due programmi diversi: 
        /// Gestione Giuridica e Gestione Economica.
        /// Se il valore è -1, si estraggono tutte le voci del menù e quindi tutte le
        /// funzioni applicative.
        /// </summary>
        protected int FunzioneApplicativa = -1;
        /// <summary>
        /// Livello delle voci di menu da visualizzare
        /// </summary>
        protected int Livello = 0;

        protected override void Page_Load(object sender, EventArgs e)
        {
            preloadEnvironment();
            designMenu(1, ((UNIF_User)MyApp.CurrentUser).getGroups());

            //Traccio l'ingresso nel log
            writeMessageToLog(MyConn, "Form_Load()", "Desktop", ((UNIF_User)MyApp.CurrentUser).UID.ToString(), Session.SessionID);

        }

        protected override void preloadEnvironment()
        {
            
            MyBypassVerifyAccess = true;

            base.preloadEnvironment();
            MySbc.InsertAndBreak("HOME", MyAspxFileName, 0);

        }

        /// <summary>
        /// Disegna la struttura del menu.
        /// </summary>
        /// <param name="p_level">Numero di colonne del menu</param>
        /// <param name="p_FunzApp">Numero di gruppi di appartenenza. null:=TUTTI</param>
        protected void designMenu(int p_numcols, Int32[] p_groups)
        {
            System.Data.Odbc.OdbcCommand _cmd = null;
            System.Data.Odbc.OdbcDataReader _rst = null;

            // Controlli
            if (MyConn == null)
                return;

            //Composizione della stringa di interrogazione del database
            System.Text.StringBuilder _sql = new System.Text.StringBuilder("SELECT ");
            _sql.Append("AFA.ID");
            _sql.Append(", AFA.DENOMINAZIONE");
            _sql.Append(", AFA.HOMELNK");
            _sql.Append(", AFA.IMGURL");
            _sql.Append(", AFA.NOTE ");
            _sql.Append("FROM ADMN_FunzioneApplicativa AFA INNER JOIN dbo.ADMN_FunzioneApplicativaGruppo AFG ON AFA.ID=AFG.FUNZIONEAPPLICATIVA ");
            _sql.Append("WHERE AFA.DATA_DEL IS NULL ");
            if (p_groups != null)
            {
                _sql.Append(" AND AFG.GRUPPO IN (");
                for (int t = 0; t < p_groups.Length; t++)
                {
                    if (t > 0)
                        _sql.Append(", ");
                    _sql.Append(p_groups[t].ToString());
                }
                _sql.Append(")");
            }

            _cmd = MyConn.CreateCommand();
            _cmd.CommandText = _sql.ToString();
            if (MyConn.State == ConnectionState.Closed)
                MyConn.Open();
            _rst = _cmd.ExecuteReader();
            if (_rst.HasRows == false)
            {
                MyConn.Close();
                _rst.Dispose();
                return;
            }
            // AGGIUNTA DEI CONTROLLI ALLA FORM
            Literal _ltrl = null;
            Image _Img = null;
            LinkButton _lnkb = null;

            _ltrl = new Literal();
            _ltrl.Text = "<table class=\"SimplexMenuTable\">";
            Master.FindControl("form1").Controls.Add(_ltrl);
            bool _even = true;
            int _count=0;
            while (_rst.Read())
            {
                if(_count==0)
                {
                    _ltrl = new Literal();
                    if (_even == true)
                        _ltrl.Text = "<tr class=\"SpxOddMenuRow\">";
                    else
                        _ltrl.Text = "<tr class=\"SpxEvenMenuRow\">";
                    Master.FindControl("form1").Controls.Add(_ltrl);
                }

                //Inserire l'immagine, se esiste
                _ltrl = new Literal();
                _ltrl.Text = "<td class=\"SpxMenuCell1\">";
                Master.FindControl("form1").Controls.Add(_ltrl);
                _ltrl = new Literal();
                if (_rst.IsDBNull(3) == false)
                {
                    _Img = new Image();
                    _Img.CssClass = "SpxMenuImage";
                    _Img.ImageUrl = _rst.GetString(3);
                    _Img.AlternateText = _rst.GetString(1);
                    Master.FindControl("form1").Controls.Add(_Img);
                }
                _ltrl = new Literal();
                _ltrl.Text = "</td><td class=\"SpxMenuCell2\">";
                Master.FindControl("form1").Controls.Add(_ltrl);


                //Inserire il Link
                if (_rst.IsDBNull(1) == false && _rst.IsDBNull(2) == false)
                {
                    _lnkb = new LinkButton();
                    _lnkb.Text = _rst.GetString(1);
                    _lnkb.ID = "R_LinkButton_" + _rst.GetInt32(0).ToString();
                    _lnkb.Click += SimplexMenu_Click;
                    _lnkb.PostBackUrl = _rst.GetString(2);
                    _lnkb.CssClass = "SpxMenuLinkB";
                    //_lnkb.Text = _rst.GetString(1);
                    Master.FindControl("form1").Controls.Add(_lnkb);
                }
                _ltrl = new Literal();
                _ltrl.Text = "</td><td class=\"SpxMenuCell3\">";
                Master.FindControl("form1").Controls.Add(_ltrl);

                //Inserire eventuali note
                if (_rst.IsDBNull(4) == false)
                {
                    _ltrl = new Literal();
                    _ltrl.Text = _rst.GetString(4);
                    Master.FindControl("form1").Controls.Add(_ltrl);
                }

                _ltrl = new Literal();
                _ltrl.Text = "</td>";

                if (_count == p_numcols - 1)
                {
                    _ltrl = new Literal();
                    _ltrl.Text = "</tr>";
                    Master.FindControl("form1").Controls.Add(_ltrl);
                    _even = _even ^ true;
                    _count = 0;
                }
                else
                {
                    _count++;
                }
            }//FINE DEL CICLO
            // Inserimento di eventuali colonne vuote in chiusura dell'ultima riga
            while (_count < p_numcols)
            {
                _ltrl = new Literal();
                _ltrl.Text = "<td class=\"SpxMenuCell1\"></td><td class=\"SpxMenuCell2\"></td><td class=\"SpxMenuCell3\"></td>";
                Master.FindControl("form1").Controls.Add(_ltrl);
                if (_count == p_numcols - 1)
                {
                    _ltrl = new Literal();
                    _ltrl.Text = "</tr>";
                    Master.FindControl("form1").Controls.Add(_ltrl);
                }
                _count++;
            }
            // CHIUSURA DELLA TABELLA
            _ltrl = new Literal();
            _ltrl.Text = "</table>";
            Master.FindControl("form1").Controls.Add(_ltrl);

            // FINE DELLA COMPOSIZIONE DELLA TABELLA
            _rst.Close();
            _cmd.Dispose();
            MyConn.Close();
        }

        /// <summary>
        /// Gestore dell'evento Click sul menu
        /// </summary>
        /// <param name="p_Obj">Oggetto Sender</param>
        /// <param name="e">Argomenti</param>
        public void SimplexMenu_Click(Object p_Obj, EventArgs e)
        {

        }
    }
}
