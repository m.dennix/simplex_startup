﻿/*
 * DEFINIZIONE DELLE MACROVARIABILI
 * - DEF_MYXMLFILEPATH:              se definita abilita la porzione di codice che consente di assegnare un path all'attributo protetto facoltativo 'MyControlsXmlFilePath'
 * - DEF_MYCONSIDERDISABLEDFIELDS   se definita abilita la porzione di codice che consente di specificare se considerare i campi disabilitati
 * 
 * _MYTABLENAME:                 abilita il codice che definisce il nome della tabella di supporto alla maschera
 * DEF_TABLENAME                 Tag di sostituzione che definisce il nome della tabella di supporto alla maschera      
 * 
 * DEF_MYCONNSTRINGNAMEBASE:    TAG DI SOSTITUZIONE. Se sostituito con _MYCONNSTRINGBASENAME abilita la porzione di codice che consente di specificare il nome della stringa di connessione sul file web.config
 * _MYCONNSTRINGNAMEBASE        abilita il codice che definisce il nome della stringa di connessione sul file web.config
 * DEF_CONNSTRINGNAMEBASE       Tag di sostituzione che definisce il nome della stringa di connessione sul file web.config      
 * 
 * DEF_MYFKCOLUMN:              TAG DI SOSTITUZIONE. Se sostituito con _MYFKCOLUMN abilita la porzione di codice che consente di specificare il nome della colonna chiave della lista.
 * _MYFKCOLUMN                  abilita il codice che definisce il nome della colonna chiave della lista delle entità associate.
 * DEF_FKCOLUMN                 Tag di sostituzione che definisce il nome della colonna chiave della lista delle entità associate.
 * 
 * DEF_MYSELECTQUERYBASE:       TAG DI SOSTITUZIONE. Se sostituito con _MYSELECTQUERYBASE abilita la porzione di codice che consente di specificare la parte PROIETTIVA della query della lista.
 * _MYSELECTQUERYBASE           abilita il codice che definisce la parte PROIETTIVA della query della lista.
 * DEF_SELECTQUERYBASE          Tag di sostituzione che definisce la parte PROIETTIVA della query della lista.
 * 
 * DEF_MYFILTERQUERYBASE:       TAG DI SOSTITUZIONE. Se sostituito con _MYFILTERQUERYBASE abilita la porzione di codice che consente di specificare la parte SELETTIVA della query della lista.
 * _MYFILTERQUERYBASE           abilita il codice che definisce la parte SELETTIVA (senza clausola WHERE) della query della lista.
 * DEF_FILTERQUERYBASE          Tag di sostituzione che definisce la parte SELETTIVA (senza clausola WHERE) della query della lista.
 * 
 * DEF_MYORDERQUERYBASE:        TAG DI SOSTITUZIONE. Se sostituito con _MYORDERQUERYBASE abilita la porzione di codice che consente di specificare la parte di ordinamento della query della lista.
 * _MYORDERQUERYBASE            abilita il codice che definisce la parte di ordinamento (senza clausola ORDER BY) della query della lista.
 * DEF_ORDERQUERYBASE           Tag di sostituzione che definisce la parte di ordinamento (senza clausola ORDER BY) della query della lista.
 * 
 * DEF_MYDIRECTORY:             TAG DI SOSTITUZIONE. Se sostituito con _MYDIRECTORY abilita la porzione di codice che consente di specificare la DIRECTORY .
 * _MYDIRECTORY                 abilita il codice che definisce la DIRECTORY.
 * DEF_DIRECTORY                Tag di sostituzione che definisce la DIRECTORY.
 * 
 * DEF_MYTOUPPER:               TAG DI SOSTITUZIONE. Se sostituito con _MYTOUPPER abilita la porzione di codice che consente di convertire i valori stringa in maiuscolo.
 * _MYTOUPPER                   abilita la porzione di codice che consente di convertire i valori stringa in maiuscolo.
 *
 * DEF_MYWRITEUSERINFOS:        TAG DI SOSTITUZIONE. Se sostituito con _MYWRITEUSERINFOS abilita la porzione di codice che consente di valorizzare le informazioni di servizio.
 * _MYWRITEUSERINFOS            abilita la porzione di codice che consente di valorizzare le informazioni di servizio.
 *
 * DEF_MYPHYSICALDELETION:      TAG DI SOSTITUZIONE. Se sostituito con _MYPHYSICALDELETION abilita la porzione di codice che consente di effettuare la cancellazione fisica.
 * _MYPHYSICALDELETION          abilita la porzione di codice che consente di effettuare la cancellazione fisica.
 * 
 * DEF_MYDELETEAFTERSAVE:       TAG DI SOSTITUZIONE. Se sostituito con _MYDELETEAFTERSAVE abilita la porzione di codice che consente di cancellare una maschera dopo un'operazione di salvataggio.
 * _MYDELETEAFTERSAVE           abilita la porzione di codice che consente di cancellare una maschera dopo un'operazione di salvataggio.
 * ----
 * v.1.0.1.0 IN DATA 03/05/2017
 * DEF_MYSAVEINFOS:             TAG DI SOSTITUZIONE. Se sostituito con _MYSAVEINFOS abilita la porzione di codice che consente di specificare CHE NON si volgliono salvare le informazioni di servizio.
 * _MYSAVEINFOS:                abilita la porzione di codice che consente di specificare CHE NON si volgliono salvare le informazioni di servizio.
 * 
 * DEF_MYDATASYS:               TAG DI SOSTITUZIONE. Se sostityito con _MYDATASYS abilita la porzione di codice che consente di specificare un nome di colonna per la DATA DI CREAZIONE DEL RECORD
 * _MYDATASYS:                  abilita la porzione di codice che consente di specificare un nome di colonna con la DATA DI CREAZIONE DEL RECORD
 * DEF_DATASYS:                 TAG DI SOSTITUZIONE che definisce un nome della colonna della data di sistema diverso da quello di default (Data_sys). Se si specifica 'null' ovvero '' (stringa vuota) allora la data di creazione non viene scritta. 
 * 
 * DEF_MYUSRSYS:                TAG DI SOSTITUZIONE. Se sostituito con _MYUSRSYS abilita la porzione di codice che consente di specificare un nome di colonna per l'utente che ha effettuato la CREAZIONE DEL RECORD
 * _MYUSRSYS:                   abilita la porzione di codice che consente di specificare un nome di colonna per l'utente che ha effettuato la CREAZIONE DEL RECORD
 * DEF_USRSYS:                  TAG DI SOSTITUZIONE che definisce un nome della colonna PER l'utenza che ha creato il record diverso da quello di default (USR). Se si specifica 'null' ovvero '' (stringa vuota) allora l'utente non viene scritto. 
 * 
 * DEF_MYDATAUM:                TAG DI SOSTITUZIONE. Se sostityito con _MYDATAUM abilita la porzione di codice che consente di specificare un nome di colonna per la DATA ULTIMA MODIFICA DEL RECORD
 * _MYDATAUM:                   abilita la porzione di codice che consente di specificare un nome di colonna con la DATA DI ULTIMA MODIFICA DEL RECORD
 * DEF_DATAUM:                  TAG DI SOSTITUZIONE che definisce un nome della colonna della data di sistema diverso da quello di default (DATA_UM). Se si specifica 'null' ovvero '' (stringa vuota) allora la data non viene scritta. 
 * 
 * DEF_MYUSRUM:                 TAG DI SOSTITUZIONE. Se sostituito con _MYUSRUM abilita la porzione di codice che consente di specificare un nome di colonna per l'utente che ha effettuato l'ULTIMA MODIFICA DEL RECORD
 * _MYUSRUM:                    abilita la porzione di codice che consente di specificare un nome di colonna per l'utente che ha effettuato l'ULTIMA MODIFICA DEL RECORD
 * DEF_USRUM:                   TAG DI SOSTITUZIONE che definisce un nome della colonna PER l'utenza che ha creato il record diverso da quello di default (USR_UM). Se si specifica 'null' ovvero '' (stringa vuota) allora l'utente non viene scritto. 
 * 
 * DEF_MYGETMAXKEY:             TAG DI SOSTITUZIONE Se sostituito con _MYGETMAXKEY abilita la porzione di codice che consente di TRATTARE la chiave della tabella come NON IDENTITY e di specificare il valore di chiave successivo.
 * _MYGETMAXKEY:                abilita la porzione di codice che consente di trattare la chiave della tabella come in campo NON IDENTITY per il quale è IL PROGRAMMA che specifica il prossimo valore di chiave,
 * 
 * DEF_MYFASTSEARCHVISIBLE:     TAG DI SOSTITUZIONE.  Se sostituito con _MYFASTSEARCHVISIBLE abilita la porzione di codice (MyFastSearchVisible) che consente di visualizzare la maschera di ricerca veloce.
 * _MYFASTSEARCHVISIBLE:        abilita la porzione di codice (MyFastSearchVisible) che consente di visualizzare la maschera di ricerca veloce.
 * 
 * DEF_MYFASTSEARCHFIELD:       TAG DI SOSTITUZIONE. Se sostituito con _MYFASTSEARCHFIELD abilita la porzione di codice (MyFastSearchField) che consente di specificare un campo testuale di ricerca.
 * _MYFASTSEARCHFIELD:          abilita la porzione di codice (MyFastSearchField) che consente di specificare un campo testuale di ricerca.
 * DEF_FASTSEARCHFIELD:         TAG DI SOSTITUZIONE che definisce il nome della colonna testuane su cui esffettuare la ricerca testuale rapida.
 */

/*
 * *** @MdN: 07/02/2020 ***
 *  DEF_NOOVVERIDE_SAVE:        TAG DI SOSTITUZIONE. Se sostituito con _NOOVVERIDE_SAVE abilita la porzione di codice che consente effettuare un override personalizzato del gestore R_Salva_Click().             
 *  _NOOVVERIDE_SAVE:           abilita la porzione di codice che consente effettuare un override personalizzato del gestore R_Salva_Click().
 */

#define _MYTABLENAME
#define _XMLFILEPATH

#define DEF_MYFASTSEARCHVISIBLE
#define DEF_MYCONNSTRINGNAMEBASE
#define DEF_MYTOUPPER
#define DEF_MYCONSIDERDISABLEDFIELDS
#define DEF_MYSELECTQUERYBASE
#define DEF_MYFILTERQUERYBASE
#define DEF_MYORDERQUERYBASE
#define DEF_MYPHYSICALDELETION

#define DEF_MYSAVEINFOS
#define DEF_MYDATASYS
#define DEF_MYUSRSYS
#define DEF_MYDATAUM
#define DEF_MYUSRUM
#define DEF_MYGETMAXKEY
#define DEF_MYFASTSEARCHFIELD
#define DEF_MYFASTSEARCHVISIBLE

#define DEF_NOOVVERIDE


using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using System.Text;
using GeneralApplication;

namespace SIMPLEX_STARTUP
{
    /// <summary>
    /// 
    /// TEMPLATE della maschera per la gestione CR.U.D. del contenuto delle tabelle di Lookup.<br></br>
    /// Può essere anche impiegata per gestire tabelle associative NxN.<br></br>
    /// 
    /// <see cref="https://gitlab.com/m.dennix/simplex_startup"/>
    /// 
    /// <hr></hr>
    /// 
    /// <i>Attributi da definire nel metodo preloadEnvironment():</i>
    /// <ul>
    /// <li>MyControlsXmlFilePath [FACOLTATIVO] : Path del file XML contenente la definizione dei controlli da inserire dinamicamente.</li>
    /// <li>MyTableName [NECESSARIO]            : Nome della tabella di riferimento della maschera dinamica.</li>
    /// <li>MySelectQuery_base [FACOLTATIVO]    :  Parte proiettiva della query di selezione della lista dei risultati (GridView): in pratica SELECT FIELD1, FIELD2, ...., FIELDN FROM TABLE
    ///                                        senza la clausola WHERE di filtraggio e le condizioni di filtraggio.
    ///                                        Se null, la parte proiettiva viene determinata a partire dall'oggetto simplex_ORM.SqlSrv.SqlTable associato alla maschera (vedi preloadEnvironment()).</li>
    /// <li>MyFilterQuery_base [FACOLTATIVO]    :  Parte selettiva della query di selezione della lista dei risultati (GridView): in pratica tutte le condizioni che seguono la clausola WHERE.
    ///                                        Per default è la condizione che selezione solo i record NON oscurati.</li>                                        
    /// <li>MyOrderQuery_base [FACOLTATIVO]     :   Parte della query di selezione della lista dei risultati che detemina l'ordinamento. Per default è nulla.</li>
    /// <li>MyConnStringName_base [FACOLTATIVO] : Nome della stringa di connessione come impostata nel file di configurazione web.config all'elemento <pre><connectionStrings></pre>.</li>
    /// <li>MyLogicalDeletion [FACOLTATIVO]     :   Atributo che imposta la cancellazione logica (true) o la cancellazione fisica (false). True è il valore di default.</li>
    /// </ul>
    /// 
    /// Deriva da <see cref="GenericLookupSpxForm.aspx"/><br></br>
    /// 
    /// <b>GenericLookupSpxForm</b><br></br>
    /// Maschera generica che consente, in assoluta indipendenza, di gestire (CR.U.D.) i record di una tabella di LookUp.<br></br>
    /// Attualmente la maschera gestisce solo tabelle dotate di chiave primaria composta da una sola colonna.<br></br>
    /// Non è necessario che il controllo GridView (R_Lista) presente nella maschera abbia definita la propietà <i>KeyNames</i>. Nel caso in cui non lo fosse è
    /// però necessario che la chave primaria sia <i>visibile</i> e posta in posizione di <i>prima colonna</i> dopo la colonna dei tasti di selezione (quindi colonna di indice '1').<br></br>
    /// Nel metodo <i>preloadEnvironment()</i> devono essere impostati diversi attributi protetti necessari per il funzionamento della maschera.
    /// 
    /// <br></br>
    /// Derivata da GenericDynamicSpxForm:GenericSpxForm:SimplexForm.
    /// Ha attributi protetti necessari per il funzionamento dell'applicazione.
    /// Esegue Override dei seguenti metodi:
    /// - preloadEnvironment()
    /// 
    /// variabili di stato impiegate:
    /// - Session["MyName"]:                Contiene l'oggetto di classe SQLTable TBL_UNDER_FORM associato alla maschera;
    /// - Session[MyName + "_MYSQL"]:               Parte proiettiva della query di selezione della lista dei risultati
    /// - Session[MyName + "_MYFILTER"]:            Parte selettiva della query di selezione della lista dei risultati
    /// - Session[MyName + "_MYORDER"]:             Criterio di ordinamento della query di selezione della lista dei risultati  
    /// - Session[MyName + "_MYCONNSTRING"]:        Stringa di connessione per il controllo SqlDataSource della GridView R_Lista
    /// ------
    /// @MdN 
    /// Crtd: 21/09/2015 v.1.0.0.0 
    /// Mdfd: 15/10/2015 v.1.0.0.1 - eliminata da drawDynamicForm() la seconda chiamata a designTabs() perchè già invocata nella invocazione base.drawDynamicForm()
    /// Mdfd: 05/10/2016 v.1.0.0.2 - Modificata R_Delete_Click() + introdotto l'attributo protetto MyLogicalDeletion
    /// Mdfd: 05-08/03/2017 v.1.0.0.3 - normalizzazione del template impegando macrovariabili e tag di sostituzione omogenei.
    /// Mdfd: 21/03/2017    v.1.0.0.4 - introdotta la macrovariabile per scegliere la cancellazione logica(default) o fisica.
    /// Versione v.1.0.0.5
    /// Mdfd: 08/04/2017    v.1.0.0.5 - correzione BUG in Page_Load() - corretti gli errori di composizione della query di caricamento della lista di declaratorie.
    /// Mdfd: 08/04/2017    v.1.0.0.5 - correzione BUG in Page_Load() - mancato richiamo del gestore della classe base --> overflow dello stack delle chiamate.
    /// Mdfd: 03-05/05/2017 v.1.0.1.0:
    ///                                 - * Introduzione di nuovi flag (vedi GenericLookupSpxForm v.1.0.1.0 del 03/05/2017)
    ///                                 - * RISTRUTTURAZIONE E SEMPLIFICAZIONE del metodo preloadEnvironment()
    ///                                 - * Corretto un BUG in Page_Load()
    ///                                 - * Introdotta la maschera di ricerca veloce
    /// Mdfd: 07/02/2020 v.1.0.1.1:
    ///                                 - * preloadEnvironment():   CARICAMENTO TBL_UNDER_FORM E MAIN_UNDER_FORM
    ///                                 - * preloadEnvironment():   IMPOSTAZIONE DELLE COLONNE CHIAVE
    ///                                 - * preloadEnvironment():   IMPOSTAZIONI DEL FILTRAGGIO BASE
    ///                                 - * R_Salva_Click():        Riportato il codice del gestore. Resa opzionale l'invocazione del gestore della superclasse mediante la Macrovariabile di compilazione condizionata DEF_NOOVERWRITE.
    /// </summary>
    public partial class TEMPLATE_GenericLookupSpxForm : GenericLookupSpxForm //: GenericDynamicSpxForm
    {
        #region COSTANTI DA DEFINIRE STATICAMENTE (TO CHANGE)
        #endregion

        #region ATTRIBUTI PROTETTI
        #endregion

        #region METODI PROTETTI

        /// <summary>
        /// <p>
        /// Restituisce una stringa che rappresenta una query SQL di selezione dei record
        /// della tabella di lookup.
        /// Prerequisito: l'attributo protetto TBL_UNDER_FORM deve essere valorizzato.
        /// </p>
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 21/09/2015
        /// </pre>
        /// </summary>
        /// <returns>Stringa che rappresenta una query SQL di selezione ovvero null.</returns>
        protected override String getSelectString()
        {
            return base.getSelectString();
        }

        /// <summary>
        /// <p>
        /// Imposta gli attributi necessari al funzionamento.
        /// Si attende in Session["APPLICATION"] l'oggetto GeneralApplication corrente.<br></br>
        /// 
        /// Dato che ci si attende che gli override di questo metodo vanno a modificare specifiche linee di programma, in questo override 
        /// si lascia il codice come nella GenericLookupSpxForm.<br></br>
        /// 
        /// Se non è presente, lancia un eccezione del tipo generalApplication TracedMessage.
        /// Valorizza:
        /// </p>
        /// <p>
        /// Valorizza i seguenti attributi:
        /// </p>
        /// <ul>
        /// <li>MyApp               :   APPLICAZIONE CORRENTE;</li>
        /// <li>MyConn              :   CONNESSIONE DI DEFAULT;</li>
        /// <li>MyMainMenu          :   CONTROLLO MENU SINISTRO;</li>
        /// <li>MySbc               :   CONTROLLO BREADCRUMBS;</li>
        /// <li>MyAspxFileName      :   NOME DELLA PAGINA WEB/WEB FORM.</li>
        /// <li>MyBackURL           :   URL DELLA PAGINAWEB/WEB FORM CHIAMANTE.</li>
        /// </ul>
        /// <pre>
        /// ------
        /// @MdN
        /// Crtd: 06/12/2014 - v.1.0.0.0 -
        /// Mdfd: 30/01/2015 - v.1.0.0.1 -
        /// Mdfd: 04/10/2016 - v.1.0.0.2 - Cambiato ViewState["MYSOMETHING"] in Session[MyName + "_MYSOMETHING"]
        /// Mdfd: 04/05/2017 - v.1.0.1.0 
        ///                     -* RISTRUTTURAZIONE E SEMPLIFICAZIONE
        ///                     -* INTRODUZIONE DELLA MASCHERA DI RICERCA VELOCE 
        /// </pre>
        /// </summary>
        protected override void preloadEnvironment()
        {
            // IMPOSTAZIONE DEGLI ATTRIBUTI PROTETTI PRIMARI
#if _MYTABLENAME
            MyTableName = "DEF_TABLENAME";
#else
            MyTableName = null;
#endif

#if _MYSELECTQUERYBASE
            MySelectQuery_base = "DEF_SELECTQUERYBASE";
#else
            MySelectQuery_base = null;
#endif

#if _MYFILTERQUERYBASE
            MyFilterQuery_base = "DEF_FILTERQUERYBASE";
#else
            MyFilterQuery_base = null;
#endif

#if _MYORDERQUERYBASE
            MyOrderQuery_base = "DEF_ORDERQUERYBASE";
#else
            MyOrderQuery_base = null;
#endif

#if _MYCONNSTRINGNAMEBASE
            MyConnStringName_base = "DEF_CONNSTRINGNAMEBASE"; //è INUTILE???
#else
            MyConnStringName_base = null;
#endif

            // LANCIO IL METODO DELLA SUPERCLASSE
            base.preloadEnvironment();

            // IMPOSTAZIONE DEGLI ALTRI ATTRIBUTI PROTETTI
#if DEF_XMLFILEPATH
            MyControlsXmlFilePath = "DEF_XMLFILEPATH";
#else
            MyControlsXmlFilePath = null;
#endif


#if DEF_MYTOUPPER
            MyToUpper = true;
#endif

#if DEF_MYCONSIDERDISABLEDFIELDS
            MyConsiderDisabledFields = true;
#else
            MyConsiderDisabledFields = false;
#endif

// <added @MdN 21/03/2017>
#if _MYPHYSICALDELETION
            MyLogicalDeletion = false;
#else
            MyLogicalDeletion = true;
#endif
// </added @MdN 21/03/2017>

// <added @MdN: 04-05/05/2017>
#if _MYSAVEINFOS
            MySaveInfos = false; // non salvare le informazioni di servizio
#endif
            
#if _MYDATASYS
            MyDataSys = DEF_DATASYS;
#endif
            
#if _MYUSRSYS
            MyUsrSys = DEF_USRSYS;
#endif
            
#if _MYDATAUM
            MyDataUM = DEF_DATAUM;
#endif
            
#if _MYUSRUM
            MyUsrUM = DEF_USRUM;
#endif

#if _MYGETMAXKEY
            MyGetMaxKeyValue = true;
#endif

#if _MYFASTSEARCHVISIBLE
            MyFastSearchVisible = true;
#endif
            
#if _MYFASTSEARCHFIELD
        MyFastSearchField = "DEF_FASTSEARCHFIELD";
#endif

//</added>
            #region CARICAMENTO TBL_UNDER_FORM E MAIN_UNDER_FORM

            getFromCallerPage();    //<added @MdN:07/02/2020 issue #

            #endregion

            #region IMPOSTAZIONE DELLE COLONNE CHIAVE (22/01/2020)

            int _numKeys = TBL_UNDER_FORM.PrimaryKeyColumns.Count;      //Numero delle chiavi
            String[] _Keys = new String[_numKeys];                      //Trasformiamo la collezione delle stringhe delle colonne chiave in un array di stringhe.
            for (int _t = 0; _t < _Keys.Length; _t++)
                _Keys[_t] = TBL_UNDER_FORM.PrimaryKeyColumns[_t];

            try
            {
                R_Lista.DataKeyNames = _Keys;
            }
            catch (Exception E)
            {

                simplex_ORM.Spx_ORMMessage _ORMM = new simplex_ORM.Spx_ORMMessage();
                _ORMM.Message = "Si verificato il seguente errore:" + E.Message + " Vedere la lista degli errori..";
                _ORMM.Source = "GestioneProvvedimenti.preloadEnvironment()";
                _ORMM.InnerMessage = E;
                _ORMM.MessageType = 1;
                if (MyConn != null && MyApp != null && MyApp.CurrentUser != null && Session != null)
                    _ORMM.traceLog(MyConn, MyApp.CurrentUser.UID.ToString(), Session.SessionID);
                MyErrors.Add(_ORMM);
                showErrors(MESSAGE_TYPE.ERROR_MSG);
                return;
            }
            #endregion

            #region IMPOSTAZIONE DEL FILTRAGGIO BASE

            if (MAIN_UNDER_FORM != null)
            {
                if (MyFilterQuery_base == null)
                    MyFilterQuery_base = "NP=" + MAIN_UNDER_FORM.getValue("NP");
                else
                    MyFilterQuery_base = MyFilterQuery_base + " AND " + MyFilterQuery_base;

                if (MyFilter == null)
                {
                    MyFilter = MyFilterQuery_base;
                    Session.Add(MyName + "_MYFILTER", MyFilter);
                }
            }

            #endregion

            #region DA CANCELLARE
            //MyName = this.Title;

            //// DETERMINAZIONE DELL'OGGETTO DI RIFERIMENTO
            //// Devono essere definite ed inizializzate MyName e MyTableName
            //if (Session[MyName] != null)
            //    TBL_UNDER_FORM = (simplex_ORM.SQLSrv.SQLTable)Session[MyName];
            //else
            //{
            //    try
            //    {
            //        TBL_UNDER_FORM = simplex_ORM.SQLSrv.SQLTable.createSQLTable(MyConn, MyTableName);
            //        Session.Add(MyName, TBL_UNDER_FORM);
            //    }
            //    catch (simplex_ORM.Spx_ORMMessage ORMM)
            //    {
            //        if(MyConn!=null && MyApp!=null && MyApp.CurrentUser!=null && Session.SessionID!=null)
            //            ORMM.traceLog(MyConn, MyApp.CurrentUser.UID.ToString(), Session.SessionID);
            //        MyErrors.Add(ORMM);
            //        showErrors();
            //        return;
            //    }
            //}

            //// DETERMINAZIONE DELLE TRE COMPONENTI DELLA QUERY DELLA GridView
            //if (Session[MyName + "_MYSQL"] != null)
            //{
            //    MySql = (String)Session[MyName + "_MYSQL"];
            //}
            //else
            //{
            //    if (MySelectQuery_base != null)
            //        MySql = MySelectQuery_base;
            //    else
            //    {
            //        MySql = getSelectString();
            //    }
            //    Session.Add(MyName + "_MYSQL", MySql);
            //}

            //if (IsPostBack == false)
            //{
            //    // MySql = MySelectQuery_base;
            //    MyFilter = MyFilterQuery_base;
            //    MyOrder = MyOrderQuery_base;
            //    Session.Add(MyName + "_MYFILTER", MyFilter);
            //    Session.Add(MyName + "_MYORDER", MyOrder);
            //}
            //else
            //{
            //    //MySql = (String)ViewState["MYSQL"];
            //    if (Session[MyName + "_MYFILTER"] != null)
            //        MyFilter = (String)Session[MyName + "_MYFILTER"];
            //    else
            //        MyFilter = MyFilterQuery_base;

            //    if (Session[MyName + "_MYORDER"] != null)
            //        MyOrder = (String)Session[MyName + "_MYORDER"];
            //    else
            //        MyOrder = MyOrderQuery_base;
            //}

            //// DETERMINAZIONE DELLA STRINGA DI CONNESSIONE DA ASSOCIARE AL CONTROLLO SQLDATASOURCE_LOOKUP
            //if (Session[MyName + "_MYCONNSTRING"] != null)
            //    MyConnString = (String)Session[MyName + "_MYCONNSTRING"];
            //else
            //{
            //    if (MyConnStringName == null && MyConn!=null)
            //        MyConnStringName = base.guessOLEDBConnectionStringName(MyConn);
            //    if (MyConnStringName == null)
            //    {
            //        //ERRORE
            //        simplex_ORM.Spx_ORMMessage ORMM = new simplex_ORM.Spx_ORMMessage("La stringa di connessione non risulta essere stata impostata nel file di configurazione web.config");
            //        if(MyConn!=null && MyApp!=null && MyApp.CurrentUser!=null && Session.SessionID!=null)
            //            ORMM.traceLog(MyConn, MyApp.CurrentUser.UID.ToString(), Session.SessionID);
            //        MyErrors.Add(ORMM);
            //        return;
            //    }
            //    // INSERIMENTO DELLA STRINGA DI CONNESSIONE NELLO STATO IN MODO DA NON DOVER RICAVARLA OGNI VOLTA
            //    System.Configuration.Configuration _webConfig = null;
            //    _webConfig = System.Web.Configuration.WebConfigurationManager.OpenWebConfiguration("~/Web.config");
            //    MyConnString = _webConfig.ConnectionStrings.ConnectionStrings[MyConnStringName].ConnectionString;
            //    //MyConnString = System.Configuration.Configuration.ConnectionStrings[MyConnStringName].connectionString;
            //    if (MyConnString == null)
            //    {
            //        //ERRORE
            //        simplex_ORM.Spx_ORMMessage ORMM = new simplex_ORM.Spx_ORMMessage("La stringa di connessione non risulta essere stata impostata nel file di configurazione web.config");
            //        if (MyConn != null && MyApp != null && MyApp.CurrentUser != null && Session.SessionID != null)
            //            ORMM.traceLog(MyConn, MyApp.CurrentUser.UID.ToString(), Session.SessionID);
            //        MyErrors.Add(ORMM);
            //        return;
            //    }
            //    // Aggiungo allo stato
            //    Session.Add(MyName + "_MYCONNSTRING", MyConnString); //MYCONNSTRING
            //}
            #endregion

            #region WRITE YOUR CODE HERE
            // WRITE YOUR CODE FROM HERE
            // WRITE YOUR CODE TO HERE
            #endregion
            
        }//fine preloadEnvironment
              

        /// <summary>
        /// <p>EN: Show the errors occurred in the last operation. At the end, clears the list of errors. 
        /// </p>
        /// <p>IT: Mostra gli errori verificatisi nell'ultima operazione. Al termine, cancella la lista degli errori.
        /// </p>
        /// </summary>
        protected override void showErrors()
        {
            base.showErrors();
        }//fine

        /// <summary>
        /// Mostra l'oggetto SQLTable che supporta la maschera.
        /// ---
        /// @MdN
        /// Crtd: 01/01/2015
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected override void Page_SaveStateComplete(object sender, EventArgs e)
        {
            base.Page_SaveStateComplete(sender, e);
        }


        /// <summary>
        /// <p>
        /// Crea una struttura di configurazione a partire dal nome di una tabella
        /// (simplex_ORM.SQLSrv.SQLTable).
        /// </p>
        /// </summary>
        /// <param name="p_Name">Nome della tabella.</param>
        protected virtual void createXMLConfigurationFile(String p_Name)
        {
            //TODO:
        }
        /// <summary>
        /// TODO:
        /// <p>
        /// Scrive su un file xml una struttura di configurazione.
        /// </p>
        /// </summary>
        /// <param name="p_config">Nome del file di configurazione.</param>
        protected virtual void writeXMLConfigurationFile(SIMPLEX_Config.Spx_XmlConfigFile p_config)
        {
            //TODO:
        }

#endregion

        #region PROPERTIES PUBBLICHE
        /// <summary>
        /// Nome della pagina visualizzato nei menu e nelle briciole di pane.
        /// @MdN
        /// ----
        /// Crtd: 18/01/2015
        /// </summary>
        public String Nome
        {
            get
            {
                return MyName;
            }
            set
            {
                if (value == null)
                    MyName = "### DA DEFINIRE";
                else
                    MyName = value;
            }
        }
        #endregion

        #region GESTORI

        /// <summary>
        /// Gestione delle selezione di una voce dalla lista.
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: ???
        /// Mdfd: 04/10/2016
        /// Mdfd: 08/04/2017
        /// </pre>
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected override void R_Lista_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            base.R_Lista_RowCommand(sender, e);
        }

        /// <summary>
        /// Evento di caricamento della pagina.<br></br>
        /// In genere vi è la necessità di effettuare l'override di questo metodo in punti specifici, quindi il template ripropone l'intero codice di GenericLookupSpxForm.
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 04/10/2016
        /// Mdfd: 08/04/2017 v.1.0.0.4 - correzione BUG - corretti gli errori di composizione della query di caricamento della lista di declaratorie.
        /// Mdfd; 04/05/2017 v.1.0.0.5 - correzione BUG
        /// </pre>
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected override void Page_Load(object sender, EventArgs e)
        {
            try
            {
                preloadEnvironment();
            }
            catch (simplex_ORM.Spx_ORMMessage ORMM)
            {
                if (MyConn != null && MyApp != null && MyApp.CurrentUser != null && Session.SessionID != null)
                    ORMM.traceLog(MyConn, MyApp.CurrentUser.UID.ToString(), Session.SessionID);
                MyErrors.Add(ORMM);
            }

            // impostazione della DqlDataSource e della GridView associata
            //SqlDataSource_LookUP.SelectCommand = MySql + " WHERE " + MyFilter + " ORDER BY " + MyOrder;
            SqlDataSource_LookUP.SelectCommand = MySql;
            if (MyFilter != null)
                SqlDataSource_LookUP.SelectCommand = SqlDataSource_LookUP.SelectCommand + " WHERE " + MyFilter;
            if (MyOrder != null)
                SqlDataSource_LookUP.SelectCommand = SqlDataSource_LookUP.SelectCommand + " ORDER BY " + MyOrder;
            //</Mdfd: @MdN 08/04/2017>
            if (MyConn != null & MyConnString != null)
            {
                SqlDataSource_LookUP.ConnectionString = MyConnString;
                R_Lista.DataSource = SqlDataSource_LookUP;
                R_Lista.DataBind();
            }
            else
            {
                // ERRORE
                simplex_ORM.Spx_ORMMessage ORMM = new simplex_ORM.Spx_ORMMessage("La stringa di connessione non risulta essere stata impostata nel file di configurazione web.config");
                if (MyConn != null && MyApp != null && MyApp.CurrentUser != null && Session.SessionID != null)
                    ORMM.traceLog(MyConn, MyApp.CurrentUser.UID.ToString(), Session.SessionID);
                MyErrors.Add(ORMM);
                showErrors();
                return;
            }

            #region GESTIONE DELLA COMPOSIZIONE DINAMICA DELLA FORM

            SIMPLEX_Config.Spx_XmlElement _head = null;
            SIMPLEX_Config.Spx_XmlConfigWriter _wr = null;
            // Determinazione del nome del file xml.

            // Il nome del file in prima istanza è quello impostato staticamente 
            // nella costante MyControlsXmlFilePath.
            String _XmlFile = MyControlsXmlFilePath;

            /* *******************************************
             * La creazione dinamica della maschera può essere fatta SE E SOLO se
             * è vera anche solo UNA delle due seguenti condizioni:
             * (1) L'utente ha definito staticamente il nome della tabella di riferimento
             *     nella costante 'MyTableName'
             * (2) L'utente ha creato la tabella TBL_UNDER_FORM all'interno del metodo preloadEnvironment().
             * 
             * ******************************************* */
            if (((TBL_UNDER_FORM != null) && (TBL_UNDER_FORM.Name != null)) || MyTableName != null)
            {
                if (_XmlFile == null)
                {
                    //Se l'utente non ha staticamente definito il path del file xml, il sistema 
                    //lo prende dal nome del file aspx.
                    _XmlFile = MyAspxFileName;
                }
                if (MyAspxFileName != null)
                {
                    _XmlFile = MyAspxFileName + ".xml";
                }

                // Se il file NON esiste, il sistema lo crea
                if (System.IO.File.Exists(MapPath(_XmlFile)) == false)
                {
                    if (IsPostBack == false)
                    {
                        //Crea la struttura di configurazione
                        if (TBL_UNDER_FORM!= null && TBL_UNDER_FORM.Name != null)
                            _head = getControlsFromTable(simplex_ORM.SQLSrv.SQLTable.createSQLTable(MyConn, TBL_UNDER_FORM.Name), null);   //TODO.
                        else
                            _head = getControlsFromTable(simplex_ORM.SQLSrv.SQLTable.createSQLTable(MyConn, MyTableName), null);   //TODO.
                        //Scrive la struttura di configurazione nel file
                        if (_head != null)
                        {
                            _wr = _head.getXmlConfigWriter(MapPath(_XmlFile));
                            if (_wr != null)
                                _wr.write();

                        }
                    }
                }// fine creazione dell'eventuale creazione file

                if (IsPostBack == false)
                    ViewState.Clear();
                //Caricamento del file
                drawDynamicForm(_XmlFile, 1);
            }// fine disegno della maschera
            #endregion

            #region WRITE YOUR CODE
            // <%WRITE YOUR CODE FROM HERE%>
            // <%WRITE YOUR CODE TO HERE%>
            #endregion

            //@MdN 15/03/215
            if (MyErrors.Count > 0)
                showErrors();
            else
                ((Literal)this.Master.FindControl("ListOfErrors")).Text = "";

            //Scrittura del log
            if (IsPostBack == false)
                //<#replace/>
                writeMessageToLog(MyConn, "Form_Load()", MySbc.ToString(), MyApp.CurrentUser.UID.ToString(), Session.SessionID);

        }

        /// <summary>
        /// Gestore dell'evento Click sul tasto "Annulla" della MessageBox
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 08/03/2015
        /// </pre>
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void R_Annulla_Click(object sender, EventArgs e)
        {
        }

        /// <summary>
        /// Salva l'oggetto corrente.<br></br>
        /// Impostare la macrovariabile _NOOVERIDE per effettuare l'override del metodo.
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 07/02/2020 issue #4 v.1.0.1.0.
        /// </pre>
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected override void R_Salva_Click(object sender, EventArgs e)
        {
#if DEF_NOOVERIDE
            base.R_Salva_Click(sender, e);
#else
            String _ownName = this.GetType().Name;
            try
            {
                this.copyValues(TBL_UNDER_FORM, "R_Dynamic", "System.Web.UI.WebControls.PlaceHolder");
                //<added @MdN: 22/02/2017>
                if (MyToUpper == true)
                {
                    foreach (String s in TBL_UNDER_FORM.SQLColumns)
                    {
                        if (TBL_UNDER_FORM.getValue(s) != null)
                            TBL_UNDER_FORM.setValue(s, TBL_UNDER_FORM.getValue(s).ToUpper());
                    }
                }
                //</added @MdN: 22/02/2017>
            }
            catch (simplex_ORM.Spx_ORMMessage ORMM)
            {
                ORMM.traceLog(MyConn, ((UNIF_User)MyApp.CurrentUser).UID.ToString(), Session.SessionID);
                MyErrors.Add(ORMM);
                if (MyErrors.Count > 0)
                    showErrors();
                return;
            }

            #region [OPZIONALE]: valorizzazione del campo chiave non autoincrementante (non identity) - <mdfd @MdN: 04/05/2017>

            if (MyGetMaxKeyValue == true && TBL_UNDER_FORM.PrimaryKeyColumns.Count == 1)
            {

                String _SingleKey = TBL_UNDER_FORM.PrimaryKeyColumns[0];
                if (TBL_UNDER_FORM.getValue(_SingleKey) == null) // NUOVO INSERIMENTO?
                {
                    Int32 _MaxKeyValue = 0;
                    System.Data.Odbc.OdbcCommand _cmd = null;
                    try
                    {
                        if (MyConn != null)
                        {
                            if (MyConn.State == ConnectionState.Closed)
                                MyConn.Open();
                            _cmd = MyConn.CreateCommand();
                            _cmd.CommandText = "select max(" + _SingleKey + ") + 1 from " + TBL_UNDER_FORM.Name;
                            _MaxKeyValue = (Int32)_cmd.ExecuteScalar();
                            // Imposto il valore di chiave
                            TBL_UNDER_FORM.setValue(_SingleKey, _MaxKeyValue.ToString());
                        }
                    }
                    catch (simplex_ORM.Spx_ORMMessage _ORMM)
                    {
                        if (MyApp.CurrentUser != null && MyApp.CurrentUser != null && MyConn != null && Session != null)
                            _ORMM.traceLog(MyConn, ((UNIF_User)MyApp.CurrentUser).UID.ToString(), Session.SessionID);
                        MyErrors.Add(_ORMM);
                        if (MyErrors.Count > 0)
                            showErrors();
                        return;
                    }
                    catch (Exception _e)
                    {
                        simplex_ORM.Spx_ORMMessage _ORMM = new simplex_ORM.Spx_ORMMessage(_e);
                        _ORMM.MessageCode = 0;
                        _ORMM.MessageType = 1;
                        if (MyApp.CurrentUser != null && MyApp.CurrentUser != null && MyConn != null && Session != null)
                            _ORMM.traceLog(MyConn, ((UNIF_User)MyApp.CurrentUser).UID.ToString(), Session.SessionID);
                        MyErrors.Add(_ORMM);
                        if (MyErrors.Count > 0)
                            showErrors();
                        return;
                    }
                    finally
                    {
                        if (MyConn.State == ConnectionState.Open)
                            MyConn.Close();
                        _cmd.Dispose();
                    }
                }
            }
            #endregion

            #region [OPZIONALE]: valorizzazione delle informazioni di servizio - <mdfd @MdN: 04/05/2017>
            /*
             * SE NON INTERESSATI ALLA VALORIZZAZIONE DELLE INFORMAZIONI DI SERVIZIO,
             * IMPOSTARE MySaveInfos=FALSE.
             * 
             * IN CASO DI VALORIZZAZIONE DI INFORMAZIONI DI SERVIZIO PERSONALIZZATE,
             * USARE LA REGION "WRITE YOUR CODE HERE".
             * 
             * <mdfd @MdN: 22/02/2017>
             * <mdfd @MdN: 04/05/2017>
             */
            if (TBL_UNDER_FORM != null && MyApp != null && MyApp.CurrentUser != null && MySaveInfos == true)
            {
                String _JustNow = simplex_ORM.Column.DateToSQLDateString(DateTime.Now);
                if (MyDataUM != null && MyDataUM.Length > 0 && TBL_UNDER_FORM.SQLColumns.Contains(MyDataUM) == true)
                {
                    TBL_UNDER_FORM.setValue(MyDataUM, _JustNow);
                }

                if (MyUsrUM != null && MyUsrUM.Length > 0 && TBL_UNDER_FORM.SQLColumns.Contains(MyUsrUM) == true)
                {
                    TBL_UNDER_FORM.setValue(MyUsrUM, MyApp.CurrentUser.UID.ToString());
                }

                if (MyDataSys != null && MyDataSys.Length > 0 && TBL_UNDER_FORM.SQLColumns.Contains(MyDataSys) == true)
                {
                    if (TBL_UNDER_FORM.getValue(MyDataSys) == null)
                    {
                        TBL_UNDER_FORM.setValue(MyDataSys, _JustNow);
                        //TBL_UNDER_FORM.setValue("USR", MyApp.CurrentUser.UID.ToString());
                    }
                }

                if (MyUsrSys != null && MyUsrSys.Length > 0 && TBL_UNDER_FORM.SQLColumns.Contains(MyUsrSys) == true)
                {
                    if (TBL_UNDER_FORM.getValue(MyUsrSys) == null)
                    {
                        TBL_UNDER_FORM.setValue(MyUsrSys, MyApp.CurrentUser.UID.ToString());
                    }
                }

            }
            #endregion

            #region COLLEGAMENTO CON LA TABELLA PRINCIPALE
            if (MAIN_UNDER_FORM != null && MAIN_UNDER_FORM.getValue("NP") != null)
                TBL_UNDER_FORM.setValue("NP", MAIN_UNDER_FORM.getValue("NP"));
            #endregion

            try
            {
                //Reset connessione
                if (MyConn.State == ConnectionState.Open)
                    MyConn.Close();
                if (MyConn.State == ConnectionState.Closed)
                    MyConn.Open();
                TBL_UNDER_FORM.save(true);
                if (MyConn.State == ConnectionState.Open)
                    MyConn.Close();


            }
            catch (simplex_ORM.Spx_ORMMessage ORMM)
            {
                ORMM.traceLog(MyConn, ((UNIF_User)MyApp.CurrentUser).UID.ToString(), Session.SessionID); //<mdfd @MdN: 04/05/2017 - risolto bug>
                MyErrors.Add(ORMM);
                if (MyErrors.Count > 0)
                    showErrors();
                return;
            }

            this.show(TBL_UNDER_FORM, "R_Dynamic", "System.Web.UI.WebControls.PlaceHolder"); //<mdfd @MdN 04/05/2017 - Fattorizzata>

            #region TRACCIAMENTO
            if (MyApp != null && MyApp.CurrentUser != null && MyConn != null && Session != null)
                writeMessageToLog(MyConn, "Inserita o Modificata la voce: " + TBL_UNDER_FORM.ToString(), MyAspxFileName + ".R_Salva_Click()", ((UNIF_User)MyApp.CurrentUser).UID.ToString(), Session.SessionID);
            #endregion

            #region WRITE YOUR CODE HERE
            //WRITE YOUR CODE FROM HERE

            //WRITE YOUR CODE TO HERE
            #endregion

            if (MyOKMessage == true)
            {
                // Messaggio finale.
                simplex_ORM.Spx_ORMMessage _mex = new simplex_ORM.Spx_ORMMessage("Inserimento o Aggiornamento dell'oggetto " + TBL_UNDER_FORM.Name + " avvenuto con successo");
                // Messaggio all'utente
                _mex.Message = "<img src=\"../Immagini/OK.png\" alt=\"SUCCESSO\">&nbsp;<span>OPERAZIONE AVVENUTA CON SUCCESSO:</span><br/>" + _mex.Message;
                MyErrors.Clear();
                MyErrors.Add(_mex);
                Session.Add(MyName + "_MYERRORS", MyErrors);
                TBL_UNDER_FORM = null;
                Session.Add(MyName, TBL_UNDER_FORM);
                // La presenza dello user control impone il redirect esplicito.
                HttpContext.Current.Response.Redirect(MyAspxFileName);
            }
            R_Lista.DataBind();

#endif
        }

        protected override void R_Back_Click(object sender, EventArgs e)
        {
            base.R_Back_Click(sender, e);
        }

        protected override void R_Delete_Click(object sender, EventArgs e)
        {

            /** IL COMANDO AGISCE IN DUE FASI:
             * Verifica il controllo sender
             * 1) Se il sender è il bottone "R_Delete" -- FASE 1: VISUALIZZA IL MESSAGE BOX E VALORIZZA IL DELEGATO DEL TASTO OK
             * 2) Se il sender è il controllo WebSimplexMessageBox "R_MessageBox" -- FASE 2: EFFETTUA L'OPERAZIONE.
             * **/
            if (sender.GetType().ToString().CompareTo("System.Web.UI.WebControls.Button") == 0)
            {
                // FASE 1:
                Session["Evt_OkButton"] = new EventHandler(this.R_Delete_Click);
                this.R_MessageBox.show((int)WebSimplex.WebSimplexMessageBox.BoxType.OkMessage);
                R_MessageBox.Visible = true;
                return;
            }
            else
            {
                //Azione
                #region WRITE YOUR CODE HERE
                //<mdfd: @MdN 05/10/2016>
                if (TBL_UNDER_FORM != null && MyApp!=null && MyApp.CurrentUser !=null  && TBL_UNDER_FORM.PrimaryKeyColumns!=null)
                {
                    if (MyLogicalDeletion == true)
                    {
                        String _justNow = simplex_ORM.Column.DateToSQLDateString(DateTime.Now);
                        TBL_UNDER_FORM.setValue("DATA_DEL", _justNow);
                        TBL_UNDER_FORM.setValue("DATA_UM", _justNow);
                        TBL_UNDER_FORM.setValue("USR_UM", MyApp.CurrentUser.UID.ToString());
                        TBL_UNDER_FORM.save(true);
                    }
                    else
                    {
                            String _sql = "DELETE FROM " + TBL_UNDER_FORM.Name + " WHERE " + TBL_UNDER_FORM.PrimaryKeyColumns[0] + " = '" + TBL_UNDER_FORM.getValue(TBL_UNDER_FORM.PrimaryKeyColumns[0]) + "'";
                            System.Data.Odbc.OdbcCommand _cmd = null;
                        try
                        {
                            if (MyConn != null)
                            {
                                if (MyConn.State == ConnectionState.Closed)
                                    MyConn.Open();
                                _cmd = MyConn.CreateCommand();
                                _cmd.CommandText = _sql;
                                _cmd.ExecuteNonQuery();
                                MyConn.Close();
                            }
                        }
                        catch (Exception _E)
                        {
                            simplex_ORM.Spx_ORMMessage _ORMM = new simplex_ORM.Spx_ORMMessage();
                            _ORMM.Message="Nell'eseguire la query [" + _sql + "] è intervenuto il seguente errore: " + _E.Message;
                            if (MyConn != null && MyApp != null && MyApp.CurrentUser != null && Session.SessionID != null)
                                _ORMM.traceLog(MyConn, MyApp.CurrentUser.UID.ToString(), Session.SessionID);
                            MyErrors.Add(_ORMM);
                            showErrors();
                            return;
                        }
                    }

                    // write to log
                    if (MyConn != null && MyApp != null && MyApp.CurrentUser != null && Session.SessionID != null)
                        writeMessageToLog(MyConn, "Eliminazione dell'oggetto " + TBL_UNDER_FORM.ToString() + " avvenuta con successo", "GestioneResponsabiliStruttura.R_Delete_Click()", MyApp.CurrentUser.UID.ToString(), Session.SessionID);

                    //cancello la maschera, pronta per una nuova immissione
                    TBL_UNDER_FORM = simplex_ORM.SQLSrv.SQLTable.createSQLTable(MyConn, MyTableName);
                    Session[MyName] = TBL_UNDER_FORM;



                    // REdirect su me stesso
                    HttpContext.Current.Response.Redirect(MyAspxFileName);
                }
                else
                {
                    simplex_ORM.Spx_ORMMessage ORMM = new simplex_ORM.Spx_ORMMessage("Impossibile continuare con la cancellazione.");
                    if (MyConn != null && MyApp != null && MyApp.CurrentUser != null && Session.SessionID != null)
                        ORMM.traceLog(MyConn, MyApp.CurrentUser.UID.ToString(), Session.SessionID);
                    MyErrors.Add(ORMM);
                    showErrors();
                    return;
                }
                //WRITE YOUR CODE TO HERE
                //<mdfd: @MdN 05/10/2016>
                #endregion
                           
            }
        }

        protected override void R_Nuovo_Click(object sender, EventArgs e)
        {
            base.R_Nuovo_Click(sender, e);
        }
        

        /// <summary>
        /// Ricerca rapida testuale
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 05/05/2017
        /// </pre>
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void R_Cerca_Click(object sender, EventArgs e)
        {
            base.R_Cerca_Click(sender, e);
        }

        /// <summary>
        /// Reset della ricerca precedente
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 05/05/2017
        /// </pre>
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void R_Reset_Click(object sender, EventArgs e)
        {
            base.R_Reset_Click(sender, e);
        }
        #endregion
    }
}
