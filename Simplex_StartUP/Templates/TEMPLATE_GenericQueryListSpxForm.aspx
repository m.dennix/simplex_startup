<%@ Page Language="C#" MasterPageFile="~/MasterPCM.Master" AutoEventWireup="true" CodeBehind="TEMPLATE_GenericQueryListSpxForm.aspx.cs" Inherits="SIMPLEX_STARTUP.TEMPLATE_GenericQueryListSpxForm" Title="TEMPLATE_GenericQueryListSpxForm" EnableEventValidation="false" %>
<%@ Register Assembly="WebSimplex" Namespace="WebSimplex" TagPrefix="WS1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MenuPlaceHolder" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
 <div id="MainFormSection">
 
 <WS1:WebSimplexMessageBox ID="R_MessageBox" runat="server" 
        AlertText="Attenzione, vedi il riquadro degli errori." 
        OkAlertButtonCssStyle="AlertButton" 
        AlertImage="Immagini/Omino_Errore.png" 
        OkMessageImage="Immagini/Megafono_tondoviola.png" 
        OkMessageText="Continuare con l'operazione?"
        OkMessageButtonCssStyle="OkButton" 
        UndoMessageButtonCssStyle="UndoButton" CssOkMsgClass="OkMsg" />
 
  <table class="FRM">
  
    <tr class="FRM_ROW">
        <td class="FRM_COL1"></td>
        <td class="FRM_COL2"></td>
        <td class="FRM_COL3">
            <asp:Button ID="R_Indietro" runat="server" Text="Indietro" 
                onclick="R_Indietro_Click" />
        </td>
    </tr>
    
    <tr class="FRM_ROW">
        <td class="FRM_COL1"></td>
        <td class="FRM_COL2"></td>
        <td class="FRM_COL3"></td>
    </tr>
    
    <tr class="FRM_ROW">
        <td class="FRM_COL1"></td>
        <td class="FRM_COL2"></td>
        <td class="FRM_COL3"></td>
    </tr>
  </table>
  
  <!-- PUT YOUR CONTROLS FROM HERE -->
    <asp:PlaceHolder ID="R_Dynamic" runat="server">
    </asp:PlaceHolder>
<!-- PUT YOUR CONTROLS TO HERE -->

    
  <table class="FRM">
    <tr class="FRM">
        <td class="FRM_COL1">
            <asp:Button ID="R_Nuovo" runat="server" onclick="R_Nuovo_Click" 
                Text="Nuova ricerca" />
        </td>
        <td class="FRM_COL2">
            <asp:Button ID="R_Cerca" runat="server" Text="Cerca" onclick="R_Cerca_Click" />
        </td>
        <td class="FRM_COL3"></td>
    </tr>
    </table>
 </div>
  <div id="SubFormSection">
      <asp:GridView ID="R_Lista" runat="server" AllowPaging="True" 
          AllowSorting="True" onrowcommand="R_Lista_RowCommand" PageSize="100" DataSourceID="SqlDataSource_MYSOURCE">
          <Columns>
              <asp:ButtonField ButtonType="Button" CommandName="seleziona" HeaderText="Comando" 
                  ShowHeader="True" Text="Seleziona" />
          </Columns>
      </asp:GridView>
      <asp:SqlDataSource ID="SqlDataSource_MYSOURCE" runat="server" 
      ConnectionString="<%$ ConnectionStrings:SIMPLEX_STARTUP_DBConnectionStringForForms %>" 
      SelectCommand="SELECT * FROM [MYTABLE]">
      </asp:SqlDataSource>
 </div>
</asp:Content>
