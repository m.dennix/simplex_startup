using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;

namespace SIMPLEX_STARTUP
{    
    /// <summary>
    /// Classe TEMPLATE per la creazione di una maschera che mostra i risultati di una ricerca.
    /// Deriva da GenericDynamicSpxForm.<br/>
    /// 
    /// I parametri derivati da GenericQueryListSpxForm e che possono essere ridefiniti sono:
    /// <ol>
    /// <li>simplex_ORM.SQLSrv.SQLTable DESTINATION_TABLE: usato per passare un oggetto alla form dei dettagli</li>
    /// <li>String MyDestinationTableName: vedi oltre</li>
    /// <li>String MyDestinationPagePath: vedi oltre</li>
    /// <li>MySearchHandler MyRicerca: delegato per specificare una particolare funzione di ricerca.</li>
    /// <li>Boolean MyButtonVisible: vedi oltre</li>
    /// <li>String MyControlsXmlFilePath: path del file xml dell'eventuale maschera di ricerca</li>
    /// <li>String MyTableName: nome della tabella da passare alla maschera dei dettgali come oggetto SQLTable</li>
    /// <li>const String _selectListaBase:  query di selezione - parte proiettiva</li>
    /// <li>const String _filterListaBase:  query di selezione - parte selettiva</li>
    /// <li>const String _orderListBase:    query di selezione - parte di ordinamento</li>
    /// <li></li>
    /// <li></li>
    /// </ol>
    /// 
    /// 
    /// La maschera può essere impostata in modo che sia possibile visualizzare i dettagli di un record selezionato.
    /// Per fare ciò è necessario:
    /// <ol>
    /// <li>Impostare l'attributo <pre>MyDestinationTableName</pre> con il nome della <b>tabella</b> contenente i dati da visualizzare.</li>
    /// <li>Impostare l'attributo <pre>MyDestinationPagePath</pre> con il percorso della maschera chè dovrà visualizzare i dettagli.</li>
    /// <li>Impostare l'attributo <pre></pre> al valore <b>true</b> in modo da rendere visibili i pulsanti di comando.</li>
    /// </ol><br/>
    /// Questa classe TEMPLATE valorizza l'attributo <pre>GridView.DataKeyNames</pre> con l'elenco delle chiavi della tabella contenente i dati da visualizzare.
    /// Se tale tabella non ha una chiave primaria formalmente definita, la maschera visualizza l'elenco dei risultati di una ricerca senza dare la possibilità
    /// di visualizzare i dettagli in un'altra maschera.
    /// 
    /// 
    /// variabili di stato impiegate:
    /// - Session["MyName"]:                Contiene l'oggetto di classe SQLTable TBL_UNDER_FORM associato alla maschera;
    /// - Session["MYCALLERNAME"]:          Contiene in nome l'oggetto proveniente dalla della maschera. Una volta usato, lo stato deve essere cancellato.
    /// - Session[Session["MYCALLERNAME"]]: Contiene l'oggetto di classe SQLTable TBL_UNDER_FORM della maschera chiamante.
    ///
    /// - ViewState["MYSQL"]:               Parte proiettiva della query di selezione della lista dei risultati
    /// - ViewState["MYFILTER"]:            Parte selettiva della query di selezione della lista dei risultati
    /// - ViewState["MYORDER"]:             Criterio di ordinamento della query di selezione della lista dei risultati  
    /// 
    /// <pre>
    /// ----
    /// @MdN
    /// Crtd: 16/11/2015 v 1.0.0.0.
    /// Mdfd: 27/11/2015 v 1.0.0.1.
    /// Mdfd: 29/11/2015: @MdN - v 1.0.0.2 - MyTableName ed MyControlsXmlFilePath sono diventati attributi che devono essere nizializzati in preloadEnvironment()>
    /// 
    /// </pre>
    /// </summary>
    public partial class TEMPLATE_GenericQueryListSpxForm : GenericQueryListSpxForm
    {

        #region ATTRIBUTI

        #endregion

        #region CONSTANTS: CHANGE HERE
        #endregion

        #region GESTORI

        /// <summary>
        /// Nuova ricerca
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 20/11/2015
        /// 
        /// </pre>
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void R_Nuovo_Click(object sender, EventArgs e)
        {
            cancella();
        }

        /// <summary>
        /// Ricerca: chiama il metodo protetto cerca()
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 20/11/2015
        /// </pre>
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void R_Cerca_Click(object sender, EventArgs e)
        {
            base.R_Cerca_Click(sender, e);
            #region ADD YOUR CODE HERE
            #endregion
        }


        protected void R_Indietro_Click(object sender, EventArgs e)
        {
            base.R_Indietro_Click(sender, e);
            #region TO CHANGE: ADD YOU CODE HERE
//            if (MySbc.PreviousLink != null)                                    //<--TO CHANGE
//                Response.Redirect(MySbc.PreviousLink);                         //<--TO CHANGE
            #endregion
        }

        protected void R_Lista_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            int _row = 0;
            int _key = 0;

            #region [MOSTRA DETTAGLI]
            if (e.CommandName.ToLower().CompareTo("seleziona") == 0)
            {
                // identificare la riga che ha scatenato l'evento: ASP.NET non lo fa in automatico
                // N.B. CommandArgument è 0-BASED!!!!
                _row = Int32.Parse(e.CommandArgument.ToString());

                // identificare il valore della chiave associata alla riga
                //_key = Int32.Parse(((GridView)e.CommandSource).Rows[_row].Cells[0].Text);

                // Gestione della Redirezione
                if (MyDestinationPagePath != null && MyDestinationTableName != null)
                {
                    //CReazione dell'oggetto SQLTable
                    if (DESTINATION_TABLE == null)
                    {
                        if (MyConn.State == ConnectionState.Open)
                            MyConn.Close();
                        if (MyConn.State == ConnectionState.Closed)
                            MyConn.Open();
                        DESTINATION_TABLE = simplex_ORM.SQLSrv.SQLTable.createSQLTable(MyConn, MyDestinationTableName);
                        if (MyConn.State == ConnectionState.Open)
                            MyConn.Close();
                    }
                    // Valorizzazione della chiave
                    int _idx = 0;
                    foreach (String _dkey in ((GridView)e.CommandSource).DataKeyNames)
                    {
                        _idx = getGridViewColumnIndex(_dkey, ((GridView)e.CommandSource));
                        if (_idx == -1)
                        {
                            simplex_ORM.Spx_ORMMessage ORMM = new simplex_ORM.Spx_ORMMessage("<strong>Le chiavi della tabella devono coincidere con le chiavi della GridView.</strong>.");
                            MyErrors.Add(ORMM);
                            if (MyApp != null && MyApp.CurrentUser != null && MyConn != null & Session.SessionID != null)
                                ORMM.traceLog(MyConn, MyApp.CurrentUser.UID.ToString(), Session.SessionID);
                            showErrors();
                            return;
                        }
                        try
                        {
                            DESTINATION_TABLE.setValue(_dkey, ((GridView)e.CommandSource).Rows[_row].Cells[_idx + 1].Text);
                        }
                        catch (simplex_ORM.Spx_ORMMessage ORMM)
                        {
                            MyErrors.Add(ORMM);
                            if (MyApp != null && MyApp.CurrentUser != null && MyConn != null & Session.SessionID != null)
                                ORMM.traceLog(MyConn, MyApp.CurrentUser.UID.ToString(), Session.SessionID);
                        }
                    }
                    // Gestione errori
                    if (MyErrors.Count > 0)
                    {
                        showErrors();
                        return;
                    }
                    //Caricamento dell'oggetto
                    try
                    {
                        DESTINATION_TABLE.load();
                    }
                    catch (simplex_ORM.Spx_ORMMessage ORMM)
                    {
                        MyErrors.Add(ORMM);
                        if (MyApp != null && MyApp.CurrentUser != null && MyConn != null & Session.SessionID != null)
                            ORMM.traceLog(MyConn, MyApp.CurrentUser.UID.ToString(), Session.SessionID);
                        showErrors();
                        return;
                    }
                    //Traccio nel log
                    writeMessageToLog(MyConn, "Caricato oggetto " + DESTINATION_TABLE.ToString(), MySbc.ToString(), MyApp.CurrentUser.UID.ToString(), Session.SessionID);
                    //Redirezione
                    Session["MYCALLERNAME"] = MyAspxFileName;
                    Session[MyName] = DESTINATION_TABLE;
                    Response.Redirect(MyDestinationPagePath);
                }
            }
            #endregion

            #region [OPERAZIONI ALTERNATIVE]
            if (MyDestinationPagePath == null || MyDestinationTableName == null)
            {
                #region [ADD YOUR CODE HERE]
                #endregion
            }
            #endregion
        }



        /// <summary>
        /// <p>Caricamento della maschera.</p>
        /// <ul>
        /// <li>Invoca preloadEnvironment()</li>
        /// <li>esegue la query di popolamento</li>
        /// </ul>
        /// <pre>
        /// @MdN
        /// ----
        /// Crtd: 30/01/2015
        /// </pre>
        /// </summary>
        /// <param name="sender">oggetto che ha invocato l'evento</param>
        /// <param name="e">parametri</param>
        protected override void Page_Load(object sender, EventArgs e)
        {
            //////NOTA: Le chiamate nel caricamento dello stato sono, nell'ordine:
            ////// Page_Load()-->preloadEnvironment()-->base.PreloadEnvironment().

            #region INIZIALIZZAZIONI: CHANGE HERE
                this.R_Cerca.Visible = false;
                this.R_Nuovo.Visible = false;
            #endregion

            #region NON MODIFICARE
                if (MyRicerca == null)
                    MyRicerca = new MySearchHandler(cerca);

                #region GESTIONE DINAMICA DELLA FORM
                //Caricamento del file
                if (MyControlsXmlFilePath != null && MyControlsXmlFilePath.Length > 0)
                {
                    // 
                    //if (DESTINATION_TABLE == null && MyTableName == null)
                    if (R_Dynamic.Controls.Count > 0)
                        drawDynamicForm(MyControlsXmlFilePath, 1);
                    // abilita il tasto di ricerca e di nuova ricerca
                    this.R_Cerca.Visible = true;
                    this.R_Nuovo.Visible = true;
                }
                #endregion

                #region GESTIONE DINAMICA DELLA GRIGLIA
                // disabilito il comando di linea
                R_Lista.Columns[0].Visible = MyButtonVisible;
                //R_Lista.DataSource = (System.Web.UI.WebControls.SqlDataSource)SqlDataSource_MYSOURCE; //<deleted @MdN: 19/11/2015 -- DataSource specificato staticamente>  
                R_Lista.AutoGenerateColumns = true;

                if (IsPostBack == false)
                    MySql = null;

                if (MySql != null)
                    this.SqlDataSource_MYSOURCE.SelectCommand = MySql;
                else
                {
                    this.SqlDataSource_MYSOURCE.SelectCommand = _selectListaBase;
                    if (MyFilter != null)
                        this.SqlDataSource_MYSOURCE.SelectCommand = this.SqlDataSource_MYSOURCE.SelectCommand + " WHERE " + MyFilter;
                    if (_orderListBase != null)
                        this.SqlDataSource_MYSOURCE.SelectCommand = this.SqlDataSource_MYSOURCE.SelectCommand + " ORDER BY " + _orderListBase;
                    //<added @MdN: 25/11/2015 - da riportare anche in GenericQueryListSpxForm>
                    ViewState["MYSQL"] = this.SqlDataSource_MYSOURCE.SelectCommand;
                    //</added @MdN: 25/11/2015 - da riportare anche in GenericQueryListSpxForm>
                }
                R_Lista.DataBind();

                #endregion


                #region [ADD YOUR CODE HERE]
                #endregion

            #endregion

                if (IsPostBack == false)
                    writeMessageToLog(MyConn, "Form_Load", MySbc.ToString(), MyApp.CurrentUser.UID.ToString(), Session.SessionID);
        }


        #endregion

        #region *** METODI PROTETTI DA NON TOCCARE ASSOLUTAMENTE ***

        /// <summary>
        /// <p>
        /// Disegna dinamicamente il contenuto della maschera.
        /// </p>
        /// <p>
        /// Le informazioni per il disegno della maschera si trovano in un file xml il cui percorso (path)
        /// viene passato come parametro.
        ///     <br>
        ///     Se il file xml non esiste, il metodo lo crea. Infatti invoca prima il metodo createXMLConfigurationFile(String p_Name) e poi
        ///     il metodo writeXMLConfigurationFile(Spx_XmlConfigFile p_config).
        ///     </br>
        ///     <br>
        ///     Se si modifica il file così generato, è possibile personalizzare l'aspetto della maschera.
        ///     </br>
        /// </p>
        /// <p>
        /// I controlli vengono distribuiti lungo una griglia (ovvero una 'table') ed incolonnati secondo il
        /// l'altro parametro specificato.
        /// </p>
        /// </summary>
        /// <param name="p_NomeFile">Nome del file di configurazione xml.</param>
        /// <param name="p_numcols">Numero di colonne lungo cui 'incolonnare' i controlli.</param>
        protected virtual void drawDynamicForm(String p_NomeFile, int p_numcols)
        {
            System.Web.UI.WebControls.PlaceHolder _cph = this.R_Dynamic; ;
            //_cph = (System.Web.UI.WebControls.PlaceHolder)FindControl("R_Dynamic");
            if (_cph != null)
            {
                // Si apre il file che ha lo stesso nome tella tabella da mostrare.
                this.designDynamicForm(MapPath(p_NomeFile), _cph, 0);
            }
        }

        /// <summary>
        /// Funzione di ricerca specifica per la pagina dei logs
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 21/11/2015
        /// </pre>
        /// </summary>
        protected void cercaLogs()
        {
            char[] _sep = { ' ' };
            MySql = _selectListaBase;
            MyOrder = _orderListBase;
            MyFilter = (String)ViewState["MYFILTER"];
            if (MyFilter == null)
                MyFilter = _filterListaBase;
            else
                if (MyFilter.Length == 0)
                    MyFilter = _filterListaBase;


            if (MyControlsXmlFilePath != null && MyControlsXmlFilePath.Length > 0)
            {
                foreach (System.Web.UI.Control _c in R_Dynamic.Controls)
                {

                    if (_c.GetType().Name.ToLower().CompareTo("textbox") == 0)
                    {
                        String[] _tmp = null;
                        String _str = null;
                        //Sottocaso data_da
                        if (_c.ID.ToLower().CompareTo("data_da") == 0)
                        {
                            _str = ((System.Web.UI.WebControls.TextBox)_c).Text;
                            if (_str != null && simplex_ORM.Column.isDateTime(_str, "DD/MM/YYYY") == true)
                                if (MyFilter != null && MyFilter.Length > 0)
                                    MyFilter = MyFilter + " AND Data_sys>=CONVERT(DATETIME, '" + simplex_ORM.Column.DateToSQLDateString(simplex_ORM.Column.parseDateTimeString(_str, "DD/MM/YYYY")) + "', 120)";
                                else
                                    MyFilter = "Data_sys>=CONVERT(DATETIME, '" + simplex_ORM.Column.DateToSQLDateString(simplex_ORM.Column.parseDateTimeString(_str, "DD/MM/YYYY")) + "', 120)";
                            _tmp = null;
                            continue;
                        }
                        //Sottocaso data_a
                        if (_c.ID.ToLower().CompareTo("data_a") == 0)
                        {
                            _str = ((System.Web.UI.WebControls.TextBox)_c).Text;
                            if (_str != null && simplex_ORM.Column.isDateTime(_str, "DD/MM/YYYY") == true)
                                if (MyFilter != null && MyFilter.Length > 0)
                                    MyFilter = MyFilter + " AND Data_sys<=CONVERT(DATETIME, '" + simplex_ORM.Column.DateToSQLDateString(simplex_ORM.Column.parseDateTimeString(_str, "DD/MM/YYYY")) + "', 120)";
                                else
                                    MyFilter = "Data_sys<=CONVERT(DATETIME, '" + simplex_ORM.Column.DateToSQLDateString(simplex_ORM.Column.parseDateTimeString(_str, "DD/MM/YYYY")) + "', 120)";
                            _str = null;
                            continue;
                        }

                        if (((System.Web.UI.WebControls.TextBox)_c).Text != null && ((System.Web.UI.WebControls.TextBox)_c).Text.Length > 0)
                        {
                            _tmp = ((System.Web.UI.WebControls.TextBox)_c).Text.Split(_sep);
                            if (_tmp != null)
                                foreach (String _s in _tmp)
                                {
                                    if (MyFilter != null && MyFilter.Length > 0)
                                        MyFilter = MyFilter + " AND " + _c.ID + " = '" + _s.Replace("'", "''") + "'";
                                    else
                                        MyFilter = _c.ID + " LIKE '%" + _s.Replace("'", "''") + "%'";
                                }
                        }
                    }

                    if (_c.GetType().Name.ToLower().CompareTo("dropdownlist") == 0)
                    {
                        if (((System.Web.UI.WebControls.DropDownList)_c).Text != null && ((System.Web.UI.WebControls.DropDownList)_c).Text.Length > 0)
                        {
                            if (MyFilter != null && MyFilter.Length > 0)
                                MyFilter = MyFilter + " AND " + _c.ID + " = '" + ((System.Web.UI.WebControls.DropDownList)_c).Text + "'";
                            else
                                MyFilter = _c.ID + " = '" + ((System.Web.UI.WebControls.DropDownList)_c).Text + "'";
                        }
                    }

                    if (_c.GetType().Name.ToLower().CompareTo("checkbox") == 0)
                    {
                        if (((System.Web.UI.WebControls.CheckBox)_c).Checked == true)
                            if (MyFilter != null && MyFilter.Length > 0)
                                MyFilter = MyFilter + " AND " + _c.ID + " = 1";
                            else
                                MyFilter = MyFilter + _c.ID + " = 1";
                    }

                }
                // filtraggio 
                if (MyFilter != null && MyFilter.Length > 0)
                    SqlDataSource_MYSOURCE.SelectCommand = MySql + " WHERE " + MyFilter;
                else
                    SqlDataSource_MYSOURCE.SelectCommand = MySql;

                if (MyOrder != null)
                    SqlDataSource_MYSOURCE.SelectCommand = SqlDataSource_MYSOURCE.SelectCommand + " ORDER BY " + MyOrder;

                R_Lista.DataBind();
                ViewState.Add("MYSQL", SqlDataSource_MYSOURCE.SelectCommand);
            }

        }
        #endregion

        #region METODI PROTETTI

        /////// <summary>
        /////// Restituisce l'indice in base 0 della colonna il cui nome è specificato come parametro,  
        /////// -1 se la colonna non esiste.
        /////// <pre>
        /////// ----
        /////// @MdN
        /////// Crtd: 21/11/2015
        /////// </pre>
        /////// </summary>
        /////// <param name="p_Column">Nome della colonne</param>
        /////// <returns>Indice in base 0 ovvero -1 nel caso in cui la colonna non sia presente nella GrtidView.</returns>
        ////protected int getGridViewColumnIndex(String p_Column, GridView p_Lista)
        ////{
        ////    String _str = null;
            
        ////    if (p_Column == null)
        ////        return -1;

        ////    if(p_Lista==null)
        ////        return -1;

        ////    if(p_Lista.DataKeyNames==null || p_Lista.DataKeyNames.Length==0)
        ////        return -1;

        ////    int _t=0;
        ////    foreach (String _tmp in p_Lista.DataKeyNames)
        ////    {
        ////        if (_tmp.ToLower().CompareTo(p_Column.ToLower()) == 0)
        ////            return _t;
        ////        _t++;
        ////    }
        ////    return -1;
        ////}

        /// <summary>
        /// <p>
        /// Imposta gli attributi necessari al funzionamento.
        /// Si attende in Session["APPLICATION"] l'oggetto GeneralApplication corrente.
        /// Se non è presente, lancia un eccezione del tipo generalApplication TracedMessage.
        /// </p>
        /// <p>
        /// Imposta in sessione l'ID dell'oggetto selezionato. La variabile di sessione ha come 
        /// stringa identificatrice il nome della tabella di riferimento dell'oggetto tutto in maiuscolo.
        /// </p>
        /// <p>Esempio: </p>
        /// <p>
        /// se la tabella è 'utente' il nome della variabile di sessione è 'UTENTE'.
        /// </p>
        /// 
        /// <p>Valorizza i seguenti attributi derivati da GenericSpxForm:
        /// </p>
        /// <ul>
        /// <li>MyApp               :   APPLICAZIONE CORRENTE;</li>
        /// <li>MyConn              :   CONNESSIONE DI DEFAULT;</li>
        /// <li>MyMainMenu          :   CONTROLLO MENU SINISTRO;</li>
        /// <li>MySbc               :   CONTROLLO BREADCRUMBS;</li>
        /// <li>MyAspxFileName      :   NOME DELLA PAGINA WEB/WEB FORM.</li>
        /// </ul>
        /// <p>
        /// Valorizza i suoi attributi caratteristici che sono:
        /// </p><p>
        /// <ul>
        /// <li>MyBackURL           :   URL DELLA PAGINA CHIAMANTE.</li>
        /// </ul>
        /// </p>
        /// <remarks>
        /// ATTENZIONE: Le assegnazioni alle variabili interne
        /// _filterListaBase e _orderListaBase NON DEVONO contenere le istruzioni SQL WHERE e ORDER BY.
        /// </remarks>
        /// <pre>
        /// ----
        /// </pre>
        /// </summary>
        protected override void preloadEnvironment()
        {
            base.preloadEnvironment();


            #region TO CHANGE: ASSEGNAZIONI
            /// ATTENZIONE: Le assegnazioni alle variabili interne
            /// _filterListaBase e _orderListaBase NON DEVONO contenere le istruzioni SQL WHERE e ORDER BY.

            
            MyButtonVisible = false;                                                    //<-- CHANGE HERE


            if (ViewState["MYSQL"] != null)
                MySql = (String)ViewState["MYSQL"];
            else
                MySql = _selectListaBase; //<-- CHANGE HERE

            if (ViewState["MYFILTER"] != null)
                MyFilter = (String)ViewState["MYFILTER"];
            else
                MyFilter = null; //<-- CHANGE HERE

            // Nome e webcrumbs
            MyName = this.Title; //"My Name";                                               //<-- CHANGE HERE
            MyAspxFileName = "~/MY DIRECTORY/" + MyAspxFileName;                            //<-- CHANGE HERE
            AddToSimplexBreadCrumbs();
            #endregion

            ////#region GESTIONE DELLA REDIREZIONE VERSO ALTRA MASCHERA

            ////if (MyDestinationPagePath != null && MyDestinationTableName != null && MyButtonVisible==true)
            ////{
            ////    String[] _keys=null;
            ////    if (MyConn.State == ConnectionState.Open)
            ////        MyConn.Close();
            ////    if (MyConn.State == ConnectionState.Closed)
            ////        MyConn.Open();
            ////    DESTINATION_TABLE = simplex_ORM.SQLSrv.SQLTable.createSQLTable(MyConn, MyDestinationTableName);
            ////    if (MyConn.State == ConnectionState.Open)
            ////        MyConn.Close();

            ////    _keys = new String[DESTINATION_TABLE.PrimaryKeyColumns.Count];
            ////    int _t = 0;
            ////    foreach (String _str in DESTINATION_TABLE.PrimaryKeyColumns)
            ////        _keys[_t++] = _str;
                                    
            ////    //Imposto la proprietà della GridView
            ////    try
            ////    {
            ////        if(R_Lista.DataKeyNames.Length==0)
            ////            R_Lista.DataKeyNames = _keys;
            ////    }
            ////    catch (Exception E)
            ////    {
            ////        simplex_ORM.Spx_ORMMessage ORMM = new simplex_ORM.Spx_ORMMessage(E);
            ////        ORMM.Message = "<strong>Le chiavi della tabella devono coincidere con le chiavi della GridView.</strong>. " + ORMM.Message;
            ////        MyErrors.Add(ORMM);
            ////        ViewState["MYERRORS"] = MyErrors;
            ////    }

            ////    //Non se ne fa di nulla
            ////    if (R_Lista.DataKeyNames == null || R_Lista.DataKeyNames.Length==0)
            ////    {
            ////        MyDestinationPagePath = null;
            ////        MyDestinationTableName = null;
            ////        MyButtonVisible = false;
            ////    }
            ////}

            ////#endregion


            #region [ADD YOUR CODE HERE]
            #endregion

        }

        /// <summary>
        /// Funzione di ricerca di default
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 07/07/2016
        /// </pre>
        /// </summary>
        protected override void cerca()
        {
            if(this.R_Dynamic!=null && R_Lista != null && SqlDataSource_MYSOURCE != null)
                base.cerca(R_Dynamic, SqlDataSource_MYSOURCE, R_Lista);
        }

        /// <summary>
        /// Cancella la form.
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 07/07/2016
        /// </pre>
        /// </summary>
        protected override void cancella()
        {
            if (this.R_Dynamic != null && R_Lista != null && SqlDataSource_MYSOURCE != null)
                base.cancella(R_Dynamic, SqlDataSource_MYSOURCE, R_Lista);
        }
        #endregion
    }
}
