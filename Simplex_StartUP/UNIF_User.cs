#define _dev

using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using GeneralApplication;

namespace SIMPLEX_STARTUP
{
    /// <summary>
    /// Rappresenta un utente dell'applicazione SIMPLEX_STARTUP ed implementa l'interfaccia
    /// GeneralApplication.IUser.<br></br>
    /// In caso si disponga del servizion di autenticazione in Microsoft Active Directory:<br></br>
    /// <li>
    /// <ol>Eliminare la definizione della variabile del compilatore _dev;</ol>
    /// <ol>Aggiungere il riferimento al servizio WEB di autenticazione (Autenticatore_AD) e chiamarlo <%NOME_PROGETTO%>_AutAD;</ol>
    /// <ol>Modificare opportunamente gli 'End Point' sul file web.config.</ol>
    /// </li>
    /// ----
    /// @MdN
    /// Crtd: 08/12/2014 - Test Version: limitazioni ai fini di test e dello sviluppo.
    /// Mdfd: 24-25/05/2016 - Autenticazione utenti interni (checkInternalUser), hash MD5 (getHash_MD5).
    /// </summary>
    public class UNIF_User:GeneralApplication.IUser
    {
        #region ATTRIBUTI
        /// <summary>
        /// Nome di login dell'utente.
        /// </summary>
        protected String myLogName = null;
        /// <summary>
        /// Password. Dopo l'autenticazione deve essere distrutta!!!
        /// </summary>
        protected String myTempPwd = null;
        /// <summary>
        /// Flag che ci dice se l'utente è già autenticato o meno.
        /// Un utente già autenticato non può autenticarsi una seconda volta perchè la password
        /// viene distrutta.
        /// </summary>
        protected Boolean myAutenticated = false;
        /// <summary>
        /// Codice identificativo numerico (ID) dell'utente nel database SIMPLEX_STARTUP
        /// </summary>
        protected Int32 myUserID = 0;
        /// <summary>
        /// Codice identificativo numerico (ID) del gruppo di appartenenza dell'utente nel database SIMPLEX_STARTUP.
        /// </summary>
        /// <remarks>DEPRECATED / SCONSIGLIATO L'USO. 
        /// Vedi MyGroupList.</remarks>
        protected Int32 myGrID = 0;
        /// <summary>
        /// Oggetto SQLTable associato
        /// </summary>
        protected simplex_ORM.SQLSrv.SQLTable MyUserSQLTable = null;
        /// <summary>
        /// Lista dei codici dei gruppi di appartenenza
        /// </summary>
        protected System.Collections.ArrayList myGroupList = null;
        #endregion

        #region PROPERTIES
        /// <summary>
        /// Imposta o ottiene (RW) lo username
        /// </summary>
        public String LogName
        {
            get
            {
                return myLogName;
            }
            set
            {
                myLogName = value;
            }
        }
        /// <summary>
        /// Imposta o ottiene (RW) la password.
        /// </summary>
        public String Password
        {
            get
            {
                return myTempPwd;
            }
            set
            {
                myTempPwd = value;
            }
        }

        /// <summary>
        /// Ottiene (RO) il codice identificativo del gruppo di appartenenza.
        /// </summary>
        public long GroupID
        {
            get
            {
                return myGrID;
            }
        }

        /// <summary>
        /// Ottiene (RO) il codice identificativo numerico dell'utente.
        /// </summary>
        public long UID
        {
            get
            {
                return myUserID;
            }
        }

        /// <summary>
        /// Ottiene (RO) il nome dell'utente
        /// </summary>
        public String Nome
        {
            get
            {
                if (MyUserSQLTable != null)
                    return MyUserSQLTable.getValue("Nome");
                else
                    return null;
            }
        }

        /// <summary>
        /// Ottiene (RO) il Cognome dell'utente
        /// </summary>
        public String Cognome
        {
            get
            {
                if (MyUserSQLTable != null)
                    return MyUserSQLTable.getValue("Cognome");
                else
                    return null;
            }
        }

        /// <summary>
        /// <p>
        /// EMail dell'utente (RO).
        /// </p>
        /// </summary>
        public String EMail
        {
            get
            {
                return MyUserSQLTable.getValue("EMail");
            }
        }

        #endregion

        #region METODI PUBBLICI
        /// <summary>
        /// Verifica se un utente viene gestito internamente dall'applicazione.<br></br>
        /// Attenzione:<br></br>
        /// Questo metodo dice solo se l'utente è un utente dell'applicativo ed è gestito internamente,
        /// cioè ha le credenziali memorizzate nel database SIMPLEX_STARTUP e non in un serizio di directory esterno.<br></br>
        /// Dunque se il metodo restituisce false può significare che l'utente non è riconosciuto o che l'utente
        /// NON è gestito!
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 24/05/2016
        /// </pre>
        /// </summary>
        /// <param name="p_reset">password interna (not null).</param>
        /// <param name="p_conn">connessione con database SIMPLEX_STARTUP (not null).</param>
        /// <returns>TRUE se l'utente è interno ed è autenticato, FALSE se l'utente non è autenticato o non è un utente interno.</returns>
        /// <exception cref="GeneralApplication.TracedMessage">Nel caso di password da modificare, il testo del messaggio deve essere "CHANGE_PWD". In caso di password non specificata il testo del messaggio è "NO_PWD".
        /// In caso di connessione non specificata il testo è "NO_CONNECTION".</exception>
        /// <exception cref="GeneralApplication.TracedMessage">Nel caso di errore di esecuzione dell'interrogazione del database, il testo del messaggio è "INTERNAL_ERROR" e si deve consultare la proprietà InnerException.</exception>
        public Boolean checkInternalUser(String p_pwd, System.Data.Odbc.OdbcConnection p_conn)
        {
            GeneralApplication.TracedMessage _tm = null;
            String _sqlQry = null;
            String _test = null;
            System.Security.Cryptography.MD5 _md5 = System.Security.Cryptography.MD5.Create();

            _test = getHash_MD5(p_pwd);

            #region CHANGE HERE IF NECESSARY
            // Query di consultazione degli utenti locali o interni. Cambiare in caso di necessità.
            // DELETE
            //            _sqlQry = @"SELECT * FROM dbo.ADMN_utente AU
            //                        INNER JOIN dbo.ADMN_PASSWORD_INTERNE API ON AU.ID=API.USR_ID
            //                        WHERE AU.DATA_OSCURAMENTO IS NULL
            //                        AND AU.LOCALE=1 and AU.ID = " + this.UID.ToString() + " AND API.PWDMD5=' " + System.Text.Encoding.ASCII.GetString(_md5.ComputeHash(System.Text.Encoding.Default.GetBytes(p_pwd))) + "'";
            // DELETE
            _sqlQry = @"SELECT * FROM dbo.ADMN_utente AU
                        INNER JOIN dbo.ADMN_PASSWORD_INTERNE API ON AU.ID=API.USR_ID
                        WHERE AU.DATA_OSCURAMENTO IS NULL
                        AND AU.LOCALE=1 and AU.ID = " + this.UID.ToString() + " AND API.PWDMD5='" + getHash_MD5(p_pwd).ToUpper() + "'";            
            #endregion

            System.Data.Odbc.OdbcCommand _cmd = null;

            // * Controllo
            if (p_pwd == null)
                throw new GeneralApplication.TracedMessage("NO_PWD", "UNIF_User","UNIF_User.checkInternalUser(String, OdbcConnection): * Controllo");
            // * Controllo
            if (p_conn == null)
                throw new GeneralApplication.TracedMessage("NO_CONNECTION", "UNIF_User", "UNIF_User.checkInternalUser(String, OdbcConnection): * Controllo");

            if (p_conn.State == ConnectionState.Closed)
                p_conn.Open();

            // Esecuzione della query
            try
            {
                _cmd = new System.Data.Odbc.OdbcCommand(_sqlQry, p_conn);
            }
            catch(Exception E)
            {
                _tm = new TracedMessage("INTERNAL_ERROR", "UNIF_User","UNIF_User.checkInternalUser(String, OdbcConnection): Esecuzione della query.",E);
                if (p_conn.State == ConnectionState.Open)
                    p_conn.Close();
                throw _tm;
            }
            
            try
            {
                using (System.Data.Odbc.OdbcDataReader _dr = _cmd.ExecuteReader())
                {
                    if(_dr.HasRows==false)                    
                        return false;               // NESSUN UTENTE INTERNO 
                    if(_dr.Read()==true)
                    {
                        if(_dr.GetInt32(_dr.GetOrdinal("FLAG_RESET"))==1)
                            _tm = new TracedMessage("CHANGE_PWD", "UNIF_User", "UNIF_User.checkInternalUser(String, OdbcConnection) ");
                    } else
                    {
                        return false;
                    }
                }
            }             
            catch(Exception E)
            {
                _tm = new TracedMessage("INTERNAL_ERROR", "UNIF_User","UNIF_User.checkInternalUser(String, OdbcConnection): Esecuzione della query.",E);
            }

            // Operazioni di chiusura
            if (_tm != null)
            {
                if (p_conn.State == ConnectionState.Open)
                    p_conn.Close();
                throw _tm;
            }
            if (p_conn.State == ConnectionState.Open)
                p_conn.Close();
            return true;
        }

        /// <summary>
        /// <p>
        /// Verifica se l'utente è un utente di dominio.
        /// Invoca un servizio WCF i cui endpoint si trovano nel file di configurazione web.config.
        /// </p>
        /// 
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 29/07/2014; 26/06/2015 (implementazione)
        /// </pre>
        /// </summary>
        /// <returns>Tre se l'utente è di dominio, False altrimenti.</returns>
        public Boolean checkADUser()
        {
#if _dev
            return false;
#else
            /*
            Questa parte DEVE essere attivata solo se l'autenticazione dell'utente viene effettuata tramite 
            il servizio di directory "Microsoft Active Directory".
            */
            SIMPLEX_STARTUP_AutAD.Service1Client _client = new SIMPLEX_STARTUP_AutAD.Service1Client();
            if (Password == null || Password.Length == 0)
                return false;
            if (LogName == null || LogName.Length == 0)
                return false;
            if (_client.Autentica(this.LogName, this.Password).ToLower().CompareTo("autenticato") == 0)
                return true;
            else
                return false;
            
            ////////UNIF_Personale_AutAD.Service1Client _client = new UNIF_Personale_AutAD.Service1Client();
            ////////if (Password == null || Password.Length == 0)
            ////////    return false;
            ////////if (LogName == null || LogName.Length == 0)
            ////////    return false;
            ////////if (_client.Autentica(this.LogName, this.Password).ToLower().CompareTo("autenticato") == 0)
            ////////    return true;
            ////////else
            ////////    return false;
#endif
        }

        /// <summary>
        /// Carica l'oggetto di classe User a partire dal nome di login.
        /// <pre>
        /// ----
        /// @MdN 
        /// Crtd: 29/07/2014
        /// Mdfd: 25/05/2016 16:00
        /// </pre>
        /// 
        /// </summary>
        /// <param name="p_logname">stringa di login</param>
        /// <param name="p_conn">connessione attiva</param>
        /// <returns>L'ID dell'utente o -1 in casi di anomalia (es. logname nulla) ovvero -2 in caso di inesistenza dell'utente da caricare.</returns>
        public long loadByLogName(String p_logname, System.Data.Odbc.OdbcConnection p_conn)
        {
            System.Data.Odbc.OdbcCommand _cmd = null;
            System.Data.Odbc.OdbcDataReader _dr = null;
            String _sql = null;
            Int32 _UID = 0;

            myUserID = 0;
            
            if (p_conn == null)
                return -1;

            #region AUTENTICAZIONE

            #endregion

            _sql = "select UG.GRUPPO, UT.ID FROM ADMN_utentegruppo UG INNER JOIN ADMN_utente UT ON UT.ID=UG.UTENTE WHERE UT.DATA_Oscuramento IS NULL AND UT.username='" + p_logname + "'";
            try
            {
                if (p_conn.State == ConnectionState.Closed)
                    p_conn.Open();
                _cmd = p_conn.CreateCommand();
                _cmd.CommandText = _sql;
                _dr = _cmd.ExecuteReader();
                if (_dr.HasRows == true)
                {
                    myGroupList = new System.Collections.ArrayList();
                    while (_dr.Read())
                    {
                        myGroupList.Add(_dr.GetInt32(0));
                        if (myUserID == 0)
                            myUserID = _dr.GetInt32(1);
                    }
                }
                else
                  myUserID = -2;
                 _cmd.Dispose();
                 _dr.Close();
                 p_conn.Close();
            }
            catch (Exception E)
            {
                _cmd.Dispose();
                _dr.Close();
                p_conn.Close();
                simplex_ORM.Spx_ORMMessage ORMM = new simplex_ORM.Spx_ORMMessage("Errore nell'esecuzione della query: " + _sql, 11 ,"UNIF_User", E);
                ORMM.traceLog(p_conn, myUserID.ToString(), "yet unknown"); //--<--TODO:
                //throw ORMM;
                return -1;
            }

            /* caricamento dei dati sull'utente */
            if (myUserID>= 0) // @MdN Corretto in data 25/05/2016 16:00
            {
                if (p_conn.State == ConnectionState.Closed)
                    p_conn.Open();
                try
                {
                    MyUserSQLTable = simplex_ORM.SQLSrv.SQLTable.createSQLTable(p_conn, "ADMN_utente");
                    MyUserSQLTable.setValue("ID", myUserID.ToString());
                    MyUserSQLTable.load();

                    //@MdN 24/06/2015: Caricamento INFOS
                    myLogName = MyUserSQLTable.getValue("Username");
                    myUserID = int.Parse(MyUserSQLTable.getValue("ID"));
                    myAutenticated = false;
                    
                }
                catch (simplex_ORM.Spx_ORMMessage ORMM)
                {
                    ORMM.traceLog(p_conn, myUserID.ToString(), myUserID.ToString() + " - " + DateTime.Now.ToString());
                }
            }

            _cmd.Dispose();
            _dr.Close();
            p_conn.Close();
            return myUserID; // NOP FOR NOW
        }

        /// <summary>
        /// <p>
        /// Restituisce un array contenente la lista degli identificativi dei gruppi 
        /// a cui l'utente appartiene.
        /// </p>
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 17/03/2015
        /// </pre>
        /// </summary>
        /// <returns>Un array contenente gli identificativi (32-bit) dei gruppi di appartenenza</returns>
        public Int32[] getGroups()
        {
            Int32[] _TORET;
            if (myGroupList == null)
                return null;
            if (myGroupList.Count == 0)
                return null;
            _TORET = (Int32[])myGroupList.ToArray(Type.GetType("System.Int32"));
            return _TORET;
        }

        /// <summary>
        /// Verifica se l'utente appartiene a gruppi abilitati all'accesso ad una Funzione Applicativa.
        /// <p>
        /// Regola:
        /// <ul>
        /// <li>Se l'utente non appartiene a gruppi, restituisce <b>FALSE</b></li>
        /// <li>Se l'applicazione non è riservata a gruppi, restituisce <b>TRUE</b></li>
        /// <li>Se l'utente appartiene a gruppi di cui almeno uno è tra quelli a cui è riservata l'aplicazione, restituisce <b>TRUE</b></li>
        /// <li>Se nessuna delle precedenti condizioni è rispettata, restituisce <b>FALSE</b>.</li>
        /// </ul>
        /// </p>
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 25/06/2015
        /// </pre>
        /// </summary>
        /// <param name="p_ApplCode">Chiave identificativa della funzione applicativa, come da web.config.</param>
        /// <returns>TRUE se l'utente può accedere all'applicazione specificata, FALSE altrimenti.</returns>        
        public Boolean verifyAccess(String p_ApplName)
        {
            int _AppCode;
            String _tmp;
            System.Data.Odbc.OdbcCommand _CMD = null;

            // Controlli 
            if (p_ApplName == null)
                return false;          
            // Prendo il codice dal file di configurazione
            _tmp = ConfigurationSettings.AppSettings[p_ApplName];
            if (this.MyUserSQLTable.CurrentConnection != null)
            {
                if(this.MyUserSQLTable.CurrentConnection.State== ConnectionState.Closed)
                    this.MyUserSQLTable.CurrentConnection.Open();
                _tmp="SELECT ID FROM ADMN_FunzioneApplicativa WHERE DENOMINAZIONE='" + p_ApplName + "'";
                _CMD = this.MyUserSQLTable.CurrentConnection.CreateCommand();
                _CMD.CommandText = _tmp;
                _AppCode = (int) _CMD.ExecuteScalar();
                this.MyUserSQLTable.CurrentConnection.Close();
                _CMD.Dispose();
                return verifyAccess(_AppCode);
            }
            this.MyUserSQLTable.CurrentConnection.Close();
            _CMD.Dispose();
            return false;
        }

        /// <summary>
        /// Verifica se l'utente appartiene a gruppi abilitati all'accesso ad una Funzione Applicativa.
        /// <p>
        /// Regola:
        /// <ul>
        /// <li>Se l'utente non appartiene a gruppi, restituisce <b>FALSE</b></li>
        /// <li>Se l'applicazione non è riservata a gruppi, restituisce <b>TRUE</b></li>
        /// <li>Se l'utente appartiene a gruppi di cui almeno uno è tra quelli a cui è riservata l'aplicazione, restituisce <b>TRUE</b></li>
        /// <li>Se nessuna delle precedenti condizioni è rispettata, restituisce <b>FALSE</b>.</li>
        /// </ul>
        /// </p>
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 25/06/2015
        /// </pre>
        /// </summary>
        /// <param name="p_ApplCode">Codice della funzione applicativa.</param>
        /// <returns>TRUE se l'utente può accedere all'applicazione specificata, FALSE altrimenti.</returns>
        public Boolean verifyAccess(int p_ApplCode)
        {
            UNIF_Application _app = new UNIF_Application();
            _app.loadConnections("OdbcConfig");
            _app.load(p_ApplCode);

            return _app.verifyAccess(this);  
        }

        /// <summary>
        /// Converte una stringa in un'altra che ne rappresenta l'hash secondo l'algoritmo MD5.
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 25/05/2016
        /// 
        /// </pre>
        /// </summary>
        /// <param name="p_input">Stringa da computare</param>
        /// <returns>Hash MD5</returns>
        public String getHash_MD5(String p_input)
        {
            String _toRet = null;
            byte[] _clear = null;
            byte[] _hash = null;
            System.Text.StringBuilder _sb = null;
            System.Security.Cryptography.MD5 _myMD5 = null;

            // * Controllo
            if (p_input == null)
                return null;

            // Conversione String --> byte[]
            _clear = System.Text.ASCIIEncoding.Default.GetBytes(p_input);

            // ** Controllo
            if (_clear == null)
                return null;

            _myMD5 = System.Security.Cryptography.MD5.Create();
            if (_myMD5 == null)
                return null;

            _hash = _myMD5.ComputeHash(_clear);
            if (_hash == null)
                return null;

            _sb = new System.Text.StringBuilder();
            foreach (byte _b in _hash)
            {
                _sb.Append(String.Format("{0:X}", _b));
            }

            _toRet = _sb.ToString();

            return _toRet;
        }

        /// <summary>
        /// Restituisce una stringa casuale di nove cartteri.
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 30/05/2016 11:00
        /// </pre>
        /// </summary>
        /// <returns>Stringa di caratteri casuali.</returns>
        public virtual String getTempPwd()
        {
            System.Text.StringBuilder _sb = new System.Text.StringBuilder();
            Random _rnd = new Random();
            byte _b = 0;
            byte[] _barray = new byte[3];

            for (int _t = 0; _t < 3; _t++)
            {
                // Lettera
                _barray[0] = (byte)_rnd.Next(65, 90);
                // Lettera
                _barray[1] = (byte)_rnd.Next(65, 90);
                // Numero
                _barray[2] = (byte)_rnd.Next(48, 57);
                // Accodamento
                _sb.Append(System.Text.Encoding.ASCII.GetString(_barray));
            }
            return _sb.ToString();
        }



        /// <summary>
        /// <P>
        /// Invia un messaggio di notifica precedentemente composto ad uno o più destinatari.
        /// Il mittente è l'utente corrente.
        /// </P>
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 30/05/2016 re-editing da GTEL (originariamente Crtd: 14/07/2015)
        /// </pre>
        /// </summary>
        /// <param name="p_dests">E mail del destinatario. Se molteplici, separarli con carattere ';' <b>SENZA SPAZI</b>. </param>
        /// <param name="p_host">Indirizzo del server smtp di destinazione.</param>
        /// <param name="p_Message">Corpo del messaggio.</param>
        /// <param name="p_subject">Oggetto del messaggio.</param>
        public void sendNotificationTo(String p_Message, String p_dests, String p_subject, String p_host)
        {
            String[] _addrs = null;
            char[] _seps = { ';' };
            String _sender = null;

            System.Net.Mail.SmtpClient _cl = new System.Net.Mail.SmtpClient(p_host);
            System.Net.Mail.MailMessage _mmex = new System.Net.Mail.MailMessage();

            // * Controllo
            if(p_Message==null)
                throw new simplex_ORM.Spx_ORMMessage("Nessun messaggio da inviare.");

            // * Controllo: SERVER
            if(p_host==null)
                throw new simplex_ORM.Spx_ORMMessage("Nessun server smtp a cui inviare il messaggio.");

            // * Controllo: INDIRIZZI DESTINATARI
            if(p_dests==null)
                throw new simplex_ORM.Spx_ORMMessage("Nessun destinatario a cui inviare il messaggio.");


            _addrs =  p_dests.Split(_seps);
            foreach (String _t in _addrs)
            {
                _mmex.To.Add(new System.Net.Mail.MailAddress(_t.Trim()));
            }

            // ALTRE INFORMAZIONI

            _mmex.Subject = p_subject;
            _mmex.Body = p_Message;
            if (this.MyUserSQLTable.getValue("EMail") == null)
                _sender = "noreply@governo.it";
            else
                _sender = this.MyUserSQLTable.getValue("EMail");

            _mmex.From = new System.Net.Mail.MailAddress(_sender);
            _mmex.Sender = new System.Net.Mail.MailAddress(_sender);

            // iscrivo il gestore all'evento callback in quanto la spedizione è 
            // asincrona.

            _cl.SendCompleted += new System.Net.Mail.SendCompletedEventHandler(On_SendCompleted);
            _cl.Port = 25;
            _cl.Credentials = System.Net.CredentialCache.DefaultNetworkCredentials;

            //_cl.SendAsync(_mmex, "Mail OK");
            try
            {
                _cl.Send(_mmex);
            }
            catch (Exception E)
            {
                simplex_ORM.Spx_ORMMessage _mx = new simplex_ORM.Spx_ORMMessage(E);
                _mx.Source = "UNIF_User.sendNotification():";
                throw _mx;
            }
            _mmex.Dispose();
        }
        #endregion

        #region COSTRUTTORI
        public UNIF_User()
        {
            //myGrID = 0;
            myLogName = "dbo";
            myUserID = 1;
        }

        public UNIF_User(System.Data.Odbc.OdbcConnection p_conn, String p_logName)
        {
            //myGrID = 0;
            myLogName = p_logName;
            loadByLogName(p_logName, p_conn);
        }
        #endregion

        #region METODI PRIVATI E PROTETTI

        private void On_SendCompleted(object sender, System.ComponentModel.AsyncCompletedEventArgs e)
        {
            //NOP
        }

        #endregion
    }
}
