<%@ Page Language="C#" MasterPageFile="~/MasterPCM.Master" AutoEventWireup="true" CodeBehind="GenericListSpxForm.aspx.cs" Inherits="SIMPLEX_STARTUP.GenericListSpxForm" Title="Gestione Dirigenti" EnableEventValidation="false" %>
<%@ Register Assembly="WebSimplex" Namespace="WebSimplex" TagPrefix="WS1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MenuPlaceHolder" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
 <div id="MainFormSection">
 
 <WS1:WebSimplexMessageBox ID="R_MessageBox" runat="server" 
        AlertText="Attenzione, vedi il riquadro degli errori." 
        OkAlertButtonCssStyle="AlertButton" 
        AlertImage="Immagini/Omino_Errore.png" 
        OkMessageImage="Immagini/Megafono_tondoviola.png" 
        OkMessageText="Continuare con l'operazione?"
        OkMessageButtonCssStyle="OkButton" 
        UndoMessageButtonCssStyle="UndoButton" CssOkMsgClass="OkMsg" />
 
  <table class="FRM">
  
    <tr class="FRM_ROW">
        <td class="FRM_COL1"></td>
        <td class="FRM_COL2"></td>
        <td class="FRM_COL3">
            <asp:Button ID="R_Indietro" runat="server" Text="Indietro" 
                onclick="R_Indietro_Click" />
        </td>
    </tr>
    
    <tr class="FRM_ROW">
        <td class="FRM_COL1"></td>
        <td class="FRM_COL2"></td>
        <td class="FRM_COL3"></td>
    </tr>
    
    <tr class="FRM_ROW">
        <td class="FRM_COL1"></td>
        <td class="FRM_COL2"></td>
        <td class="FRM_COL3"></td>
    </tr>
  
    <tr class="FRM">
        <td class="FRM_COL1">
            <asp:Label ID="L_Azioni" runat="server" Text="Azioni"></asp:Label>
        </td>
        <td class="FRM_COL2">
            <asp:Button ID="R_Nuovo" runat="server" Text="Cerca" />
        </td>
        <td class="FRM_COL3"></td>
    </tr>
    </table>
 </div>
  <div id="SubFormSection">
      <asp:GridView ID="R_Lista" runat="server" AllowPaging="True" 
          AllowSorting="True" onrowcommand="R_Lista_RowCommand">
          <Columns>
              <asp:ButtonField ButtonType="Button" CommandName="Edit" HeaderText="Comando" 
                  ShowHeader="True" Text="Seleziona" />
          </Columns>
      </asp:GridView>
      <asp:SqlDataSource ID="SqlDataSource_MYSOURCE" runat="server" 
      ConnectionString="<%$ ConnectionStrings:SIMPLEX_STARTUP_DBConnectionStringForForms %>" 
      SelectCommand="SELECT * FROM [MYTABLE]">
      </asp:SqlDataSource>
 </div>
</asp:Content>
