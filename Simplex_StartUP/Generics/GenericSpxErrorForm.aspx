<%@ Page Language="C#" MasterPageFile="~/MasterPCM.Master" AutoEventWireup="true" CodeBehind="GenericSpxErrorForm.aspx.cs" Inherits="SIMPLEX_STARTUP.GenericSpxErrorForm" Title="Errore" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MenuPlaceHolder" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<div id="MainFormSection">
        <asp:Panel ID="Panel_MessageBox" runat="server" CssClass="MessageBox">
            <asp:Label CssClass="MessageText" ID="L_Message" runat="server" Text="Confermare l'Operazione ?"></asp:Label><br /><br /><br />
            <asp:Button ID="R_Annulla" runat="server" Text="Torna al Desktop" 
                onclick="R_Annulla_Click" />
            &nbsp;
            &nbsp;
            &nbsp;
            <asp:Button ID="R_Conferma" runat="server" Text="Torna Indietro" 
                onclick="R_Conferma_Click" />
        </asp:Panel>
<!-- PUT YOUR CONTROLS FROM HERE -->
<!-- PUT YOUR CONTROLS TO HERE -->
</div>
</asp:Content>
