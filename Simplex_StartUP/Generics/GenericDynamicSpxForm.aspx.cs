using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using GeneralApplication;
using WebSimplex;

namespace SIMPLEX_STARTUP
{
    /// <summary>
    /// Derivata da GenericDynamicSpxForm:GenericSpxForm:SimplexForm.
    /// Ha attributi protetti necessari per il funzionamento dell'applicazione.<br></br>
    /// <ol>
    /// <li>
    /// MyControlsXmlFilePath:  Path del file XML contenente la definizione dei controlli da inserire dinamicamente.
    /// </li>
    /// <li>
    /// MyTableName:    Nome della tabella di riferimento della maschera dinamica.
    /// </li>
    /// </ol><br></br>
    /// 
    /// Esegue Override dei seguenti metodi:
    /// <remarks>TODO: DA PORTARE IN WebSimplexFormLibrary</remarks>
    /// ------
    /// @MdN 
    /// Crtd: 06/12/2014 - 1.0.0.0.
    /// Mdfd: 28/11/2015 - 1.0.0.1: 
    /// Mdfd: 20/06/2016 - Ristrutturazione e rimosse le costanti - 1.0.0.2
    /// Mdfd: 09/04/2018 - Monterotondo -Introdotto MyToUpper, Modificata R_Salva_Click() in modo che porti tutto in maiuscolo - 1.0.0.3. 
    /// Mdfd: 13/04/2018 - Aggiunto il ciclo di visualizzazione delle eventuali "sezioni" - 1.0.0.4
	/// Mdfd: 28/04/2018 - Refactoring - 1.0.0.5:
    ///                    * Il gestore R_Salva_Click() da virtual passa ad override perché¨ stato introdotto nella superclasse.
    ///                    * rimossi gli attributi MyTableName, MyToUpper
    /// Mdfd: 21/03/2021 - aggiunto showSimpleErrors() e modificato showErrors() invocando esplicitamente GenericSpxForm.showErrors() - 1.0.0.6.
    /// </summary>
    public partial class GenericDynamicSpxForm : GenericSpxForm
    {
        #region ATTRIBUTI DA DEFINIRE IN preLoadEnvironment()
        /// <summary>
        /// <p>
        /// Path del file XML contenente la definizione dei controlli da inserire dinamicamente.
        /// </p>
        /// </summary>
        protected String MyControlsXmlFilePath = null;

        #region [TO DO:] da rimuovere a partire dalla versione successiva alla v.1.0.0.5
        // </deleted @MdN: 01/05/2018>
        /// <summary>
        /// Nome della tabella di riferimento della maschera dinamica.
        /// </summary>
        //protected String MyTableName=null;
        // <deleted @MdN: 01/05/2018>

        // </deleted @MdN: 29/04/2018>
        /// <summary>
        /// Se True, converte in maiuscolo tutti i campi dell'oggetto TBL_UNDER_FORM.
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 09/04/2018 Monterotondo 
        /// </pre>
        /// </summary>
        // protected Boolean MyToUpper = false;
        // </deleted @MdN: 29/04/2018>
        #endregion

        #endregion

        #region METODI PROTETTI
        /// <summary>
        /// <p>
        /// Imposta gli attributi necessari al funzionamento.
        /// Si attende in Session["APPLICATION"] l'oggetto GeneralApplication corrente.
        /// Se non è presente, lancia un eccezione del tipo generalApplication TracedMessage.
        /// Valorizza:
        /// </p>
        /// <p>
        /// Valorizza i seguenti attributi:
        /// </p>
        /// <ul>
        /// <li>MyApp               :   APPLICAZIONE CORRENTE;</li>
        /// <li>MyConn              :   CONNESSIONE DI DEFAULT;</li>
        /// <li>MyMainMenu          :   CONTROLLO MENU SINISTRO;</li>
        /// <li>MySbc               :   CONTROLLO BREADCRUMBS;</li>
        /// <li>MyAspxFileName      :   NOME DELLA PAGINA WEB/WEB FORM.</li>
        /// <li>MyBackURL           :   URL DELLA PAGINAWEB/WEB FORM CHIAMANTE.</li>
        /// </ul>
        /// <pre>
        /// ------
        /// @MdN
        /// Crtd: 06/12/2014
        /// Mdfd: 30/01/2015 - Mancata gestione del caso in cui this.R_Dynamic=null - TODO: gestire in SimplexForm.searchControl il caso in cui il primo parametro è null
        /// </pre>
        /// </summary>
        protected override void preloadEnvironment()
        {
            base.preloadEnvironment();

            #region WRITE YOUR CODE HERE
            // WRITE YOUR CODE FROM HERE

            // WRITE YOUR CODE TO HERE
            #endregion
            
        }//fine preloadEnvironment
              

        /// <summary>
        /// <p>EN: Show the errors occurred in the last operation. At the end, clears the list of errors. 
        /// </p>
        /// <p>IT: Mostra gli errori verificatisi nell'ultima operazione. Al termine, cancella la lista degli errori.
        /// </p>
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 21/03/2021 ridenominazione di showErrors().
        /// </pre>
        /// </summary>
        protected override void showSimpleErrors()
        {
            if (MyErrors.Count > 0)
            {
                Boolean l_pari = false;
                Literal l_ErrArea = null;
                System.Text.StringBuilder l_ErrString = new System.Text.StringBuilder("<table class=\"ErrTab\">");
                l_ErrArea = (Literal)this.Master.FindControl("ListOfErrors");

                foreach (simplex_ORM.Spx_ORMMessage ORMM in MyErrors)
                {
                    if (l_pari == true)
                        l_ErrString.Append("<tr class=\"ErrRowPari\">");
                    else
                        l_ErrString.Append("<tr class=\"ErrRowDisPari\">");
                    l_pari = !l_pari;
                    l_ErrString.Append("<td class=\"ErrCodCol\">");
                    l_ErrString.Append(ORMM.MessageCode.ToString());
                    l_ErrString.Append("</td><td class=\"ErrDescCol\">");
                    l_ErrString.Append(ORMM.Message);
                    l_ErrString.Append("</td></tr>");
                }
                l_ErrString.Append("</table>");
                l_ErrArea.Text = l_ErrString.ToString();
                MyErrors.Clear(); //<-- ATTENZIONE
            }
        }//fine

        /// <summary>
        /// Mostra i messaggi a video, in testa alla pagina web, rappresentandoli in modo 
        /// diverso secondo tipologia.<br></br>
        /// Sostituisce la vecchia showErrors(), ridenominata showSimpleErrors().
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 21/03/2021 
        /// </pre>
        /// </summary>
        protected virtual void showErrors()
        {
            base.showErrors();
        }


        /// <summary>
        /// Mostra l'oggetto SQLTable che supporta la maschera.
        /// ---
        /// @MdN
        /// Crtd: 01/01/2015
        /// Mdfd: 13/04/2018 - Aggiunto il ciclo di visualizzazione delle eventuali "sezioni"
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected override void Page_SaveStateComplete(object sender, EventArgs e)
        {
            System.Web.UI.WebControls.MultiView _MV = null;
            System.Web.UI.WebControls.PlaceHolder _ph = null;
            if (TBL_UNDER_FORM != null)
            {
                this.show(TBL_UNDER_FORM, "ContentPlaceHolder1", "System.Web.UI.WebControls.ContentPlaceHolder");
                //R_Dynamic
                this.show(TBL_UNDER_FORM, "R_Dynamic", "System.Web.UI.WebControls.PlaceHolder");
                // Estendere lo show ad ogni View del MultiView
                //<added @MdN: 28/11/2015: TODO: gestire in SimplexForm.searchControl il caso in cui il primo parametro è null!!!>
                if (this.R_Dynamic != null)
                {
                    try
                    {
                        _MV = (System.Web.UI.WebControls.MultiView)searchControl(this.R_Dynamic, "R_DynamicMultiView", "System.Web.UI.WebControls.MultiView");
                    }
                    catch (Exception E)
                    {
                        _MV = null;
                    }
                }
                //<added @MdN: 28/11/2015: TODO: gestire in SimplexForm.searchControl il caso in cui il primo parametro è null!!!>
                //_MV = (System.Web.UI.WebControls.MultiView)this.FindControl("R_DynamicMultiView");
                if (_MV != null)
                {
                    foreach (System.Web.UI.WebControls.View _vw in _MV.Views)
                    {
                        this.show(TBL_UNDER_FORM, _vw.ID, "System.Web.UI.WebControls.View");
                    }
                }
            }
            /*
             Gestione dello stato della maschera
             * <added @MdN: 13/04/2018>
             */
            System.Web.UI.WebControls.ContentPlaceHolder _cph = (System.Web.UI.WebControls.ContentPlaceHolder)this.searchControl(this, "ContentPlaceHolder1", "System.Web.UI.WebControls.ContentPlaceHolder");
            //System.Web.UI.WebControls.PlaceHolder _ph = null;
            if (MyFormState != null && MyFormState.Count > 0 & _cph!=null)
            {
                foreach (System.Web.UI.Control _c in _cph.Controls)
                {
                    if ((_c.GetType()).Name.CompareTo("PlaceHolder") == 0)
                    {
                        _ph = (System.Web.UI.WebControls.PlaceHolder)_c;
                        show(MyFormState, _ph.ID, "System.Web.UI.WebControls.PlaceHolder", null);
                    }
                }
            }
            //</added @MdN: 13/04/2018>
        }

        /// <summary>
        /// <p>
        /// Disegna dinamicamente il contenuto della maschera.
        /// </p>
        /// <p>
        /// Le informazioni per il disegno della maschera si trovano in un file xml il cui percorso (path)
        /// viene passato come parametro.
        ///     <br>
        ///     Se il file xml non esiste, il metodo lo crea. Infatti invoca prima il metodo createXMLConfigurationFile(String p_Name) e poi
        ///     il metodo writeXMLConfigurationFile(Spx_XmlConfigFile p_config).
        ///     </br>
        ///     <br>
        ///     Se si modifica il file così generato, è possibile personalizzare l'aspetto della maschera.
        ///     </br>
        /// </p>
        /// <p>
        /// I controlli vengono distribuiti lungo una griglia (ovvero una 'table') ed incolonnati secondo il
        /// l'altro parametro specificato.
        /// </p>
        /// </summary>
        /// <param name="p_NomeFile">Nome del file di configurazione xml.</param>
        /// <param name="p_numcols">Numero di colonne lungo cui 'incolonnare' i controlli.</param>
        protected virtual void drawDynamicForm(String p_NomeFile, int p_numcols)
        {
            System.Web.UI.WebControls.PlaceHolder _cph = this.R_Dynamic; ;
            //_cph = (System.Web.UI.WebControls.PlaceHolder)FindControl("R_Dynamic");
            if (_cph != null)
            {
                // Si apre il file che ha lo stesso nome tella tabella da mostrare.
               this.designDynamicForm(MapPath(p_NomeFile), _cph, 0);
            }
        }

        /// <summary>
        /// <p>
        /// Disegna i controlli della maschera in modo dinamico prelevando le informazioni da un 
        /// file xml il cui path viene passato quale parametro. I controlli vengono disegnati in
        /// un placeholder specificato in uno dei parametri passati al metodo.
        /// </p>
        /// </summary>
        /// <param name="p_XML"></param>
        /// <param name="p_PH"></param>
        /// <param name="p_NumCols"></param>
        public override void  designDynamicForm(string p_XML, PlaceHolder p_PH, int p_NumCols)
        {
            // il metodo base disegna solo la sezione 'main'.
            // Adesso si devono disegnare eventuali TAB.
            try
            {
                base.designDynamicForm(p_XML, p_PH, p_NumCols);
            }
            catch (simplex_ORM.Spx_ORMMessage ORMM)
            {
                MyErrors.Add(ORMM);                
            }

            /* disegno dei tabs */
            try
            {
                designTabs(p_XML, p_PH, p_NumCols);
            }
            catch (simplex_ORM.Spx_ORMMessage ORMM)
            {
                MyErrors.Add(ORMM);
            }
        }
        /// <summary>
        /// Mostra i tabs.
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 29/04/2015
        /// Mdfd: 03/05/2015 - Gestita l'assenza degli elementi 'tab'
        /// </pre>
        /// </summary>
        /// <param name="p_XML"></param>
        /// <param name="p_PH"></param>
        /// <param name="p_NumCols"></param>
        protected virtual void designTabs(string p_XML, PlaceHolder p_PH, int p_NumCols)
        {
            /* *** MAI DISDEGNARE UN PO' DI COPIA-INCOLLA *** */
            /* Da: SimplexForm.designDynamicForm()            */

            SIMPLEX_Config.Spx_XmlElement _config = null;
            SIMPLEX_Config.Spx_XmlElement _head = null;
            System.Web.UI.WebControls.Literal _ltrl = null;
            System.Web.UI.WebControls.PlaceHolder _plh = p_PH;
            System.Web.UI.WebControls.View _vw = null;
            System.Web.UI.WebControls.MultiView _mv = null;
            WebSimplex.WebSimplexTabs _WST = null;
            System.Web.UI.WebControls.Literal _ltr = null;

            String _strNumCols = null;
            int _numCols = p_NumCols;
            int _countCols = 0;                                                             // contatore di colonne

            if (p_PH == null)
                return;
            if (p_XML == null)
            {
                _ltrl = new Literal();
                _ltrl.Text = "<b>ERRORE: file XML assente!!!</b>.";
                return;
            }
            _config = SIMPLEX_Config.Spx_XmlConfigFile.load(p_XML);
            if (_config == null)
            {
                _ltrl = new Literal();
                _ltrl.Text = "<b>ERRORE: file XML non valido!!!</b>.";
                return;
            }
            /* **** **** **** **** **** 
             * Il primo elemento deve essere di tipo 'dynamic'
             * **** **** **** **** **** */
            SIMPLEX_Config.Spx_XmlElement _elem = _config;
            if (_elem.Name.ToLower().CompareTo("dynamic") != 0)
            {
                _ltrl = new Literal();
                _ltrl.Text = "<b>ERRORE: il primo elemento del file XML NON è 'dynamic'</b>.";
                return;
            }

            /* **** **** **** **** **** 
             * Gli elementi del livello immediatamente inferiore alla radice 'dynamic'
             * rappresentano le sezioni di una maschera che sono:
             * Main               - sezione principale (solo una) 
             * Tab                - eventuali Tab
             **** **** **** **** **** */
            /* *** FINE DEL COPIA-INCOLLA *** */
            #region BLOCCHI TAB

            /* *** BLOCCHI TAB *** */
            _elem = _config;                            //riparto da 'dynamic'
            _elem = _elem.Inner;                        //scendo di un livello

            while (_elem != null)
            {
                if (_elem.Name.ToLower().CompareTo("tab") != 0)
                {
                    _elem = _elem.Next;
                    continue;
                }
                /* TROVATO !!! */
                /* **** 
                 * La prima cosa da fare in questi casi è vedere se il controllo
                 * MultiView è stato già aggiunto alla maschera, altrimenti lo si aggiunge.
                 * :-)
                 */
                if(_mv==null)
                {
                    // Aprire la sottosezione
                    _ltr = new Literal();
                    _ltr.Text = "<div id=\"tabSection\">";
                    p_PH.Controls.Add(_ltr);

                    _WST = new WebSimplex.WebSimplexTabs();
                    _WST.ID = "R_DynamicTABS";
                    _WST.Name = _WST.ID;
                    _plh.Controls.Add(_WST);

                    _mv = new MultiView();
                    _mv.ID = "R_DynamicMultiView";                    
                }
                // Ora abbiamo il MultiView ed il WebSimplexTabs

                // **** Prelevo il controllo View corrispondente all'elemento corrente **** */
                _vw =  getTab(_elem, p_NumCols);
                if (_vw != null)
                {
                    //_plh.Controls.Add(_vw);               // Aggiunta della View
                    _mv.Views.Add(_vw);                     // Aggiunta del MultiViev...
                    _WST.Add(_elem.getValue("Name"));       // e del nome
                }
                // avanzamento
                _elem = _elem.Next;
            } //fine ciclo di ricerca dei blocchi 'tab'

            // @MdN 03/05/2015
            /* *************** *
             * Se l'elemento 'tab' non è stato trovato allora il controllo MultiView
             * non è stato creato. Di conseguenza se _mw==null l'eaborazione termina.
             * *************** */
            if (_mv == null)
                return;

            // Aggiungo il MultiView ed associo al Tab
            p_PH.Controls.Add(_mv);            
            _WST.AssociatedMultiView = _mv;
            _mv.ActiveViewIndex = 0;

            // chiudere la sottosezione
            _ltr = new Literal();
            _ltr.Text = "</div>";
            p_PH.Controls.Add(_ltr);
            #endregion
        } //fine designTabs()

        /// <summary>
        /// <p>
        /// Dato un elemento XML di tipo 'tab', restituisce un controllo System.Web.UI.WebControls.View
        /// popolato dai controlli corrispondenti agli elementi contenuti nel suddetto elemento di tipo 'tab'.
        /// </p>
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 29/04/2015
        /// </pre>
        /// </summary>
        /// <param name="p_tab">elemento di tipo 'tab'</param>
        /// <param name="p_NumCols">Numero di colonne. Se 0, viene letto il valore dell'attributo 'NumCols'.</param>
        /// <returns>un controllo View ovvero null se l'elemento passato è nullo o di tipo diverso da 'tab'.</returns>
        /// 
        protected virtual System.Web.UI.WebControls.View getTab(SIMPLEX_Config.Spx_XmlElement p_tab, int p_NumCols)
        {
            SIMPLEX_Config.Spx_XmlElement _elem;
            int _numCols = 1;
            int _countCols = 0; 
            String _strNumCols = null;

            // per il calcolo della larghezza delle colonne
            // _col3 DEVE essere sempre a 0
            // _col1 DEVE sempre essere il 20% di _col2
            // _col1 + _col2 = 100/p_NumCols
            int _col1 = 0;
            int _col2 = 0;
            int _col3 = 0;
                       
            // controlli preliminari
            if(p_tab==null)
                return null;
            if (p_tab.Name.ToLower().CompareTo("tab") != 0)
                return null;
            if (p_NumCols < 0)
                return null;

            // determinazione del numero di colonne
            if (p_NumCols == 0)
            {
                _strNumCols = p_tab.getValue("NumCols");
                if (_strNumCols != null && simplex_ORM.Column.isPureInteger(_strNumCols) == true)
                {
                    _numCols = int.Parse(_strNumCols);
                    if (_numCols == 0)
                        _numCols = 1;
                }
            }
            else
            {
                _numCols = p_NumCols;
            }
            // Abbiamo il numero di colonne.
            // Determinazione della loro dimensione
            // UN PO' DI COPIA-INCOLLA NON SI RIFIUTA MAI!!!!

            /* DETERMINAZIONE DELLA LARGHEZZA DELLE COLONNE */
            int _ratio = (int)Math.Floor((decimal)(100 / _numCols));
            _col2 = (int)Math.Floor((decimal)(_ratio / 5)) * 4;
            _col1 = (int)Math.Floor((decimal)(_ratio / 5));


            //CReazione del controllo View
            View _plh = new View();
            _plh.ID = p_tab.getValue("ID");

            /* 
 * LOGICA PER INSERIRE PIU' CONTROLLI IN UNA SOLA CELLA
 * @MdN 19/09/2015
 * ------
 * Si introducono le variabili booleane _continue ed _opened
 * Il valore di _continue è determinato da un attributo dell'elemento XML corrispondente al controllo da posizionare.
 * Il valore true di _opened indica che la cella <TD> è aperta. Il valore di default di _opened è 'false' e commuta in 
 * 'true' una volta che è stato scritto il tag <TD> nel controllo Literal.
 * 
 * 1. Se _opened=false AND _continue=false -> scrivi il <TD> -> commuta _opened=true -> posiziona il controllo.
 * 2. Se _opened=false AND _continue=true -> scrivi il <TD> -> commuta _opened=true -> posiziona il controllo.
 * 3. Se _opened=true AND _continue=true -> posiziona il controllo.
 * 4. Se _opened=true AND _continue=flase -> posiziona il controllo -> scrivi </TD> -> commuta _opened=false.
 * 
 * Il funzionamento di default attiva solo i comportamenti 1. e 4. in uno stesso ciclo.
 * La scrittura di più controlli in una stessa cella non comporta alcun incremento del contatore '_countCols'!!!
 * 
 * La variabile intera _onTheSameCell conta quanti controlli vengono inseriti nella medesima cella.
 */
            Boolean _opened = false;
            Boolean _continue = false;
            int _onTheSameCell = 0;
                       
            /* INIZIO DEL DRAFT DELLA MASCHERA 
            /* UN PO' DI SANO COPIA-INCOLLA */
            if(p_tab.InnerElementsCount>0)
            {
                System.Web.UI.WebControls.Label _lbl = null; ;
                System.Web.UI.WebControls.Literal _ltr = null;
                System.Web.UI.WebControls.WebControl _test = null;
                _ltr = new Literal();
                _ltr.Text = "<table class=\"DYN\">"; //;
                _plh.Controls.Add(_ltr);
                _elem = p_tab.Inner;                                                //Primo degli elementi di livello inferiore
                while (_elem != null)
                {
                    //if (_countCols == 0)
                    //{
                    //    _ltr = new Literal();
                    //    _ltr.Text = "<tr class=\"DYN_ROW\">";
                    //    _plh.Controls.Add(_ltr);
                    //}

                    #region @MdN 19/09/2015
                    //@MdN 19/09/2015
                    if (_countCols == 0 && _continue == false)
                    {
                        _ltr = new Literal();
                        _ltr.Text = "<tr class=\"DYN_ROW\">";
                        _plh.Controls.Add(_ltr);
                    }
                    //@MdN 19/09/2015
                    #endregion

                    // colonna 1 -- Label
                    _ltr = new Literal();
                    //_ltr.Text = "<td class=\"DYN_COL1\" width=\"" + _col1.ToString() + "%\" >";

                    #region @MdN 19/09/2015
                    //@MdN 19/09/2015
                    if (_continue == false && _opened == false)
                    {
                        _ltr.Text = "<td class=\"DYN_COL1\" width=\"" + _col1.ToString() + "%\" >";
                        _opened = true;
                    }
                    else
                        _ltr.Text = "&nbsp;";
                    //@MdN 19/09/2015
                    #endregion

                    _plh.Controls.Add(_ltr);
                    // etichetta associata: SOLO SE IL CONTROLLO NON E' A SUA VOLTA UN'ETICHETTA
                    if (_elem.Name.ToLower().CompareTo("lbl") != 0)
                    {
                        _lbl = getAssociatedLabel(_elem);
                        if (_lbl != null)
                            _plh.Controls.Add(_lbl); //JUMP!!!
                    }

                    // colonna 2 -- INSERIMENTO CONTROLLO WEB
                    _ltr = new Literal();
                    //_ltr.Text = "</td><td class=\"DYN_COL2\" width=\"" + _col2.ToString() + "%\">";

                    #region @MdN 19/09/2015
                    //@MdN 19/09/2015
                    //if ((_continue == false && _opened==true) || _onTheSameCell==0)
                    if ((_continue == false && _opened == true))
                    {
                        _ltr.Text = "</td><td class=\"DYN_COL2\" width=\"" + _col2.ToString() + "%\">";
                        //_opened = true;
                    }
                    else
                        _ltr.Text = "&nbsp;";
                    //@MdN 19/09/2015
                    #endregion
                    
                    _plh.Controls.Add(_ltr);
                    try
                    {
                        System.Web.UI.WebControls.WebControl _debugwb = null;
                        _debugwb = getWebControl(_elem);
                        _plh.Controls.Add(_debugwb);
                        // - eventuale associazione con data source
                        if (_elem.Name.ToLower().CompareTo("sqlddl") == 0)
                        {
                            System.Web.UI.WebControls.SqlDataSource _sqlds = getAssociatedSqlDataSource(_elem);
                            if (_sqlds != null)
                                _plh.Controls.Add(_sqlds);
                        }
                        //_plh.Controls.Add(getWebControl(_elem));
                    }
                    catch (simplex_ORM.Spx_ORMMessage ORMM)
                    {
                        MyErrors.Add(ORMM);
                    }
                    // clonna 3 -- chiusura
                    _ltr = new Literal();
                    //_ltr.Text = "</td><td class=\"DYN_COL3\" width=\"0%\"></td>";

                    #region @MdN 19/09/2015
                    //@MdN 19/09/2015

                    // Determina se si deve continuare o meno
                    if (_elem.getValue("Continue") != null && _elem.getValue("Continue").ToLower().CompareTo("true") == 0)
                        _continue = true;
                    else
                        _continue = false;

                    if (_continue == false && _opened == true)
                    {
                        _ltr.Text = "</td><td class=\"DYN_COL3\" width=\"0%\"></td>";
                        _opened = false;
                        _onTheSameCell = 0;
                    }
                    else
                    {
                        _onTheSameCell++;
                        _ltr.Text = "&nbsp;";
                    }

                    //@MdN 19/09/2015
                    #endregion

                    _plh.Controls.Add(_ltr);

                    // nuova riga, Sì/No?       //_countCols++;
                    #region @MdN 19/09/2015
                    //@MdN 19/09/2015
                    if (_continue == false)
                        _countCols++;
                    //@MdN 19/09/2015
                    #endregion

                    if (_countCols == _numCols)
                    {
                        //Sì
                        _countCols = 0;
                        _ltr = new Literal();
                        //_ltr.Text = "</tr>";

                        #region @MdN 19/09/2015
                        //@MdN 19/09/2015
                        if (_continue == false)
                            _ltr.Text = "</tr>";
                        else
                            _ltr.Text = "&nbsp;";
                        //@MdN 19/09/2015
                        #endregion

                        _plh.Controls.Add(_ltr);
                        _countCols = 0;
                    }
                    _elem = _elem.Next;
                }// fine ciclo while

                // Si deve gestire la chiusura dell' ultima riga nel caso in cui _countCols < _NumCols
                if (_countCols > 0 && _countCols < _numCols)
                {
                    while (_countCols < _numCols)
                    {
                        _ltr = new Literal();
                        //_ltr.Text = "<td class=\"FRM_COL1\">" + "</td><td class=\"FRM_COL2\">" + "</td><td class=\"FRM_COL3\"></td>";
                        _ltr.Text = "<td class=\"DYN_COL1\" width=\"" + _col1.ToString() + "%\" >" + "</td><td class=\"DYN_COL2\" width=\"" + _col2.ToString() + "%\">" + "</td><td class=\"DYN_COL3\" width=\"0%\"></td>";
                        _plh.Controls.Add(_ltr);
                        _countCols++;
                    }
                    _ltr = new Literal();
                    _ltr.Text = "</tr>";
                    _plh.Controls.Add(_ltr);
                }

                // -- chiusura tabella
                _ltr = new Literal();
                _ltr.Text = "</table>";
                _plh.Controls.Add(_ltr);

            } // blocco principale per 'tab'
            return _plh;
        }

        /// <summary>
        /// <p>
        /// Crea una struttura di configurazione a partire dal nome di una tabella
        /// (simplex_ORM.SQLSrv.SQLTable).
        /// </p>
        /// </summary>
        /// <param name="p_Name">Nome della tabella.</param>
        protected virtual void createXMLConfigurationFile(String p_Name)
        {
            //TODO:
        }
        /// <summary>
        /// TODO:
        /// <p>
        /// Scrive su un file xml una struttura di configurazione.
        /// </p>
        /// </summary>
        /// <param name="p_config">Nome del file di configurazione.</param>
        protected virtual void writeXMLConfigurationFile(SIMPLEX_Config.Spx_XmlConfigFile p_config)
        {
            //TODO:
        }

#endregion

        #region PROPERTIES PUBBLICHE
        /// <summary>
        /// Nome della pagina visualizzato nei menu e nelle briciole di pane.
        /// @MdN
        /// ----
        /// Crtd: 18/01/2015
        /// </summary>
        public String Nome
        {
            get
            {
                return MyName;
            }
            set
            {
                if (value == null)
                    MyName = "### DA DEFINIRE";
                else
                    MyName = value;
            }
        }
        #endregion

        #region GESTORI
        protected virtual void Page_Load(object sender, EventArgs e)
        {

            preloadEnvironment();
            
            // Evento del BOX dei messaggi
            if (Session["GenericSpxForm1.R_Conferma.Click"] != null)
            {
                /* Parte del codice necessaria ad impostare il più opportuno gestore dell'evento
                 * Click del tasto R_Conferma del MessageBox "embedded".
                 * Vedere la documentazione a corredo.
                 */
                // <%WRITE YOUR CODE FROM HERE%>

                // <%WRITE YOUR CODE TO HERE%>
            }            

            #region GESTIONE DELLA COMPOSIZIONE DINAMICA DELLA FORM

            SIMPLEX_Config.Spx_XmlElement _head = null;
            SIMPLEX_Config.Spx_XmlConfigWriter _wr = null;
            // Determinazione del nome del file xml.

            // Il nome del file in prima istanza è quello impostato staticamente 
            // nella costante MyControlsXmlFilePath.
            String _XmlFile = MyControlsXmlFilePath;

            /* *******************************************
             * La creazione dinamica della maschera può essere fatta SE E SOLO se
             * è vera anche solo UNA delle due seguenti condizioni:
             * (1) L'utente ha definito staticamente il nome della tabella di riferimento
             *     nella costante 'MyTableName'
             * (2) L'utente ha creato la tabella TBL_UNDER_FORM all'interno del metodo preloadEnvironment().
             * 
             * ******************************************* */
            if (((TBL_UNDER_FORM != null) && (TBL_UNDER_FORM.Name != null)) || MyTableName != null)
            {
                #region RIMUOVERE NELLE VERSIONI SUCCESSIVE ALLA v.1.0.0.5
                //<deleted @MdN: 27/05/2018>
                //if (_XmlFile == null)
                //{
                //    //Se l'utente non ha staticamente definito il path del file xml, il sistema 
                //    //lo prende dal nome del file aspx.
                //    _XmlFile = MyAspxFileName;
                //}
                //if (MyAspxFileName != null)
                //{
                //    _XmlFile = MyAspxFileName + ".xml";
                //}
                //</deleted @MdN: 27/05/2018>
                #endregion

                //<added @MdN 27/05/2018
                if (_XmlFile == null)
                {
                    //Se l'utente non ha staticamente definito il path del file xml, il sistema 
                    //lo prende dal nome del file aspx.
                    _XmlFile = MyAspxFileName + ".xml";
                }
                //</added @MdN 27/05/2018

                // Se il file NON esiste, il sistema lo crea
                if (System.IO.File.Exists(MapPath(_XmlFile)) == false)
                {
                    if (IsPostBack == false)
                    {
                        //Crea la struttura di configurazione
                        if (MyConn.State == ConnectionState.Open)           //@MdN 06/07/2015 BugFixed
                            MyConn.Close();                                 //@MdN 06/07/2015 BugFixed

                        if (TBL_UNDER_FORM!= null && TBL_UNDER_FORM.Name != null)
                            _head = getControlsFromTable(simplex_ORM.SQLSrv.SQLTable.createSQLTable(MyConn, TBL_UNDER_FORM.Name), null);   //TODO.
                        else
                            _head = getControlsFromTable(simplex_ORM.SQLSrv.SQLTable.createSQLTable(MyConn, MyTableName), null);   //TODO.
                        //Scrive la struttura di configurazione nel file
                        if (_head != null)
                        {
                            _wr = _head.getXmlConfigWriter(MapPath(_XmlFile));
                            if (_wr != null)
                                _wr.write();

                        }
                    }
                }// fine creazione dell'eventuale creazione file

                //<deleted @MdN: 06/12/2015>
                //if (IsPostBack == false)
                //    ViewState.Clear();
                //</deleted @MdN: 06/12/2015>
                //Caricamento del file
                drawDynamicForm(_XmlFile, 1);
            }// fine disegno della maschera
            #endregion

            #region WRITE YOUR CODE
            // <%WRITE YOUR CODE FROM HERE%>
            // <%WRITE YOUR CODE TO HERE%>
            #endregion

            //@MdN 15/03/215
            if (MyErrors.Count > 0)
                showErrors();
            else
                ((Literal)this.Master.FindControl("ListOfErrors")).Text = "";

            //Scrittura del log
            if (IsPostBack == false)
                writeMessageToLog(MyConn, "Form_Load()", MySbc.ToString(), ((UNIF_User)(MyApp.CurrentUser)).UID.ToString(), Session.SessionID);

        }

        /// <summary>
        /// Gestore dell'evento Click sul tasto "Annulla" della MessageBox
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 08/03/2015
        /// </pre>
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void R_Annulla_Click(object sender, EventArgs e)
        {
            MyMessageBoxVisible = false;
            Session["GenericSpxForm1.Panel_MessageBox.Visible"] = MyMessageBoxVisible;
            //Panel_MessageBox.Visible = MyMessageBoxVisible; //OBSOLETO 
            Session["GenericSpxForm1.R_Conferma.Click"] = null;
        }

        
        /// <summary>
        /// Esegue il salvataggio dell'oggetto eseguendo in sequenza le seguenti azioni:<br></br>
        /// 1. Copia i valori dei campi della maschera nell'oggetto;<br></br>
        /// 2. Eventuale conversione in maiuscolo. <br></br>
        /// 3. Eventuale incremento della chiave intera. <br></br>
        /// 4. Eventuale impostazione delle informazioni di servizio.<br></br>
        /// 5. Salvataggio. <br></br>
        /// 
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: ???
        /// Mdfd: 28/04/2018 - Monterotondo
        /// </pre>
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected override void R_Salva_Click(object sender, EventArgs e)
        {

            #region WRITE YOUR CODE HERE
            //WRITE YOUR CODE FROM HERE

            //WRITE YOUR CODE TO HERE
            #endregion

            //modify(TBL_UNDER_FORM, "R_Dynamic", "System.Web.UI.WebControls.PlaceHolder");             //<deleted @MdN: 28/04/2018>
																	  

            #region Copio i valori dei campi della maschera nell'oggetto
            copyValues(TBL_UNDER_FORM, "R_Dynamic", "System.Web.UI.WebControls.PlaceHolder");           //<added @MdN: 28/04/2018>
            #endregion
            
            try
            {
                #region [OPZIONALE]: Eventuale conversione in maiuscolo. <added @MdN: 09/04/2018>
                if (MyToUpper == true)
                {
                    foreach (String s in TBL_UNDER_FORM.SQLColumns)
                    {
                        if (TBL_UNDER_FORM.getValue(s) != null)
                            TBL_UNDER_FORM.setValue(s, TBL_UNDER_FORM.getValue(s).ToUpper());
                    }
                }
                //</added @MdN: 09/04/2018>
                #endregion

                #region [OPZIONALE]: Eventuale incremento della chiave intera. <added @MdN: 28/04/2018>
                if (MyConn != null && MyGetMaxKeyValue == true)
                    incrementNotIdentity();
                #endregion

                #region [OPZIONALE]: Eventuale impostazione delle informazioni di servizio (MySaveInfos == true).
                if (TBL_UNDER_FORM != null && MyApp != null && MySaveInfos == true)
                {
                    setServiceInfos();
                }
                #endregion

                #region Salvataggio <added @MdN: 28/04/2018>
                if (MyConn != null)
                {
                    if (MyConn.State == ConnectionState.Closed)
                        MyConn.Open();
                    save();
                    if (MyConn.State == ConnectionState.Open)
                        MyConn.Close();
                }
                #endregion
            }
            catch (simplex_ORM.Spx_ORMMessage ORMM)
            {
                ORMM.traceLog(MyConn, ((UNIF_User)MyApp.CurrentUser).UID.ToString(), Session.SessionID);
                MyErrors.Add(ORMM);
                if (MyErrors.Count > 0)
                    showErrors();
                return;
            }

        }

        protected virtual void R_Back_Click(object sender, EventArgs e)
        {
            String _destination;
            #region WRITE YOUR CODE HERE
            //WRITE YOUR CODE FROM HERE

            //WRITE YOUR CODE TO HERE
            #endregion
            _destination = MySbc.PreviousLink;
            Response.Redirect(_destination);
        }

        protected virtual void R_Delete_Click(object sender, EventArgs e)
        {

            /** IL COMANDO AGISCE IN DUE FASI:
             * Verifica il controllo sender
             * 1) Se il sender è il bottone "R_Delete" -- FASE 1: VISUALIZZA IL MESSAGE BOX E VALORIZZA IL DELEGATO DEL TASTO OK
             * 2) Se il sender è il controllo WebSimplexMessageBox "R_MessageBox" -- FASE 2: EFFETTUA L'OPERAZIONE.
             * **/
            if (sender.GetType().ToString().CompareTo("System.Web.UI.WebControls.Button") == 0)
            {
                // FASE 1:
                Session["Evt_OkButton"] = new EventHandler(this.R_Delete_Click);
                this.R_MessageBox.show((int)WebSimplexMessageBox.BoxType.OkMessage);
                R_MessageBox.Visible = true;
                return;
            }
            else
            {
                //Azione
                #region WRITE YOUR CODE HERE
                //WRITE YOUR CODE FROM HERE

                //WRITE YOUR CODE TO HERE
                #endregion
            }
        }
        #endregion

        #region TEST - DA ELIMINARE - DELETEME
        protected void Change_Test(object sender, EventArgs e)
        {
        //    simplex_ORM.Spx_ORMMessage ORMM = new simplex_ORM.Spx_ORMMessage("Change_Test", 0);
        //    ORMM.traceLog(MyConn, ((UNIF_User)MyApp.CurrentUser).UID.ToString(), Session.SessionID);
        }
        #endregion

    }
}
