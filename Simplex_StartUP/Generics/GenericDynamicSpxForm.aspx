<%@ Page Language="C#" MasterPageFile="~/MasterPCM.Master" AutoEventWireup="true" CodeBehind="GenericDynamicSpxForm.aspx.cs" Inherits="SIMPLEX_STARTUP.GenericDynamicSpxForm" Title="Pagina senza titolo" %>
<%@ Register Assembly="WebSimplex" Namespace="WebSimplex" TagPrefix="WS1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MenuPlaceHolder" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

<WS1:WebSimplexMessageBox ID="R_MessageBox" runat="server" 
        AlertText="Attenzione, vedi il riquadro degli errori." 
        OkAlertButtonCssStyle="AlertButton" 
        AlertImage="Immagini/Omino_Errore.png" 
        OkMessageImage="Immagini/Megafono_tondoviola.png" 
        OkMessageText="Continuare con l'operazione?"
        OkMessageButtonCssStyle="OkButton" 
        UndoMessageButtonCssStyle="UndoButton" CssOkMsgClass="OkMsg" />

<div id="MainFormSection">
<table class="FRM">
    <tr class="FRM">
    <td class="FRM_COL1"></td>
    <td class="FRM_COL2"></td>
    <td class="FRM_COL3">
        <asp:Button ID="R_Back" runat="server" Text="Indietro" onclick="R_Back_Click" />
        </td>
    </tr>
    <tr class="FRM">
        <td class="FRM_COL1"></td>
        <td class="FRM_COL2">
        <!-- MessageBox -->
        <!-- END MessageBox -->
        </td>
    <td class="FRM_COL3"></td>
    </tr>
</table>

<!-- PUT YOUR CONTROLS FROM HERE -->
    <asp:PlaceHolder ID="R_Dynamic" runat="server">
    </asp:PlaceHolder>
<!-- PUT YOUR CONTROLS TO HERE -->

<table class="FRM">
    <tr class="FRM">
    <td class="FRM_COL1"></td>
    <td class="FRM_COL2">
        <asp:Button ID="R_Salva" runat="server" Text="Salva" onclick="R_Salva_Click" />
        </td>
    <td class="FRM_COL3">
        <asp:Button ID="R_Delete" runat="server" Text="Elimina" 
            onclick="R_Delete_Click" />
        </td>
    </tr>
</table>

</div>
</asp:Content>
