using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;

namespace SIMPLEX_STARTUP
{
    /// <summary>
    /// Classe TEMPLATE per la creazione di una maschera che mostra una lista di oggetti.
    /// <pre>
    /// ----
    /// @MdN
    /// Mdfd: 02/08/2015 - Merano - Fattorizzata Cancella()
    /// Mdfd: 30/08/2015 - regione delle costanti
    /// Mdfd: 09/11/2015 - V.1.1.0.0
    /// Mdfd: 26/05/2016 - Introdotto l'attributo MyOrder
    /// Mdfd: 26/05/2016 - preloadEnvironment(): passaggio da Session a ViewState per le componenti della query della lista -
    /// </pre>

    /// </summary>
    public partial class GenericListSpxForm : GenericSpxForm
    {
        /// Stringa della query
        /// </summary>
        protected String MySql = null;
        /// <summary>
        /// Stringa del filtro
        /// </summary>
        protected String MyFilter = null;
        /// <summary>
        /// Stringa dell'ordinamento
        /// </summary>
        protected String MyOrder = null;
        /// <summary>
        /// Criteri di selezione associati al dirigente
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 07/11/2015
        /// </pre>
        /// </summary>
        //protected SelectionCriteria MyCritera = null;
        /// <summary>
        /// Query di base della lista
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 31/08/2015
        /// </pre>
        /// </summary>
        protected String _selectListaBase = "SELECT * FROM MYTABLE"; //<-- CHANGE HERE
        /// <summary>
        /// Filtraggio di base della lista
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 09/11/2015
        /// </pre>
        /// </summary>
        protected String _filterListaBase = ""; //<-- CHANGE HERE
        /// <summary>
        /// Ordinamento delle righe della lista
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 09/11/2015
        /// </pre>
        /// </summary>
        protected String _orderListBase = "";

        #region CONSTANTS
        #endregion
        
        
        /// <summary>
        /// <p>Caricamento della maschera.</p>
        /// <ul>
        /// <li>Invoca preloadEnvironment()</li>
        /// <li>esegue la query di popolamento</li>
        /// </ul>
        /// <pre>
        /// @MdN
        /// ----
        /// Crtd: 30/01/2015
        /// </pre>
        /// </summary>
        /// <param name="sender">oggetto che ha invocato l'evento</param>
        /// <param name="e">parametri</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            preloadEnvironment();

            #region [ADD YOUR CODE HERE]
            #endregion

            if (IsPostBack == false)
                writeMessageToLog(MyConn, "Form_Load", MySbc.ToString(), MyApp.CurrentUser.UID.ToString(), Session.SessionID);
        }

        /// <summary>
        /// <p>
        /// Imposta gli attributi necessari al funzionamento.
        /// Si attende in Session["APPLICATION"] l'oggetto GeneralApplication corrente.
        /// Se non è presente, lancia un eccezione del tipo generalApplication TracedMessage.
        /// </p>
        /// <p>
        /// Imposta in sessione l'ID dell'oggetto selezionato. La variabile di sessione ha come 
        /// stringa identificatrice il nome della tabella di riferimento dell'oggetto tutto in maiuscolo.
        /// </p>
        /// <p>Esempio: </p>
        /// <p>
        /// se la tabella è 'utente' il nome della variabile di sessione è 'UTENTE'.
        /// </p>
        /// 
        /// <p>Valorizza i seguenti attributi derivati da GenericSpxForm:
        /// </p>
        /// <ul>
        /// <li>MyApp               :   APPLICAZIONE CORRENTE;</li>
        /// <li>MyConn              :   CONNESSIONE DI DEFAULT;</li>
        /// <li>MyMainMenu          :   CONTROLLO MENU SINISTRO;</li>
        /// <li>MySbc               :   CONTROLLO BREADCRUMBS;</li>
        /// <li>MyAspxFileName      :   NOME DELLA PAGINA WEB/WEB FORM.</li>
        /// <li>MyDirectory         :   NOME DELLA DIRECTORY DELLA FUNZIONE APPLICATIVA</li>
        /// </ul>
        /// <p>
        /// Valorizza i suoi attributi caratteristici che sono:
        /// </p><p>
        /// <ul>
        /// <li>MyBackURL           :   URL DELLA PAGINA CHIAMANTE.</li>
        /// </ul>
        /// </p>
        /// </summary>
        protected override void preloadEnvironment()
        {
            base.preloadEnvironment();

            #region TO CHANGE: ASSEGNAZIONI
            if (ViewState["MYSQL"] != null)
                MySql = (String)ViewState["MYSQL"];
            else
                MySql = _selectListaBase + _filterListaBase + _orderListBase;

            if (ViewState["MYFILTER"] != null)
                MyFilter = (String)ViewState["MYFILTER"];
            else
                MyFilter = _filterListaBase;

            if (ViewState["MYORDER"] != null)
                MyOrder = _orderListBase;
            else
                MyOrder = (String)ViewState["MYORDER"];

            // Nome e webcrumbs : DOTO: ELIMINARE E PORTARE IN 
            if(MyName==null)
                MyName = this.Title;                                                //<-- CHANGE HERE
            if(MyDirectory==null)
                MyAspxFileName = "~/" + MyAspxFileName;                             //<-- CHANGE HERE
            else
                MyAspxFileName = "~/" + MyDirectory + "/" + MyAspxFileName;
            AddToSimplexBreadCrumbs();
            #endregion

            #region [ADD YOUR CODE HERE]
            #endregion

        }

        /// <summary>
        /// Cancella la maschera e reimposta le variabili di stato utili per la ricerca
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 02/08/2015
        /// </pre>
        /// </summary>
        protected virtual void cancella()
        {
            Session["MYSQL"] = null;
            Session["MYFILER"] = null;

            #region TO CHANGE: ASSEGNAZIONI
            MySql = "SELECT * FROM SIMPLEX_STARTUP_VW_CENT_DIPENDENTI";                        //<-- CHANGE HERE
            MyFilter = null;                                                        //<-- CHANGE HERE
            this.SqlDataSource_MYSOURCE.SelectCommand = MySql;                      //<-- CHANGE HERE
            #endregion

            #region [ADD YOUR CODE HERE]
            // aggiungere in questo punto la materiale cancellazione dei campi della Form
            this.R_Lista.DataBind();
            #endregion
        }

        protected void R_Indietro_Click(object sender, EventArgs e)
        {
            #region TO CHANGE: ASSEGNAZIONI
            if (MySbc.PreviousLink != null)                                    //<--TO CHANGE
                Response.Redirect(MySbc.PreviousLink);                         //<--TO CHANGE
            #endregion
        }

        protected virtual void R_Lista_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            int _row = 0;
            int _key = 0;

            // identificare la riga che ha scatenato l'evento: ASP.NET non lo fa in automatico
            // N.B. CommandArgument è 0-BASED!!!!
            _row = Int32.Parse(e.CommandArgument.ToString());

            // identificare il valore della chiave associata alla riga
            _key = Int32.Parse(((GridView)e.CommandSource).Rows[_row].Cells[0].Text);

            #region [ADD YOUR CODE HERE]
            #endregion
        }
    }
}
