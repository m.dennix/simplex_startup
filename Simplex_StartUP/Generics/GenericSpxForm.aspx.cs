using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using GeneralApplication;
using WebSimplex;
using simplex_ORM;                      // v.1.1.0.13 del 08/11/2020

namespace SIMPLEX_STARTUP
{
    /// <summary>
    /// Derivata da SimplexForm.
    /// Ha attributi protetti necessari per il funzionamento dell'applicazione.
    /// Stati:<br></br>
    /// <ul>
    /// <li>Session["APPLICATION"]: contiene l'oggetto di classe <i>CORR_Application</i> corrente.</li>
    /// <li>ViewState["MYERRORS"] e Session[MyName + "_MYERRORS"]: Lista degli errori.</li>
    /// <li></li>
    /// </ul>
    /// 
    /// Esegue Override dei seguenti metodi:
    /// 
    /// 
    /// <pre>
    /// ------
    /// @MdN 
    /// Crtd: 06/12/2014
    /// Mdfd: 25/06/2015 test di accesso alla pagina
    /// Mdfd: 26/05/2015 test di sessione attiva
    /// Mdfd: 07/09/2015 Pulizia dell'area degli errori
    /// Mdfd: 18/10/2015 Nuovo test di accesso alla pagina in quanto quello del 25/06 non funzionava
    /// Mdfd: 21/11/2015 v. 1.0.0.1 - Passaggio della lista degli errori attreverso la ViewState
    /// Mdfd: 26/05/2016 v. 1.0.0.2 - Introdotto l'attributo protetto MyDirectory che indica l'eventuale directory associata alla funzione applicativa.
    /// Mdfd: 09/06/2016 v. 1.0.0.3 - Introdotta redirezione verso pagina d'errore.
    /// Mdfd: 04/07/2016 v. 1.0.0.4 - Ristrutturazione. Page_Load(), R_Back_Click(), R_Annulla_Click() diventano virtual.
    /// Mdfd: 05/12/2016 v. 1.0.0.5 - Aggiunta la lettura degli errori anche dalla sessione Session[MyName + "_MYERRORS"].
    /// Mdfd: 26/12/2016 v. 1.0.0.6 - Introdotta NoSessionSpxErrorForm.aspx in preloadEnvironment()
    /// Mdfd: 08/03/2017 v. 1.0.0.7 - Introdotta la propertu FunzioneApplicativa che restituisce la denominazione della funzione applicativa di apparetnenza.
    /// Mdfd: 05/07/2017 v. 1.0.1.0 - Introdotti i metodi:
    ///                                 - getFormPath(String) che restituisce il path di una Form di cui è noto il nome;
    ///                                 - passTo() efefttua il passaggio del controllo verso una maschera di destinazione con passaggio di un oggetto di classe simplex_ORM.SQLSrv.SQLTable. 3 Overload.
    ///                                 - getPassedTable() preleva un oggetto di classe simplex_ORM.SQLSrv.SQLTable dalla sessione.
    /// Mdfd: 10/07/2017 v. 1.0.1.1 - Aggiunta variabile di stato in passTo().
    /// Mdfd: 25/07/2017 v. 1.0.2.0 - Aggiunte:
    ///                                 - enumerazione MESSAGE_TYPE.
    ///                                 - overload del metodo protetto showErrors()
    /// Mdfd: 03/08/2017 v. 1.0.2.1 - Corretto BUG sull'overload del metodo protetto showErrors()
	/// Mdfd: 11/09/2017 v. 1.0.2.3 - Aggiunta la possibilità di specificare una SQLTable nulla delegando il chiamato alla creazione di un oggetto nuovo.
    /// Mdfd: 27/04/2018 v. 1.0.2.4 - Refactoring
    ///                               * aggiunto MyGetMaxKeyValue 
    ///                               * Inserito il gestore dell’evento virtual R_Ralva_Click().
    ///                               * Inserito il metodo physicalDelete() (29/04/2018).
    ///                               * Introdotto attributo protetto MyDataDelColumn (29/04/2018).
    ///                               * Introdotto attributo protetto MySaveInfos (29/04/2018)
    ///                               * Inserite MySql_base; MyFilter_base; MyOrder_base; MyGroup_base (02/06/2018).
    ///                               * Inserite MySql; MyFilter; MyOrder; MyGroup (NUOVA) (10/06/2018).
    /// Mdfd: 21/01/2020 v. 1.0.2.5 - Aggiunta:
    ///                                 - MAIN_UNDER_FORM (protected).
    ///                                 - getFromCallerPage().
    /// Mdfd: 21/11/2020 v. 1.0.2.6 - Aggiunta:
    ///                                 - showErrors(): NUOVA versione del metodo che mostra gli errori rappresentandoli 
    ///                                                 diversamente secondo la tipologia (simplex_ORM.Spx_ORMMessage.MESSAGE_TYPE)
    ///                                 - showSimpleErrors(): vecchia showErrors().
    /// </pre>
    /// </summary>
    public partial class GenericSpxForm : simplex_FORM.SimplexForm
    {

        #region ATTRIBUTI PUBBLICI
        ///// Tabella REFERENZIATA dalla tabella corrente.
        ///// <pre>
        ///// ----
        ///// @MdN
        ///// Crtd: 21/01/2020
        ///// </pre>
        ///// </summary>
        //protected simplex_ORM.SQLSrv.SQLTable MAIN_UNDER_FORM = null;

        /// <summary>
        /// Tipologia di messaggio mostrato a video con la funzione showErrors()
        /// <remarks>DEPRECATED</remarks>
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 25/07/2017
        /// </pre>
        /// </summary>
        // public enum MESSAGE_TYPE {SIMPLE_MSG, WARNING_MSG, ERROR_MSG, INFO_MSG};
        #endregion

        #region ATTRIBUTI PROTETTI
        /// Tabella REFERENZIATA dalla tabella corrente.
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 21/01/2020
        /// </pre>
        /// </summary>
        protected simplex_ORM.SQLSrv.SQLTable MAIN_UNDER_FORM = null;

        /// Stringa della query di BASE senza filtraggio ed ordinamento
        /// </summary>
        protected String MySql_base = null;
        /// <summary>
        /// Stringa del filtro di BASE (senza clausola WHERE)
        /// </summary>
        protected String MyFilter_base = null;
        /// <summary>
        /// Stringa dei criteri di ordinamento di BASE  (Senza clausola ORDER BY)
        /// </summary>
        protected String MyOrder_base = null;
        /// <summary>
        /// Stringa dei criteri di raggruppamento di BASE  (Senza clausola GROUP BY)
        /// </summary>
        protected String MyGroup_base = null;

        #region REFACTORING 10/06/2018
        /// Stringa della query attuale (valorizzata da ViewSytate["MYSQL"])
        /// </summary>
        protected String MySql = null;
        /// <summary>
        /// Stringa del filtro attuale (valorizzata da ViewSytate["MYFILTER"])
        /// </summary>
        protected String MyFilter = null;
        /// <summary>
        /// Stringa dei criteri di ordinamento attuale (valorizzata da ViewSytate["MYORDER"])
        /// </summary>
        protected String MyOrder = null;
        /// <summary>
        /// Stringa dei campi di raggruppamento
        /// </summary>
        protected String MyGroup = null;
        #endregion

        /// <summary>
        /// Oggetto applicazione corrente.
        /// </summary>
        protected GeneralApplication.GeneralApplication MyApp = null;
        /// <summary>
        /// Oggetto connessione corrente al database GTEL
        /// DEVE essere sempre chiusa.
        /// </summary>
        protected System.Data.Odbc.OdbcConnection MyConn = null;
        /// <summary>
        /// Nome della pagina come visualizzata nel menu.
        /// </summary>
        protected String MyName = null;
        /// <summary>
        /// Riferimento all' oggetto di classe WebSimplexBreadCrumbs.
        /// </summary>
        protected WebSimplex.WebSimplexBreadCrumbs MySbc = null;
        /// <summary>
        /// Nome del file della pagina corrente. Viene valorizzato automaticamente 
        /// dal metodo preloadEnvironment().
        /// </summary>
        protected String MyAspxFileName = null;
        /// <summary>
        /// URL della pagina chiamante.
        /// </summary>
        protected String MyBackURL = null;
        /// <summary>
        /// Directory dell'evenntuale funzione applicativa.
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 26/05/2016
        /// </pre>
        /// </summary>
        protected string MyDirectory = null;

        /// <summary>
        /// Chiave della funzione applicativa, come risulta nel file di configurazione
        /// web.config.
        /// <remarks>Se null indica il desktop applicativo.</remarks>
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 25/06/2015
        /// </pre>
        /// </summary>
        protected String MyFunzioneApplicativa = null;

        /// <summary>
        /// Indica se il riquadro dei messaggi deve essere visibile o meno.
        /// Per default NON è visibile.
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 08/03/2015
        /// </pre>
        /// </summary>
        protected Boolean MyMessageBoxVisible = false;

        /// <summary>
        /// Menu verticale associato alla maschera
        /// </summary>
        protected System.Web.UI.WebControls.Menu MyMainMenu = null;

        /// <summary>
        /// Nome di una eventuale maschera chiamante.
        /// </summary>
        protected String MyCallerName = null;

        /// <summary>
        /// Nome della colonna/attributo che se valorizzata rende un record logicamente cancellato, cioè OSCURATO.<br></br>
        /// Fondamentale per le operazioni di cancellazione logica.<br></br>
        /// Il valore di default è "DATA_DEL" perchè tale è il nome che viene dato a questa colonna nei database 
        /// che sono compliant con il framework simplex.<br></br>
        /// Per cambiare il nome di questa colonna ridefinire l'attributo in preloadEnvironment() o cambiare 
        /// l'impostazione nel template.<br></br>
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 28/04/2018
        /// </pre>
        /// </summary>
        protected String MyDataDelColumn = "DATA_DEL";

        /// <summary>
        /// Attributo OPZIONALE utile per le operazioni di cancellazione logica.<br></br>
        /// Normalmente l'utente che ha effettuato la cancellazione logica è quello che ha effettuato l'ultima modifica
        /// (vedi MyUsrUM) in  quanto la cancellazione logica è l'ultima modifica apportata al record/oggetto, tuttavia 
        /// se si vuole diversificare tra l'utente che ha effettuato la cancellazione logica  e quello che ha apportato
        /// l'ultima modifica VISIBILE, basta ridefinire il valore di questo attributo in preloadEnvironment().<br></br>
        /// Per quanto appena detto il valore di default è "USR_UM".<br></br>
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 06/05/2018
        /// </pre>
        /// </summary>
        protected String MyUserDelColumn = "USR_UM";

        /// <summary>
        /// Indica il nome della colonna di una tabella destinata a ricevere l’informazione (di tipo datetime) 
        /// che determina il momento di creazione di un dato record.
        /// Il valore di default è "DATA_SYS" perchè tale è il nome che viene dato a questa colonna nei database che sono compliant
        /// con il framework simplex.<br></br>
        /// Per cambiare il nome di questa colonna ridefinire l'attributo in preloadEnvironment() o cambiare l'impostazione
        /// nel template.<br></br>
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 29/04/2018. Refactoring da GenericLookupSpxForm.
        /// </pre>
        /// </summary>
        protected String MyDataSys = "DATA_SYS";

        /// <summary>
        /// Indica il nome della colonna di una tabella destinata a ricevere l’informazione (di tipo Int) 
        /// che determina l'ID dell'utente che ha creato un dato record.
        /// Il valore di default è "USR" perchè tale è il nome che viene dato a questa colonna nei database che sono compliant
        /// con il framework simplex.<br></br>
        /// Per cambiare il nome di questa colonna ridefinire l'attributo in preloadEnvironment() o cambiare l'impostazione
        /// nel template.<br></br>
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 29/04/2018. Refactoring da GenericLookupSpxForm.
        /// </pre>
        /// </summary>
        protected String MyUsrSys = "USR";

        /// <summary>
        /// Indica il nome della colonna di una tabella destinata a ricevere l’informazione (di tipo datetime) 
        /// che determina il momento in cui è stata apportata l'ultima modifica a un dato record.
        /// Il valore di default è "DATA_UM" perchè tale è il nome che viene dato a questa colonna nei database che sono compliant
        /// con il framework simplex.<br></br>
        /// Per cambiare il nome di questa colonna ridefinire l'attributo in preloadEnvironment() o cambiare l'impostazione
        /// nel template.<br></br>
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 29/04/2018. Refactoring da GenericLookupSpxForm.
        /// </pre>
        /// </summary>
        protected String MyDataUM = "DATA_UM";

        /// <summary>
        /// Indica il nome della colonna di una tabella destinata a ricevere l’informazione (di tipo Int) 
        /// che determina l'ID dell'utente che ha apportato l'ultima modifica ad un dato record, compresa la cancellazione logica.
        /// Il valore di default è "USR_UM" perchè tale è il nome che viene dato a questa colonna nei database che sono compliant
        /// con il framework simplex.<br></br>
        /// Per cambiare il nome di questa colonna ridefinire l'attributo in preloadEnvironment() o cambiare l'impostazione
        /// nel template.<br></br>
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 29/04/2018. Refactoring da GenericLookupSpxForm.
        /// </pre>
        /// </summary>
        protected String MyUsrUM = "USR_UM";

            #region SETTINGS: CHANGE HERE
            /// <summary>
            /// Nelle pagine in cui si vuole bypassare il controllo utente si deve porre il flag a true.
            /// <pre>
            /// ----
            /// @MdN
            /// Crtd: 18/10/2015
            /// </pre>
            /// </summary>
            protected Boolean MyBypassVerifyAccess = false;         //<-- TO CHANGE

            /// <summary>
            /// FLAG IMPORTANTISSIMO: SE impostato a TRUE e SE il campo chiave è UNICO e SE NON è identity, prima del salvataggio il campo chiave viene impostato da programma con il massimo valore
            /// di chiave presente sulla tabella incrementato di uno.
            /// <pre>
            /// ----
            /// @MdN
            /// Crtd: 04/05/2017 --> importato in GenericSpxForm in data 27/04/2018 v.1.0.2.4
            /// </pre>
            /// </summary>
            protected Boolean MyGetMaxKeyValue = false;

            /// <summary>
            /// Cancellazione logica (false) o fisica (true)
            /// Default: false.<br></br>
            /// DEtermina se la cancellazione dell'ogegtto TBL_UNDER_FORM è logica (default) o fisica.<br></br>
            /// La valorizzazione dell'attributo DEVE essere fatta esclusivamente nel metodo preloadEnvironment().<br></br>
            /// La scelta di quale valorizzazione applicare a questo attributo protetto DEVE essere fatta a tempo di compilazione nel TEMPLATE mediante una apposita macrovariabile
            /// di switch.<br></br>
            /// Il comporamento è il seguente:<br></br>
            /// <ul>
            /// <li>Se MyPysicalDeletion=true: la cancellazione è fisica.</li>
            /// <li>Se MyPysicalDeletion=false: la cancellazione è logica.</li>
            /// </ul>
            /// ----
            /// @MdN
            /// Crtd: 29/04/2018 Refactoring. Precedentemente definita in GenericAssociativeSpxForm in data 01/03/2017
            /// </summary>
            protected Boolean MyPhysicalDeletion = false;

            /// <summary>
            /// Indica se devono essere valorizzate le informazioni di servizio (MyDataSys, MyUsrSys, MyDataUM, MyUsrUM).
            /// ----
            /// @MdN
            /// Crtd: 29/04/2018 Refactoring. Precedentemente definita in GenericLookupSpxForm.
            /// </summary>
            protected Boolean MySaveInfos = true;

            /// <summary>
            /// Indica che l'impostazione della data della colonna DATA_SYS è automatica (DEFAULT GETDATE())
            /// ----
            /// @MdN
            /// Crtd: 01/05/2018
            /// </summary>
            protected Boolean MyAutoDataSys = true;

            #endregion

        #endregion

        #region METODI PUBBLICI

        public override Boolean validateForm()
        {
            // NA MAZZA
            return base.validateForm();
        }

        #endregion

        #region METODI PROTETTI

        /// <summary>
        /// Cancella lo stato della maschera di destinazione.<br></br>
        /// Viene chiamata prima del passaggio per pulire lo stato da precedenti passaggi. 
        /// </summary>
        /// <param name="p_NomeDestinazione"></param>
        protected virtual void cancellaStatoDestinazione(String p_NomeDestinazione)
        {
        }

        /// <summary>
        /// Restituisce il path di una Form a partire al suo nome.
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 05/07/2017 v.1.0.0.1
        /// </pre>
        /// </summary>
        /// <param name="p_FormName">Nome della Form come riportato nella colonna DESCRIZIONE della tabella UTILS_MENU.</param>
        /// <returns>In path della Form come riportato nella colonna LINK della tabella UTILS_MENU.</returns>
        protected virtual String getFormPath(String p_FormName)
        {
            String _MySql = "SELECT LINK FROM UTILS_MENU WHERE DESCRIZIONE=? AND DATA_DEL IS NULL";
            System.Data.Odbc.OdbcCommand _cmd=null;
            System.Data.Odbc.OdbcParameter _para=null;
            String _toRet = null;


            if (MyConn != null && p_FormName != null)
            {

                try
                {
                    if (MyConn.State == ConnectionState.Closed)
                        MyConn.Open();

                    _cmd = MyConn.CreateCommand();
                    _cmd.CommandText = _MySql;
                    _para = _cmd.CreateParameter();
                    _para.ParameterName = "@Name";
                    _para.OdbcType = System.Data.Odbc.OdbcType.VarChar;
                    _para.Value = p_FormName;
                    _cmd.Parameters.Add(_para);
                    _cmd.Prepare();
                    _toRet = (String)_cmd.ExecuteScalar();
                }
                catch (Exception E)
                {
                    simplex_ORM.Spx_ORMMessage _ORMM = new simplex_ORM.Spx_ORMMessage(E);
                    _ORMM.Source = "GenericSpxForm.getFormPath(Sring)";
                    _ORMM.MessageCode = 1;
                    _ORMM.MessageType = 1;
                    if (MyApp != null && MyApp.CurrentUser != null && MyConn != null && Session != null)
                        _ORMM.traceLog(MyConn, MyApp.CurrentUser.UID.ToString(), Session.SessionID);
                    throw _ORMM;
                }
                finally
                {
                    _cmd.Dispose();
                    if (MyConn.State == ConnectionState.Open)
                        MyConn.Close();
                }
            }
            return _toRet;
        } // fine getFormPath()

        /// <summary>
        /// Carica, se non nulle, le seguenti variabili dalla Session:<br></br>
        /// - MyCallerName: da Session[MyName + "_MYCALLERNAME"]<br></br>
        /// - TBL_UNDER_FORM: da Session[MyName]<br></br>
        /// - MAIN_UNDER_FORM: da Session[MyCallerName].<br></br>
        /// <br></br>
        /// Se i due oggetti sono già valorizzati, il metodo NON sovrascrive il loro valore attuale.
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 21/01/2020
        /// </pre>
        /// </summary>
        protected void getFromCallerPage()
        {
            if (Session[MyName + "_MYCALLERNAME"] != null && MyCallerName == null)
                MyCallerName = (String)Session[MyName + "_MYCALLERNAME"];

            if (Session[MyName] != null && TBL_UNDER_FORM == null)
                TBL_UNDER_FORM = (simplex_ORM.SQLSrv.SQLTable)Session[MyName];

            if (MyCallerName != null && Session[MyCallerName] != null && MAIN_UNDER_FORM == null)
                MAIN_UNDER_FORM = (simplex_ORM.SQLSrv.SQLTable)Session[MyCallerName];
        }

        /// <summary>
        /// Passa il controllo del programma ad una maschera di destinazione insieme ad un oggetto di classe simplex_ORM.SQLSrv.SQLTable.<br></br>
        /// Se il parametro p_ID è null, l'oggetto di classe simplex_ORM.SQLSrv.SQLTable diventa l'ggetto ORM di default della maschera di destinazione.
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 05/07/2017
        /// Mdfd: 10/07/2017: aggiunta variabile di stato
        /// Mdfd: 11/09/2017: Correzione BUG logico.
        /// </pre>
        /// </summary>
        /// <param name="p_Table">Oggetto di classe simplex_ORM.SQLSrv.SQLTable da passare</param>
        /// <param name="p_Path">Path della maschera di destinazione</param>
        /// <param name="p_Name">Nome della maschera di destinazione</param>
        /// <param name="p_ID">VARIABILE DI SESSIONE OPZIONALE: Stringa identificativa dell'oggetto da passare alla maschera di destinazione.</param>
        protected virtual void passTo(simplex_ORM.SQLSrv.SQLTable p_Table, String p_Path, String p_Name, String p_ID)
        {
            // * Controlli
            // if (p_Table == null || p_Path == null || p_Name == null)
            if (p_Path == null || p_Name == null) //<mdfd @MdN 11/09/2017: il p_Table può essere null per far generare un oggetto simplex_ORM.SQLSrv.SQLTable VUOTO nella maschera di destinazione.
                return;
            // Salvo comunque il chiamante per usi futuri.
            Session["MYCALLERNAME"] = MyName;           // COMPATIBILITà A RITROSO
            Session[p_Name + "_MYCALLERNAME"] = MyName; // <added @MdN: 10/07/2017>

            // Salvo l'oggetto da passare
            if (p_ID != null && p_ID.Length > 0)
                Session[p_Name + "_" + p_ID] = p_Table;
            else
                Session[p_Name] = p_Table;

            HttpContext.Current.Response.Redirect(p_Path);
        }

        /// <summary>
        /// Passa il controllo del programma ad una maschera di destinazione insieme ad un oggetto di classe simplex_ORM.SQLSrv.SQLTable.<br></br>
        /// L'oggetto di classe simplex_ORM.SQLSrv.SQLTable passato diventa l'oggetto ORM di default della maschera di destinazione.
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 05/07/2017
        /// </pre>
        /// </summary>
        /// <param name="p_Table">Oggetto di classe simplex_ORM.SQLSrv.SQLTable da passare</param>
        /// <param name="p_Path">Path della maschera di destinazione</param>
        /// <param name="p_Name">Nome della maschera di destinazione</param>
        protected virtual void passTo(simplex_ORM.SQLSrv.SQLTable p_Table, String p_Path, String p_Name)
        {
            passTo(p_Table, p_Path, p_Name, null);
        }

        /// <summary>
        /// Passa il controllo del programma ad una maschera di destinazione insieme ad un oggetto di classe simplex_ORM.SQLSrv.SQLTable.<br></br>
        /// L'oggetto di classe simplex_ORM.SQLSrv.SQLTable passato diventa l'oggetto ORM di default della maschera di destinazione.
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 05/07/2017
        /// </pre>
        /// </summary>
        /// <param name="p_Table">Oggetto di classe simplex_ORM.SQLSrv.SQLTable da passare</param>
        /// <param name="p_Name">Nome della maschera di destinazione</param>
        protected virtual void passTo(simplex_ORM.SQLSrv.SQLTable p_Table, String p_Name)
        {
            String _Path = null;
            try
            {
                _Path = getFormPath(p_Name);
            }
            catch (simplex_ORM.Spx_ORMMessage _ormm)
            {
                _ormm.Source = "GenericSpxForm.passTo():" + _ormm.Source;
                if (MyApp != null && MyApp.CurrentUser != null && MyConn != null && Session != null)
                    _ormm.traceLog(MyConn, MyApp.CurrentUser.UID.ToString(), Session.SessionID);
                throw _ormm;
            }
            if(_Path!=null)
                passTo(p_Table, _Path, p_Name, null);
        }

        /// <summary>
        /// Prende dalla variabile di sessione specificata dal parametro p_ID un oggetto di classe simplex_ORM.SQLSrv.SQLTable.<br></br>
        /// Se p_ID = null la variabile di sessione considerata è Session[MyName].<br></br>
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 05/07/2017 v.1.0.0.1
        /// </pre>
        /// </summary>
        /// <param name="p_ID">Identificatore della variabile di sessione. Se null viene considerata Session[MyName] </param>
        protected virtual simplex_ORM.SQLSrv.SQLTable getPassedTable(String p_ID)
        {
            
            if (p_ID == null)
                return (simplex_ORM.SQLSrv.SQLTable)Session[MyName];
            else
            {
                return (simplex_ORM.SQLSrv.SQLTable)Session[p_ID + "_" + MyName];
            }
        }

        /// <summary>
        /// Prende dalla variabile di sessione Session[MyName] un oggetto di classe simplex_ORM.SQLSrv.SQLTable.<br></br>
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 05/07/2017 v.1.0.0.1
        /// </pre>
        /// </summary>
        protected virtual simplex_ORM.SQLSrv.SQLTable getPassedTable()
        {
               return (simplex_ORM.SQLSrv.SQLTable)Session[MyName];
        }

        /// <summary>
        /// <p>EN: Show the errors occurred in the last operation. At the end, clears the list of errors. 
        /// </p>
        /// <p>IT: Mostra gli errori verificatisi nell'ultima operazione. Al termine, cancella la lista degli errori.
        /// </p>
        /// <pre>
        /// @MdN
        /// ----
        /// Mdfd: 18/04/2015
        /// Mdfd: 08/11/2020: ridenominata da showErrors() a showSimpleErrors()
        /// </pre>
        /// </summary>
        protected virtual void showSimpleErrors()
        {
            if (MyErrors.Count > 0)
            {
                Boolean l_pari = false;
                Literal l_ErrArea = null;
                System.Text.StringBuilder l_ErrString = new System.Text.StringBuilder("<table class=\"ErrTab\">");
                l_ErrArea = (Literal)this.Master.FindControl("ListOfErrors");

                l_ErrString.Append("<tr class=\"ErrHeaders\"><th class=\"HeadCodCol\">Codice</th><th class=\"HeadDescCol\">Descrizione</th><tr>");

                foreach (simplex_ORM.Spx_ORMMessage ORMM in MyErrors)
                {
                    if (l_pari == true)
                        l_ErrString.Append("<tr class=\"ErrRowPari\">");
                    else
                        l_ErrString.Append("<tr class=\"ErrRowDisPari\">");
                    l_pari = !l_pari;

                    l_ErrString.Append("<td class=\"ErrCodCol\">");
                    l_ErrString.Append(ORMM.MessageCode.ToString());
                    l_ErrString.Append("</td><td class=\"ErrDescCol\">");
                    l_ErrString.Append(ORMM.Message);
                    l_ErrString.Append("</td></tr>");
                }
                l_ErrString.Append("</table>");
                l_ErrArea.Text = l_ErrString.ToString();
                MyErrors.Clear(); //<-- ATTENZIONE
            }
        }//fine

        /// <summary>
        /// Mostra i messaggi a video, in testa alla pagina web, diversificandoli per tipologia.<br></br>
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 25/07/2017
        /// </pre>
        /// </summary>
        /// <param name="p_type">Tipo di messaggio: normale (0), Errore (1), Avvertimento (2), Infromativo (3)</param>
        protected virtual void showErrors(Spx_ORMMessage.MESSAGE_TYPE p_type)
        {
            // compatibilità verso il basso
            if (p_type == Spx_ORMMessage.MESSAGE_TYPE.SIMPLE_MSG)
            {
                showSimpleErrors();
                return;
            }

            // parte nuova
            if (MyErrors.Count > 0)
            {
                Boolean l_pari = false;
                Literal l_ErrArea = null;
                System.Text.StringBuilder l_ErrString = new System.Text.StringBuilder("<table class=\"ErrTab\">");
                l_ErrArea = (Literal)this.Master.FindControl("ListOfErrors");

                l_ErrString.Append("<tr class=\"ErrHeaders\"><th class=\"HeadCodCol\">Codice</th><th class=\"HeadDescCol\">Descrizione</th><tr>");

                foreach (simplex_ORM.Spx_ORMMessage ORMM in MyErrors)
                {
                    if (l_pari == true)
                        l_ErrString.Append("<tr class=\"ErrRowPari\">");
                    else
                        l_ErrString.Append("<tr class=\"ErrRowDisPari\">");
                    l_pari = !l_pari;

                    l_ErrString.Append("<td class=\"ErrCodCol\">");
                    l_ErrString.Append(ORMM.MessageCode.ToString());
                    l_ErrString.Append("</td><td class=\"ErrDescCol\">");
                    switch(p_type)
                    {
                        case(simplex_ORM.Spx_ORMMessage.MESSAGE_TYPE.ERROR_MSG):
                            {
                                l_ErrString.Append("<img src=\"/Immagini/ERROR.png\"><span class=\"msg_error\">ATTENZIONE ERRORI</span>:<br>");
                                break;
                            }
                        case(Spx_ORMMessage.MESSAGE_TYPE.WARNING_MSG):
                                {
                                    l_ErrString.Append("<img src=\"/Immagini/WARN.png\"><span class=\"msg_warn\">ATTENZIONE</span>:<br>");
                                    break;
                                }
                        case(Spx_ORMMessage.MESSAGE_TYPE.INFO_MSG):
                                {
                                    l_ErrString.Append("<img src=\"/Immagini/INFO.png\"><span class=\"msg_ok\">INFORMAZIONI</span>:<br>");
                                    break;
                                }
                        default:
                                {
                                    break;
                                }
                                
                    //if(p_type==MESSAGE_TYPE.ERROR_MSG)
                    //    l_ErrString.Append("<img src=\"/Immagini/ERROR.png\"><span class=\"msg_error\">ATTENZIONE ERRORI</span>:<br>");
                    //if(p_type== MESSAGE_TYPE.WARNING_MSG)
                    //    l_ErrString.Append("<img src=\"/Immagini/WARN.png\"><span class=\"msg_warn\">ATTENZIONE</span>:<br>");
                    //else
                    //    l_ErrString.Append("<img src=\"/Immagini/INFO.png\"><span class=\"msg_ok\">INFORMAZIONI</span>:<br>");
                    }
                    l_ErrString.Append(ORMM.Message);
                    l_ErrString.Append("</td></tr>");
                }
                l_ErrString.Append("</table>");
                l_ErrArea.Text = l_ErrString.ToString();
                MyErrors.Clear(); //<-- ATTENZIONE
            }
        }

        /// <summary>
        /// Mostra i messaggi a video, in testa alla pagina web, rappresentandoli in modo 
        /// diverso secondo tipologia.<br></br>
        /// Sostituisce la vecchia showErrors(), ridenominata showSimpleErrors().
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 08/11/2020: 
        /// </pre>
        /// </summary>
        protected virtual void showErrors()
        {
            // parte nuova
            if (MyErrors.Count > 0)
            {
                Boolean l_pari = false;
                Literal l_ErrArea = null;
                System.Text.StringBuilder l_ErrString = new System.Text.StringBuilder("<table class=\"ErrTab\">");
                l_ErrArea = (Literal)this.Master.FindControl("ListOfErrors");

                l_ErrString.Append("<tr class=\"ErrHeaders\"><th class=\"HeadCodCol\">Codice</th><th class=\"HeadDescCol\">Descrizione</th><tr>");

                foreach (simplex_ORM.Spx_ORMMessage ORMM in MyErrors)
                {
                    if (l_pari == true)
                        l_ErrString.Append("<tr class=\"ErrRowPari\">");
                    else
                        l_ErrString.Append("<tr class=\"ErrRowDisPari\">");
                    l_pari = !l_pari;

                    l_ErrString.Append("<td class=\"ErrCodCol\">");
                    l_ErrString.Append(ORMM.MessageCode.ToString());
                    l_ErrString.Append("</td><td class=\"ErrDescCol\">");
                    switch (ORMM.MessageType)
                    {
                        case ((int)simplex_ORM.Spx_ORMMessage.MESSAGE_TYPE.ERROR_MSG):
                            {
                                // MESSAGE_TYPE.ERROR_MSG
                                l_ErrString.Append("<img src=\"/Immagini/ERROR.png\"><span class=\"msg_error\">ATTENZIONE ERRORI</span>:<br>");
                                break;
                            }
                        case ((int)simplex_ORM.Spx_ORMMessage.MESSAGE_TYPE.WARNING_MSG):
                            {
                                //MESSAGE_TYPE.WARNING_MSG
                                l_ErrString.Append("<img src=\"/Immagini/WARN.png\"><span class=\"msg_warn\">ATTENZIONE</span>:<br>");
                                break;
                            }
                        case ((int)simplex_ORM.Spx_ORMMessage.MESSAGE_TYPE.INFO_MSG):
                            {
                                //MESSAGE_TYPE.INFO_MSG
                                l_ErrString.Append("<img src=\"/Immagini/INFO.png\"><span class=\"msg_ok\">INFORMAZIONI</span>:<br>");
                                break;
                            }
                        case ((int)simplex_ORM.Spx_ORMMessage.MESSAGE_TYPE.OK_MSG):
                            {
                                //MESSAGE_TYPE.INFO_MSG
                                l_ErrString.Append("<img src=\"/Immagini/OK.png\"><span class=\"msg_ok\">OK</span>:<br>");
                                break;
                            }
                        default:
                            {
                                break;
                            }

                        //if(p_type==MESSAGE_TYPE.ERROR_MSG)
                        //    l_ErrString.Append("<img src=\"/Immagini/ERROR.png\"><span class=\"msg_error\">ATTENZIONE ERRORI</span>:<br>");
                        //if(p_type== MESSAGE_TYPE.WARNING_MSG)
                        //    l_ErrString.Append("<img src=\"/Immagini/WARN.png\"><span class=\"msg_warn\">ATTENZIONE</span>:<br>");
                        //else
                        //    l_ErrString.Append("<img src=\"/Immagini/INFO.png\"><span class=\"msg_ok\">INFORMAZIONI</span>:<br>");
                    }
                    l_ErrString.Append(ORMM.Message);
                    l_ErrString.Append("</td></tr>");
                }
                l_ErrString.Append("</table>");
                l_ErrArea.Text = l_ErrString.ToString();
                MyErrors.Clear(); //<-- ATTENZIONE
            }
        }



        /// <summary>
        /// 
        /// </summary>
        /// <param name="p_str"></param>
        /// <param name="p_format"></param>
        /// <returns></returns>
        protected bool isAValidDateString(String p_str, String p_format)
        {
            //casi banali
            if (p_str == null)
                return false;

            if (p_format == null)
                return false;

            Boolean _toRet = true;
            int _len = p_format.Length;
            String _teststr = null;
            if (p_str.Length > _len)
                _teststr = p_str.Substring(0, _len);
            else
                _teststr = p_str;

            try
            {
                /* verifica la sintassi */
                _toRet = simplex_ORM.Column.isDateTime(p_str, p_format);
                if (_toRet == true)
                {
                    /* verifica la semantica */
                    simplex_ORM.Column.parseDateTimeString(p_str, p_format);
                }
            }
            catch (simplex_ORM.Spx_ORMMessage ormm)
            {
                _toRet = false;
            }
            return _toRet;
        }


        /// <summary>
        /// Aggiunge la pagina corrente alla catena delle briciole di pane (SimplexBreadCrumbs)
        /// della maschera corrente.
        /// @MdN
        /// ----
        /// Crtd: 18/01/2015
        /// Mdfd: 25/01/2015
        /// Mdfd: 17/05/2016
        /// </summary>
        /// <remarks>La proprietà MySbc DEVE essere valorizzata.</remarks>
        protected virtual void AddToSimplexBreadCrumbs()
        {
            int _lvl;
            // controllo
            if (MySbc == null)
                return;
            // AGGIUNTA DI UNA VOCE NELLA CATENA BREADCRUMBS
            if (MyMainMenu != null)
            {
                // Se esiste un menu mi pongo il problema della coerenza tra il menu 
                // e la catena di BreadCrumbs.

                _lvl = MasterPCM.getMenuVoiceLevel(MyConn, MyName);             
                
                // Eccezione: la pagina di indice 0 è sempre la root
                if (_lvl == 0)
                {
                    MySbc.InsertAndBreak(MyName, MyAspxFileName, _lvl);         //@MdN 17/05/2016
                    return;
                }
                
                _lvl = MySbc.getPositionOf(MyName);
                if (_lvl == -1)
                    MySbc.Add(MyName, MyAspxFileName);
                else
                    MySbc.InsertAndBreak(MyName, MyAspxFileName, _lvl);         //@MdN 17/05/2015

                //<deleted @MdN: 17/05/2016>
                //else
                //{
                //    if (_lvl > MySbc.Count)
                //        MySbc.Add(MyName, MyAspxFileName);                      //@MdN 25/01/2015
                //    else
                //        MySbc.InsertAndBreak(MyName, MyAspxFileName, _lvl);     //@MdN 25/01/2015
                //}
                //</deleted @MdN: 17/05/2016>
            }
            else
            {
                // Se il menu non esiste non mi pongo problemi ed accodo all'ultimo anello della catena.
                if (MySbc != null)
                    MySbc.Add(MyName, MyAspxFileName);
            }
        }//fine

        /// <summary>
        /// <p>
        /// Imposta gli attributi necessari al funzionamento.
        /// Si attende in Session["APPLICATION"] l'oggetto GeneralApplication corrente.
        /// Se non è presente, lancia un eccezione del tipo generalApplication TracedMessage.
        /// Valorizza:
        /// </p>
        /// <p>
        /// Valorizza i seguenti attributi:
        /// </p>
        /// <ul>
        /// <li>MyApp               :   APPLICAZIONE CORRENTE;</li>
        /// <li>MyConn              :   CONNESSIONE DI DEFAULT;</li>
        /// <li>MyMainMenu          :   CONTROLLO MENU SINISTRO;</li>
        /// <li>MySbc               :   CONTROLLO BREADCRUMBS;</li>
        /// <li>MyAspxFileName      :   NOME DELLA PAGINA WEB/WEB FORM.</li>
        /// <li>MyBackURL           :   URL DELLA PAGINAWEB/WEB FORM CHIAMANTE.</li>
        /// </ul>
        /// <pre>
        /// ------
        /// @MdN
        /// Crtd: 06/12/2014
        /// Mdfd: 30/01/2015
        /// Mdfd: 25/06/2015 test di accesso alla pagina
        /// Mdfd: 26/05/2015 test di sessione attiva
        /// Mdfd: 07/07/2015 - Correzione bug sulla connessione odbc lasciata aperta
        /// Mdfd: 21/11/2015 - Passaggio degli errori attraverso la ViewState
        /// Mdfd: 09/06/2016 - Introdotta redirezione verso pagina d'errore.
        /// Mdfd: 26/12/2016 - Introdotta NoSessionSpxErrorForm.aspx
        /// Mdfd: 08/03/2017 v. 1.0.0.7 - Introdotta parte di codice che mostra la funzione applicativa di appartenenza.
        /// </pre>
        /// </summary>
        protected override void preloadEnvironment()
        {
            base.preloadEnvironment();
            MyApp = (UNIF_Application)Session["APPLICATION"];
            if (MyApp != null)
                MyConn = (System.Data.Odbc.OdbcConnection)MyApp.DefaultConnection;
            else
            {
                //@MdN 26/05/2015: test di sessione attiva
                //Response.Redirect("~/GenericSpxErrorForm.aspx"); //@MdN 09/06/2016
                Response.Redirect("~/NoSessionSpxErrorForm.aspx"); //@MdN 26/12/2016
            }

            /* @MdN: 07/07/2015 - Correzione bug sulla connessione odbc lasciata aperta */
            if (MyConn.State == ConnectionState.Open)
                MyConn.Close();

            // valorizzazione del riferimento al menu principale della maschera
            try
            {
                MyMainMenu = (Menu)this.Master.FindControl("R_MainMenu");
            }
            catch (Exception E)
            {
                MyMainMenu = null;
            }

            // valorizzazione delle briciole di pane
            try
            {
                MySbc = (WebSimplex.WebSimplexBreadCrumbs)this.Master.FindControl("R_BreadCrumbs");
            }
            catch (Exception E)
            {
                MySbc = null;
            }

            //if (this.PreviousPage != null)
            //    MyBackURL = this.PreviousPage.AppRelativeVirtualPath.Substring(this.PreviousPage.AppRelativeVirtualPath.LastIndexOf('/') + 1);
            //else
            //    MyBackURL = (String)Session["CALLING-PAGE"];

            MyAspxFileName = this.Page.AppRelativeVirtualPath.Substring(this.Page.AppRelativeVirtualPath.LastIndexOf("/") + 1);
            //if(IsPostBack==false)
            //    Session["CALLING-PAGE"] = MyAspxFileName;

            ////Scrittura del log
            //if(IsPostBack==false)
            //    writeMessageToLog(MyConn, "Form_Load()", MySbc.ToString(), ((UNIF_User)MyApp.CurrentUser).UID.ToString(), Session.SessionID);

            // Determina la visibilità del MessageBox
            if (Session["GenericSpxForm1.Panel_MessageBox.Visible"] != null)
                MyMessageBoxVisible = (Boolean)Session["GenericSpxForm1.Panel_MessageBox.Visible"];
            else
                MyMessageBoxVisible = false;
            Session["GenericSpxForm1.Panel_MessageBox.Visible"] = null;

            /* @MdN: 18/06/2015 test di accesso alla pagina  */
            //if (((UNIF_Application)MyApp).verifyAccess((UNIF_User)MyApp.CurrentUser) == false)            
            //{
            //    Session["SpxErrorMessage"] = "Utente non abilitato all'accesso alla funzione";
            //    Response.Redirect("~/GenericSpxErrorForm.aspx");
            //}
            /* @MdN: 25/06/2015 test di accesso alla pagina  */

            #region GESTIONE DELLA TABELLA ASSOCIATA
            //Eventuale tabella associata
            MyName = this.Title;

            if (Session[MyName] != null)
                TBL_UNDER_FORM = (simplex_ORM.SQLSrv.SQLTable)Session[MyName];
            else
            {
                try
                {
                    TBL_UNDER_FORM = simplex_ORM.SQLSrv.SQLTable.createSQLTable(MyConn, null);  //<-- TO CHANGE
                    Session[MyName] = TBL_UNDER_FORM;
                }
                catch (simplex_ORM.Spx_ORMMessage ORMM)
                {
                    if (ORMM.MessageCode == 1 && MyConn != null)
                        TBL_UNDER_FORM = null;
                    else
                        MyErrors.Add(ORMM);
                }
            }

            // MyAspxFileName = "~/TO_CHANGE/" + MyAspxFileName;                                //<-- TO CHANGE
            MyName = this.Title;
            AddToSimplexBreadCrumbs();            
            #endregion

            /*
             * @MdN 18/10/2015: BUG Sul controllo dell'accesso.
             * Il controllo dell'accesso non funzionava perchè veniva eseguito sulla funzione applicativa 
             * corrente, non su quella a cui la pagina corrente appartiene. Il risultato del controllo, quindi,
             * era sempre positivo!!!
             * 
             * Si è sviluppato un nuovo metodo protetto (virtuale) della pagina verifyAccess() che verifica
             * se l'utente corrente può accedere alla pagina!
             * 
             *
             MyFunzioneApplicativa = ((UNIF_Application)MyApp).Name;
            
             if (MyFunzioneApplicativa != null)
             {
                if (((UNIF_User)MyApp.CurrentUser).verifyAccess(MyFunzioneApplicativa) == false)
                {
                    Session["SpxErrorMessage"] = "Utente non abilitato all'accesso alla funzione";
                    Response.Redirect("~/GenericSpxErrorForm.aspx");
                }
             }
             * Tale metodo deve essere invocato fuori dal Postback
            */

            #region GESTIONE FUNZIONE APPLICATIVA E CONTOLLO ACCESSI

            MyFunzioneApplicativa = ((UNIF_Application)MyApp).Name;
            if (IsPostBack == false)
            {
                if (verifyAccess() == false)
                {
                    Session["SpxErrorMessage"] = "Utente non abilitato all'accesso alla funzione";
                    Response.Redirect("~/ForbiddenAccessSpxErrorForm.aspx");
                }
            }

            #endregion

            #region MOSTRA EVENTUALE NOME FUNZIONE APPLICATIVA
            //<added @MdN: 08/03/2017>
            System.Web.UI.WebControls.Label _lbl=null;
            String _fa = null;
            _fa = FunzioneApplicativa;
            _lbl = (System.Web.UI.WebControls.Label)this.Master.Page.Form.FindControl("R_InfoFunzioneApplicatifa");
            if (_lbl != null && _fa!=null)
            {
                _lbl.Text = _fa;
            }
            //</added @MdN: 08/03/2017>
            #endregion

            #region GESTIONE ERRORI E MESSAGGI
            // <added @MdN: 21/11/2015 - Prendo gli errori>
            MyErrors = (System.Collections.Generic.List<simplex_ORM.Spx_ORMMessage>)ViewState["MYERRORS"];
            ViewState["MYERRORS"]=null;
            if(MyErrors==null)
                MyErrors = new System.Collections.Generic.List<simplex_ORM.Spx_ORMMessage>();
            // </added @MdN: 21/11/2015 - Prendo gli errori>

            //<added @MdN: 05/12/2016. Prendo gli errori anche dalla sesssione e ne faccio il merge con gli eventuali presenti in sessione>
            if (Session[MyName + "_MYERRORS"] != null && ((System.Collections.Generic.List<simplex_ORM.Spx_ORMMessage>)Session[MyName + "_MYERRORS"]).Count > 0)
            {
                foreach (simplex_ORM.Spx_ORMMessage _err in ((System.Collections.Generic.List<simplex_ORM.Spx_ORMMessage>)Session[MyName + "_MYERRORS"]))
                    MyErrors.Add(_err);
            }
            Session[MyName + "_MYERRORS"] = null;
            //</added @MdN: 05/12/2016. Prendo gli errori anche dalla sesssione e ne faccio il merge con gli eventuali presenti in sessione>
            #endregion

        }//fine preloadEnvironment        

        /// <summary>
        /// Verifica se l'utente può avere accesso alla pagina corrente.
        /// <remarks>
        /// Verifica se il flag di bypass 'MyBypassVerifyAccess' è impostato.
        /// </remarks>
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 18/10/2015
        /// Mdfd: 25/05/2016 - diventa virtual!
        /// </pre>
        /// </summary>
        /// <returns>True se l'utente può avere accesso, False altrimenti.</returns>
        protected virtual Boolean verifyAccess()
        {
            String _funzAppl = null;
            String _sql = null;
            System.Data.Odbc.OdbcCommand _cmd = null;

            if (MyBypassVerifyAccess == true)
                return true;

            // Controllo preliminare
            if (MyApp == null || MyApp.CurrentUser == null || MyConn == null)
                return false;

            // Ricava la funzione applicativa di appartenenza dal nome (che deve essere uguale 
            _sql = @"select FA.DENOMINAZIONE
                    from dbo.ADMN_FunzioneApplicativa FA INNER JOIN dbo.UTILS_MENU UM
                    ON FA.ID=UM.FUNZIONE_APPLICATIVA
                    WHERE FA.DATA_DEL IS NULL AND UM.DESCRIZIONE = '" + MyName.Replace("'","''") + "'";
     
            if (MyConn.State == ConnectionState.Open)
                MyConn.Close();
            if (MyConn.State == ConnectionState.Closed)
                MyConn.Open();
            _cmd = MyConn.CreateCommand();
            _cmd.CommandText = _sql;
            try
            {
                _funzAppl = (String)_cmd.ExecuteScalar();
                MyConn.Close();
            }
            catch(Exception E)
            {
                simplex_ORM.Spx_ORMMessage ORMM = new simplex_ORM.Spx_ORMMessage(E);
                ORMM.traceLog(MyConn, MyApp.CurrentUser.UID.ToString(), Session.SessionID);
                return false;
            }
            //Verifica vera e propria
            return (MyApp.CurrentUser.verifyAccess(_funzAppl));
        }

        /// <summary>
        /// Agisce modificando l'attributo chiave dell'oggetto SQLTable passato per argomento nel caso sia la chiave primaria
        /// di tipo non autoincremntante (Non Identity).<br></br>
        /// Introdotto solo per gestire transazioni verso database legacy o di dubbia progettazione.<br></br>
        /// <remarks>Usare con cautela! Possibili conflitti di chiave in quanto il metodo non effettua controlli di concorrenza 
        /// né verifica che effettivamente l'attributo chiave NON sia un'identuty.</remarks>
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 27/04/2018 Monterotondo
        /// </pre>
        /// </summary>
        /// <exception cref="simplex_ORM.Spx_ORMMessage">(1) Se il parametro passato è nullo; (2) in caso di errore di interrogazione del database.</exception>
        protected virtual void incrementNotIdentity(simplex_ORM.SQLSrv.SQLTable p_Table)
        {
            if (p_Table != null && MyConn != null)
            {
                #region Valorizzazione del campo chiave non autoincrementante (non identity)
                // Controllo Interno di sicurezza
                if (MyGetMaxKeyValue == true && p_Table.PrimaryKeyColumns.Count == 1)
                {
                    String _SingleKey = p_Table.PrimaryKeyColumns[0];
                    if (p_Table.getValue(_SingleKey) == null) // NUOVO INSERIMENTO?
                    {
                        Int32 _MaxKeyValue = 0;
                        System.Data.Odbc.OdbcCommand _cmd = null;
                        try
                        {
                            if (MyConn != null)
                            {
                                if (MyConn.State == ConnectionState.Closed)
                                    MyConn.Open();
                                _cmd = MyConn.CreateCommand();
                                _cmd.CommandText = "select max(" + _SingleKey + ") + 1 from " + p_Table.Name;
                                _MaxKeyValue = (Int32)_cmd.ExecuteScalar();
                                // Imposto il valore di chiave
                                p_Table.setValue(_SingleKey, _MaxKeyValue.ToString());
                            }
                        }
                        catch (simplex_ORM.Spx_ORMMessage _ORMM)
                        {
                            _ORMM.Source = _ORMM.Source + "-->" + "GenericSpxForm.incrementNotIdentity()";
                            _ORMM.MessageCode = 0;
                            _ORMM.MessageType = 1;
                            if (MyApp.CurrentUser != null && MyApp.CurrentUser != null && MyConn != null && Session != null)
                                _ORMM.traceLog(MyConn, ((UNIF_User)MyApp.CurrentUser).UID.ToString(), Session.SessionID);
                            throw _ORMM;
                        }
                        catch (Exception _e)
                        {
                            simplex_ORM.Spx_ORMMessage _ORMM = new simplex_ORM.Spx_ORMMessage(_e);
                            _ORMM.MessageCode = 0;
                            _ORMM.MessageType = 1;
                            _ORMM.MessageField = _e.Message;
                            if (MyApp.CurrentUser != null && MyApp.CurrentUser != null && MyConn != null && Session != null)
                                _ORMM.traceLog(MyConn, ((UNIF_User)MyApp.CurrentUser).UID.ToString(), Session.SessionID);
                        }
                        finally
                        {
                            if (MyConn.State == ConnectionState.Open)
                                MyConn.Close();
                            _cmd.Dispose();
                        }
                    }
                }
                #endregion
            }
            else
            {
                simplex_ORM.Spx_ORMMessage _ORMM = new simplex_ORM.Spx_ORMMessage("Il parametro SQLTable passato è null.");
                _ORMM.MessageCode = 0;
                _ORMM.MessageType = 1;
                _ORMM.Source = "GenericSpxForm.incrementNotIdentity()";
                if (MyApp.CurrentUser != null && MyApp.CurrentUser != null && MyConn != null && Session != null)
                    _ORMM.traceLog(MyConn, ((UNIF_User)MyApp.CurrentUser).UID.ToString(), Session.SessionID);
                throw _ORMM;
            }// fine if principale
        }

        /// <summary>
        /// Agisce modificando l'attributo chiave dell'oggetto memorizzato in TBL_UNDER_FORM nel caso sia la chiave primaria
        /// di tipo non autoincremntante (Non Identity).<br></br>
        /// Introdotto solo per gestire transazioni verso database legacy o di dubbia progettazione.<br></br>
        /// <remarks>Usare con cautela! Possibili conflitti di chiave in quanto il metodo non effettua controlli di concorrenza 
        /// né verifica che effettivamente l'attributo chiave NON sia un'identuty.</remarks>
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 27/04/2018 Monterotondo
        /// </pre>
        /// </summary>
        /// <exception cref="simplex_ORM.Spx_ORMMessage">(1) Se il parametro passato è nullo; (2) in caso di errore di interrogazione del database.</exception>
        protected virtual void incrementNotIdentity()
        {
            incrementNotIdentity(TBL_UNDER_FORM);
        }

        /// <summary>
        /// Cancella FISICAMENTE l'oggetto referenziato dalla maschera.
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 29/04/2018 - Deriva per fattorizzazione da GenericAssociativeSpxForm, creata il 01/02/2017.
        /// </pre>
        /// </summary>
        /// <exception cref="simplex_ORM.Spx_ORMMessage">Lancia un'eccezione in caso di errori di esecuzione della query di cancellazione.</exception>
        protected virtual void physicalDelete()
        {
            //WRITE YOUR CODE FROM HERE
            if (TBL_UNDER_FORM != null && MyApp != null && MyApp.CurrentUser != null && MyPhysicalDeletion == true)
            {
                // <added @MdN: 29/04/2018 <-- 01/02/2017 - CANCELLAZIONE FISICA>
                System.Data.Odbc.OdbcCommand _cmd = null;
                if (TBL_UNDER_FORM.CurrentConnection != null && TBL_UNDER_FORM.CurrentConnection.State == ConnectionState.Closed)
                    TBL_UNDER_FORM.CurrentConnection.Open();
                _cmd = TBL_UNDER_FORM.CurrentConnection.CreateCommand();
                _cmd.CommandText = "DELETE FROM " + TBL_UNDER_FORM.Name + " WHERE ";
                String _filter = null;
                foreach (String _s in TBL_UNDER_FORM.PrimaryKeyColumns)
                {
                    if (_filter != null)
                        _filter = _filter + " AND ";
                    _filter = _filter + _s + "=";
                    // Se stringa - apice di apertura
                    if (TBL_UNDER_FORM.isTextField(_s) == true)
                        _filter = _filter + "'";
                    _filter = _filter + TBL_UNDER_FORM.getValue(_s);
                    // Se stringa - apice di chiusura
                    if (TBL_UNDER_FORM.isTextField(_s) == true)
                        _filter = _filter + "'";
                }
                _cmd.CommandText = _cmd.CommandText + _filter;
                // pronta la query: ESECUZIONE
                try
                {
                    _cmd.ExecuteNonQuery();
                }
                catch (Exception E)
                {
                    simplex_ORM.Spx_ORMMessage ORMM = new simplex_ORM.Spx_ORMMessage("ERRORE DI CANCELLAZIONE NELL'ESECUZIONE DELLA SEQUENTE QUERY: " + _cmd.CommandText + ". Il server ha risposto: " + E.Message);
                    if (MyConn != null && MyApp != null && MyApp.CurrentUser != null && Session.SessionID != null)
                        ORMM.traceLog(MyConn, MyApp.CurrentUser.UID.ToString(), Session.SessionID);
                    ORMM.Message = ORMM.Message = "<img src=\"../Immagini/ERROR.png\" alt=\"ERRORE\">&nbsp;<span class=\"MSG_ERR\">ERRORE:</span><br/>Errore generico nella cancellazione FISICA dell'oggetto. Vedi registro dei log." + "<br>";
                    MyErrors.Add(ORMM);
                    Session[MyName + "_MYERRORS"] = MyErrors;
                }
                finally
                {
                    if (TBL_UNDER_FORM.CurrentConnection.State == ConnectionState.Open)
                        TBL_UNDER_FORM.CurrentConnection.Close();
                    _cmd.Dispose();
                }
                //</added>
            }
        }

        /// <summary>
        /// Cancella LOGICAMENTE l'oggetto scrivendo una data nella colonna dichiarata nell'attributo MyDataDelColumn.<br></br>
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 01/05/2018
        /// </pre>
        /// </summary>
        /// <exception cref="simplex_ORM.Spx_ORMMessage">Lancia un'eccezione in caso di errori di esecuzione della query di cancellazione.</exception>
        protected virtual void logicalDelete()
        {
            String _justNow = null;

            if (TBL_UNDER_FORM != null && MyApp != null && MyApp.CurrentUser != null && MyDataDelColumn!=null)
            {
                if (MyPhysicalDeletion == false)
                {

                    _justNow = simplex_ORM.Column.DateToSQLDateString(DateTime.Now);
                    TBL_UNDER_FORM.setValue(MyDataDelColumn, _justNow);
                    TBL_UNDER_FORM.setValue(MyUserDelColumn, MyApp.CurrentUser.UID.ToString());

                    // impostati gli attributi con le necessarie informazioni, apportiamo le modifiche sul database
                    try
                    {
                        TBL_UNDER_FORM.save(true);
                    }
                    catch (simplex_ORM.Spx_ORMMessage _ormm)
                    {
                        _ormm.Source = "GenericSpxForm.logicalDelete()-->" + _ormm.Source;
                        if (MyConn != null && Session != null)
                            _ormm.traceLog(MyConn, MyApp.CurrentUser.UID.ToString(), Session.SessionID);
                        throw _ormm;
                    }
                    catch (Exception _e)
                    {
                        simplex_ORM.Spx_ORMMessage _ormm = new simplex_ORM.Spx_ORMMessage(_e);
                        _ormm.MessageField = _e.Message;
                        _ormm.MessageType = (int)Spx_ORMMessage.MESSAGE_TYPE.ERROR_MSG;
                        _ormm.Source = "GenericSpxForm.logicalDelete()-->" + _e.Source;
                        if (MyConn != null && Session != null)
                            _ormm.traceLog(MyConn, MyApp.CurrentUser.UID.ToString(), Session.SessionID);
                        throw _ormm;
                    }
                } // fine test (if) di attivazione dell'opzione di cancellazione 
            } // fine test (if) principale
        } // fine logicalDelete()


        /// <summary>
        /// Inserisce nell'oggetto di riferimento dalla maschera (TBL_UNDER_FORM) le informazioni di servizio che 
        /// sono contenute nei seguenti attributi:<br></br>
        /// <ul>
        /// <li>MyDataSys (DATA_SYS): da scrivere EVENTUALMENTE (vedi MyAutoDataSys) solo in fase di creazione del record.</li>
        /// <li>MyUsrSys (USR): da scrivere solo in fase di creazione del record</li>
        /// <li>MyDataUM (DATA_UM): da scrivere sempre</li>
        /// <li>MyUsrUM (USR_UM): da sccrivere sempre</li>
        /// </ul>
        /// <br></br>
        /// In casi di informazioni di servizio diverse, effettuare l'override del metodo.<br></br>
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 22/07/2016 per fattorizzazione (refactoring) da R_Salva_Click
        /// Mdfd: 01/05/2018 Refactoring da GenericAssociativeSpxForm.
        /// </pre>
        /// </summary>
        protected virtual void setServiceInfos()
        {
            if (TBL_UNDER_FORM != null && MyApp!=null && MyApp.CurrentUser != null && MySaveInfos==true)
            {                
                String _JustNow=simplex_ORM.Column.DateToSQLDateString(DateTime.Now);
                String _usr = null;
                String _usrum = null;
                String _datasys = null;
                String _dataum = null;

                // * Controllo: scrive le informazioni di servizio SOLO SE nella tabella 
                //              di destinazione sono contemplate
                foreach(String _s in TBL_UNDER_FORM.SQLColumns)
                {
                    if(_s.CompareTo(MyDataSys)==0)
                        _datasys = MyDataSys;
                    if(_s.CompareTo(MyDataUM)==0)
                        _dataum = MyDataUM;
                    if(_s.CompareTo(MyUsrUM)==0)
                        _usrum = MyUsrUM;
                    if(_s.CompareTo(MyUsrSys)==0)
                        _usr = MyUsrSys;
                }
                // Le informazioni di ultima modifica vanno sempre scritte
                if(_dataum!=null)
                    TBL_UNDER_FORM.setValue(_dataum, _JustNow);
                if(_usrum!=null)
                    TBL_UNDER_FORM.setValue(_usrum, MyApp.CurrentUser.UID.ToString());

                // Informazioni sulla data di creazione: solo se il record NON è ancora stato scritto e
                // MyAutoDataSys è falso, cioè la colonna destinata a raccogliere la data di sistema non
                // ha valore di default.
                try                                                                             
                {   
                    if(_datasys!=null && MyAutoDataSys==false)
                    {
                        if (TBL_UNDER_FORM.getValue(_datasys) == null)
                        {
                            TBL_UNDER_FORM.setValue(_datasys, _JustNow);
                        }
                    }                
                }
                catch (simplex_ORM.Spx_ORMMessage ORMM)
                {
                    if(MyConn!=null && MyApp!=null && MyApp.CurrentUser!=null && Session!=null)
                    ORMM.traceLog(MyConn, ((UNIF_User)MyApp.CurrentUser).UID.ToString(), Session.SessionID);

                    MyErrors.Add(ORMM);
                    if (MyErrors.Count > 0)
                        showErrors();
                    return;
                }                                                                              

                // Informazioni sull'utente di creazione del record: da inserire SOLO in fase di creazione del record.
                if (_usr != null)
                    if(TBL_UNDER_FORM.getValue(_usr) == null)
                        TBL_UNDER_FORM.setValue(_usr, MyApp.CurrentUser.UID.ToString());
            }
        }// fine setServiceInfos


        #endregion

        #region GESTORI

        protected virtual void Page_Load(object sender, EventArgs e)
        {
            preloadEnvironment();

            // Pulizia dell'area degli errori: @MdN fattorizzato in data 07/09/2015
            System.Web.UI.WebControls.Literal l_ErrArea;
            l_ErrArea = (Literal)this.Master.FindControl("ListOfErrors");
            l_ErrArea.Text = null;

            // <%WRITE YOUR CODE FROM HERE%>

            // <%WRITE YOUR CODE TO HERE%>

            // Evento del BOX dei messaggi
            if (Session["GenericSpxForm1.R_Conferma.Click"] != null)
            {
                /* Parte del codice necessaria ad impostare il più opportuno gestore dell'evento
                 * Click del tasto R_Conferma del MessageBox.
                 * Vedere la documentazione a corredo.
                 */
                // <%WRITE YOUR CODE FROM HERE%>

                // <%WRITE YOUR CODE TO HERE%>
            }

            //@MdN 15/03/215
            if (MyErrors.Count > 0)
                showErrors();
            else
                ((Literal)this.Master.FindControl("ListOfErrors")).Text = "";

            //Scrittura del log:
            //Adattare questa funzione alle singole fattispecie
            if (IsPostBack == false)
                writeMessageToLog(MyConn, "GenericSpxForm.Form_Load()", MySbc.ToString(), ((UNIF_User)(MyApp.CurrentUser)).UID.ToString(), Session.SessionID);

        }

        /// <summary>
        /// Mostra l'oggetto SQLTable che supporta la maschera.
        /// ---
        /// @MdN
        /// Crtd: 01/01/2015
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected virtual void Page_SaveStateComplete(object sender, EventArgs e)
        {
            if (TBL_UNDER_FORM != null)
                this.show(TBL_UNDER_FORM, "ContentPlaceHolder1", "System.Web.UI.WebControls.ContentPlaceHolder");
        }

        /// <summary>
        /// Gestore dell'evento Click sul tasto "Annulla" della MessageBox
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 08/03/2015
        /// </pre>
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected virtual void R_Annulla_Click(object sender, EventArgs e)
        {
            MyMessageBoxVisible = false;
            Session["GenericSpxForm1.Panel_MessageBox.Visible"] = MyMessageBoxVisible;
            //Panel_MessageBox.Visible = MyMessageBoxVisible; //OBSOLETO 
            Session["GenericSpxForm1.R_Conferma.Click"] = null;
        }

        protected virtual void R_Back_Click(object sender, EventArgs e)
        {
            if(MySbc.PreviousLink!=null)
                Response.Redirect(MySbc.PreviousLink);
        }

        /// <summary>
        /// Gestore dell'evento di pressione del tasto R_Salva() a cui è generalmente associata l'azione di 
        /// salvataggio dell'oggetto TBL_UNDER_FORM.<br></br>
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 27/04/2018 - Monterotondo
        /// </pre>
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected virtual void R_Salva_Click(object sender, EventArgs e)
        {
            #region [CAMBIARE QUI']: COPIA I VALORI DELLA MASCHERA - AGGIORNAMENTO DELLO STATO DELL'OGGETTO
            copyValues(TBL_UNDER_FORM, "ContentPlaceHolder1", "System.Web.UI.WebControls.ContentPlaceHolder");
            #endregion

            #region [OPZIONALE]: Eventuale valorizzazione del campo chiave non autoincrementante (non identity) - <mdfd @MdN: 04/05/2017>
            if (MyGetMaxKeyValue == true && TBL_UNDER_FORM.PrimaryKeyColumns.Count == 1)
            {
                try
                {
                    incrementNotIdentity();
                }
                catch (simplex_ORM.Spx_ORMMessage _ormm)
                {
                    _ormm.MessageField = "Impossibile salvare: " + _ormm.MessageField;
                    _ormm.Source = "(" + MyName + ").R_Salva_Click()--> " + _ormm.Source;
                    MyErrors.Add(_ormm);
                    if (MyErrors.Count > 0)
                        showErrors();
                    return;
                }

                #region DA CANCELLARE NELLE VERSIONI SUCCESSIVE ALLA v.1.0.2.4
                //String _SingleKey = TBL_UNDER_FORM.PrimaryKeyColumns[0];
                //if (TBL_UNDER_FORM.getValue(_SingleKey) == null) // NUOVO INSERIMENTO?
                //{
                //    Int32 _MaxKeyValue = 0;
                //    System.Data.Odbc.OdbcCommand _cmd = null;
                //    try
                //    {
                //        if (MyConn != null)
                //        {
                //            if (MyConn.State == ConnectionState.Closed)
                //                MyConn.Open();
                //            _cmd = MyConn.CreateCommand();
                //            _cmd.CommandText = "select max(" + _SingleKey + ") + 1 from " + TBL_UNDER_FORM.Name;
                //            _MaxKeyValue = (Int32)_cmd.ExecuteScalar();
                //            // Imposto il valore di chiave
                //            TBL_UNDER_FORM.setValue(_SingleKey, _MaxKeyValue.ToString());
                //        }
                //    }
                //    catch (simplex_ORM.Spx_ORMMessage _ORMM)
                //    {
                //        if (MyApp.CurrentUser != null && MyApp.CurrentUser != null && MyConn != null && Session != null)
                //            _ORMM.traceLog(MyConn, ((UNIF_User)MyApp.CurrentUser).UID.ToString(), Session.SessionID);
                //        MyErrors.Add(_ORMM);
                //        if (MyErrors.Count > 0)
                //            showErrors();
                //        return;
                //    }
                //    catch (Exception _e)
                //    {
                //        simplex_ORM.Spx_ORMMessage _ORMM = new simplex_ORM.Spx_ORMMessage(_e);
                //        _ORMM.MessageCode = 0;
                //        _ORMM.MessageType = 1;
                //        if (MyApp.CurrentUser != null && MyApp.CurrentUser != null && MyConn != null && Session != null)
                //            _ORMM.traceLog(MyConn, ((UNIF_User)MyApp.CurrentUser).UID.ToString(), Session.SessionID);
                //        MyErrors.Add(_ORMM);
                //        if (MyErrors.Count > 0)
                //            showErrors();
                //        return;
                //    }
                //    finally
                //    {
                //        if (MyConn.State == ConnectionState.Open)
                //            MyConn.Close();
                //        _cmd.Dispose();
                //    }
                //}
                #endregion
            }
            #endregion

            #region [OPZIONALE]: Eventuale impostazione delle informazioni di servizio (MySaveInfos == true).
            if (TBL_UNDER_FORM != null && MyApp != null && MySaveInfos == true)
            {
                setServiceInfos();
            }
            #endregion

            #region SALVATAGGIO DELL'OGGETTO
            try
            {
                TBL_UNDER_FORM.save(true);
            }
            catch (simplex_ORM.Spx_ORMMessage _ormm)
            {
                _ormm.Source = "GenericSpxForm.R_Sava_Click()-->" + _ormm.Source;
                if (MyConn != null && MyApp != null && MyApp.CurrentUser != null && Session != null)
                    _ormm.traceLog(MyConn, MyApp.CurrentUser.UID.ToString(), Session.SessionID);
            }
            #endregion
        }

        /// <summary>
        /// Elimina l'oggetto corrente in modalità Fisica o Logica secondo il valore dell'attributo MyPhysicalDeletion.<br></br>
        /// Al termine annulla l'oggetto TBL_UNDER_FORM e Session[MyName].
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 06/05/2018 Monterotondo
        /// </pre>
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected virtual void R_Elimina_Click(object sender, EventArgs e)
        {
            /** IL COMANDO AGISCE IN DUE FASI:
            * Verifica il controllo sender
             * 1) Se il sender è il bottone "R_Elimina" -- FASE 1: VISUALIZZA IL MESSAGE BOX E VALORIZZA IL DELEGATO DEL TASTO OK
             * 2) Se il sender è il controllo WebSimplexMessageBox "R_MessageBox" -- FASE 2: EFFETTUA L'OPERAZIONE.
             * **/
            if (sender.GetType().ToString().CompareTo("System.Web.UI.WebControls.Button") == 0)
            {
                // FASE 1:
                Session["Evt_OkButton"] = new EventHandler(this.R_Elimina_Click);
                this.R_MessageBox.show((int)WebSimplexMessageBox.BoxType.OkMessage);
                R_MessageBox.Visible = true;
                return;
            }
            else
            {
                String _tipoAzione = null;
                //Azione
                if (MyPhysicalDeletion == true)
                {
                    try
                    {
                        physicalDelete();
                        _tipoAzione = "fisica";
                    }
                    catch (simplex_ORM.Spx_ORMMessage _ormm)
                    {
                        _ormm.Source = "[" + MyName + "].R_Elimina_Click(" + _tipoAzione + ")-->" + _ormm.Source;
                        if (MyConn != null && MyApp != null && MyApp.CurrentUser != null && Session != null)
                            _ormm.traceLog(MyConn, MyApp.CurrentUser.UID.ToString(), Session.SessionID);
                        MyErrors.Add(_ormm);
                        showErrors(Spx_ORMMessage.MESSAGE_TYPE.ERROR_MSG);
                        return;
                    }
                }
                else
                {
                    try
                    {
                        logicalDelete();
                        _tipoAzione = "logica";
                    }
                    catch (simplex_ORM.Spx_ORMMessage _ormm)
                    {
                        _ormm.Source = "[" + MyName + "].R_Elimina_Click(" + _tipoAzione + ")-->" + _ormm.Source;
                        if (MyConn != null && MyApp != null && MyApp.CurrentUser != null && Session != null)
                            _ormm.traceLog(MyConn, MyApp.CurrentUser.UID.ToString(), Session.SessionID);
                        MyErrors.Add(_ormm);
                        showErrors(Spx_ORMMessage.MESSAGE_TYPE.ERROR_MSG);
                        return;
                    }
                }

                // Azioni conclusive
                TBL_UNDER_FORM = null;
                Session[MyName] = null;
                if (MyConn != null && MyApp != null && MyApp.CurrentUser != null && Session != null)
                    writeMessageToLog(MyConn, "Cancellazione " + _tipoAzione + " avvenuta con successo.", "GenericSpxForm.R_Elimina_Click()", MyApp.CurrentUser.UID.ToString(), Session.SessionID);
            }
        }// fine R_Elimina_Click();

        #endregion

        #region PROPERTIES
        /// <summary>
        /// Nome della pagina visualizzato nei menu e nelle briciole di pane.
        /// @MdN
        /// ----
        /// Crtd: 18/01/2015
        /// </summary>
        public virtual String Nome
        {
            get
            {
                return MyName;
            }
            set
            {
                if (value == null)
                    MyName = "### DA DEFINIRE";
                else
                    MyName = value;
            }
        }

        /// <summary>
        /// Restituisce la funzione applicativa di appartenenza della maschera corrente, secondo quanto impostato in UTILS_MENU e in ADMN_FunzioneApplicativa.<br></br>
        /// In caso di errore restituisce null ed aggiunge un messaggio nella coda MyErrors.
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 08/03/2017 - v. 1.0.0.7
        /// </pre>
        /// </summary>
        public String FunzioneApplicativa
        {
            get
            {
                String _funzAppl = null;
                String _sql = null;
                System.Data.Odbc.OdbcCommand _cmd = null;

                // Controllo preliminare
                if (MyApp == null || MyApp.CurrentUser == null || MyConn == null)
                    return null;

                if (MyName == null)
                    return null;

                // Ricava la funzione applicativa di appartenenza dal nome corrente della maschera
                _sql = @"select FA.DENOMINAZIONE
                    from dbo.ADMN_FunzioneApplicativa FA INNER JOIN dbo.UTILS_MENU UM
                    ON FA.ID=UM.FUNZIONE_APPLICATIVA
                    WHERE FA.DATA_DEL IS NULL AND UM.DESCRIZIONE = '" + MyName.Replace("'", "''") + "'";

                if (MyConn.State == ConnectionState.Open)
                    MyConn.Close();
                if (MyConn.State == ConnectionState.Closed)
                    MyConn.Open();
                _cmd = MyConn.CreateCommand();
                _cmd.CommandText = _sql;
                try
                {
                    _funzAppl = (String)_cmd.ExecuteScalar();
                    MyConn.Close();
                }
                catch (Exception E)
                {
                    simplex_ORM.Spx_ORMMessage ORMM = new simplex_ORM.Spx_ORMMessage(E);
                    ORMM.traceLog(MyConn, MyApp.CurrentUser.UID.ToString(), Session.SessionID);
                    MyErrors.Add(ORMM);
                    return null;
                }
                //restituisce il nome
                return _funzAppl;
            }
        }
        #endregion

    }
}
