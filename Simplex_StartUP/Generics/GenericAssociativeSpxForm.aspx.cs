using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using System.Text;
using GeneralApplication;

namespace SIMPLEX_STARTUP
{
    /// <summary>
    /// <p>
    /// Maschera che consente l'associazione di una collezione di oggetti di una classe, detta CLASSE ASSOCIATA,
    /// dall'istanza di una classe detta CLASSE PRINCIPALE.
    /// In pratica si può realizzare una maschera per popolare dati:
    /// <ul>
    /// <li>in associazione 1-N tra la classe PRINCIPALE e la classe ASSOCIATA.</li>
    /// <li>in associazione N-N tra la classe PRINCIPALE ed un'altra classe attraverso la classe ASSOCIATA.</li>
    /// </ul>
    /// 
    /// </p>
    /// <p>
    /// La maschera memorizza l'oggetto della classe ASSOCIATA nell'attributo protetto TBL_UNDER_FORM e 
    /// l'oggetto della classe PRINCIPALE nell'attributo protetto MAIN_UNDER_FORM.
    /// </p>
    /// Derivata da GenericLookupSpxForm:GenericDynamicSpxForm:GenericSpxForm:SimplexForm.
    /// Ha attributi protetti necessari per il funzionamento dell'applicazione.
    /// Esegue Override dei seguenti metodi:
    /// <pre>
    /// - preloadEnvironment()
    /// 
    /// variabili di stato impiegate:
    /// - Session["MyName"]:                            Contiene l'oggetto di classe SQLTable TBL_UNDER_FORM associato alla maschera;
    /// - Session[MyName + "_MYCALLERNAME"]:            Contiene il nome dell'oggetto proveniente dalla della maschera. Una volta usato, lo stato deve essere cancellato.
    /// - Session[Session[MyName + "_MYCALLERNAME"]]:   Contiene l'oggetto di classe SQLTable TBL_UNDER_FORM della maschera chiamante, ossia MAIN_UNDER_FORM.
    ///
    /// - ViewState["MYSQL"]:               Parte proiettiva della query di selezione della lista dei risultati
    /// - ViewState["MYFILTER"]:            Parte selettiva della query di selezione della lista dei risultati
    /// - ViewState["MYORDER"]:             Criterio di ordinamento della query di selezione della lista dei risultati  
    /// - ViewState["MYCONNSTRING"]:        Stringa di connessione per il controllo SqlDataSource della GridView R_Lista
    /// 
    /// ------
    /// @MdN 
    /// Crtd: 10/10/2015
    /// Mdfd: 15/10/2015 - eliminata da drawDynamicForm() la seconda chiamata a designTabs() perchè già invocata nella invocazione base.drawDynamicForm()
    /// Mdfd: 21/06/2016 - Ristrutturazione v 1.0.0.1
    /// Mdfd: 08/07/2016 - Page_Init() divenuta virtual - v. 1.0.0.2.
    /// Mdfd: 26/07/2016 - Controllo preliminare (*) su TBL_UNDER_FORM - (versione invariata) v.1.0.0.2.
    /// Mdfd: 04/08/2016 - Migliorata la composizione della query della GridView - (versione invariata) v.1.0.0.2.
    /// </pre>
    /// <remarks>TODO: GENERALIZZARE E PORTARE IN WebSimplexFormLibrary.</remarks>
    /// </summary>
    public partial class GenericAssociativeSpxForm : GenericDynamicSpxForm //GenericDynamicSpxForm
    {
        #region ATTRIBUTI PROTETTI DA DEFINIRE (TO CHANGE)
        /// <summary>
        /// <p>
        /// Path del file XML contenente la definizione dei controlli da inserire dinamicamente.
        /// </p>
        /// </summary>
        protected String MyControlsXmlFilePath = null;                                            //<- TO CHANGE
        /// <summary>
        /// Nome della tabella di riferimento della maschera dinamica.
        /// </summary>
        protected String MyTableName = "<%MyTableName%>";                                                      //test <- TO CHANGE
        /// <summary>
        /// Parte proiettiva della query di selezione della lista dei risultati (GridView): in pratica SELECT FIELD1, FIELD2, ...., FIELDN FROM TABLE
        /// senza la clausola WHERE di filtraggio e le condizioni di filtraggio.
        /// Se null, la parte proiettiva viene determinata a partire dall'oggetto simplex_ORM.SqlSrv.SqlTable associato alla maschera (vedi preloadEnvironment()).
        /// </summary>
        protected String MySelectQuery_base = null;                                                   //<- TO CHANGE
        /// <summary>
        /// Parte selettiva della query di selezione della lista dei risultati (GridView): in pratica tutte le condizioni che seguono la clausola WHERE.
        /// Per default è la condizione che selezione solo i record NON oscurati.
        /// </summary>
        protected String MyFilterQuery_base = "DATA_DEL IS NULL";                                     //<- TO CHANGE
        /// <summary>
        /// Parte della query di selezione della lista dei risultati che detemina l'ordinamento.
        /// Per default è nulla.
        /// </summary>
        protected String MyOrderQuery_base = null;                                                    //<- TO CHANGE
        /// <summary>
        /// Nome della stringa di connessione come impostata nel file di configurazione web.config all'elemento <b><connectionStrings></b>.
        /// </summary>        
        protected String MyConnStringName_base = null;                                                //<- TO CHANGE                                                             
        /// <summary>
        /// Parte proiettiva della query di selezione della lista dei risultati (GridView). 
        /// Per default, se MySelectQuery_base è diversa da null: MySql = MySelectQuery_base.
        /// </summary>
        protected String MySql;       //= MySelectQuery_base;
        /// <summary>
        /// Parte selettiva della query di selezione della lista dei risultati (GridView)
        /// Per default, se MyFilterQuery_base è diversa da null: MyFilter = MyFilterQuery_base.
        /// </summary>
        protected String MyFilter;    // = MyFilterQuery_base;
        /// <summary>
        /// Criteri di ordinamento della query di selezione della lista dei risultati (GridView)
        /// Per default, se MyOrderQuery_base è diversa da null: MyOrder = MyOrderQuery_base.
        /// </summary>
        protected String MyOrder;      // = MyOrderQuery_base;
        /// <summary>
        /// Nome della stringa di connessione come impostata nel file di configurazione web.config all'elemento <pre><connectionStrings></pre>.
        /// Per default, se MyConnStringName_base è diversa da null: MyConnStringName = MyConnStringName_base, altrimenti cerca di determinarla
        /// invocando il metodo guessOLEDBConnectionStringName().
        /// <see cref="guessOLEDBConnectionStringName()"/>
        /// </summary>
        protected String MyConnStringName; // = MyConnStringName_base;
        /// <summary>
        /// Stringa di connessione come impostata nel file di configurazione web.config all'elemento <pre><connectionStrings></pre>.
        /// <see cref="MyConnStringName"/>
        /// </summary>
        protected String MyConnString = null;
        /// <summary>
        /// Nome della maschera chiamante. Usato per entrare in sessione e prelevare 
        /// l'oggetto di classe SQLTable che rappresenta la tabella PRINCIPALE.
        /// </summary>
        protected String MyCallerName = null;
        /// <summary>
        /// Oggetto di classe SQLTable che rappresenta la tabella PRINCIPALE 
        /// </summary>
        protected simplex_ORM.SQLSrv.SQLTable MAIN_UNDER_FORM = null;
        #endregion

        #region METODI PROTETTI

        /// <summary>
        /// <p>
        /// Restituisce una stringa che rappresenta una query SQL di selezione dei record
        /// della tabella di lookup.
        /// Prerequisito: l'attributo protetto TBL_UNDER_FORM deve essere valorizzato.
        /// </p>
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 21/09/2015
        /// </pre>
        /// </summary>
        /// <returns>Stringa che rappresenta una query SQL di selezione ovvero null.</returns>
        protected String getSelectString()
        {
            StringBuilder _toRet = new StringBuilder("SELECT ");
            Boolean _first = true;


            // controlli
            if (TBL_UNDER_FORM == null)
                return null;

            //ciclo di composizione della query
            System.Collections.Specialized.StringCollection _keyColumns = null;
            System.Collections.Specialized.StringCollection _Columns = null;
            // inserisco la chiave
            foreach (String _test in TBL_UNDER_FORM.PrimaryKeyColumns)
            {
                if (_first == false)
                    _toRet.Append(", ");
                _toRet.Append(_test);
                _first = false;
            }
            // inserisco le altre colonne nella query: doppio ciclo
            Boolean _alreadyUsed = false;
            foreach (String _ext in TBL_UNDER_FORM.SQLColumns)
            {
                if ((TBL_UNDER_FORM.PrimaryKeyColumns).Contains(_ext) == false)
                {
                    if (_first == false)
                        _toRet.Append(", ");
                    _toRet.Append(_ext);
                    _first = false;
                }
            }
            // clausola FROM 
            _toRet.Append(" FROM ").Append(MyTableName);
            return _toRet.ToString();
        }

        /// <summary>
        /// <p>
        /// Imposta gli attributi necessari al funzionamento.
        /// Si attende in Session["APPLICATION"] l'oggetto GeneralApplication corrente.
        /// Se non è presente, lancia un eccezione del tipo generalApplication TracedMessage.
        /// Valorizza:
        /// </p>
        /// <p>
        /// Valorizza i seguenti attributi:
        /// </p>
        /// <ul>
        /// <li>MyApp               :   APPLICAZIONE CORRENTE;</li>
        /// <li>MyConn              :   CONNESSIONE DI DEFAULT;</li>
        /// <li>MyMainMenu          :   CONTROLLO MENU SINISTRO;</li>
        /// <li>MySbc               :   CONTROLLO BREADCRUMBS;</li>
        /// <li>MyAspxFileName      :   NOME DELLA PAGINA WEB/WEB FORM.</li>
        /// <li>MyBackURL           :   URL DELLA PAGINAWEB/WEB FORM CHIAMANTE.</li>
        /// </ul>
        /// <pre>
        /// ------
        /// @MdN
        /// Crtd: 06/12/2014
        /// Mdfd: 30/01/2015
        /// </pre>
        /// </summary>
        protected override void preloadEnvironment()
        {
            MySql = MySelectQuery_base;
            MyFilter = MyFilterQuery_base;
            MyOrder = MyOrderQuery_base;

            base.preloadEnvironment();

            // PRELIEVO DELLA CLASSE PRINCIPALE
            if (Session[this.MyName + "_MYCALLERNAME"] != null)
            {
                MyCallerName = (String)Session[this.MyName + "_MYCALLERNAME"];
                if (Session[MyCallerName] != null)
                    MAIN_UNDER_FORM = (simplex_ORM.SQLSrv.SQLTable)Session[MyCallerName];

                // <deleted @MdN: 21/06/2016>
                // Azzera il tutto
                // Session[MyCallerName] = null;
                // Session["MYCALLERNAME"] = null;
                // <deleted @MdN: 21/06/2016>
            }

            // DETERMINAZIONE DELL'OGGETTO DI RIFERIMENTO
            // Devono essere definite ed inizializzate MyName e MyTableName
            if (Session[MyName] != null)
                TBL_UNDER_FORM = (simplex_ORM.SQLSrv.SQLTable)Session[MyName];
            else
            {
                try
                {
                    TBL_UNDER_FORM = simplex_ORM.SQLSrv.SQLTable.createSQLTable(MyConn, MyTableName);
                    Session.Add(MyName, TBL_UNDER_FORM);
                }
                catch (simplex_ORM.Spx_ORMMessage ORMM)
                {
                    if(MyConn!=null && MyApp!=null && MyApp.CurrentUser!=null && Session.SessionID!=null)
                        ORMM.traceLog(MyConn, MyApp.CurrentUser.UID.ToString(), Session.SessionID);
                    MyErrors.Add(ORMM);
                    showErrors();
                    return;
                }
            }

            // DETERMINAZIONE DELLE TRE COMPONENTI DELLA QUERY DELLA GridView
            if (ViewState["MYSQL"] != null)
            {
                MySql = (String)ViewState["MYSQL"];
            }
            else
            {
                if (MySelectQuery_base != null)
                    MySql = MySelectQuery_base;
                else
                {
                    MySql = getSelectString();
                }
                ViewState.Add("MYSQL", MySql);
            }

            if (IsPostBack == false)
            {
                // MySql = MySelectQuery_base;
                MyFilter = MyFilterQuery_base;
                MyOrder = MyOrderQuery_base;
                ViewState.Add("MYFILTER", MyFilter);
                ViewState.Add("MYORDER", MyOrder);
            }
            else
            {
                //MySql = (String)ViewState["MYSQL"];
                if (ViewState["MYFILTER"] != null)
                    MyFilter = (String)ViewState["MYFILTER"];
                else
                    MyFilter = MyFilterQuery_base;

                if (ViewState["MYORDER"] != null)
                    MyOrder = (String)ViewState["MYORDER"];
                else
                    MyOrder = MyOrderQuery_base;
            }

            // DETERMINAZIONE DELLA STRINGA DI CONNESSIONE DA ASSOCIARE AL CONTROLLO SQLDATASOURCE_LOOKUP
            if (ViewState["MYCONNSTRING"] != null)
                MyConnString = (String)ViewState["MYCONNSTRING"];
            else
            {
                if (MyConnStringName == null && MyConn!=null)
                    MyConnStringName = base.guessOLEDBConnectionStringName(MyConn);
                if (MyConnStringName == null)
                {
                    //ERRORE
                    simplex_ORM.Spx_ORMMessage ORMM = new simplex_ORM.Spx_ORMMessage("La stringa di connessione non risulta essere stata impostata nel file di configurazione web.config");
                    if(MyConn!=null && MyApp!=null && MyApp.CurrentUser!=null && Session.SessionID!=null)
                        ORMM.traceLog(MyConn, MyApp.CurrentUser.UID.ToString(), Session.SessionID);
                    MyErrors.Add(ORMM);
                    return;
                }
                // INSERIMENTO DELLA STRINGA DI CONNESSIONE NELLO STATO IN MODO DA NON DOVER RICAVARLA OGNI VOLTA
                System.Configuration.Configuration _webConfig = null;
                _webConfig = System.Web.Configuration.WebConfigurationManager.OpenWebConfiguration("~/Web.config");
                MyConnString = _webConfig.ConnectionStrings.ConnectionStrings[MyConnStringName].ConnectionString;
                //MyConnString = System.Configuration.Configuration.ConnectionStrings[MyConnStringName].connectionString;
                if (MyConnString == null)
                {
                    //ERRORE
                    simplex_ORM.Spx_ORMMessage ORMM = new simplex_ORM.Spx_ORMMessage("La stringa di connessione non risulta essere stata impostata nel file di configurazione web.config");
                    if (MyConn != null && MyApp != null && MyApp.CurrentUser != null && Session.SessionID != null)
                        ORMM.traceLog(MyConn, MyApp.CurrentUser.UID.ToString(), Session.SessionID);
                    MyErrors.Add(ORMM);
                    return;
                }
                // Aggiungo allo stato
                ViewState.Add("MYCONNSTRING", MyConnString); //MYCONNSTRING
            }
         
            #region WRITE YOUR CODE HERE
            // WRITE YOUR CODE FROM HERE
            // WRITE YOUR CODE TO HERE
            #endregion
            
        }//fine preloadEnvironment

        /// <summary>
        /// <p>
        /// A partire da una delle colonne del vincolo di integrità referenziale,
        /// il metodo preleva tutte le colonne delle tabelle referente e referenziata che partecipano 
        /// al vincolo e copia i valori delle colonne della tabella referenziata nelle corrispondenti 
        /// colonne della tabella referente!!!
        /// </p>
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 10/10/2015
        /// Tstd: 11/10/2015 - integrità referenziale su chiave costituita di UN SOLO attributo!!!
        /// </pre>
        /// </summary>
        /// <param name="p_fieldName"></param>
        protected virtual void bindMainObject(String p_fieldName)
        {
            int _cntr = 0;
            if (p_fieldName == null)
                return;
            if (TBL_UNDER_FORM == null || MAIN_UNDER_FORM == null)
            {
                simplex_ORM.Spx_ORMMessage ORMM = new simplex_ORM.Spx_ORMMessage("Uno degli oggetti rappresentanti la tabella referente o la tabella referenziata (o entrambi) non è istanziato.");
                if(MyConn!=null && MyApp!=null && MyApp.CurrentUser!=null && Session.SessionID!=null)
                    ORMM.traceLog(MyConn, MyApp.CurrentUser.UID.ToString(), Session.SessionID);
                throw ORMM;
            }

            simplex_ORM.SQLSrv.SQLForeignKey _fk = null;
            _fk = TBL_UNDER_FORM.getForeignKey(p_fieldName);
            /*
             * 
             * CICLO FONDAMENTALE: A partire da una delle colonne del vincolo di integrità referenziale,
             * il metodo preleva tutte le colonne delle tabelle referente e referenziata che partecipano 
             * al vincolo e copia i valori delle colonne della tabella referenziata nelle corrispondenti 
             * colonne della tabella referente!!!
             * 
             */
            if(_fk!=null && _fk.ReferencedColumns.Length> 0 && _fk.ReferencedColumns.Length==_fk.ReferencingColumns.Length)
            {
                for (_cntr = 0; _cntr < _fk.ReferencingColumns.Length; _cntr++)
                {
                    TBL_UNDER_FORM.setValue(_fk.ReferencingColumns[_cntr], MAIN_UNDER_FORM.getValue(_fk.ReferencedColumns[_cntr]));
                }
            }            
        }


        /// <summary>
        /// <p>EN: Show the errors occurred in the last operation. At the end, clears the list of errors. 
        /// </p>
        /// <p>IT: Mostra gli errori verificatisi nell'ultima operazione. Al termine, cancella la lista degli errori.
        /// </p>
        /// </summary>
        protected override void showErrors()
        {
            if (MyErrors.Count > 0)
            {
                Boolean l_pari = false;
                Literal l_ErrArea = null;
                System.Text.StringBuilder l_ErrString = new System.Text.StringBuilder("<table class=\"ErrTab\">");
                l_ErrArea = (Literal)this.Master.FindControl("ListOfErrors");

                foreach (simplex_ORM.Spx_ORMMessage ORMM in MyErrors)
                {
                    if (l_pari == true)
                        l_ErrString.Append("<tr class=\"ErrRowPari\">");
                    else
                        l_ErrString.Append("<tr class=\"ErrRowDisPari\">");
                    l_pari = !l_pari;
                    l_ErrString.Append("<td class=\"ErrCodCol\">");
                    l_ErrString.Append(ORMM.MessageCode.ToString());
                    l_ErrString.Append("</td><td class=\"ErrDescCol\">");
                    l_ErrString.Append(ORMM.Message);
                    l_ErrString.Append("</td></tr>");
                }
                l_ErrString.Append("</table>");
                l_ErrArea.Text = l_ErrString.ToString();
                MyErrors.Clear(); //<-- ATTENZIONE
            }
        }//fine

        /// <summary>
        /// Mostra l'oggetto SQLTable che supporta la maschera.
        /// ---
        /// @MdN
        /// Crtd: 01/01/2015
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected override void Page_SaveStateComplete(object sender, EventArgs e)
        {
            if (TBL_UNDER_FORM != null)
            {
                this.show(TBL_UNDER_FORM, "ContentPlaceHolder1", "System.Web.UI.WebControls.ContentPlaceHolder");
                //R_Dynamic
                this.show(TBL_UNDER_FORM, "R_Dynamic", "System.Web.UI.WebControls.PlaceHolder");
            }
        }

        /// <summary>
        /// <p>
        /// Disegna dinamicamente il contenuto della maschera.
        /// </p>
        /// <p>
        /// Le informazioni per il disegno della maschera si trovano in un file xml il cui percorso (path)
        /// viene passato come parametro.
        ///     <br>
        ///     Se il file xml non esiste, il metodo lo crea. Infatti invoca prima il metodo createXMLConfigurationFile(String p_Name) e poi
        ///     il metodo writeXMLConfigurationFile(Spx_XmlConfigFile p_config).
        ///     </br>
        ///     <br>
        ///     Se si modifica il file così generato, è possibile personalizzare l'aspetto della maschera.
        ///     </br>
        /// </p>
        /// <p>
        /// I controlli vengono distribuiti lungo una griglia (ovvero una 'table') ed incolonnati secondo il
        /// l'altro parametro specificato.
        /// </p>
        /// </summary>
        /// <param name="p_NomeFile">Nome del file di configurazione xml.</param>
        /// <param name="p_numcols">Numero di colonne lungo cui 'incolonnare' i controlli.</param>
        protected virtual void drawDynamicForm(String p_NomeFile, int p_numcols)
        {
            System.Web.UI.WebControls.PlaceHolder _cph = this.R_Dynamic; ;
            //_cph = (System.Web.UI.WebControls.PlaceHolder)FindControl("R_Dynamic");
            if (_cph != null)
            {
                // Si apre il file che ha lo stesso nome tella tabella da mostrare.
               this.designDynamicForm(MapPath(p_NomeFile), _cph, 0);
            }
        }

        /// <summary>
        /// <p>
        /// Disegna i controlli della maschera in modo dinamico prelevando le informazioni da un 
        /// file xml il cui path viene passato quale parametro. I controlli vengono disegnati in
        /// un placeholder specificato in uno dei parametri passati al metodo.
        /// </p>
        /// </summary>
        /// <param name="p_XML"></param>
        /// <param name="p_PH"></param>
        /// <param name="p_NumCols"></param>
        public override void  designDynamicForm(string p_XML, PlaceHolder p_PH, int p_NumCols)
        {
            // il metodo base disegna solo la sezione 'main'.
            // Adesso si devono disegnare eventuali TAB.
            try
            {
                base.designDynamicForm(p_XML, p_PH, p_NumCols);
            }
            catch (simplex_ORM.Spx_ORMMessage ORMM)
            {
                MyErrors.Add(ORMM);                
            }

            /* deleted @MdN 15/10/2015 */
            ///* disegno dei tabs */
            //try
            //{
            //    designTabs(p_XML, p_PH, p_NumCols);
            //}
            //catch (simplex_ORM.Spx_ORMMessage ORMM)
            //{
            //    MyErrors.Add(ORMM);
            //}
            /* deleted @MdN 15/10/2015 */
        }
        /// <summary>
        /// Mostra i tabs.
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 29/04/2015
        /// Mdfd: 03/05/2015 - Gestita l'assenza degli elementi 'tab'
        /// </pre>
        /// </summary>
        /// <param name="p_XML"></param>
        /// <param name="p_PH"></param>
        /// <param name="p_NumCols"></param>
        protected virtual void designTabs(string p_XML, PlaceHolder p_PH, int p_NumCols)
        {
            /* *** MAI DISDEGNARE UN PO' DI COPIA-INCOLLA *** */
            /* Da: SimplexForm.designDynamicForm()            */

            SIMPLEX_Config.Spx_XmlElement _config = null;
            SIMPLEX_Config.Spx_XmlElement _head = null;
            System.Web.UI.WebControls.Literal _ltrl = null;
            System.Web.UI.WebControls.PlaceHolder _plh = p_PH;
            System.Web.UI.WebControls.View _vw = null;
            System.Web.UI.WebControls.MultiView _mv = null;
            WebSimplex.WebSimplexTabs _WST = null;
            System.Web.UI.WebControls.Literal _ltr = null;

            String _strNumCols = null;
            int _numCols = p_NumCols;
            int _countCols = 0;                                                             // contatore di colonne

            if (p_PH == null)
                return;
            if (p_XML == null)
            {
                _ltrl = new Literal();
                _ltrl.Text = "<b>ERRORE: file XML assente!!!</b>.";
                return;
            }
            _config = SIMPLEX_Config.Spx_XmlConfigFile.load(p_XML);
            if (_config == null)
            {
                _ltrl = new Literal();
                _ltrl.Text = "<b>ERRORE: file XML non valido!!!</b>.";
                return;
            }
            /* **** **** **** **** **** 
             * Il primo elemento deve essere di tipo 'dynamic'
             * **** **** **** **** **** */
            SIMPLEX_Config.Spx_XmlElement _elem = _config;
            if (_elem.Name.ToLower().CompareTo("dynamic") != 0)
            {
                _ltrl = new Literal();
                _ltrl.Text = "<b>ERRORE: il primo elemento del file XML NON è 'dynamic'</b>.";
                return;
            }

            /* **** **** **** **** **** 
             * Gli elementi del livello immediatamente inferiore alla radice 'dynamic'
             * rappresentano le sezioni di una maschera che sono:
             * Main               - sezione principale (solo una) 
             * Tab                - eventuali Tab
             **** **** **** **** **** */
            /* *** FINE DEL COPIA-INCOLLA *** */
            #region BLOCCHI TAB

            /* *** BLOCCHI TAB *** */
            _elem = _config;                            //riparto da 'dynamic'
            _elem = _elem.Inner;                        //scendo di un livello

            while (_elem != null)
            {
                if (_elem.Name.ToLower().CompareTo("tab") != 0)
                {
                    _elem = _elem.Next;
                    continue;
                }
                /* TROVATO !!! */
                /* **** 
                 * La prima cosa da fare in questi casi è vedere se il controllo
                 * MultiView è stato già aggiunto alla maschera, altrimenti lo si aggiunge.
                 * :-)
                 */
                if(_mv==null)
                {
                    // Aprire la sottosezione
                    _ltr = new Literal();
                    _ltr.Text = "<div id=\"tabSection\">";
                    p_PH.Controls.Add(_ltr);

                    _WST = new WebSimplex.WebSimplexTabs();
                    _WST.ID = "R_DynamicTABS";
                    _WST.Name = _WST.ID;
                    _plh.Controls.Add(_WST);

                    _mv = new MultiView();
                    _mv.ID = "R_DynamicMultiView";                    
                }
                // Ora abbiamo il MultiView ed il WebSimplexTabs

                // **** Prelevo il controllo View corrispondente all'elemento corrente **** */
                _vw =  getTab(_elem, p_NumCols);
                if (_vw != null)
                {
                    //_plh.Controls.Add(_vw);               // Aggiunta della View
                    _mv.Views.Add(_vw);                     // Aggiunta del MultiViev...
                    _WST.Add(_elem.getValue("Name"));       // e del nome
                }
                // avanzamento
                _elem = _elem.Next;
            } //fine ciclo di ricerca dei blocchi 'tab'

            // @MdN 03/05/2015
            /* *************** *
             * Se l'elemento 'tab' non è stato trovato allora il controllo MultiView
             * non è stato creato. Di conseguenza se _mw==null l'eaborazione termina.
             * *************** */
            if (_mv == null)
                return;

            // Aggiungo il MultiView ed associo al Tab
            p_PH.Controls.Add(_mv);            
            _WST.AssociatedMultiView = _mv;
            _mv.ActiveViewIndex = 0;

            // chiudere la sottosezione
            _ltr = new Literal();
            _ltr.Text = "</div>";
            p_PH.Controls.Add(_ltr);
            #endregion
        } //fine designTabs()

        /// <summary>
        /// <p>
        /// Dato un elemento XML di tipo 'tab', restituisce un controllo System.Web.UI.WebControls.View
        /// popolato dai controlli corrispondenti agli elementi contenuti nel suddetto elemento di tipo 'tab'.
        /// </p>
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 29/04/2015
        /// </pre>
        /// </summary>
        /// <param name="p_tab">elemento di tipo 'tab'</param>
        /// <param name="p_NumCols">Numero di colonne. Se 0, viene letto il valore dell'attributo 'NumCols'.</param>
        /// <returns>un controllo View ovvero null se l'elemento passato è nullo o di tipo diverso da 'tab'.</returns>
        /// 
        protected virtual System.Web.UI.WebControls.View getTab(SIMPLEX_Config.Spx_XmlElement p_tab, int p_NumCols)
        {
            SIMPLEX_Config.Spx_XmlElement _elem;
            int _numCols = 1;
            int _countCols = 0; 
            String _strNumCols = null;

            // per il calcolo della larghezza delle colonne
            // _col3 DEVE essere sempre a 0
            // _col1 DEVE sempre essere il 20% di _col2
            // _col1 + _col2 = 100/p_NumCols
            int _col1 = 0;
            int _col2 = 0;
            int _col3 = 0;
                       
            // controlli preliminari
            if(p_tab==null)
                return null;
            if (p_tab.Name.ToLower().CompareTo("tab") != 0)
                return null;
            if (p_NumCols < 0)
                return null;

            // determinazione del numero di colonne
            if (p_NumCols == 0)
            {
                _strNumCols = p_tab.getValue("NumCols");
                if (_strNumCols != null && simplex_ORM.Column.isPureInteger(_strNumCols) == true)
                {
                    _numCols = int.Parse(_strNumCols);
                    if (_numCols == 0)
                        _numCols = 1;
                }
            }
            else
            {
                _numCols = p_NumCols;
            }
            // Abbiamo il numero di colonne.
            // Determinazione della loro dimensione
            // UN PO' DI COPIA-INCOLLA NON SI RIFIUTA MAI!!!!

            /* DETERMINAZIONE DELLA LARGHEZZA DELLE COLONNE */
            int _ratio = (int)Math.Floor((decimal)(100 / _numCols));
            _col2 = (int)Math.Floor((decimal)(_ratio / 5)) * 4;
            _col1 = (int)Math.Floor((decimal)(_ratio / 5));


            //CReazione del controllo View
            View _plh = new View();
            _plh.ID = p_tab.getValue("ID");
                       

            /* INIZIO DEL DRAFT DELLA MASCHERA 
            /* UN PO' DI SANO COPIA-INCOLLA */

            /* 
 * LOGICA PER INSERIRE PIU' CONTROLLI IN UNA SOLA CELLA
 * @MdN 19/09/2015
 * ------
 * Si introducono le variabili booleane _continue ed _opened
 * Il valore di _continue è determinato da un attributo dell'elemento XML corrispondente al controllo da posizionare.
 * Il valore true di _opened indica che la cella <TD> è aperta. Il valore di default di _opened è 'false' e commuta in 
 * 'true' una volta che è stato scritto il tag <TD> nel controllo Literal.
 * 
 * 1. Se _opened=false AND _continue=false -> scrivi il <TD> -> commuta _opened=true -> posiziona il controllo.
 * 2. Se _opened=false AND _continue=true -> scrivi il <TD> -> commuta _opened=true -> posiziona il controllo.
 * 3. Se _opened=true AND _continue=true -> posiziona il controllo.
 * 4. Se _opened=true AND _continue=flase -> posiziona il controllo -> scrivi </TD> -> commuta _opened=false.
 * 
 * Il funzionamento di default attiva solo i comportamenti 1. e 4. in uno stesso ciclo.
 * La scrittura di più controlli in una stessa cella non comporta alcun incremento del contatore '_countCols'!!!
 * 
 * La variabile intera _onTheSameCell conta quanti controlli vengono inseriti nella medesima cella.
 */
            Boolean _opened = false;
            Boolean _continue = false;
            int _onTheSameCell = 0;             //DEBUG

            if(p_tab.InnerElementsCount>0)
            {
                System.Web.UI.WebControls.Label _lbl = null; ;
                System.Web.UI.WebControls.Literal _ltr = null;
                System.Web.UI.WebControls.WebControl _test = null;
                _ltr = new Literal();
                _ltr.Text = "<table class=\"DYN\">"; //;
                _plh.Controls.Add(_ltr);
                _elem = p_tab.Inner;                                                //Primo degli elementi di livello inferiore
                while (_elem != null)
                {
                    //if (_countCols == 0)
                    //{
                    //    _ltr = new Literal();
                    //    _ltr.Text = "<tr class=\"DYN_ROW\">";
                    //    _plh.Controls.Add(_ltr);
                    //}

                    #region @MdN 19/09/2015
                    //@MdN 19/09/2015
                    if (_countCols == 0 && _continue == false)
                    {
                        _ltr = new Literal();
                        _ltr.Text = "<tr class=\"DYN_ROW\">";
                        _plh.Controls.Add(_ltr);
                    }
                    //@MdN 19/09/2015
                    #endregion

                    // colonna 1 -- Label
                    _ltr = new Literal();
                    //_ltr.Text = "<td class=\"DYN_COL1\" width=\"" + _col1.ToString() + "%\" >";

                    #region @MdN 19/09/2015
                    //@MdN 19/09/2015
                    if (_continue == false && _opened == false)
                    {
                        _ltr.Text = "<td class=\"DYN_COL1\" width=\"" + _col1.ToString() + "%\" >";
                        _opened = true;
                    }
                    else
                        _ltr.Text = "&nbsp;";
                    //@MdN 19/09/2015
                    #endregion                    
                    
                    _plh.Controls.Add(_ltr);
                    // etichetta associata: SOLO SE IL CONTROLLO NON E' A SUA VOLTA UN'ETICHETTA
                    if (_elem.Name.ToLower().CompareTo("lbl") != 0)
                    {
                        _lbl = getAssociatedLabel(_elem);
                        if (_lbl != null)
                            _plh.Controls.Add(_lbl); //JUMP!!!
                    }

                    // colonna 2 -- INSERIMENTO CONTROLLO WEB
                    _ltr = new Literal();
                    //_ltr.Text = "</td><td class=\"DYN_COL2\" width=\"" + _col2.ToString() + "%\">";

                    #region @MdN 19/09/2015
                    //@MdN 19/09/2015
                    //if ((_continue == false && _opened==true) || _onTheSameCell==0)
                    if ((_continue == false && _opened == true))
                    {
                        _ltr.Text = "</td><td class=\"DYN_COL2\" width=\"" + _col2.ToString() + "%\">";
                        //_opened = true;
                    }
                    else
                        _ltr.Text = "&nbsp;";
                    //@MdN 19/09/2015
                    #endregion
                                        
                    _plh.Controls.Add(_ltr);
                    try
                    {
                        System.Web.UI.WebControls.WebControl _debugwb = null;
                        _debugwb = getWebControl(_elem);
                        _plh.Controls.Add(_debugwb);
                        // - eventuale associazione con data source
                        if (_elem.Name.ToLower().CompareTo("sqlddl") == 0)
                        {
                            System.Web.UI.WebControls.SqlDataSource _sqlds = getAssociatedSqlDataSource(_elem);
                            if (_sqlds != null)
                                _plh.Controls.Add(_sqlds);
                        }
                        //_plh.Controls.Add(getWebControl(_elem));
                    }
                    catch (simplex_ORM.Spx_ORMMessage ORMM)
                    {
                        MyErrors.Add(ORMM);
                    }
                    // clonna 3 -- chiusura
                    _ltr = new Literal();
                    //_ltr.Text = "</td><td class=\"DYN_COL3\" width=\"0%\"></td>";

                    #region @MdN 19/09/2015
                    //@MdN 19/09/2015

                    // Determina se si deve continuare o meno
                    if (_elem.getValue("Continue") != null && _elem.getValue("Continue").ToLower().CompareTo("true") == 0)
                        _continue = true;
                    else
                        _continue = false;

                    if (_continue == false && _opened == true)
                    {
                        _ltr.Text = "</td><td class=\"DYN_COL3\" width=\"0%\"></td>";
                        _opened = false;
                        _onTheSameCell = 0;
                    }
                    else
                    {
                        _onTheSameCell++;
                        _ltr.Text = "&nbsp;";
                    }

                    //@MdN 19/09/2015
                    #endregion

                    _plh.Controls.Add(_ltr);

                    // nuova riga, Sì/No?
                    //_countCols++;

                    // nuova riga, Sì/No?       //_countCols++;
                    #region @MdN 19/09/2015
                    //@MdN 19/09/2015
                    if (_continue == false)
                        _countCols++;
                    //@MdN 19/09/2015
                    #endregion

                    if (_countCols == _numCols)
                    {
                        //Sì
                        _countCols = 0;
                        _ltr = new Literal();
                        //_ltr.Text = "</tr>";

                        #region @MdN 19/09/2015
                        //@MdN 19/09/2015
                        if (_continue == false)
                            _ltr.Text = "</tr>";
                        else
                            _ltr.Text = "&nbsp;";
                        //@MdN 19/09/2015
                        #endregion

                        _plh.Controls.Add(_ltr);
                        _countCols = 0;
                    }
                    _elem = _elem.Next;
                }// fine ciclo while

                // Si deve gestire la chiusura dell' ultima riga nel caso in cui _countCols < _NumCols
                if (_countCols > 0 && _countCols < _numCols)
                {
                    while (_countCols < _numCols)
                    {
                        _ltr = new Literal();
                        //_ltr.Text = "<td class=\"FRM_COL1\">" + "</td><td class=\"FRM_COL2\">" + "</td><td class=\"FRM_COL3\"></td>";
                        _ltr.Text = "<td class=\"DYN_COL1\" width=\"" + _col1.ToString() + "%\" >" + "</td><td class=\"DYN_COL2\" width=\"" + _col2.ToString() + "%\">" + "</td><td class=\"DYN_COL3\" width=\"0%\"></td>";
                        _plh.Controls.Add(_ltr);
                        _countCols++;
                    }
                    _ltr = new Literal();
                    _ltr.Text = "</tr>";
                    _plh.Controls.Add(_ltr);
                }

                // -- chiusura tabella
                _ltr = new Literal();
                _ltr.Text = "</table>";
                _plh.Controls.Add(_ltr);

            } // blocco principale per 'tab'
            return _plh;
        }

        /// <summary>
        /// <p>
        /// Crea una struttura di configurazione a partire dal nome di una tabella
        /// (simplex_ORM.SQLSrv.SQLTable).
        /// </p>
        /// </summary>
        /// <param name="p_Name">Nome della tabella.</param>
        protected virtual void createXMLConfigurationFile(String p_Name)
        {
            //TODO:
        }
        /// <summary>
        /// TODO:
        /// <p>
        /// Scrive su un file xml una struttura di configurazione.
        /// </p>
        /// </summary>
        /// <param name="p_config">Nome del file di configurazione.</param>
        protected virtual void writeXMLConfigurationFile(SIMPLEX_Config.Spx_XmlConfigFile p_config)
        {
            //TODO:
        }

#endregion

        #region PROPERTIES PUBBLICHE
        /// <summary>
        /// Nome della pagina visualizzato nei menu e nelle briciole di pane.
        /// @MdN
        /// ----
        /// Crtd: 18/01/2015
        /// </summary>
        public String Nome
        {
            get
            {
                return MyName;
            }
            set
            {
                if (value == null)
                    MyName = "### DA DEFINIRE";
                else
                    MyName = value;
            }
        }
        #endregion

        #region GESTORI
        /// <summary>
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: ????
        /// Mdfd: 08/07/2016 - passata a virtual
        /// </pre>
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected virtual void Page_Init(object sender, EventArgs e)
        {

            if (IsPostBack == false)
            {
                System.Web.UI.WebControls.ButtonField _ubtn = new ButtonField();
                _ubtn.ButtonType = ButtonType.Button;
                _ubtn.CommandName = "seleziona";
                _ubtn.Text = "Seleziona";
                _ubtn.Visible = true;
                _ubtn.Initialize(true, R_Lista);
                //R_Lista.EnableViewState = true;
                //R_Lista.RowCommand += new GridViewCommandEventHandler(R_Lista_RowCommand);
                R_Lista.Columns.Add(_ubtn);
            }
            R_Lista.RowCommand += new GridViewCommandEventHandler(R_Lista_RowCommand);
        }

        protected virtual void R_Lista_RowCommand(object sender, GridViewCommandEventArgs e)
        {

            Int32 _row=0;
            Int32  _key=0;
            if (e.CommandName.ToLower().CompareTo("seleziona") == 0)
            {
                // identificare la riga che ha scatenato l'evento: ASP.NET non lo fa in automatico
                // N.B. CommandArgument è 0-BASED!!!!
                _row = Int32.Parse(e.CommandArgument.ToString());

                // identificare il valore della chiave associata alla riga
                _key = Int32.Parse(((GridView)e.CommandSource).Rows[_row].Cells[1].Text);

                // creazione dell'oggetto corrispondente
                try
                {
                    TBL_UNDER_FORM = simplex_ORM.SQLSrv.SQLTable.createSQLTable(MyConn, MyTableName);
                    TBL_UNDER_FORM.setValue(TBL_UNDER_FORM.PrimaryKeyColumns[0], _key.ToString());
                    TBL_UNDER_FORM.load();
                    Session.Add(MyName, TBL_UNDER_FORM);
                    Response.Redirect(MyAspxFileName);
                }
                catch (simplex_ORM.Spx_ORMMessage ORMM)
                {
                    if (MyConn != null && MyApp != null && MyApp.CurrentUser != null && Session.SessionID != null)
                        ORMM.traceLog(MyConn, MyApp.CurrentUser.UID.ToString(), Session.SessionID);
                    MyErrors.Add(ORMM);
                    showErrors();
                    return;
                }


            }
        }

        /// <summary>
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: ???
        /// Mdfd: 04/08/2016 - migliorata la composizione della query alla base della lista di oggetti.
        /// </pre>
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected override void Page_Load(object sender, EventArgs e)
        {
            try
            {
                preloadEnvironment();
            }
            catch (simplex_ORM.Spx_ORMMessage ORMM)
            {
                if (MyConn != null && MyApp != null && MyApp.CurrentUser != null && Session.SessionID != null)
                    ORMM.traceLog(MyConn, MyApp.CurrentUser.UID.ToString(), Session.SessionID);
                MyErrors.Add(ORMM);
            }

            //mostro gli errori di pre-caricamento: impossibile continuare.
            if (MyErrors.Count > 0)
            {
                showErrors();
                return;
            }

            // impostazione della DqlDataSource e della GridView associata
            // SqlDataSource_LookUP.SelectCommand = MySql + " WHERE " + MyFilter + " " + MyOrder; //<deleted @MdN: 21/06/2016/>

            //<added @MdN: 04/08/2016>
            if (MyFilter != null)
                SqlDataSource_LookUP.SelectCommand = MySql + " WHERE " + MyFilter;
            else
                SqlDataSource_LookUP.SelectCommand = MySql;

            if(MyOrder != null)
                SqlDataSource_LookUP.SelectCommand = SqlDataSource_LookUP.SelectCommand + " ORDER BY " + MyOrder;
            //</added @MdN: 04/08/2016>

            if (MyConn != null & MyConnString != null)
            {
                SqlDataSource_LookUP.ConnectionString = MyConnString;
                //R_Lista.DataSource = SqlDataSource_LookUP;
                R_Lista.DataBind();
            }
            else
            {
                // ERRORE
                simplex_ORM.Spx_ORMMessage ORMM = new simplex_ORM.Spx_ORMMessage("La stringa di connessione non risulta essere stata impostata nel file di configurazione web.config");
                if (MyConn != null && MyApp != null && MyApp.CurrentUser != null && Session.SessionID != null)
                    ORMM.traceLog(MyConn, MyApp.CurrentUser.UID.ToString(), Session.SessionID);
                MyErrors.Add(ORMM);
                showErrors();
                return;
            }

            //AGGIUNTA DEI TASTI DI COMANDO
            // - comando "seleziona"
            //TemplateField _tfld = new TemplateField();
            //System.Web.UI.WebControls.ButtonField _ubtn = new ButtonField();
            //_ubtn.ButtonType = ButtonType.Button;
            //_ubtn.CommandName="seleziona";
            //_ubtn.Text = "Seleziona";
            //_ubtn.Visible=true;
            //_ubtn.Initialize(true, R_Lista);
            //R_Lista.Columns.Add(_ubtn);

            
            // Evento del BOX dei messaggi
            if (Session["GenericSpxForm1.R_Conferma.Click"] != null)
            {
                /* Parte del codice necessaria ad impostare il più opportuno gestore dell'evento
                 * Click del tasto R_Conferma del MessageBox "embedded".
                 * Vedere la documentazione a corredo.
                 */
                // <%WRITE YOUR CODE FROM HERE%>

                // <%WRITE YOUR CODE TO HERE%>
            }            

            #region GESTIONE DELLA COMPOSIZIONE DINAMICA DELLA FORM

            SIMPLEX_Config.Spx_XmlElement _head = null;
            SIMPLEX_Config.Spx_XmlConfigWriter _wr = null;
            // Determinazione del nome del file xml.

            // Il nome del file in prima istanza è quello impostato staticamente 
            // nella costante MyControlsXmlFilePath.
            String _XmlFile = MyControlsXmlFilePath;

            /* *******************************************
             * La creazione dinamica della maschera può essere fatta SE E SOLO se
             * è vera anche solo UNA delle due seguenti condizioni:
             * (1) L'utente ha definito staticamente il nome della tabella di riferimento
             *     nella costante 'MyTableName'
             * (2) L'utente ha creato la tabella TBL_UNDER_FORM all'interno del metodo preloadEnvironment().
             * 
             * ******************************************* */
            if (((TBL_UNDER_FORM != null) && (TBL_UNDER_FORM.Name != null)) || MyTableName != null)
            {
                if (_XmlFile == null)
                {
                    //Se l'utente non ha staticamente definito il path del file xml, il sistema 
                    //lo prende dal nome del file aspx.
                    _XmlFile = MyAspxFileName;
                }
                if (MyAspxFileName != null)
                {
                    _XmlFile = MyAspxFileName + ".xml";
                }

                // Se il file NON esiste, il sistema lo crea
                if (System.IO.File.Exists(MapPath(_XmlFile)) == false)
                {
                    if (IsPostBack == false)
                    {
                        //Crea la struttura di configurazione
                        if (TBL_UNDER_FORM!= null && TBL_UNDER_FORM.Name != null)
                            _head = getControlsFromTable(simplex_ORM.SQLSrv.SQLTable.createSQLTable(MyConn, TBL_UNDER_FORM.Name), null);   //TODO.
                        else
                            _head = getControlsFromTable(simplex_ORM.SQLSrv.SQLTable.createSQLTable(MyConn, MyTableName), null);   //TODO.
                        //Scrive la struttura di configurazione nel file
                        if (_head != null)
                        {
                            _wr = _head.getXmlConfigWriter(MapPath(_XmlFile));
                            if (_wr != null)
                                _wr.write();

                        }
                    }
                }// fine creazione dell'eventuale creazione file

                if (IsPostBack == false)
                    ViewState.Clear();
                //Caricamento del file
                drawDynamicForm(_XmlFile, 1);
            }// fine disegno della maschera
            #endregion

            #region WRITE YOUR CODE
            // <%WRITE YOUR CODE FROM HERE%>
            // <%WRITE YOUR CODE TO HERE%>
            #endregion

            //@MdN 15/03/215
            if (MyErrors.Count > 0)
                showErrors();
            else
                ((Literal)this.Master.FindControl("ListOfErrors")).Text = "";

            // <added @MdN: 22/07/2016 - BreadCrumbs>
            AddToSimplexBreadCrumbs();

            //Scrittura del log
            if (IsPostBack == false)
                //<#replace/>
                writeMessageToLog(MyConn, "Form_Load()", MySbc.ToString(), MyApp.CurrentUser.UID.ToString(), Session.SessionID);

        }

        /// <summary>
        /// Gestore dell'evento Click sul tasto "Annulla" della MessageBox
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 08/03/2015
        /// </pre>
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected virtual void R_Annulla_Click(object sender, EventArgs e)
        {
            //MyMessageBoxVisible = false;
            //Session["GenericSpxForm1.Panel_MessageBox.Visible"] = MyMessageBoxVisible;
            //Panel_MessageBox.Visible = MyMessageBoxVisible;
            //Session["GenericSpxForm1.R_Conferma.Click"] = null;
        }

        /// <summary>
        /// Inserisce nell'oggetto sotteso dalla maschera (TBL_UNDER_FORM) le informazioni di servizio che, nella versione base, sono:<br></br>
        /// <ul>
        /// <li>Data_sys o DATA_SYS</li>
        /// <li>Usr o USR</li>
        /// <li>Data_um o DATA_UM</li>
        /// <li>Usr_um o USR_UM</li>
        /// </ul>
        /// <br></br>
        /// In casi di informazioni di servizio diverse, effettuare l'override del metodo.<br></br>
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 22/07/2016 per fattorizzazione (refactoring) da R_Salva_Click
        /// </pre>
        /// </summary>
        protected virtual void setServiceInfos()
        {
            if (TBL_UNDER_FORM != null && MyApp!=null && MyApp.CurrentUser != null)
            {                
                String _JustNow=simplex_ORM.Column.DateToSQLDateString(DateTime.Now);
                String _usr = null;
                String _usrum = null;
                String _datasys = null;
                String _dataum = null;

                // * Controllo 
                foreach(String _s in TBL_UNDER_FORM.SQLColumns)
                {
                    if(_s.CompareTo("Data_sys")==0)
                        _datasys = "Data_sys";
                    if(_s.CompareTo("DATA_SYS")==0)
                        _datasys = "DATA_SYS";
                    if(_s.CompareTo("Data_um")==0)
                        _dataum = "Data_um";
                    if(_s.CompareTo("DATA_UM")==0)
                        _dataum = "DATA_UM";
                    if(_s.CompareTo("Usr_um")==0)
                        _usrum = "Usr_um";
                    if(_s.CompareTo("USR_UM")==0)
                        _usrum = "USR_UM";
                    if(_s.CompareTo("Usr")==0)
                        _usr = "Usr";
                    if(_s.CompareTo("USR")==0)
                        _usr = "USR";
                }
                if(_dataum!=null)
                    TBL_UNDER_FORM.setValue(_dataum, _JustNow);
                if(_usrum!=null)
                    TBL_UNDER_FORM.setValue(_usrum, MyApp.CurrentUser.UID.ToString());
                try                                                                             //<added @MdN: 22/06/2016>
                {   
                    if(_datasys!=null)
                    {
                        if (TBL_UNDER_FORM.getValue(_datasys) == null)
                        {
                            TBL_UNDER_FORM.setValue(_datasys, _JustNow);
                            if(_usr!=null)
                                TBL_UNDER_FORM.setValue(_usr, MyApp.CurrentUser.UID.ToString());
                        }
                    }                
                }
                catch (simplex_ORM.Spx_ORMMessage ORMM)
                {
                    if(MyConn!=null && MyApp!=null && MyApp.CurrentUser!=null && Session!=null)
                    ORMM.traceLog(MyConn, ((UNIF_User)MyApp.CurrentUser).UID.ToString(), Session.SessionID);

                    MyErrors.Add(ORMM);
                    if (MyErrors.Count > 0)
                        showErrors();
                    return;
                }                                                                              //</added @MdN: 22/06/2016> 

            }
        }// fine setServiceInfos

        /// <summary>
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: ???
        /// Mdfd: 22/06/2016
        /// </pre>
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected virtual void R_Salva_Click(object sender, EventArgs e)
        {
            try
            {
                this.copyValues(TBL_UNDER_FORM, "R_Dynamic", "System.Web.UI.WebControls.PlaceHolder");
            }
            catch (simplex_ORM.Spx_ORMMessage ORMM)
            {
                ORMM.traceLog(MyConn, ((GeneralApplication.GeneralUser)MyApp.CurrentUser).UID.ToString(), Session.SessionID);
                MyErrors.Add(ORMM);
                if (MyErrors.Count > 0)
                    showErrors();
                return;
            }

            #region [OPZIONALE]: valorizzazione delle informazioni di servizio
            /*
             * SE NON INTERESSATI ALLA VALORIZZAZIONE DELLE INFORMAZIONI DI SERVIZIO,
             * COMMENTARE TUTTO IL CODICE SCRITTO IN QUESTA REGION.
             * 
             * IN CASO DI VALORIZZAZIONE DI INFORMAZIONI DI SERVIZIO PERSONALIZZATE,
             * USARE LA REGION "WRITE YOUR CODE HERE".
             */

            setServiceInfos();

            #endregion

            #region [WRITE YOUR CODE HERE]
            //WRITE YOUR CODE FROM HERE

            //WRITE YOUR CODE TO HERE
            #endregion

            

            try
            {
                //Reset connessione
                if (MyConn.State == ConnectionState.Open)
                    MyConn.Close();
                if (MyConn.State == ConnectionState.Closed)
                    MyConn.Open();
                TBL_UNDER_FORM.save(true);
                if (MyConn.State == ConnectionState.Open)
                    MyConn.Close();

                this.show(TBL_UNDER_FORM, "R_Dynamic", "System.Web.UI.WebControls.PlaceHolder");
            }
            catch (simplex_ORM.Spx_ORMMessage ORMM)
            {
                ORMM.traceLog(MyConn, ((GeneralApplication.IUser)MyApp.CurrentUser).UID.ToString(), Session.SessionID);
                MyErrors.Add(ORMM);
                if (MyErrors.Count > 0)
                    showErrors();
                return;
            }

            #region WRITE YOUR CODE HERE
            //WRITE YOUR CODE FROM HERE

            //WRITE YOUR CODE TO HERE
            #endregion

            R_Lista.DataBind();
        }

        protected virtual void R_Back_Click(object sender, EventArgs e)
        {
            String _destination;
            #region WRITE YOUR CODE HERE
            //WRITE YOUR CODE FROM HERE

            //WRITE YOUR CODE TO HERE
            #endregion
            _destination = MySbc.PreviousLink;
            Response.Redirect(_destination);
        }

        protected virtual void R_Delete_Click(object sender, EventArgs e)
        {

            /** IL COMANDO AGISCE IN DUE FASI:
             * Verifica il controllo sender
             * 1) Se il sender è il bottone "R_Delete" -- FASE 1: VISUALIZZA IL MESSAGE BOX E VALORIZZA IL DELEGATO DEL TASTO OK
             * 2) Se il sender è il controllo WebSimplexMessageBox "R_MessageBox" -- FASE 2: EFFETTUA L'OPERAZIONE.
             * **/
            if (sender.GetType().ToString().CompareTo("System.Web.UI.WebControls.Button") == 0)
            {

                // * Controllo
                if (TBL_UNDER_FORM != null && TBL_UNDER_FORM.getValue("ID") == null)
                {
                    simplex_ORM.Spx_ORMMessage ORMM = new simplex_ORM.Spx_ORMMessage("<b>ATTENZIONE</b></br>Nessun oggetto da cancellare.");
                    if (MyConn != null && MyApp != null && MyApp.CurrentUser != null && Session.SessionID != null)
                        ORMM.traceLog(MyConn, MyApp.CurrentUser.UID.ToString(), Session.SessionID);
                    MyErrors.Add(ORMM);
                    showErrors();
                    return;
                }

                // FASE 1:
                Session["Evt_OkButton"] = new EventHandler(this.R_Delete_Click);
                this.R_MessageBox.show((int)WebSimplex.WebSimplexMessageBox.BoxType.OkMessage);
                R_MessageBox.Visible = true;
                return;
            }
            else
            {
                //Azione
                #region WRITE YOUR CODE HERE
                //WRITE YOUR CODE FROM HERE
                if (TBL_UNDER_FORM != null && MyApp!=null && MyApp.CurrentUser !=null)
                {
                    String _justNow=simplex_ORM.Column.DateToSQLDateString(DateTime.Now);
                    TBL_UNDER_FORM.setValue("DATA_DEL", _justNow);
                    TBL_UNDER_FORM.setValue("DATA_UM", _justNow);
                    TBL_UNDER_FORM.setValue("USR_UM", MyApp.CurrentUser.UID.ToString());
                    TBL_UNDER_FORM.save(true);

                    //cancello la maschera, pronta per una nuova immissione
                    TBL_UNDER_FORM = simplex_ORM.SQLSrv.SQLTable.createSQLTable(MyConn, MyTableName);
                    Session[MyName] = TBL_UNDER_FORM;

                    // REdirect su me stesso
                    HttpContext.Current.Response.Redirect(MyAspxFileName);
                }
                else
                {
                    simplex_ORM.Spx_ORMMessage ORMM = new simplex_ORM.Spx_ORMMessage("Impossibile continuare con la cancellazione.");
                    if (MyConn != null && MyApp != null && MyApp.CurrentUser != null && Session.SessionID != null)
                        ORMM.traceLog(MyConn, MyApp.CurrentUser.UID.ToString(), Session.SessionID);
                    MyErrors.Add(ORMM);
                    showErrors();
                    return;
                }
                //WRITE YOUR CODE TO HERE
                #endregion
            }
        }

        protected void R_Nuovo_Click(object sender, EventArgs e)
        {
            if (MyConn != null)
                try
                {
                    TBL_UNDER_FORM = simplex_ORM.SQLSrv.SQLTable.createSQLTable(MyConn, MyTableName);
                    Session[MyName] = TBL_UNDER_FORM;
                    Response.Redirect(MyAspxFileName);
                }
                catch (simplex_ORM.Spx_ORMMessage ORMM)
                {
                    if (MyConn != null && MyApp != null && MyApp.CurrentUser != null && Session.SessionID != null)
                        ORMM.traceLog(MyConn, MyApp.CurrentUser.UID.ToString(), Session.SessionID);
                    MyErrors.Add(ORMM);
                    showErrors();
                    return;
                }
            else
            {
                simplex_ORM.Spx_ORMMessage ORMM = new simplex_ORM.Spx_ORMMessage("Non esiste una connessione.");
                if (MyConn != null && MyApp != null && MyApp.CurrentUser != null && Session.SessionID != null)
                    ORMM.traceLog(MyConn, MyApp.CurrentUser.UID.ToString(), Session.SessionID);
                MyErrors.Add(ORMM);
                showErrors();
                return;
            }

        }
        #endregion

        #region TEST - DA ELIMINARE - DELETEME
        protected void Change_Test(object sender, EventArgs e)
        {
        //    simplex_ORM.Spx_ORMMessage ORMM = new simplex_ORM.Spx_ORMMessage("Change_Test", 0);
        //    ORMM.traceLog(MyConn, ((GeneralApplication.GeneralUser)MyApp.CurrentUser).UID.ToString(), Session.SessionID);
        }
        #endregion


    }
}
