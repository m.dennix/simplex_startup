<%@ Page Language="C#" MasterPageFile="~/MasterPCM.Master" AutoEventWireup="true" CodeBehind="GenericDetailedListSpxForm.aspx.cs" Inherits="SIMPLEX_STARTUP.GenericDetailedListSpxForm" Title="Titolo.GenericDetailedSpxForm" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MenuPlaceHolder" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="MainFormSection">
  <table class="FRM">
    <tr class="FRM">
        <td class="FRM_COL1">
            &nbsp;</td>
        <td class="FRM_COL2">
            &nbsp;</td>
        <td class="FRM_COL3">
            <asp:Button ID="R_Indietro" runat="server" Text="Indietro" 
                onclick="R_Indietro_Click" />
        </td>
    </tr>
    <tr class="FRM">
        <td class="FRM_COL1">
            &nbsp;</td>
        <td class="FRM_COL2">
            &nbsp;</td>
        <td class="FRM_COL3">&nbsp;</td>
    </tr>
    <tr class="FRM">
        <td class="FRM_COL1">
            <asp:Label ID="L_Azioni" runat="server" Text="Azioni"></asp:Label>
        </td>
        <td class="FRM_COL2">
            <asp:Button ID="R_Nuovo" runat="server" Text="Nuovo" onclick="R_Nuovo_Click" />
        &nbsp;&nbsp;
            <asp:Button ID="R_Salva" runat="server" onclick="R_Salva_Click" Text="Salva" />
        </td>
        <td class="FRM_COL3">
            <asp:Button ID="R_Elimina" runat="server" Text="Elimina" />
        </td>
    </tr>
    </table>
 </div>
  <div id="SubFormSection">
  <!--
      <asp:GridView ID="R_Lista" runat="server" AllowPaging="True" 
          AllowSorting="True">
          <Columns>
              <asp:ButtonField ButtonType="Button" CommandName="Edit" HeaderText="Comando" 
                  ShowHeader="True" Text="Seleziona" />
          </Columns>
      </asp:GridView>
  -->
      <asp:GridView ID="R_Lista" runat="server" AllowPaging="True">
          <RowStyle CssClass="FRM_Cell_Item" />
          <Columns>
            
              <asp:ButtonField ButtonType="Button" Text="Seleziona" CommandName="seleziona"/>
            
          </Columns>
          <FooterStyle CssClass="FRM_Cell_Footer" />
          <PagerStyle CssClass="FRM_Cell_Footer" />
          <HeaderStyle CssClass="FRM_Cell_Header" />
          <AlternatingRowStyle CssClass="FRM_Cell_Item_Alt" />
      </asp:GridView>
      <asp:SqlDataSource ID="R_SqlDataSource" runat="server" SelectCommand="SELECT * FROM [MYTABLE]"></asp:SqlDataSource>
 </div>
</asp:Content>
