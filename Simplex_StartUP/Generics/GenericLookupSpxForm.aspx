﻿<%@ Page Language="C#" MasterPageFile="~/MasterPCM.Master" AutoEventWireup="true" CodeBehind="GenericLookupSpxForm.aspx.cs" Inherits="SIMPLEX_STARTUP.GenericLookupSpxForm" Title="GenericLookupSpxForm" %>
<%@ Register Assembly="WebSimplex" Namespace="WebSimplex" TagPrefix="WS1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MenuPlaceHolder" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

<WS1:WebSimplexMessageBox ID="R_MessageBox" runat="server" 
        AlertText="Attenzione, vedi il riquadro degli errori." 
        OkAlertButtonCssStyle="AlertButton" 
        AlertImage="Immagini/Omino_Errore.png" 
        OkMessageImage="Immagini/Megafono_tondoviola.png" 
        OkMessageText="Continuare con l'operazione?"
        OkMessageButtonCssStyle="OkButton" 
        UndoMessageButtonCssStyle="UndoButton" CssOkMsgClass="OkMsg" />

<div id="MainFormSection">
<table class="FRM">
    <tr class="FRM">
    <td class="FRM_COL1"></td>
    <td class="FRM_COL2"></td>
    <td class="FRM_COL3">
        <asp:Button ID="R_Back" runat="server" Text="Indietro" onclick="R_Back_Click" />
        </td>
    </tr>
    <tr class="FRM">
        <td class="FRM_COL1"></td>
        <td class="FRM_COL2">
        <!-- MessageBox -->
        <!-- END MessageBox -->
        </td>
    <td class="FRM_COL3"></td>
    </tr>
</table>

<!-- PUT YOUR CONTROLS FROM HERE -->
    <asp:PlaceHolder ID="R_Dynamic" runat="server">
    </asp:PlaceHolder>
<!-- PUT YOUR CONTROLS TO HERE -->

<table class="FRM">
    <tr class="FRM">
    <td class="FRM_COL1">
        <asp:Button ID="R_Nuovo" runat="server" onclick="R_Nuovo_Click" Text="Nuovo" />
        </td>
    <td class="FRM_COL2">
        <asp:Button ID="R_Salva" runat="server" Text="Salva" onclick="R_Salva_Click" />
        </td>
    <td class="FRM_COL3">
        <asp:Button ID="R_Delete" runat="server" Text="Elimina" 
            onclick="R_Delete_Click" />
        </td>
    </tr>
</table>

    <asp:Panel ID="R_PannelloRicerca" runat="server" BackColor="#9999FF" 
        Height="56px" Width="997px">
        <br />
        <asp:Label ID="L_Ricerca" runat="server" Text="Ricerca rapida su " 
            CssClass="lbl_"></asp:Label>
        <asp:TextBox ID="R_TestoRicerca" runat="server" CssClass="FRM_txt" 
            Width="487px"></asp:TextBox>
        &nbsp;
        <asp:Button ID="R_Cerca" runat="server" CssClass="FRM_btn_HighLight" 
            Text="Cerca" onclick="R_Cerca_Click" />
        &nbsp;<asp:Button ID="R_Reset" runat="server" onclick="R_Reset_Click" Text="Reset" />
        <br />
    </asp:Panel>
    
</div>
<div ID="SubFormSection">

    <asp:GridView ID="R_Lista" runat="server" CssClass="FRM_Cell_Item">
        <FooterStyle CssClass="FRM_Cell_Footer" />
        <HeaderStyle CssClass="FRM_Cell_Header" />
        <AlternatingRowStyle CssClass="FRM_Cell_Item_Alt" />
    </asp:GridView>
    <br />
    <asp:SqlDataSource ID="SqlDataSource_LookUP" runat="server"></asp:SqlDataSource>

</div>
</asp:Content>
