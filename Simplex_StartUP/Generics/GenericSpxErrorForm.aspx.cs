using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using GeneralApplication;
using WebSimplex;

namespace SIMPLEX_STARTUP
{
    /// <summary>
    /// Derivata da SimplexForm.
    /// Ha attributi protetti necessari per il funzionamento dell'applicazione.
    /// <br></br>
    /// Per passare un messaggio alla maschera usare la variabile di sessione "SpxErrorMessage"
    /// <br></br>
    /// Es. Session.Add("SpxErrorMessage", _sorm);
    /// <br></br>
    /// <remarks>Per passare in controllo a questa pagina non usare il semplice metodo di redirezione (redirect) ma il seguente codice:<br></br>
    /// <pre>
    /// HttpContext.Current.Response.Redirect("~/GenericSpxErrorForm");
    /// </pre>
    /// </remarks>
    /// <br></br>
    /// Esegue Override dei seguenti metodi:
    /// <pre>
    /// ------
    /// @MdN 
    /// Crtd: 06/12/2014
    /// Mdfd: 14/03/2016 - test se la sessione è scaduta.
    /// Mdfd: 09/06/2016 - ancora sulla sessione scaduta.
    /// </pre>
    /// </summary>
    public partial class GenericSpxErrorForm : simplex_FORM.SimplexForm
    {
        #region ATTRIBUTTI
        /// <summary>
        /// Oggetto applicazione corrente.
        /// </summary>
        protected GeneralApplication.GeneralApplication MyApp = null;
        /// <summary>
        /// Oggetto connessione corrente al database UNIF_PERSONALE
        /// DEVE essere sempre chiusa.
        /// </summary>
        protected System.Data.Odbc.OdbcConnection MyConn = null;
        /// <summary>
        /// Nome della pagina come visualizzata nel menu.
        /// </summary>
        protected String MyName = null;
        /// <summary>
        /// Riferimento all' oggetto di classe WebSimplexBreadCrumbs.
        /// </summary>
        protected WebSimplexBreadCrumbs MySbc = null;
        /// <summary>
        /// Nome del file della pagina corrente. Viene valorizzato automaticamente 
        /// dal metodo preloadEnvironment().
        /// </summary>
        protected String MyAspxFileName = null;
        /// <summary>
        /// URL della pagina chiamante.
        /// </summary>
        protected String MyBackURL = null;
        /// <summary>
        /// Messaggio di errore: viene passato attraverso la variabile di Sessione "SpxErrorMessage".
        /// </summary>
        protected String MyMessageError = null;
        /// <summary>
        /// Indica se il riquadro dei messaggi deve essere visibile o meno.
        /// Per default NON è visibile.
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 08/03/2015
        /// </pre>
        /// </summary>
        protected Boolean MyMessageBoxVisible = false;
        /// <summary>
        /// Menu verticale associato alla maschera
        /// </summary>
        protected System.Web.UI.WebControls.Menu MyMainMenu = null;
        #endregion

        #region METODI PROTETTI

        protected virtual Boolean validateForm()
        {
            // NA MAZZA
            return true;
        }
        

        /// <summary>
        /// 
        /// </summary>
        /// <param name="p_str"></param>
        /// <param name="p_format"></param>
        /// <returns></returns>
        protected bool isAValidDateString(String p_str, String p_format)
        {
            //casi banali
            if (p_str == null)
                return false;

            if (p_format == null)
                return false;

            Boolean _toRet = true;
            int _len = p_format.Length;
            String _teststr = null;
            if (p_str.Length > _len)
                _teststr = p_str.Substring(0, _len);
            else
                _teststr = p_str;

            try
            {
                /* verifica la sintassi */
                _toRet = simplex_ORM.Column.isDateTime(p_str, p_format);
                if (_toRet == true)
                {
                    /* verifica la semantica */
                    simplex_ORM.Column.parseDateTimeString(p_str, p_format);
                }
            }
            catch (simplex_ORM.Spx_ORMMessage ormm)
            {
                _toRet = false;
            }
            return _toRet;
        }


        /// <summary>
        /// Aggiunge la pagina corrente alla catena delle briciole di pane (SimplexBreadCrumbs)
        /// della maschera corrente.
        /// @MdN
        /// ----
        /// Crtd: 18/01/2015
        /// Mdfd: 25/01/2015
        /// </summary>
        /// <remarks>La proprietà MySbc DEVE essere valorizzata.</remarks>
        protected virtual void AddToSimplexBreadCrumbs()
        {
            int _lvl;
            // controllo
            if (MySbc == null)
                return;
            // AGGIUNTA DI UNA VOCE NELLA CATENA BREADCRUMBS
            if (MyMainMenu != null)
            {
                // Se esiste un menu mi pongo il problema della coerenza tra il menu 
                // e la catena di BreadCrumbs.
                _lvl = MasterPCM.getMenuVoiceLevel(MyConn, MyName);
                if (_lvl == -1)
                    MySbc.Add(MyName, MyAspxFileName);
                else
                {
                    if (_lvl > MySbc.Count)
                        MySbc.Add(MyName, MyAspxFileName);                      //@MdN 25/01/2015
                    else
                        MySbc.InsertAndBreak(MyName, MyAspxFileName, _lvl);     //@MdN 25/01/2015
                }
            }
            else
            {
                // Se il menu non esiste non mi pongo problemi ed accodo all'ultimo anello della catena.
                if (MySbc != null)
                    MySbc.Add(MyName, MyAspxFileName);
            }
        }//fine

        /// <summary>
        /// <p>
        /// Imposta gli attributi necessari al funzionamento.
        /// Si attende in Session["APPLICATION"] l'oggetto GeneralApplication corrente.
        /// Se non è presente, lancia un eccezione del tipo generalApplication TracedMessage.
        /// Valorizza:
        /// </p>
        /// <p>
        /// Valorizza i seguenti attributi:
        /// </p>
        /// <ul>
        /// <li>MyApp               :   APPLICAZIONE CORRENTE;</li>
        /// <li>MyConn              :   CONNESSIONE DI DEFAULT;</li>
        /// <li>MyMainMenu          :   CONTROLLO MENU SINISTRO;</li>
        /// <li>MySbc               :   CONTROLLO BREADCRUMBS;</li>
        /// <li>MyAspxFileName      :   NOME DELLA PAGINA WEB/WEB FORM.</li>
        /// <li>MyBackURL           :   URL DELLA PAGINAWEB/WEB FORM CHIAMANTE.</li>
        /// </ul>
        /// <pre>
        /// ------
        /// @MdN
        /// Crtd: 06/12/2014
        /// Mdfd: 30/01/2015
        /// Mdfd: 15/04/2016 12:18 - Inserita creazione nuova UNIF_Application
        /// Mdfd: 18/04/2016 19:40 - Corretto il mancato controllo di un oggetto non istanziato.
        /// </pre>
        /// </summary>
        protected override void preloadEnvironment()
        {
            base.preloadEnvironment();
            MyApp = (UNIF_Application)Session["APPLICATION"];
            if (MyApp != null)
                MyConn = (System.Data.Odbc.OdbcConnection)MyApp.DefaultConnection;
            else
            {
                //throw new TracedMessage("Istanza di applicazione inesistente", "UNIF_PERSONALE", "preloadEnvironment()");

                // @MdN inserito in data 15/04/2016 - 12:18 
                MyApp = new UNIF_Application();
                MyApp.loadConnections("OdbcConfig");
                MyConn = (System.Data.Odbc.OdbcConnection)MyApp.DefaultConnection;
                // @MdN inserito in data 15/04/2016 - 12:18 
            }

            // valorizzazione del riferimento al menu principale della maschera
            try
            {
                MyMainMenu = (Menu)this.Master.FindControl("R_MainMenu");
            }
            catch (Exception E)
            {
                MyMainMenu = null;
            }

            // valorizzazione delle briciole di pane
            try
            {
                MySbc = (WebSimplexBreadCrumbs)this.Master.FindControl("R_BreadCrumbs");
            }
            catch (Exception E)
            {
                MySbc = null;
            }

            MyAspxFileName = this.Page.AppRelativeVirtualPath.Substring(this.Page.AppRelativeVirtualPath.LastIndexOf("/") + 1);

            // Determina la visibilità del MessageBox
            MyMessageBoxVisible = true;

            /* @MdN: prelievo messaggio dalla Session */
            MyMessageError = (String)Session["SpxErrorMessage"];
            if (MyMessageError == null)
                MyMessageError = "Sessione scaduta";
            Session["SpxErrorMessage"] = null;

            /* @MdN: se la variabile della pagina chiamante è nulla, oscura il tasto Indietro. */
            MyBackURL = (String)Session["CALLING-PAGE"];
            if (MyBackURL == null)
            {
                this.R_Conferma.Visible = false;
                MyBackURL = "~/Login.aspx";
            }

            ////Scrittura del log - @MdN Sportato in data 15/04/2016 - 12:18 
            if (IsPostBack == false)
            {
                if (Session != null && MyApp!=null && MyConn!=null && MyApp.CurrentUser !=null) //@MdN Corretto in data 18/04/2016 19:40
                    writeMessageToLog(MyConn, MyMessageError, "GenericSpxErrorForm.Form_Load()", ((UNIF_User)MyApp.CurrentUser).UID.ToString(), Session.SessionID);
                else
                    writeMessageToLog(MyConn, "Sessione scaduta", "GenericSpxErrorForm.Form_Load()", "0", null);

            }

        }//fine preloadEnvironment

        /// <summary>
        /// Testa se la sessione attuale è in piedi.
        /// Basata sull'articolo <a href="http://aspalliance.com/520_Detecting_ASPNET_Session_Timeouts.2">http://aspalliance.com/520_Detecting_ASPNET_Session_Timeouts.2</a>
        /// <br></br>
        /// Da fattorizzare!!!
        /// <br></br>
        /// 
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 14/03/2016
        /// </pre>
        /// </summary>
        protected bool checkSessionTimeOut
        {
            get
            {
                if (Session != null)
                {
                    if (Session.IsNewSession == true)
                    {
                        // OK, testiamo se nella Request è presente il cookie
                        String _silentCookie = Request.Headers["Cookie"];
                        if ((_silentCookie != null) && (_silentCookie.IndexOf("ASP.NET_SessionId") >= 0))
                        {
                            return true;
                        }
                    }
                }
                return false;
            }
        }

        #endregion

        /// <summary>
        /// Caricamento della pagina degli errori.
        /// <pre>
        /// ----
        /// @MdN
        /// Mdfd: 09/06/2016
        /// </pre>
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                preloadEnvironment();
            }
            catch (TracedMessage _tm)
            {
                //per tracciare l'evento nel log devo necessariamente aprire la connessione di default ex-novo
                UNIF_Application MyAppl = new UNIF_Application();
                MyAppl.loadConnections("OdbcConfig");
                MyConn = MyAppl.DefaultConnection;
                if (MyAppl.DefaultConnection != null)
                {
                    //_tm.toLog(MyApp.DefaultConnection);
                    simplex_ORM.Spx_ORMMessage _som = new simplex_ORM.Spx_ORMMessage(_tm.Message);
                    _som.traceLog(MyConn, "system", _tm.Message);
                    MyErrors.Add(_som);
                }
                // @MdN 14/03/2016
                if (this.checkSessionTimeOut == true)
                {
                    MyMessageError = "Sessione scaduta, prego rieffettuare il login.";
                    simplex_ORM.Spx_ORMMessage _som = new simplex_ORM.Spx_ORMMessage(MyMessageError);
                    _som.traceLog(MyConn, "system", _tm.Message);
                    MyErrors.Add(_som);
                }
                // @MdN 14/03/2016
            }
            // @MdN 14/03/2016
            // <%WRITE YOUR CODE FROM HERE%>
            if (MyMessageError != null)
            {
                this.L_Message.Text = MyMessageError;
                if (Session["MyMessageError"] == null)
                {
                    this.R_Annulla.Text = "Login";                          //@MdN 09/06/2016
                    MySbc.Visible = false;
                }
            }

            // <%WRITE YOUR CODE TO HERE%>
            // @MdN 14/03/2016

            // Evento del BOX dei messaggi
            if (Session["GenericSpxForm1.R_Conferma.Click"] != null)
            {
                /* Parte del codice necessaria ad impostare il più opportuno gestore dell'evento
                 * Click del tasto R_Conferma del MessageBox.
                 * Vedere la documentazione a corredo.
                 */
                // <%WRITE YOUR CODE FROM HERE%>

                // <%WRITE YOUR CODE TO HERE%>
            }

            //@MdN 15/03/2015
            if (MyErrors.Count > 0)
                showErrors();
            else
                ((Literal)this.Master.FindControl("ListOfErrors")).Text = "";

            //Scrittura del log:
            //Adattare questa funzione alle singole fattispecie
            if (IsPostBack == false && MyConn!=null && MySbc!=null && MyApp !=null && Session!=null && MyApp.CurrentUser !=null)
                writeMessageToLog(MyConn, "GenericSpxForm.Form_Load()", MySbc.ToString(), ((UNIF_User)(MyApp.CurrentUser)).UID.ToString(), Session.SessionID);

        }

        /// <summary>
        /// <p>EN: Show the errors occurred in the last operation. At the end, clears the list of errors. 
        /// </p>
        /// <p>IT: Mostra gli errori verificatisi nell'ultima operazione. Al termine, cancella la lista degli errori.
        /// </p>
        /// <pre>
        /// @MdN
        /// ----
        /// Mdfd: 18/04/2015
        /// </pre>
        /// </summary>
        protected virtual void showErrors()
        {
            if (MyErrors.Count > 0)
            {
                Boolean l_pari = false;
                Literal l_ErrArea = null;
                System.Text.StringBuilder l_ErrString = new System.Text.StringBuilder("<table class=\"ErrTab\">");
                l_ErrArea = (Literal)this.Master.FindControl("ListOfErrors");

                l_ErrString.Append("<tr class=\"ErrHeaders\"><th class=\"HeadCodCol\">Codice</th><th class=\"HeadDescCol\">Descrizione</th><tr>");

                foreach (simplex_ORM.Spx_ORMMessage ORMM in MyErrors)
                {
                    if (l_pari == true)
                        l_ErrString.Append("<tr class=\"ErrRowPari\">");
                    else
                        l_ErrString.Append("<tr class=\"ErrRowDisPari\">");
                    l_pari = !l_pari;

                    l_ErrString.Append("<td class=\"ErrCodCol\">");
                    l_ErrString.Append(ORMM.MessageCode.ToString());
                    l_ErrString.Append("</td><td class=\"ErrDescCol\">");
                    l_ErrString.Append(ORMM.Message);
                    l_ErrString.Append("</td></tr>");
                }
                l_ErrString.Append("</table>");
                l_ErrArea.Text = l_ErrString.ToString();
                MyErrors.Clear(); //<-- ATTENZIONE
            }
        }//fine

        /// <summary>
        /// Mostra l'oggetto SQLTable che supporta la maschera.
        /// ---
        /// @MdN
        /// Crtd: 01/01/2015
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected virtual void Page_SaveStateComplete(object sender, EventArgs e)
        {
            if (TBL_UNDER_FORM != null)
                this.show(TBL_UNDER_FORM, "ContentPlaceHolder1", "System.Web.UI.WebControls.ContentPlaceHolder");
        }

        /// <summary>
        /// Nome della pagina visualizzato nei menu e nelle briciole di pane.
        /// @MdN
        /// ----
        /// Crtd: 18/01/2015
        /// </summary>
        public virtual String Nome
        {
            get
            {
                return MyName;
            }
            set
            {
                if (value == null)
                    MyName = "### DA DEFINIRE";
                else
                    MyName = value;
            }
        }

        /// <summary>
        /// Gestore dell'evento Click sul tasto "Annulla" della MessageBox
        /// Torna al DESKTOP
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 19/06/2015
        /// </pre>
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void R_Annulla_Click(object sender, EventArgs e)
        {
            if (MyMessageError != null && Session["MyMessageError"]!=null)
                Response.Redirect("~/Default.aspx");
            else
                Response.Redirect("~/Login.aspx");
        }

        /// <summary>
        /// <p>
        /// Gestore dell'evento click sul tasto "Indietro".
        /// </p>
        /// <pre>
        /// @MdN
        /// ----
        /// Crtd: 19/06/2015
        /// </pre>
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void R_Conferma_Click(object sender, EventArgs e)
        {
            if (MyBackURL == null)
                return;
            Response.Redirect(MyBackURL);
        }
    }
}
