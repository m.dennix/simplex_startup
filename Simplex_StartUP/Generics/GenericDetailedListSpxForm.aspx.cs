using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;

namespace SIMPLEX_STARTUP
{
    /// <summary>
    /// <p>
    /// Classe che implementa una maschera <i>STATICA</i> che mostra una lista di oggetti.<br></br>
    /// Basata su GenericSpxForm v. 1.0.0.3 del 14/07/2016 o superiori.<br></br>
    /// </p>
    /// <b>Nota Bene:</b><br></br>
    /// La maschera possiede un controllo GridView chiamato <i>R_Lista</i> di cui devono essere definiti:<br></br>
    /// - Le chiavi (attributo MyDataKeys): per default la maschera si aspetta un solo attributo chiave denominato '<b>ID</b>'.<br></br>
    /// - L'origine dati (Atributo MyDBConnectionStringName): da definire dinamicamente nel metodo <i>preloadEnvironment()</i>.<br></br>
    /// <p>
    /// Stato:<br></br>
    /// - ViewSytate["MYSQL"]: Query attuale<br></br>
    /// - ViewSytate["MYFILTER"]: Filtro attuale<br></br>
    /// - ViewSytate["MYORDER"]: Ordinamento attuale<br></br>
    /// <br></br>
    /// - Session[MyName]: oggetto di classe Simplex_ORM.SQLSrv.SQLTable di riferimento per il dettaglio (oggetto corrente).
    /// </p>
    /// <p>
    /// Stato iniziale delle variabili da valorizzare in <i>preloadEnvironment()</i>:<br></br>
    /// -  MyTargetForm = null: Se impostato a null, redirige su se stessa; <br></br>
    /// -  MySql_base = null; <br></br>
    /// -  MyFilter_base = null; <br></br>
    /// -  MyOrder_base = null; <br></br>
    /// -  MyDBConnectionStringName = null; <br></br>
    /// </p>
    /// <pre>
    /// ----
    /// @MdN
    /// Crtd: 14/07/2016 v.1.0.0.0
    /// </pre>
    /// </summary>
    public partial class GenericDetailedListSpxForm : GenericSpxForm
    {

        #region ATTRIBUTI PROTETTI DA DEFINIRE
        /// Stringa della query di BASE senza filtraggio ed ordinamento
        /// </summary>
        protected String MySql_base = null;
        /// <summary>
        /// Stringa del filtro di BASE (senza clausola WHERE)
        /// </summary>
        protected String MyFilter_base = null;
        /// <summary>
        /// Stringa dei criteri di ordinamento di BASE  (Senza clausola ORDER BY)
        /// </summary>
        protected String MyOrder_base = null;
        /// <summary>
        /// NOME nome stringa di connessione al database, come risulta dal file web.config
        /// </summary>
        protected String MyDBConnectionStringName = null;
        /// <summary>
        /// Stringa di connessione al database, come risulta dal file web.config e corrispondente al
        /// nome impostato nell'attributo <see cref="MyDBConnectionStringName"/>.<br></br>
        /// <b>Nota:</b><br></br>
        /// Viene impostato in preloadEnvironment()
        /// </summary>
        protected String MyConnString = null;
        /// <summary>
        /// Array delle stringhe che rappresentano le chiavi della GridView R_Lista.<br></br>
        /// Per default la maschera si aspetta una sola chiave di nome "<b>ID</b>".
        /// Modificare in caso di chiave composita o di chiave con nome di verso da "ID".<br></br>
        /// </summary>
        protected String[] MyDataKeys = {"ID"};
        /// <summary>
        /// Se valorizzato questo attributo sposta il controllo su una finestra che mostra i dettagli dell'oggetto di classe SQLTable; <br></br>
        /// se non è valorizzato il controllo rimane sulla maschera stessa e l'oggetto di classe SQLTable viene visualizzato in una 
        /// sezione apposita.
        /// </summary>
        protected String MyTargetForm = null;
        #endregion

        #region ATTRIBUTI PROTETTI
        /// Stringa della query attuale (valorizzata da ViewSytate["MYSQL"])
        /// </summary>
        protected String MySql = null;
        /// <summary>
        /// Stringa del filtro attuale (valorizzata da ViewSytate["MYFILTER"])
        /// </summary>
        protected String MyFilter = null;
        /// <summary>
        /// Stringa dei criteri di ordinamento attuale (valorizzata da ViewSytate["MYORDER"])
        /// </summary>
        protected String MyOrder = null;
        #endregion

        #region GESTORI

        /// <summary>
        /// Salva l'oggetto.<br></br>
        /// Se possibile, eseguire in modo opportuno l'override del metodo.
        /// <pre>
        /// -----
        /// @MdN
        /// Crtd: 19/07/2016
        /// </pre>
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected virtual void R_Salva_Click(object sender, EventArgs e)
        {
            if (TBL_UNDER_FORM != null)
            {
                try
                {
                    Boolean _isNew = true;
                    if (TBL_UNDER_FORM.getValue("USR") != null)
                        _isNew = false;

                    // Impostazione delle informazioni di controllo
                    TBL_UNDER_FORM.setValue("USR_UM", MyApp.CurrentUser.UID.ToString());
                    TBL_UNDER_FORM.setValue("DATA_UM", simplex_ORM.SQLSrv.SQLColumn.DateToSQLDateString(DateTime.Now));
                    if (_isNew == true)
                        TBL_UNDER_FORM.setValue("USR", MyApp.CurrentUser.UID.ToString());
                    // Salvataggio
                    this.modify(TBL_UNDER_FORM, "ContentPlaceHolder1", "System.Web.UI.WebControls.ContentPlaceHolder");

                    // TRacciamento
                    if (MyApp != null && MyApp.CurrentUser != null && MyConn != null && Session != null)
                    {
                        if (_isNew == true)
                            writeMessageToLog(MyConn, "Inserito nuovo oggetto " + TBL_UNDER_FORM.Name, this.MyAspxFileName + ":R_Salva_Click()", MyApp.CurrentUser.UID.ToString(), Session.SessionID);
                        else
                            writeMessageToLog(MyConn, "Aggiornato:" + TBL_UNDER_FORM.Name + ": " + TBL_UNDER_FORM.ToString(), this.MyAspxFileName + ":R_Salva_Click()", MyApp.CurrentUser.UID.ToString(), Session.SessionID);
                    }
                }
                catch (simplex_ORM.Spx_ORMMessage _ORMM)
                {
                    if (MyConn != null && MyApp != null && MyApp.CurrentUser != null && Session.SessionID != null)
                        _ORMM.traceLog(MyConn, MyApp.CurrentUser.UID.ToString(), Session.SessionID);
                    MyErrors.Add(_ORMM);
                    showErrors();
                    return;
                }
            }
        } // Fine R_Salva_Click()


        /// <summary>
        /// Azione intrapresa premendo il tasto "Seleziona".
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 15/07/2016
        /// </pre>
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected virtual void R_Lista_RowCommand(object sender, GridViewCommandEventArgs e)
        {

            Int32 _row = 0;
            Int32 _key = 0;
            String _KeyStr = null;
            if (e.CommandName.ToLower().CompareTo("seleziona") == 0)
            {
                // identificare la riga che ha scatenato l'evento: ASP.NET non lo fa in automatico
                // N.B. CommandArgument è 0-BASED!!!!
                _row = Int32.Parse(e.CommandArgument.ToString());

                _KeyStr = (String)((System.Web.UI.WebControls.GridView)e.CommandSource).DataKeys[0].Value.ToString();

                TBL_UNDER_FORM = null;

                if (MyConn != null)
                {

                    TBL_UNDER_FORM = simplex_ORM.SQLSrv.SQLTable.createSQLTable(MyConn, TableName);
                    TBL_UNDER_FORM.setValue("ID", _KeyStr);
                    TBL_UNDER_FORM.load();
                    Session.Add(MyName, TBL_UNDER_FORM);
                }
                //Redirezione
                //String _destination = "~";
                //if(MyDirectory!=null)
                //    _destination = _destination + "/" + MyDirectory;
                //if(MyTargetForm!=null)
                //    _destination = _destination + "/" + MyTargetForm;
                //else
                //    _destination = _destination + "/" + MyAspxFileName;

                //Response.Redirect(_destination);
                Response.Redirect(MyAspxFileName);

                //// identificare il valore della chiave associata alla riga
                //_key = Int32.Parse(((GridView)e.CommandSource).Rows[_row].Cells[1].Text);

                #region [ADD YOUR CODE HERE]

                #endregion
            }
        }

        /// <summary>
        /// Inizializzazione della maschera e dei suoi controlli.<br></br>
        /// Nei casi di override, assegnare <i>MyDBConnectionStringName</i> prima di invocare <i>base.Page_Init()</i>.<br></br>
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 15/07/2016
        /// </pre>
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected virtual void Page_Init(object sender, EventArgs e)
        {
            //Definizione della GridView R_Lista.
            R_Lista.RowCommand += new GridViewCommandEventHandler(R_Lista_RowCommand);
            #region [IMPOSTAZIONI R_Lista]
            // CHIAVI
            R_Lista.DataKeyNames = MyDataKeys;
            // DATA SOURCE
            R_SqlDataSource.ConnectionString = MyDBConnectionStringName;
            #endregion
        }


        /// <summary>
        /// <p>Caricamento della maschera.</p>
        /// <ul>
        /// <li>Invoca preloadEnvironment()</li>
        /// <li>esegue la query di popolamento</li>
        /// </ul>
        /// <pre>
        /// @MdN
        /// ----
        /// Crtd: 30/01/2015
        /// </pre>
        /// </summary>
        /// <param name="sender">oggetto che ha invocato l'evento</param>
        /// <param name="e">parametri</param>
        protected override void Page_Load(object sender, EventArgs e)
        {
            #region [ADD YOUR CODE HERE]
            #endregion

            //preloadEnvironment(); //<removed @MdN: 19/07/2016>
            base.Page_Load(sender, e);

            #region [ADD YOUR CODE HERE]
                #region [GESTIONE VISUALIZZAZIONE LISTA OGGETTI]
                if (MySql != null)
                {
                    this.R_SqlDataSource.SelectCommand = MySql;
                    this.R_SqlDataSource.ConnectionString = this.MyConnString; ;
                    this.R_Lista.DataSource = this.R_SqlDataSource;
                    //this.R_Lista.AutoGenerateSelectButton=true;                   
                    this.R_Lista.DataBind();
                }
                #endregion
            #endregion
        }

        /// <summary>
        /// Torna indietro al chiamante.<br></br>
        /// Non pulisce lo stato.<br></br>
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 18/07/2016
        /// </pre>
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected virtual void R_Indietro_Click(object sender, EventArgs e)
        {
            #region [ADD YOUR CODE HERE]
            #endregion

            Response.Redirect(MySbc.PreviousLink);

            #region [ADD YOUR CODE HERE]
            #endregion
        }

        /// <summary>
        /// Cancella la maschera e l'oggetto sotteso dalla maschera.
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 20/07/2016
        /// </pre>
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected virtual void R_Nuovo_Click(object sender, EventArgs e)
        {
            cancella();
            Response.Redirect(MyAspxFileName);
        }
        #endregion

        #region METODI PROTETTI
        /// <summary>
        /// Cancella lo <b>stato interno</b> della maschera, non i controlli.
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 15/07/2016
        /// </pre>
        /// </summary>
        protected virtual void cancella()
        {
            ViewState["MYSQL"] = null;
            ViewState["MYFILTER"] = null;
            ViewState["MYORDER"] = null;

            Session[MyName] = null;
            if(MyConn!=null)
                TBL_UNDER_FORM = simplex_ORM.SQLSrv.SQLTable.createSQLTable(MyConn, this.TableName);
        }

        /// <summary>
        /// <p>
        /// Imposta gli attributi necessari al funzionamento.
        /// Si attende in Session["APPLICATION"] l'oggetto GeneralApplication corrente.
        /// Se non è presente, lancia un eccezione del tipo generalApplication TracedMessage.
        /// </p>
        /// <p>
        /// Memorizza nella variabile protetta ereditata TBL_UNDER_FORM l'oggetto da mostrare 
        /// nella maschera dei dettagli, leggendola dalla sessione Session[MyName].<br></br>
        /// </p>
        /// <p>
        /// <b> - obsoleto - </b>
        /// Imposta in sessione l'ID dell'oggetto selezionato. La variabile di sessione ha come 
        /// stringa identificatrice il nome della tabella di riferimento dell'oggetto tutto in maiuscolo.
        /// <b> - obsoleto - </b>
        /// </p>
        /// <p>Esempio: </p>
        /// <p>
        /// se la tabella è 'utente' il nome della variabile di sessione è 'UTENTE'.
        /// </p>
        /// 
        /// <p>Valorizza i seguenti attributi derivati da GenericSpxForm:
        /// </p>
        /// <ul>
        /// <li>MyApp               :   APPLICAZIONE CORRENTE;</li>
        /// <li>MyConn              :   CONNESSIONE DI DEFAULT;</li>
        /// <li>MyMainMenu          :   CONTROLLO MENU SINISTRO;</li>
        /// <li>MySbc               :   CONTROLLO BREADCRUMBS;</li>
        /// <li>MyAspxFileName      :   NOME DELLA PAGINA WEB/WEB FORM.</li>
        /// </ul>
        /// <p>
        /// Valorizza i suoi attributi caratteristici che sono:
        /// </p>
        /// -  MyTargetForm = null: Se impostato a null, redirige su se stessa; <br></br>
        /// -  MySql_base = null; <br></br>
        /// -  MyFilter_base = null; <br></br>
        /// -  MyOrder_base = null; <br></br>
        /// -  MyDBConnectionStringName = null; <br></br>
        /// -  MyBackURL (OBSOLETO):   URL DELLA PAGINA CHIAMANTE.</li>
        /// <p>
        /// <p>
        /// Note:<br></br>
        /// In preloadEnvironment(): La classe crea l'oggetto TBL_UNDER_FORM e lo pone in sessione; <br></br>
        /// 
        /// </p>
        /// <ul>
        /// <li></li>
        /// </ul>
        /// </p>
        /// </summary>
        protected override void preloadEnvironment()
        {
            #region [DEFINIZIONE DEGLI ATTRIBUTI CHIAVE]

            #endregion

            #region [ADD YOUR CODE HERE]
            #endregion

            base.preloadEnvironment();

            #region [GESTIONE DELLA TABELLA SOTTESA]

            //Oggetto: La classe GenericSpxForm NON crea direttamente l'oggetto. Lo deve fare la classe derivata!
            if (Session[MyName] != null)
                TBL_UNDER_FORM = (simplex_ORM.SQLSrv.SQLTable)Session[MyName];
            else
            {
                if (TableName != null)
                {
                    try
                    {
                        if (MyConn.State == ConnectionState.Closed)
                            MyConn.Open();

                        TBL_UNDER_FORM = simplex_ORM.SQLSrv.SQLTable.createSQLTable(MyConn, TableName);  //<-- TO CHANGE
                        Session[MyName] = TBL_UNDER_FORM;

                        if (MyConn.State == ConnectionState.Open)
                            MyConn.Close();
                    }
                    catch (simplex_ORM.Spx_ORMMessage ORMM)
                    {
                        if (ORMM.MessageCode == 1 && MyConn != null)
                            TBL_UNDER_FORM = null;
                        else
                            MyErrors.Add(ORMM);
                    }
                }
            }
            #endregion 

            #region [GESTIONE DELLA QUERY DELLA GRIDVIEW]
            // Query
            if (ViewState["MYSQL"] != null)
                MySql = (String)ViewState["MYSQL"];

            if (ViewState["MYFILTER"] != null)
                MyFilter = (String)ViewState["MYFILTER"];
            else
                MyFilter = MyFilter_base;

            if (ViewState["MYORDER"] != null)
                MyOrder = (String)ViewState["MYORDER"];
            else
                MyOrder = MyOrder_base;

            if(MySql==null)
            {
                MySql = MySql_base;
                if (MyFilter != null)
                    MySql = MySql + " WHERE " + MyFilter;
                if (MyOrder != null)
                    MySql = MySql + " ORDER BY " + MyOrder;

                ViewState.Add("MYSQL", MySql);
            }

            #endregion

            #region [STRINGA DI CONNESSIONE ]
            // STRINGA DI CONNESSIONE 
            // INSERIMENTO DELLA STRINGA DI CONNESSIONE NELLO STATO IN MODO DA NON DOVER RICAVARLA OGNI VOLTA
            if (ViewState["MYCONNSTRING"]==null && this.MyDBConnectionStringName != null)
            {
                System.Configuration.Configuration _webConfig = null;
                _webConfig = System.Web.Configuration.WebConfigurationManager.OpenWebConfiguration("~/Web.config");
                MyConnString = _webConfig.ConnectionStrings.ConnectionStrings[this.MyDBConnectionStringName].ConnectionString;
                //MyConnString = System.Configuration.Configuration.ConnectionStrings[MyConnStringName].connectionString;
                if (MyConnString == null)
                {
                    //ERRORE
                    simplex_ORM.Spx_ORMMessage ORMM = new simplex_ORM.Spx_ORMMessage("La stringa di connessione non risulta essere stata impostata nel file di configurazione web.config");
                    if (MyConn != null && MyApp != null && MyApp.CurrentUser != null && Session.SessionID != null)
                        ORMM.traceLog(MyConn, MyApp.CurrentUser.UID.ToString(), Session.SessionID);
                    MyErrors.Add(ORMM);
                    return;
                }
                // Aggiungo allo stato
                ViewState.Add("MYCONNSTRING", MyConnString); //MYCONNSTRING
            }
            // Ancora: Ora ho la stringa di connessione
            if (MyConnString == null)
                MyConnString = (String)ViewState["MYCONNSTRING"];
            #endregion

            #region [ERRORI DA SESSIONE E/O DA VIEWSTATE]
            if (MyErrors != null)
            {
                if (MyErrors.Count == 0)
                {
                    if(Session[MyName + "_MYERRORS"]!=null)
                        MyErrors = (System.Collections.Generic.List<simplex_ORM.Spx_ORMMessage>)Session[MyName + "_MYERRORS"];                    
                    //Session[MyName + "_MYERRORS"] = null;
                }
                else
                {
                    foreach (simplex_ORM.Spx_ORMMessage _x in (System.Collections.Generic.List<simplex_ORM.Spx_ORMMessage>)Session[MyName + "_MYERRORS"])
                    {
                        // Fondo i messaggi che vengono di due canali: Session e ViewState
                        MyErrors.Add(_x);
                    }
                }
                Session[MyName + "_MYERRORS"] = null;
                //showErrors();
            }
                
            #endregion            
        }

        #endregion
    }
}
