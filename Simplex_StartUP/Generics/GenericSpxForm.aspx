<%@ Page Language="C#" MasterPageFile="~/MasterPCM.Master" AutoEventWireup="true" CodeBehind="GenericSpxForm.aspx.cs" Inherits="SIMPLEX_STARTUP.GenericSpxForm" Title="Pagina senza titolo" %>
<%@ Register Assembly="WebSimplex" Namespace="WebSimplex" TagPrefix="WS1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MenuPlaceHolder" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<div id="MainFormSection">

<WS1:WebSimplexMessageBox ID="R_MessageBox" runat="server" 
        AlertText="Attenzione, vedi il riquadro degli errori." 
        OkAlertButtonCssStyle="AlertButton" 
        AlertImage="Immagini/Omino_Errore.png" 
        OkMessageImage="Immagini/Megafono_tondoviola.png" 
        OkMessageText="Continuare con l'operazione?"
        OkMessageButtonCssStyle="OkButton" 
        UndoMessageButtonCssStyle="UndoButton" CssOkMsgClass="OkMsg" />

<table class="FRM">
    <tr class="FRM">
    <td class="FRM_COL1"></td>
    <td class="FRM_COL2"></td>
    <td class="FRM_COL3">
        <asp:Button ID="R_Back" runat="server" onclick="R_Back_Click" Text="Indietro" />
        </td>
    </tr>
    <tr class="FRM">
        <td class="FRM_COL1"></td>
        <td class="FRM_COL2">
        <!-- MessageBox -->

        <!-- END MessageBox -->
        </td>
    <td class="FRM_COL3"></td>
    </tr>

<!-- PUT YOUR CONTROLS FROM HERE -->
<!-- PUT YOUR CONTROLS TO HERE -->
    <tr class="FRM">
        <td class="FRM_COL1">&nbsp;</td>
        <td class="FRM_COL2">
            &nbsp;</td>
    <td class="FRM_COL3">&nbsp;</td>
    </tr>

    <tr class="FRM">
        <td class="FRM_COL1">&nbsp;</td>
        <td class="FRM_COL2">
            &nbsp;</td>
    <td class="FRM_COL3">&nbsp;</td>
    </tr>

    <tr class="FRM">
        <td class="FRM_COL1">&nbsp;</td>
        <td class="FRM_COL2">
            <asp:Button ID="R_Salva" runat="server" Text="Salva" onclick="R_Salva_Click" />
        </td>
    <td class="FRM_COL3">
        <asp:Button ID="R_Elimina" runat="server" Text="Elimina" 
            onclick="R_Elimina_Click" />
        </td>
    </tr>

</table>

</div>
</asp:Content>
