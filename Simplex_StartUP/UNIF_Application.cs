using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace SIMPLEX_STARTUP
{
    /// <summary>
    /// Rappresenta una funzione applicativa.
    /// <pre>
    /// ----
    /// @MdN
    /// Mdfd: 12/07/2015 - introdotto l'attributo protetto sulla session ID e la property di accesso RW.
    /// Mdfd: 16/10/2015 - reso virtual il metodo load().
    /// </pre>
    /// </summary>
    public class UNIF_Application : GeneralApplication.GeneralApplication
    {
        #region ATTRIBUTI PROTETTI
        /// <summary>
        /// <p>
        /// Tabella che contiene i dati della funzione applicativa.
        /// Convenzonalmente solo il DESKTOP può avere valore MyTable=null.
        /// </p>
        /// <pre>
        /// @MdN
        /// ----
        /// Crtd: 22/03/2015
        /// </pre>
        /// </summary>
        protected simplex_ORM.SQLSrv.SQLTable MyTable = null;

        /// <summary>
        /// <a>
        /// ID dell'applicazione
        /// </a>
        /// <pre>
        /// @MdN
        /// ----
        /// Crtd: 22/03/2015
        /// </pre>
        /// </summary>
        protected Int32 MyID=0;

        /// <summary>
        /// <p>
        /// Lista dei gruppi che possono accedere in qualche modo all'applicazione.
        /// </p>
        /// <pre>
        /// @MdN
        /// ----
        /// Crtd: 22/03/2015
        /// </pre>
        /// </summary>
        protected Int32[] MyGroups = null;

        /// <summary>
        /// <p>
        /// Indica se l'applicazione agisce in modalità produzione (MyDEV="false") oppure
        /// in modalità sviluppo (MyDEV="true").
        /// </p>
        /// </summary>
        protected Boolean MyDEV = false;

        /// <summary>
        /// <p>
        /// Nelle applicazioni web indica l'ID della sessione corrente.
        /// </p>
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd:   12/07/2015
        /// </pre>
        /// </summary>
        protected String MySessionID = null;

        
        #endregion

        #region COSTRUTTORI
        public UNIF_Application()
            : base()
        {
            //NOP
            String _dev = null;
            _dev = ConfigurationSettings.AppSettings["DEV"];
            if (_dev == null)
                MyDEV = false;
            else
            {
                if (_dev.ToLower().CompareTo("true") == 0)
                    MyDEV = true;
                else
                    MyDEV = false;
            }
        }

        public UNIF_Application(System.Data.Odbc.OdbcConnection p_conn)
            : base()
        {
            String _dev = null;
            _dev = ConfigurationSettings.AppSettings["DEV"];
            if (_dev == null)
                MyDEV = false;
            else
            {
                if (_dev.ToLower().CompareTo("true") == 0)
                    MyDEV = true;
                else
                    MyDEV = false;
            }

            base.DefaultConnection = p_conn;
        }
        #endregion

        #region METODI PROTETTI

        #endregion

        #region METODI PUBBLICI

        public virtual Boolean load(Int32 p_ID)
        {
            MyID = p_ID;
            return load();
        }

        /// <summary>
        /// <P>
        /// Carica dal database le informazioni sull'applicazione e la lista dei gruppi abilitati
        /// all'accesso.
        /// </P>
        /// <pre>
        /// @MdN
        /// ----
        /// Crtd: 22/03/2015
        /// Mdfd: 06/07/2015 Merano - popolamento dell'attributo Name
        /// Mdfd: 16/10/2015 Reso Virtual
        /// </pre>
        /// </summary>
        /// <returns>TRUE: se il caricamento avviene positivamente, FALSE altrimenti.</returns>
        public virtual Boolean load()
        {
            System.Text.StringBuilder _sql;
            System.Data.Odbc.OdbcCommand _cmd;
            System.Data.Odbc.OdbcDataReader _dr;
            System.Collections.ArrayList _lst;

            // controllo della connessione
            if (DefaultConnection == null)
                return false;

            if (MyTable == null)
                MyTable = simplex_ORM.SQLSrv.SQLTable.createSQLTable(DefaultConnection, "ADMN_FunzioneApplicativa");

            MyTable.setValue("ID", MyID.ToString());

            try
            {
                if (DefaultConnection.State == ConnectionState.Closed)
                    DefaultConnection.Open();
                MyTable.load();
            }
            catch (Exception E)
            {
                return false;
            }

            /* @MdN 06/07/2015 Merano: popolamento di alcuni valori */
            this.Name = MyTable.getValue("DENOMINAZIONE");
            


            _sql = new System.Text.StringBuilder("SELECT GRUPPO FROM ADMN_FunzioneApplicativaGruppo WHERE DATA_DEL IS NULL AND FUNZIONEAPPLICATIVA=").Append(MyID.ToString());
            _cmd = DefaultConnection.CreateCommand();
            _cmd.CommandText = _sql.ToString();
            try
            {
                _dr = _cmd.ExecuteReader();
                if (_dr.HasRows == true)
                {
                    _lst = new System.Collections.ArrayList();
                    while (_dr.Read())
                    {
                        _lst.Add((Int32)_dr.GetInt32(0));
                    }
                    MyGroups = (Int32[])_lst.ToArray(Type.GetType("System.Int32"));
                }
            }
            catch (Exception E)
            {
                return false;
            }
            finally
            {
                _cmd.Dispose();
                DefaultConnection.Close();
            }
            return true;
        }

        /// <summary>
        /// <p>
        /// Verifica se l'utente può accedere all'applicazione o meno.
        /// </p>
        /// <br>
        /// Si suggerisce di eseguire il controllo in ogni pagina di una data applicazione e non solo 
        /// nel menu iniziale.
        /// </br>
        /// <br>
        /// Il metodo esegue un doppio ciclo ci riconoscimento tra i gruppi supportati dall'applicazione
        /// ed i gruppi di appartenenza dell'utente. Se viene trovata almeno una corrispondenza restituisce il valore
        /// di verità.
        /// </br>
        /// <pre>
        /// ----
        /// @MdN
        /// @Crtd: 15/06/2015
        /// </pre>
        /// </summary>
        /// <param name="p_usr">Utente corrente</param>
        /// <returns>True se l'utente può accedere, false altrimenti.</returns>
        public Boolean verifyAccess(UNIF_User p_usr)
        {

            // controlli
            if (p_usr == null)
                return false;

            if (p_usr.getGroups() == null)
                return false;

            /*
             Se la funzione applicativa non possiede gruppi allora 
             tutti possono accedere.
             */
            if (this.MyGroups == null)
                return true;

            /*
            Se la funzione applicativa non possiede gruppi allora 
            tutti possono accedere.
            */
            if (this.MyGroups.Count() == 0)
                return false;

            int[] _usergroups;
            _usergroups = p_usr.getGroups();

            // DOPPIO CICLO
            foreach (int _grpex in MyGroups)
            {
                foreach (int _grpint in _usergroups)
                {
                    if (_grpint == _grpex)
                    {
                        // TROVATO!!!
                        return true;
                    }
                }
            }
            return false;

        }

        /// <summary>
        /// <p>
        /// Restituisce l'ID della funzione applicativa a partire dalla denominazione.
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 01/07/2015
        /// </pre>
        /// </p>
        /// </summary>
        /// <param name="p_description">DEnominazione della funzien applicativa.</param>
        /// <returns>
        /// Il valore dell'identificatore ID ovvero:
        /// <ul>
        /// <li> -1: parametro non specificato;</li>
        /// <li> -2: Connessione ODBC non istanziata;</li>
        /// <li> -1: Errore SQL.;</li>
        /// </ul>
        /// </returns>
        public virtual Int32 getIDByName(String p_description)
        {
            String _sql = null;
            System.Data.Odbc.OdbcCommand _cmd;
            Int32 _toRet = -1;

            if (p_description == null)
                return -1;

            if (this.DefaultConnection == null)
            {
                return -2;
            }

            _sql = "SELECT id FROM ADMN_FunzioneApplicativa WHERE DENOMINAZIONE='" + p_description + "'";
            if (DefaultConnection.State == ConnectionState.Closed)
                DefaultConnection.Open(); 
            _cmd = DefaultConnection.CreateCommand();
            _cmd.CommandText = _sql;
            try
            {
                _toRet = (Int32)_cmd.ExecuteScalar();
            }
            catch (Exception EE)
            {
                DefaultConnection.Close();
                return -3;
            }

            DefaultConnection.Close();
            _cmd.Dispose();
            return _toRet;
        }


        /// <summary>
        /// <p>
        /// Recupera dalla tabella UTILS_MENU il codice ID di una voce di menu, data la sua descrizione testuale esatta!!!!
        /// </p>
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 26/06/2015
        /// </pre>
        /// </summary>
        /// <param name="p_MenuVoiceDescr">Nome esatto della voce di menu.</param>
        /// <returns>
        /// <br>Il codice ID della voce di menu specificata ovvero un numero negativo in caso di errore.</br>
        /// <br>In particolare:</br>
        /// <ul>
        /// <li>-1: parametro nullo;</li>
        /// <li>-2: parametro stringa VUOTA;</li>
        /// <li>-3: connessione nulla;</li>
        /// <li>-4: Errore nella query;</li>
        /// </ul>
        /// </returns>
        public Int32 lookForMenuVoiceID(String p_MenuVoiceDescr)
        {
            Int32 _toRet = 0;
            
            if (p_MenuVoiceDescr == null)
                return -1;

            if (p_MenuVoiceDescr.Length ==0 )
                return -2;

            if (DefaultConnection == null)
                return -3;
            

            String _sql = "SELECT ID FROM dbo.UTILS_MENU WHERE DESCRIZIONE = '" + p_MenuVoiceDescr + "'";

            if (DefaultConnection.State == ConnectionState.Closed)
                DefaultConnection.Open();

            System.Data.Odbc.OdbcCommand _cmd = DefaultConnection.CreateCommand();
            _cmd.CommandText = _sql;

            try
            {
                _toRet = (Int32)_cmd.ExecuteScalar();
            }
            catch(Exception EE)
            {
                simplex_ORM.Spx_ORMMessage EX = new simplex_ORM.Spx_ORMMessage(EE);
                EX.traceLog(DefaultConnection, ((UNIF_User)CurrentUser).UID.ToString(), "session id sconosciuta");
                DefaultConnection.Close();
                return -4;
            }
            DefaultConnection.Close();
            _cmd.Dispose();
            return _toRet;
        }
        #endregion

        #region PROPERTIES
        /// <summary>
        /// <p>
        /// Restituisce (RO) il livello (cioè l'ID) della voce 'padre' del menu principale della funzione applicativa.
        /// In caso di errori restituisce il valore -1.
        /// </p>
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 24/06/2015
        /// </pre>
        /// </summary>
        public Int32 LivelloPadre
        {
            get
            {
                Int32 _toRet = -1;
                String _sql = "SELECT padre FROM dbo.UTILS_MENU WHERE PADRE = ID AND FUNZIONE_APPLICATIVA=" + this.MyID.ToString();
                System.Data.Odbc.OdbcCommand _cmd;                
                try
                {
                    if (this.DefaultConnection.State == ConnectionState.Closed)
                        DefaultConnection.Open();
                    _cmd = DefaultConnection.CreateCommand();
                    _cmd.CommandText = _sql;
                    _toRet = (Int32)_cmd.ExecuteScalar();
                }
                catch (Exception EE)
                {
                    _toRet = -1;
                }
                DefaultConnection.Close();
                return _toRet;
            }
        }

        /// <summary>
        /// <p>
        /// REstituisce l'ID della funzione applicativa.
        /// </p>
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 24/06/2015
        /// </pre>
        /// </summary>
        public Int32 ID
        {
            get
            {
                return MyID;
            }
        }

        /// <summary>
        /// Verifica (RO) se l'applicazione è il modalità DEV/PROD.
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 25/06/2015
        /// </pre>
        /// </summary>
        public Boolean IsDEV
        {
            get
            {
                return MyDEV;
            }
        }

        /// <summary>
        /// <p>
        /// Imposta e restituisce (RW) l'ID della sessine WEB attuale.
        /// </p>
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 12/07/2015
        /// </pre>
        /// </summary>
        public String SessionID
        {
            get
            {
                return MySessionID;
            }
            set
            {
                MySessionID = value;
            }
        }

        #endregion
    }
}
