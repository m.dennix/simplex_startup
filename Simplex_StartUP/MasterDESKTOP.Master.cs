using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using GeneralApplication;
using System.Data.Odbc;

namespace SIMPLEX_STARTUP
{

    public partial class MasterDESKTOP : System.Web.UI.MasterPage
    {
        //protected GeneralApplication.GeneralApplication MyApp=null;                    
        protected void Page_Load(object sender, EventArgs e)
        {
            OdbcConnection l_conn = null;
            GeneralApplication.GeneralApplication l_Appl = null;
            l_Appl = (GeneralApplication.GeneralApplication)Session["APPLICATION"];
            if(l_Appl!=null)
                l_conn = l_Appl.DefaultConnection;

            //loadVMenu(l_conn);
        }


        /// <summary>
        /// <p>EN: Loads dynamically the items of the two level menu on the left side of the master page.</p>
        /// <p>IT: Carica dinamicamente le voci del menu a due livelli posto a sinistra della master page.</p>
        /// </summary>
        /// <param name="p_conn">Active connection / Connessione attiva</param>
        protected void loadVMenu(OdbcConnection p_conn)
        {
            String l_sql = "SELECT I1.ID, I1.descrizione, I1.LinK, I1.PADRE, ISNULL(I1.PADRE,I1.ID) AS ORDINE, I2.DESCRIZIONE FROM UTILS_MENU I1 LEFT JOIN UTILS_MENU I2 ON I1.PADRE=I2.ID WHERE I1.DATA_DEL IS NULL AND I2.DATA_DEL IS NULL order by ORDINE, PADRE";
            loadVMenu(p_conn, l_sql);
        }
        
        /// <summary>
        /// <p>EN: Loads dynamically the items of the two level menu on the left side of the master page.</p>
        /// <p>IT: Carica dinamicamente le voci del menu a due livelli posto a sinistra della master page.</p>
        /// </summary>
        /// <param name="p_conn">Active connection to the database that contains the table of the menu items / Connessione attiva al database contenete a struttura delle voci.</param>
        /// <param name="p_sql">Query SQL that extract the two level menu's items / Query SQL di estrazione delle voci del menu a due livelli.</param>
        /// <remarks>
        /// <p>
        /// EN: The table conatining the items has the following structure:
        /// </p>
        /// <p>
        /// IT: La tabella contenente le voci ha la seguente struttura:
        /// </p>
        /// <pre>
        /// CREATE TABLE dbo.UTILS_MENU
        /// (
        /// ID INT NOT NULL
        /// , DESCRIZIONE			VARCHAR(24) NOT NULL
        /// , PADRE					INT NULL
        /// , LINK					NVARCHAR(1024) NOT NULL
        /// , USR					INT NOT NULL
        /// , NOTE                  NVARCHAR(1024)
        /// , IMG_URL               NVARCHAR(1024)
        /// , DATA_SYS				DATETIME NOT NULL DEFAULT GETDATE()
        /// , DATA_DEL				DATETIME NULL
        /// )
        /// </pre>
        /// </remarks>
        protected void loadVMenu(OdbcConnection p_conn, String p_sql)
        {
            OdbcDataReader l_DR = null;
            OdbcCommand l_Cmd = null;
            Menu MyMainMenu = null;

            MyMainMenu = (Menu)this.FindControl("R_MainMenu");

            // this.R_MainMenu = new Menu();
            // R_MainMenu.Items.Clear();                               // cancella le voci precedenti
            MyMainMenu.Items.Clear();
            // Imposta la query per caricare le voci di menu
            if (p_conn == null)
                return;
            if (p_sql == null)
                return;
            if (p_conn.State == System.Data.ConnectionState.Closed)
                p_conn.Open();

            l_Cmd = p_conn.CreateCommand();
            l_Cmd.CommandText = p_sql;
            l_DR = l_Cmd.ExecuteReader();
            if (l_DR != null)
            {
                MenuItem l_MI = null;
                MenuItem l_PI = null;       //ITEM PADRE
                while (l_DR.HasRows && l_DR.Read())
                {
                    /* *** ATTENZIONE *** 
                     * PER COME E' STATA IMPOSTATA LA QUERY, TUTTE LE SOTTOVOCI DI UNA STESSA VOCE PADRE
                     * SONO CONTIGUE E LA STESSA VOCE PADRE E' LA CAPOLISTA!!!
                     * 
                     * SELECT I1.ID, I1.descrizione, I1.LinK, I1.PADRE, ISNULL(I1.PADRE,I1.ID) AS ORDINE, I2.DESCRIZIONE
                     * FROM UTILS_MENU I1 LEFT JOIN UTILS_MENU I2 ON I1.PADRE=I2.ID
                     * WHERE I1.DATA_DEL IS NULL
                     * AND I2.DATA_DEL IS NULL
                     * order by ORDINE, PADRE
                     * 
                     * ***/

                    //l_MI = new MenuItem(l_DR[1].ToString(), l_DR[0].ToString(), null, l_DR[2].ToString());
                    l_MI = new MenuItem(l_DR[1].ToString(), l_DR[2].ToString(), null, null);
                    if (l_DR.IsDBNull(3) == true)
                    {
                        l_PI = l_MI;                            // trovato un candidato PADRE
                        //R_MainMenu.Items.Add(l_PI);
                        MyMainMenu.Items.Add(l_PI);
                    }
                    else
                    {
                        if (l_PI != null) // && l_DR[3].ToString().CompareTo(l_PI.Value)==0)
                        {
                            // CONTROLLO ED AFFILIO AL PADRE
                            l_PI.ChildItems.Add(l_MI);
                        }
                            
                    }                    
                } //fine while
                l_DR.Close();
                l_Cmd.Dispose();
                p_conn.Close();
            }
        }

        /// <summary>
        /// Ricava il livello della voce di menu dalla radice del menu.
        /// @MdN
        /// ----
        /// Crtd: 18/01/2015
        /// </summary>
        /// <param name="p_Conn">Connessione attiva.</param>
        /// <param name="p_MenuVoice">Stringa che rappresenta la voce di menu.</param>
        /// <returns>Il livello della voce di menu (0 based) o -1 in caso di anomalia.</returns>
        public static int getMenuVoiceLevel(OdbcConnection p_conn, string p_MenuVoice)
        {
            OdbcDataReader l_DR = null;
            OdbcCommand l_Cmd = null;
            Int32 l_ID=0;
            Int32 l_IDPadre=0;
            int _level=0;
            Boolean _isRoot=false;


            // Controlli
            if (p_conn == null)
                return -1;
            if (p_MenuVoice == null)
                return -1;

            if (p_conn.State == System.Data.ConnectionState.Closed)
                p_conn.Open();

            l_Cmd = p_conn.CreateCommand();

            // PRELEVO L'ID DELLA VOCE DI MENU
            l_Cmd.CommandText = "SELECT ID FROM dbo.UTILS_MENU WHERE DESCRIZIONE = '" + p_MenuVoice + "'";
            l_DR = l_Cmd.ExecuteReader();
            if (l_DR != null && l_DR.Read()==true)
            {
                l_ID=l_DR.GetInt32(0);
                l_DR.Close();
            } else
            {
                l_DR.Close();
                p_conn.Close();
                return -1;
            }
            
            // CERCO LA RADICE O UNA RADICE
            l_IDPadre = l_ID;
            while (_isRoot == false)
            {
                l_Cmd.CommandText = "SELECT PADRE FROM dbo.UTILS_MENU WHERE ID= " + l_IDPadre;
                l_DR = l_Cmd.ExecuteReader();
                if (l_DR != null & l_DR.Read()==true)
                {
                    if (l_DR.IsDBNull(0) == false)
                    {
                        l_IDPadre = l_DR.GetInt32(0);
                        _level += 1;
                        l_DR.Close();
                    }
                    else
                    {
                        l_DR.Close();
                        _isRoot = true;
                    }
                        
                }
                else
                {
                    l_DR.Close();
                    _isRoot = true;
                }
            } // FINE CERCO LA RADICE O UNA RADICE
            // TTROVATA LA RADICE.
            p_conn.Close();
            return _level;
        }

        protected void R_MainMenu_MenuItemClick(object sender, MenuEventArgs e)
        {
            //  CANCELLAZIONE DEGLI STATI (TUTTI)
            Session["ATTIVITA"] = null;
            Session["APPLICATIVO"] = null;
            Session["UTENTE"] = null;


            // GESTIONE DEI REDIRECT
            Response.Redirect(e.Item.Value);

        }// FINE
    }
}