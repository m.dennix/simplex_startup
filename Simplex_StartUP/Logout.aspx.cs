using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using GeneralApplication;
using WebSimplex;

namespace SIMPLEX_STARTUP
{
    /// <summary>
    /// Derivata da SimplexForm.
    /// Ha attributi protetti necessari per il funzionamento dell'applicazione.
    /// Esegue Override dei seguenti metodi:
    /// ------
    /// @MdN 
    /// Crtd: 06/12/2014
    /// </summary>
    public partial class Logout : simplex_FORM.SimplexForm
    {
        /// <summary>
        /// Oggetto applicazione corrente.
        /// </summary>
        protected GeneralApplication.GeneralApplication MyApp = null;
        /// <summary>
        /// Oggetto connessione corrente al database SIMPLEX_STARTUP
        /// DEVE essere sempre chiusa.
        /// </summary>
        protected System.Data.Odbc.OdbcConnection MyConn = null;
        /// <summary>
        /// Nome della pagina come visualizzata nel menu.
        /// </summary>
        protected String MyName = null;
        /// <summary>
        /// Riferimento all' oggetto di classe WebSimplexBreadCrumbs.
        /// </summary>
        protected WebSimplexBreadCrumbs MySbc = null;
        /// <summary>
        /// Nome del file della pagina corrente. Viene valorizzato automaticamente 
        /// dal metodo preloadEnvironment().
        /// </summary>
        protected String MyAspxFileName = null;
        /// <summary>
        /// URL della pagina chiamante.
        /// </summary>
        protected String MyBackURL = null;

        /// <summary>
        /// Chiave della funzione applicativa, come risulta nel file di configurazione
        /// web.config.
        /// <remarks>Se null indica il desktop applicativo.</remarks>
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 25/06/2015
        /// </pre>
        /// </summary>
        protected String MyFunzioneApplicativa = null;

        /// <summary>
        /// Indica se il riquadro dei messaggi deve essere visibile o meno.
        /// Per default NON è visibile.
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 08/03/2015
        /// </pre>
        /// </summary>
        protected Boolean MyMessageBoxVisible = false;

        protected virtual Boolean validateForm()
        {
            // NA MAZZA
            return true;
        }
        


        /// <summary>
        /// 
        /// </summary>
        /// <param name="p_str"></param>
        /// <param name="p_format"></param>
        /// <returns></returns>
        protected bool isAValidDateString(String p_str, String p_format)
        {
            //casi banali
            if (p_str == null)
                return false;

            if (p_format == null)
                return false;

            Boolean _toRet = true;
            int _len = p_format.Length;
            String _teststr = null;
            if (p_str.Length > _len)
                _teststr = p_str.Substring(0, _len);
            else
                _teststr = p_str;

            try
            {
                /* verifica la sintassi */
                _toRet = simplex_ORM.Column.isDateTime(p_str, p_format);
                if (_toRet == true)
                {
                    /* verifica la semantica */
                    simplex_ORM.Column.parseDateTimeString(p_str, p_format);
                }
            }
            catch (simplex_ORM.Spx_ORMMessage ormm)
            {
                _toRet = false;
            }
            return _toRet;
        }



        /// <summary>
        /// <p>
        /// Imposta gli attributi necessari al funzionamento.
        /// Si attende in Session["APPLICATION"] l'oggetto GeneralApplication corrente.
        /// Se non è presente, lancia un eccezione del tipo generalApplication TracedMessage.
        /// Valorizza:
        /// </p>
        /// <p>
        /// Valorizza i seguenti attributi:
        /// </p>
        /// <ul>
        /// <li>MyApp               :   APPLICAZIONE CORRENTE;</li>
        /// <li>MyConn              :   CONNESSIONE DI DEFAULT;</li>
        /// <li>MyMainMenu          :   CONTROLLO MENU SINISTRO;</li>
        /// <li>MySbc               :   CONTROLLO BREADCRUMBS;</li>
        /// <li>MyAspxFileName      :   NOME DELLA PAGINA WEB/WEB FORM.</li>
        /// <li>MyBackURL           :   URL DELLA PAGINAWEB/WEB FORM CHIAMANTE.</li>
        /// </ul>
        /// <pre>
        /// ------
        /// @MdN
        /// Crtd: 06/12/2014
        /// Mdfd: 30/01/2015
        /// </pre>
        /// </summary>
        protected void preloadEnvironment()
        {
            MyApp = (UNIF_Application)Session["APPLICATION"];
            if (MyApp != null)
                MyConn = (System.Data.Odbc.OdbcConnection)MyApp.DefaultConnection;
            else
            {
                //@MdN 26/05/2015
                Response.Redirect("~/GenericSpxErrorForm.aspx");
                //throw new TracedMessage("Istanza di applicazione inesistente", "SIMPLEX_STARTUP", "preloadEnvironment()");
            }           
        }//fine preloadEnvironment

        protected void Page_Load(object sender, EventArgs e)
        {
            preloadEnvironment();
            // <%WRITE YOUR CODE FROM HERE%>

            // <%WRITE YOUR CODE TO HERE%>
            //Adattare questa funzione alle singole fattispecie
            if (MyConn.State == ConnectionState.Open)
                MyConn.Close();
            writeMessageToLog(MyConn, "Logout dell'utente " + ((UNIF_User)MyApp.CurrentUser).LogName, "Logout.aspx", ((UNIF_User)(MyApp.CurrentUser)).UID.ToString(), Session.SessionID);
            Session["APPLICATION"] = null;
            Session.Clear();
            Session.Abandon();
            Response.Redirect("~/Login.aspx");
        }

        /// <summary>
        /// Nome della pagina visualizzato nei menu e nelle briciole di pane.
        /// @MdN
        /// ----
        /// Crtd: 18/01/2015
        /// </summary>
        public virtual String Nome
        {
            get
            {
                return MyName;
            }
            set
            {
                if (value == null)
                    MyName = "### DA DEFINIRE";
                else
                    MyName = value;
            }
        }
    }
}
