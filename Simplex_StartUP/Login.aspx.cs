using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using GeneralApplication;
using WebSimplex;

namespace SIMPLEX_STARTUP
{
    /// <summary>
    /// <p>
    /// Esegue il login e riconosce l'utente dalle sue credenziali.
    /// Se abilita l'accesso, istanzia l'utente e lo passa tramite 
    /// la variabile di sessione "CURRENT-USER" alla pagina Default.aspx.
    /// </p>
    /// <p>
    /// Creata da una compia di GenericSpxForm.
    /// Derivata da SimplexForm.
    /// 
    /// Ha attributi protetti necessari per il funzionamento dell'applicazione.
    /// Esegue Override dei seguenti metodi:
    /// </p>
    /// <pre>
    /// ------
    /// @MdN 
    /// Crtd: 20/06/2015
    /// </pre>
    /// </summary>
    public partial class Login : simplex_FORM.SimplexForm
    {
        #region ATTRIBUTI PROTETTI
        /// <summary>
        /// Oggetto applicazione corrente.
        /// </summary>
        protected GeneralApplication.GeneralApplication MyApp = null;
        /// <summary>
        /// Oggetto connessione corrente al database SIMPLEX_STARTUP
        /// DEVE essere sempre chiusa.
        /// </summary>
        protected System.Data.Odbc.OdbcConnection MyConn = null;
        /// <summary>
        /// Nome della pagina come visualizzata nel menu.
        /// </summary>
        protected String MyName = null;
        /// <summary>
        /// Riferimento all' oggetto di classe WebSimplexBreadCrumbs.
        /// </summary>
        protected WebSimplexBreadCrumbs MySbc = null;
        /// <summary>
        /// Nome del file della pagina corrente. Viene valorizzato automaticamente 
        /// dal metodo preloadEnvironment().
        /// </summary>
        protected String MyAspxFileName = null;
        /// <summary>
        /// URL della pagina chiamante.
        /// </summary>
        protected String MyBackURL = null;

        /// <summary>
        /// Indica se il riquadro dei messaggi deve essere visibile o meno.
        /// Per default NON è visibile.
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 08/03/2015
        /// </pre>
        /// </summary>
        protected Boolean MyMessageBoxVisible = false;

        /// <summary>
        /// Menu verticale associato alla maschera
        /// </summary>
        protected System.Web.UI.WebControls.Menu MyMainMenu = null;
        /// <summary>
        /// Username.
        /// </summary>
        protected String MyUsername = null;
        /// <summary>
        /// Login.
        /// <remarks>
        /// La differenza tra Username e Login è che Login è la stringa immessa dall'utente,
        /// la Username è una stringa derivata dalla Login secondo le regole aziendali.
        /// </remarks>
        /// </summary>
        protected String MyLogin = null;
        /// <summary>
        /// Password
        /// </summary>
        protected String MyPwd = null;

        #endregion

        #region PROPRIETA'

        /// <summary>
        /// <p>
        /// Deriva la User Name a partire dalla login immessa dall'utente dalla maschera di autenticazione.
        /// </p>
        /// <remarks>
        /// In caso di errori restituisce <b>null</b>.
        /// </remarks>
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 22/06/2015
        /// </pre>
        /// </summary>
        protected virtual String UserName
        {
            get
            {
                if (MyLogin == null)
                    return MyLogin;
                /* 
                 * Le regole aziendali PCM impongono che la login
                 * sia un indirizzo di posta elettronica del tipo Inizialenome.Cognome@governo.it ovvero Inizialenome.Cognome@palazzochigi.it
                 * mentre la username sia semplicemente inizialenomeCognome.
                 * La procedura deve, allora:
                 * 1) Verificare che la login abbia un dominio. Se si procedere al punto 2, altimenti passare al punto 3.
                 * 2) separare il dominio dal nome della casella e controllare che sia nell'insieme (governo.it o palazzochigi.it). 
                 *      Se si, passare al punto 3, altrimenti fallire l'autenticazione.
                 * 3) separare Inizialenome da Cognome e ricongiungerli senza l'interposizione del punto.
                 *      Se dall'origine Inizialenome e Cognome non sono separati dal punto, passare al punto successivo.
                 * 4) Restituire la stringa.
                 */

                /*  TEST DEL 23/06/2015
                    m.dennix@somethign.it			null		ok
                    m.denittis@somethign.it			null		ok
                    m.denittis@palazzochigi.it		mdenittis	ok
                    mdenittis@palazzochigi.it		mdenittis	ok
                    m.denittis				        mdenittis	ok
                    mdenittis				        mdenittis	ok
                 * FINE TEST DEL 23/06/2015
                 */

                char[] _ATseparator = { '@' };
                char[] _DOTseparator = { '.' };
                char[] _DOMseparator = { ';' };
                String[] _FirstStep = null;
                String[] _LastStep = null;
                String[] _Domains = null;
                String _toRet = null;

                String _DOMList = null;
                // Prendo le stringhe dei domini di EMail dal file di configurazione
                _DOMList = ConfigurationSettings.AppSettings["EmailDomains"];
                if (_DOMList == null)
                    return null;
                _Domains = _DOMList.Split(_DOMseparator);
                if (_Domains == null)
                    return null;

                // Separo l'eventiale dominio
                if (MyLogin.Contains("@") == true)
                {
                    _FirstStep = MyLogin.Split(_ATseparator);
                    if (_FirstStep[1] == null)
                        return null;
                    foreach (String _str in _Domains)
                    {
                        if (_str.ToLower().CompareTo(_FirstStep[1].ToLower()) == 0)
                            _toRet = _FirstStep[0];                                 // se il dominio è presente, prendo la Login, altrimenti continua ...
                    }
                    // se _toRet è ancora NULLO allora non è stato trovata alcuna corrispondenza!!!
                    if (_toRet == null)
                        return null;
                }
                else
                {
                    _toRet = MyLogin;
                }

                //elimino gli eventuali  punti
                _LastStep =  _toRet.Split(_DOTseparator);
                _toRet = null;
                if (_LastStep != null)
                {
                    foreach (String _str in _LastStep)
                    {
                        _toRet = _toRet + _str;
                    }
                        
                }
                // fine
                return _toRet;
            }
        }
        #endregion

        #region METODI PROTETTI
        /// <summary>
        /// <p>
        /// Esegue la validazione dei campi
        /// </p>
        /// </summary>
        /// <returns></returns>
        protected virtual Boolean validateForm()
        {
            // NA MAZZA
            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="p_str"></param>
        /// <param name="p_format"></param>
        /// <returns></returns>
        protected bool isAValidDateString(String p_str, String p_format)
        {
            //casi banali
            if (p_str == null)
                return false;

            if (p_format == null)
                return false;

            Boolean _toRet = true;
            int _len = p_format.Length;
            String _teststr = null;
            if (p_str.Length > _len)
                _teststr = p_str.Substring(0, _len);
            else
                _teststr = p_str;

            try
            {
                /* verifica la sintassi */
                _toRet = simplex_ORM.Column.isDateTime(p_str, p_format);
                if (_toRet == true)
                {
                    /* verifica la semantica */
                    simplex_ORM.Column.parseDateTimeString(p_str, p_format);
                }
            }
            catch (simplex_ORM.Spx_ORMMessage ormm)
            {
                _toRet = false;
            }
            return _toRet;
        }


        /// <summary>
        /// Aggiunge la pagina corrente alla catena delle briciole di pane (SimplexBreadCrumbs)
        /// della maschera corrente.
        /// @MdN
        /// ----
        /// Crtd: 18/01/2015
        /// Mdfd: 25/01/2015
        /// </summary>
        /// <remarks>La proprietà MySbc DEVE essere valorizzata.</remarks>
        protected virtual void AddToSimplexBreadCrumbs()
        {
            int _lvl;
            // controllo
            if (MySbc == null)
                return;
            // AGGIUNTA DI UNA VOCE NELLA CATENA BREADCRUMBS
            if (MyMainMenu != null)
            {
                // Se esiste un menu mi pongo il problema della coerenza tra il menu 
                // e la catena di BreadCrumbs.
                _lvl = MasterPCM.getMenuVoiceLevel(MyConn, MyName);
                if (_lvl == -1)
                    MySbc.Add(MyName, MyAspxFileName);
                else
                {
                    if (_lvl > MySbc.Count)
                        MySbc.Add(MyName, MyAspxFileName);                      //@MdN 25/01/2015
                    else
                        MySbc.InsertAndBreak(MyName, MyAspxFileName, _lvl);     //@MdN 25/01/2015
                }
            }
            else
            {
                // Se il menu non esiste non mi pongo problemi ed accodo all'ultimo anello della catena.
                if (MySbc != null)
                    MySbc.Add(MyName, MyAspxFileName);
            }
        }//fine

        /// <summary>
        /// <p>
        /// Imposta gli attributi necessari al funzionamento.
        /// Si attende in Session["APPLICATION"] l'oggetto GeneralApplication corrente.
        /// Se non è presente, lancia un eccezione del tipo generalApplication TracedMessage.
        /// Valorizza:
        /// </p>
        /// <p>
        /// Valorizza i seguenti attributi:
        /// </p>
        /// <ul>
        /// <li>MyApp               :   APPLICAZIONE CORRENTE;</li>
        /// <li>MyConn              :   CONNESSIONE DI DEFAULT;</li>
        /// <li>MyMainMenu          :   CONTROLLO MENU SINISTRO;</li>
        /// <li>MySbc               :   CONTROLLO BREADCRUMBS;</li>
        /// <li>MyAspxFileName      :   NOME DELLA PAGINA WEB/WEB FORM.</li>
        /// <li>MyBackURL           :   URL DELLA PAGINAWEB/WEB FORM CHIAMANTE.</li>
        /// </ul>
        /// <pre>
        /// ------
        /// @MdN
        /// Crtd: 06/12/2014
        /// Mdfd: 30/01/2015
        /// </pre>
        /// </summary>
        protected override void preloadEnvironment()
        {
            // Preleva le informazioni utente
            // * PARAMETRI DI CONNESSIONE ODBC
            OdbcConfiguration l_config = null;
            l_config = (OdbcConfiguration)System.Configuration.ConfigurationSettings.GetConfig("Connections/OdbcConfig");
            if (l_config == null)
            {
                simplex_ORM.Spx_ORMMessage Mex = new simplex_ORM.Spx_ORMMessage("Errore nel caricamento delle impostazioni di connessione col database", 1009);
                throw Mex;
            }

            MyConn = new System.Data.Odbc.OdbcConnection(l_config.ToString());
            if(MyConn==null)
            {
                simplex_ORM.Spx_ORMMessage Mex = new simplex_ORM.Spx_ORMMessage("Errore nell'apertura della connessione col database",2);
                throw Mex;
            }    
        }//fine preloadEnvironment

        /// <summary>
        /// <p>EN: Show the errors occurred in the last operation. At the end, clears the list of errors. 
        /// </p>
        /// <p>IT: Mostra gli errori verificatisi nell'ultima operazione. Al termine, cancella la lista degli errori.
        /// </p>
        /// <pre>
        /// @MdN
        /// ----
        /// Mdfd: 18/04/2015
        /// </pre>
        /// </summary>
        protected virtual void showErrors()
        {
            if (MyErrors.Count > 0)
            {
                Boolean l_pari = false;
                Literal l_ErrArea = null;
                System.Text.StringBuilder l_ErrString = new System.Text.StringBuilder("<table class=\"ErrTab\">");
                l_ErrArea = (Literal)this.Master.FindControl("ListOfErrors");

                l_ErrString.Append("<tr class=\"ErrHeaders\"><th class=\"HeadCodCol\">Codice</th><th class=\"HeadDescCol\">Descrizione</th><tr>");

                foreach (simplex_ORM.Spx_ORMMessage ORMM in MyErrors)
                {
                    if (l_pari == true)
                        l_ErrString.Append("<tr class=\"ErrRowPari\">");
                    else
                        l_ErrString.Append("<tr class=\"ErrRowDisPari\">");
                    l_pari = !l_pari;

                    l_ErrString.Append("<td class=\"ErrCodCol\">");
                    l_ErrString.Append(ORMM.MessageCode.ToString());
                    l_ErrString.Append("</td><td class=\"ErrDescCol\">");
                    l_ErrString.Append(ORMM.Message);
                    l_ErrString.Append("</td></tr>");
                }
                l_ErrString.Append("</table>");
                l_ErrArea.Text = l_ErrString.ToString();
                MyErrors.Clear(); //<-- ATTENZIONE
            }
        }//fine


        #endregion

        #region GESTORI
        protected void Page_Load(object sender, EventArgs e)
        {
            #region [WRITE YOUR CODE FROM HERE]
            // <%WRITE YOUR CODE FROM HERE%>
            
            // @MdN 20/06/2015
            try
            {
                preloadEnvironment();
            }
            catch (simplex_ORM.Spx_ORMMessage MyMex)
            {
                MyErrors.Add(MyMex);
                showErrors();
                return;
            }

            // Il messaggio di errore deve essere inizialmente oscurato.
            if (Session["GenericSpxForm1.Panel_MessageBox.Visible"] == null)
                MyMessageBoxVisible = false;
            else
                MyMessageBoxVisible = (Boolean)Session["GenericSpxForm1.Panel_MessageBox.Visible"];
            Panel_MessageBox.Visible = MyMessageBoxVisible;

            // <%WRITE YOUR CODE TO HERE%>
            #endregion

            // Evento del BOX dei messaggi
            if (Session["GenericSpxForm1.R_Conferma.Click"] != null)
            {
                /* Parte del codice necessaria ad impostare il più opportuno gestore dell'evento
                 * Click del tasto R_Conferma del MessageBox.
                 * Vedere la documentazione a corredo.
                 */
                // <%WRITE YOUR CODE FROM HERE%>

                // <%WRITE YOUR CODE TO HERE%>
            }

            //@MdN 15/03/215
            if (MyErrors.Count > 0)
                showErrors();
            else
                ((Literal)this.Master.FindControl("ListOfErrors")).Text = "";
        }


        /// <summary>
        /// Mostra l'oggetto SQLTable che supporta la maschera.
        /// ---
        /// @MdN
        /// Crtd: 01/01/2015
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected virtual void Page_SaveStateComplete(object sender, EventArgs e)
        {
            if (TBL_UNDER_FORM != null)
                this.show(TBL_UNDER_FORM, "ContentPlaceHolder1", "System.Web.UI.WebControls.ContentPlaceHolder");
        }

        /// <summary>
        /// Nome della pagina visualizzato nei menu e nelle briciole di pane.
        /// @MdN
        /// ----
        /// Crtd: 18/01/2015
        /// </summary>
        public virtual String Nome
        {
            get
            {
                return MyName;
            }
            set
            {
                if (value == null)
                    MyName = "### DA DEFINIRE";
                else
                    MyName = value;
            }
        }

        /// <summary>
        /// Gestore dell'evento Click sul tasto "Annulla" della MessageBox
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 08/03/2015
        /// </pre>
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void R_Annulla_Click(object sender, EventArgs e)
        {
            MyMessageBoxVisible = false;
            Session["GenericSpxForm1.Panel_MessageBox.Visible"] = MyMessageBoxVisible;
            Panel_MessageBox.Visible = MyMessageBoxVisible;
            Session["GenericSpxForm1.R_Conferma.Click"] = null;
        }

        /// <summary>
        /// Esegue materialmente l'autenticazione.
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 22/06/2015
        /// Mdfd: 24-26/05/2016 15:35
        /// </pre>
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void R_Login_Click(object sender, EventArgs e)
        {
            Boolean _autenticated = false;  // <added @MdN 24/05/2016>

            this.MyLogin= R_UserName.Text;
            this.MyPwd = this.R_Password.Text;

            // OKKIO
            MyUsername = UserName;
            if (MyUsername == null)
            {
                MyMessageBoxVisible = true;
                Session["GenericSpxForm1.Panel_MessageBox.Visible"] = MyMessageBoxVisible;
                Panel_MessageBox.Visible = MyMessageBoxVisible;
            }
            // CONTROLLO UTENZA
            if (MyConn == null)
            {
                simplex_ORM.Spx_ORMMessage _mex = new simplex_ORM.Spx_ORMMessage("PROBLEMI NELL'APERTURA DELLA CONNESSIONE CON IL DATABASE.");
                MyErrors.Add(_mex);
                showErrors();
                return;
            }
            if (MyConn.State == ConnectionState.Closed)
                MyConn.Open();
            UNIF_User _tmpUsr = new UNIF_User();
            Int32 _esito = (Int32)_tmpUsr.loadByLogName(MyUsername, MyConn);
            MyConn.Close();
            switch(_esito)
            {
                case -1:
                    {
                        simplex_ORM.Spx_ORMMessage _mex = new simplex_ORM.Spx_ORMMessage("Riscontrati problemi TECNICI nel riconoscimento dell'utente.");
                        MyErrors.Add(_mex);

                        MyMessageBoxVisible = true;
                        Session["GenericSpxForm1.Panel_MessageBox.Visible"] = MyMessageBoxVisible;
                        Panel_MessageBox.Visible = MyMessageBoxVisible;

                        showErrors();
                        return;                        
                    }
                case -2:
                    {
                        simplex_ORM.Spx_ORMMessage _mex = new simplex_ORM.Spx_ORMMessage("Riscontrati problemi TECNICI nel riconoscimento dell'utente.");
                        MyErrors.Add(_mex);

                        MyMessageBoxVisible = true;
                        Session["GenericSpxForm1.Panel_MessageBox.Visible"] = MyMessageBoxVisible;
                        Panel_MessageBox.Visible = MyMessageBoxVisible;

                        showErrors();
                        return;                        
                    }
                default:
                    {
                        Session["UNIF_User"] = _tmpUsr;
                        _tmpUsr.LogName = MyUsername;
                        _tmpUsr.Password = MyPwd;

                        // <added @MdN 24/05/2016>
                        /* CONTROLLO TRA GLI UTENTI INTERNI */
                        try
                        {
                            if (_tmpUsr.checkInternalUser(MyPwd, MyConn) == false)
                                _autenticated = false;
                            else
                                _autenticated = true;                           // <added @MdN 24/05/2016>
                        }
                        catch (GeneralApplication.TracedMessage _tm)
                        {
                            if (_tm.TraceMessage.CompareTo("CHANGE_PWD") == 0)
                            {
                                Session.Add("UNIF_User", null);                 //Sposto l'utente su un'altra variabile di stato per evitare
                                Session.Add("USR_CHANGE_PWD", _tmpUsr);         //che possa in qualche modo accedere alle funzioni applicative.
                                writeMessageToLog(MyConn, "Cambio pasword dell'utente " + _tmpUsr.LogName, "Login.aspx", _tmpUsr.UID.ToString(), Session.SessionID);
                                Response.Redirect("~/CambiaPassword.aspx");
                            }
                            else
                            {
                                simplex_ORM.Spx_ORMMessage _mex = new simplex_ORM.Spx_ORMMessage(_tm.TraceMessage,_tm.TracePoint);
                                MyErrors.Add(_mex);

                                MyMessageBoxVisible = true;
                                Session["GenericSpxForm1.Panel_MessageBox.Visible"] = MyMessageBoxVisible;
                                Panel_MessageBox.Visible = MyMessageBoxVisible;

                                showErrors();
                                return;  
                            }
                        }
                        catch (Exception E)
                        {
                            simplex_ORM.Spx_ORMMessage _mex = new simplex_ORM.Spx_ORMMessage("Riscontrati problemi TECNICI nel riconoscimento dell'utente: " + E.Message);
                            MyErrors.Add(_mex);

                            MyMessageBoxVisible = true;
                            Session["GenericSpxForm1.Panel_MessageBox.Visible"] = MyMessageBoxVisible;
                            Panel_MessageBox.Visible = MyMessageBoxVisible;

                            showErrors();
                            return;  
                        }
                        // </added @MdN 24/05/2016>

                        // CONTROLLO TRA GLI UTENTI DI DOMINIO - Mdfd @MdN 26/05/2016 11:15
                        if(ConfigurationSettings.AppSettings["DEV"].ToLower().CompareTo("false")==0 && _autenticated==false) //@MdN: 15/06/2016
                        {
                            if (_tmpUsr.checkADUser() == false)
                                _autenticated = false;
                            else
                                _autenticated = true;
                        }

                        if(_autenticated==false)
                        {
                            MyMessageBoxVisible = true;
                            Session["GenericSpxForm1.Panel_MessageBox.Visible"] = MyMessageBoxVisible;
                            Panel_MessageBox.Visible = MyMessageBoxVisible;
                            return;
                        }


                        /* La maschera NON ha un oggetto SQLTable associato. In questo modo usa la connessione aperta */
                        writeMessageToLog(MyConn, "Login dell'utente " + _tmpUsr.LogName, "Login.aspx", _tmpUsr.UID.ToString(), Session.SessionID);
                        Response.Redirect("~/Default.aspx");
                        break;
                    }
            }


        }
        #endregion
    }
}
