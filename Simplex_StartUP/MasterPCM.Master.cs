using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using GeneralApplication;
using System.Data.Odbc;

namespace SIMPLEX_STARTUP
{
    /// <summary>
    /// <pre>
    /// ----
    /// @MdN
    /// Crtd:
    /// Mdfd: 24/09/2015 - Gestione interruzione della sessione.
    ///
    /// </pre>
    /// </summary>
    public partial class MasterPCM : System.Web.UI.MasterPage
    {
        //protected GeneralApplication.GeneralApplication MyApp=null;                    

        /// <summary>
        /// Importata da MasterPCM.master.cs dell'applicazione UNIF_PERSONALE in data 09/06/2016
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 09/06/2016
        /// </pre>
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            OdbcConnection l_conn = null;
            UNIF_Application l_Appl = null;
            l_Appl = (UNIF_Application)Session["APPLICATION"];
            // @MdN: 14/03/2016
            if (l_Appl != null)
                l_conn = l_Appl.DefaultConnection;
            // @MdN: 14/03/2016


            /* ** INSERIMENTO DELLA VOCE DESKTOP - PROFILO - LOGOUT - ** */
            R_MainMenu.Items.Clear();

            MenuItem _mi = new MenuItem("Desktop Applicativi", "0", null, "SimplexDesktop.aspx");
            this.R_MainMenu.Items.Add(_mi);

            // @MdN: 14/03/2016
            if (l_conn != null)
                loadVMenu(l_conn, false);
            // @MdN: 14/03/2016

            // @MdN: 14/03/2016
            if (l_Appl != null)
                R_InfoUtente.Text = ((UNIF_User)l_Appl.CurrentUser).Nome + " " + ((UNIF_User)l_Appl.CurrentUser).Cognome;
            // @MdN: 14/03/2016

        }

        ////protected void Page_Load(object sender, EventArgs e)
        ////{
        ////    OdbcConnection l_conn = null;
        ////    UNIF_Application l_Appl = null;
        ////    l_Appl = (UNIF_Application)Session["APPLICATION"];
        ////    if (l_Appl == null)
        ////        Response.Redirect("GenericSpxErrorForm.aspx");  //24/09/2015 - Gestione interruzione della sessione.
        ////    l_conn = l_Appl.DefaultConnection;



        ////    /* ** INSERIMENTO DELLA VOCE DESKTOP - PROFILO - LOGOUT - ** */
        ////    R_MainMenu.Items.Clear();

        ////    MenuItem _mi = new MenuItem("Desktop Applicativi", "0", null, "SimplexDesktop.aspx");
        ////    this.R_MainMenu.Items.Add(_mi);

        ////    loadVMenu(l_conn,false);

        ////    R_InfoUtente.Text = ((UNIF_User)l_Appl.CurrentUser).Nome + " " + ((UNIF_User)l_Appl.CurrentUser).Cognome;

        ////}

        /// <summary>
        /// <p>EN: Loads dynamically the items of the two level menu on the left side of the master page.</p>
        /// <p>IT: Carica dinamicamente le voci del menu a due livelli posto a sinistra della master page.</p>
        /// </summary>
        /// <param name="p_conn">Active connection / Connessione attiva</param>
        protected void loadVMenu(OdbcConnection p_conn)
        {
            loadVMenu(p_conn, true);
        }

        /// <summary>
        /// <p>EN: Loads dynamically the items of the two level menu on the left side of the master page.</p>
        /// <p>IT: Carica dinamicamente le voci del menu a due livelli posto a sinistra della master page.</p>
        /// </summary>
        /// <param name="p_conn">Active connection / Connessione attiva</param>
        protected void loadVMenu(OdbcConnection p_conn, Boolean p_clear)
        {
            //String l_sql = "SELECT I1.ID, I1.descrizione, I1.LinK, I1.PADRE, ISNULL(I1.PADRE,I1.ID) AS ORDINE, I2.DESCRIZIONE FROM UTILS_MENU I1 LEFT JOIN UTILS_MENU I2 ON I1.PADRE=I2.ID WHERE I1.DATA_DEL IS NULL AND I2.DATA_DEL IS NULL order by ORDINE, PADRE";
            Int32 _len =0;

            // CONTROLLO
            if (((UNIF_User)((UNIF_Application)Session["APPLICATION"]).CurrentUser).getGroups() == null)
                return;

            System.Text.StringBuilder l_sql = new System.Text.StringBuilder("select FAP.ID, FAP.DENOMINAZIONE, FAP.HOMELNK, NULL FROM ADMN_FunzioneApplicativa FAP INNER JOIN ADMN_FunzioneApplicativaGruppo FAG on FAP.ID=FAG.FUNZIONEAPPLICATIVA WHERE FAP.DATA_DEL IS NULL AND FAG.GRUPPO IN (");
            _len = ((UNIF_User)((UNIF_Application)Session["APPLICATION"]).CurrentUser).getGroups().Count();
            for (int t = 0; t < _len; t++)
            {
                if (t > 0)
                    l_sql.Append(", ");
                l_sql.Append((((UNIF_User)((UNIF_Application)Session["APPLICATION"]).CurrentUser).getGroups()).ElementAt(t));
            }
            l_sql.Append(")");

            loadVMenu(p_conn, l_sql.ToString(),p_clear);
        }

        /// <summary>
        /// <p>EN: Loads dynamically the items of the two level menu on the left side of the master page.</p>
        /// <p>IT: Carica dinamicamente le voci del menu a due livelli posto a sinistra della master page.</p>
        /// </summary>
        /// <param name="p_conn">Active connection to the database that contains the table of the menu items / Connessione attiva al database contenete a struttura delle voci.</param>
        /// <param name="p_sql">Query SQL that extract the two level menu's items / Query SQL di estrazione delle voci del menu a due livelli.</param>
        /// <remarks>
        /// <p>
        /// EN: The table conatining the items has the following structure:
        /// </p>
        /// <p>
        /// IT: La tabella contenente le voci ha la seguente struttura:
        /// </p>
        /// <pre>
        /// CREATE TABLE dbo.UTILS_MENU
        /// (
        /// ID INT NOT NULL
        /// , DESCRIZIONE			VARCHAR(24) NOT NULL
        /// , PADRE					INT NULL
        /// , LINK					NVARCHAR(1024) NOT NULL
        /// , USR					INT NOT NULL
        /// , NOTE                  NVARCHAR(1024)
        /// , IMG_URL               NVARCHAR(1024)
        /// , DATA_SYS				DATETIME NOT NULL DEFAULT GETDATE()
        /// , DATA_DEL				DATETIME NULL
        /// )
        /// </pre>
        /// </remarks>
        protected void loadVMenu(OdbcConnection p_conn, String p_sql)
        {
            loadVMenu(p_conn, p_sql, true);
        }

        /// <summary>
        /// <p>EN: Loads dynamically the items of the two level menu on the left side of the master page.</p>
        /// <p>IT: Carica dinamicamente le voci del menu a due livelli posto a sinistra della master page.</p>
        /// </summary>
        /// <param name="p_conn">Active connection to the database that contains the table of the menu items / Connessione attiva al database contenete a struttura delle voci.</param>
        /// <param name="p_sql">Query SQL that extract the two level menu's items / Query SQL di estrazione delle voci del menu a due livelli.</param>
        /// <param name="p_clear">True: cancella le voci di menu precedentemente immesse.</param>
        /// <remarks>
        /// <p>
        /// EN: The table conatining the items has the following structure:
        /// </p>
        /// <p>
        /// IT: La tabella contenente le voci ha la seguente struttura:
        /// </p>
        /// <pre>
        /// CREATE TABLE dbo.UTILS_MENU
        /// (
        /// ID INT NOT NULL
        /// , DESCRIZIONE			VARCHAR(24) NOT NULL
        /// , PADRE					INT NULL
        /// , LINK					NVARCHAR(1024) NOT NULL
        /// , USR					INT NOT NULL
        /// , NOTE                  NVARCHAR(1024)
        /// , IMG_URL               NVARCHAR(1024)
        /// , DATA_SYS				DATETIME NOT NULL DEFAULT GETDATE()
        /// , DATA_DEL				DATETIME NULL
        /// )
        /// ----
        /// @MdN 
        /// Mdfd: 26/06/2015: inserimento link ESCI 
        /// </pre>

        /// </remarks>
        protected void loadVMenu(OdbcConnection p_conn, String p_sql, Boolean p_clear)
        {
            OdbcDataReader l_DR = null;
            OdbcCommand l_Cmd = null;
            Menu MyMainMenu = null;

            MyMainMenu = (Menu)this.FindControl("R_MainMenu");

            // this.R_MainMenu = new Menu();
            // R_MainMenu.Items.Clear();                               // cancella le voci precedenti

            if(p_clear==true)
                MyMainMenu.Items.Clear();                                
            
            // Imposta la query per caricare le voci di menu
            if (p_conn == null)
                return;
            if (p_sql == null)
                return;
            if (p_conn.State == System.Data.ConnectionState.Closed)
                p_conn.Open();

            l_Cmd = p_conn.CreateCommand();
            l_Cmd.CommandText = p_sql;
            l_DR = l_Cmd.ExecuteReader();
            if (l_DR != null)
            {
                MenuItem l_MI = null;
                MenuItem l_PI = null;       //ITEM PADRE
                while (l_DR.HasRows && l_DR.Read())
                {
                    /* *** ATTENZIONE *** 
                     * PER COME E' STATA IMPOSTATA LA QUERY, TUTTE LE SOTTOVOCI DI UNA STESSA VOCE PADRE
                     * SONO CONTIGUE E LA STESSA VOCE PADRE E' LA CAPOLISTA!!!
                     * 
                     * SELECT I1.ID, I1.descrizione, I1.LinK, I1.PADRE, ISNULL(I1.PADRE,I1.ID) AS ORDINE, I2.DESCRIZIONE
                     * FROM UTILS_MENU I1 LEFT JOIN UTILS_MENU I2 ON I1.PADRE=I2.ID
                     * WHERE I1.DATA_DEL IS NULL
                     * AND I2.DATA_DEL IS NULL
                     * order by ORDINE, PADRE
                     * 
                     * ***/

                    //l_MI = new MenuItem(l_DR[1].ToString(), l_DR[0].ToString(), null, l_DR[2].ToString());
                    //l_MI = new MenuItem(l_DR[1].ToString(), HttpContext.Current.Server.MapPath(l_DR[2].ToString()), null, null);
                    l_MI = new MenuItem(l_DR[1].ToString(), "~/" + l_DR[2].ToString(), null, null);
                    if (l_DR.IsDBNull(3) == true)
                    {
                        l_PI = l_MI;                            // trovato un candidato PADRE
                        //R_MainMenu.Items.Add(l_PI);
                        MyMainMenu.Items.Add(l_PI);
                    }
                    else
                    {
                        if (l_PI != null) // && l_DR[3].ToString().CompareTo(l_PI.Value)==0)
                        {
                            // CONTROLLO ED AFFILIO AL PADRE
                            l_PI.ChildItems.Add(l_MI);
                        }
                            
                    }                    
                } //fine while

                /* *** @MdN 26/06/2015: inserimento link ESCI *** */
                l_MI = new MenuItem("ESCI", "~/Logout.aspx");
                MyMainMenu.Items.Add(l_MI);

                l_DR.Close();
                l_Cmd.Dispose();
                p_conn.Close();
            }
        }

        /// <summary>
        /// Ricava il livello della voce di menu dalla radice del menu.
        /// @MdN
        /// ----
        /// Crtd: 18/01/2015
        /// Mdfd: @MdN 22/03/2015
        /// </summary>
        /// <param name="p_Conn">Connessione attiva.</param>
        /// <param name="p_MenuVoice">Stringa che rappresenta la voce di menu.</param>
        /// <returns>Il livello della voce di menu (0 based) o -1 in caso di anomalia.</returns>
        public static int getMenuVoiceLevel(OdbcConnection p_conn, string p_MenuVoice)
        {
            OdbcDataReader l_DR = null;
            OdbcCommand l_Cmd = null;
            Int32 l_ID=0;
            Int32 l_IDPadre=0;
            int _level=0;
            Boolean _isRoot=false;


            // Controlli
            if (p_conn == null)
                return -1;
            if (p_MenuVoice == null)
                return -1;

            if (p_conn.State == System.Data.ConnectionState.Closed)
                p_conn.Open();

            l_Cmd = p_conn.CreateCommand();

            // PRELEVO L'ID DELLA VOCE DI MENU
            l_Cmd.CommandText = "SELECT ID FROM dbo.UTILS_MENU WHERE DESCRIZIONE = '" + p_MenuVoice + "'";
            l_DR = l_Cmd.ExecuteReader();
            if (l_DR != null && l_DR.Read()==true)
            {
                l_ID=l_DR.GetInt32(0);
                l_DR.Close();
            } else
            {
                l_DR.Close();
                p_conn.Close();
                return -1;
            }
            
            // CERCO LA RADICE O UNA RADICE
            l_IDPadre = l_ID;
            while (_isRoot == false)
            {
                l_Cmd.CommandText = "SELECT PADRE FROM dbo.UTILS_MENU WHERE ID= " + l_IDPadre;
                l_DR = l_Cmd.ExecuteReader();
                if (l_DR != null & l_DR.Read()==true)
                {
                    if ((l_DR.IsDBNull(0) == false) && (l_DR.GetInt32(0)!=l_IDPadre)) //@MdN 22/03/2015
                    {
                        l_IDPadre = l_DR.GetInt32(0);
                        _level += 1;
                        l_DR.Close();
                    }
                    else
                    {
                        l_DR.Close();
                        _isRoot = true;
                    }
                        
                }
                else
                {
                    l_DR.Close();
                    _isRoot = true;
                }
            } // FINE CERCO LA RADICE O UNA RADICE
            // TTROVATA LA RADICE.
            p_conn.Close();
            return _level;
        }

        protected void R_MainMenu_MenuItemClick(object sender, MenuEventArgs e)
        {
            //  CANCELLAZIONE DEGLI STATI (TUTTI)
            Session["ATTIVITA"] = null;
            Session["APPLICATIVO"] = null;
            Session["UTENTE"] = null;


            // GESTIONE DEI REDIRECT
            Response.Redirect(e.Item.Value);

        }// FINE
    }
}