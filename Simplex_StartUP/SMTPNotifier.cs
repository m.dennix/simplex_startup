using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;

namespace SIMPLEX_STARTUP
{
    public class SMTPNotifier
    {
        protected String MyMex = null;
        protected String MyDestinations = null;
        protected String MySubject = null;
        /// <summary>
        /// <p>
        /// Indirizzo del mittente.
        /// Viene valorizzata dal costruttore prelevando il valore dal file di configurazione web.config
        /// alla chiave [MITTENTE_NOTIFICHE].
        /// </p>
        /// </summary>
        protected String MyAddress = null;

        /// <summary>
        /// <p>
        /// Nome della tabella delle NOTIFICHE.
        /// Viene valorizzata dal costruttore prelevando il valore dal file di configurazione web.config
        /// alla chiave [NOTIFICHE].
        /// </p>
        /// </summary>
        protected String MyTable = null;

        /// <summary>
        /// Lista degli indirizzi di e-mail separati dal carattere ";" o dal carattere "," con eventuale spazio.
        /// </summary>
        protected UNIF_Application MyApp = null;

        /// <summary>
        /// <p>
        /// Nome del server SMTP.
        /// Viene valorizzata dal costruttore prelevando il valore dal file di configurazione web.config
        /// alla chiave [SERVER_NOTIFICHE].
        /// </p>
        /// </summary>
        protected String MyServer = null;
        
        /// <summary>
        /// <p>
        /// Collezione di stringhe.
        /// Usata per creare un lista UNIVOCA (cioè senza ripetizioni) di indirizzi e-mail.
        /// </p>
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 15/07/2015
        /// </pre>
        /// </summary>
        protected System.Collections.Generic.List<String> MyList = null;

        /// <summary>
        /// Caratteri separatori. 
        /// </summary>
        private char[] _separators = { ';', ',' };

        #region COSTRUTTORI

        public SMTPNotifier (UNIF_Application p_app, string p_mex, string p_destinations, string p_subject)
        {
            MyServer = System.Configuration.ConfigurationSettings.AppSettings["SERVER_NOTIFICHE"];
            MyAddress = System.Configuration.ConfigurationSettings.AppSettings["MITTENTE_NOTIFICHE"];
            MyTable = System.Configuration.ConfigurationSettings.AppSettings["NOTIFICHE"];
            MyMex = p_mex;
            MyDestinations = p_destinations;
            MySubject = p_subject;
            MyApp = p_app;
        }

        public SMTPNotifier(UNIF_Application p_app, string p_mex, string p_subject)
        {
            MyServer = System.Configuration.ConfigurationSettings.AppSettings["SERVER_NOTIFICHE"];
            MyAddress = System.Configuration.ConfigurationSettings.AppSettings["MITTENTE_NOTIFICHE"];
            MyTable = System.Configuration.ConfigurationSettings.AppSettings["NOTIFICHE"];
            MyMex = p_mex;
            MySubject = p_subject;
            MyApp = p_app;
            MyDestinations = getRecipients(p_app.ID);
        }

        #endregion

        /// <summary>
        /// <p>
        /// Preleva dalla tabella GUAS_NOTIFICHE l'elenco degli indirizzi di eMail
        /// dei destinatari SENZA DUPLICAZIONI.
        /// 
        /// </p>
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 15/07/2015
        /// </pre>
        /// </summary>
        /// <param name="p_FunzioneApplicativa"></param>
        /// <returns>elenco degli indirizzi di eMail</returns>
        public String getRecipients(int p_FunzioneApplicativa)
        {
            String _toRet = null;
            System.Data.Odbc.OdbcCommand _cmd = null;
            System.Data.Odbc.OdbcDataReader _rs = null;
            String[] _components = null;
            
            if (MyApp.DefaultConnection.State == ConnectionState.Closed)
                MyApp.DefaultConnection.Open();

            _cmd = MyApp.DefaultConnection.CreateCommand();
            _cmd.CommandText = "SELECT EMAIL FROM " + MyTable + " WHERE FUNZIONE_APPLICATIVA=" + p_FunzioneApplicativa;

            // prendo dalla tabella GUAS_NOTIFICHE gli indirizzi di e-mail dei destanatari
            try
            {
                _rs=_cmd.ExecuteReader();
                while (_rs.Read())
                {
                    if (_rs.IsDBNull(0) == false)
                    {
                        if (_toRet == null)
                            _toRet = _rs.GetString(0);
                        else
                            _toRet = _toRet + ";" + _rs.GetString(0);
                    }
                }

            } catch(Exception _ex)
            {
                simplex_ORM.Spx_ORMMessage _mex = new simplex_ORM.Spx_ORMMessage("Errore nella query: " + "[" + _cmd.CommandText + "] ...");
                _mex.traceLog(MyApp.DefaultConnection, MyApp.CurrentUser.UID.ToString(), MyApp.SessionID);
                _mex = new simplex_ORM.Spx_ORMMessage(_ex);
                _mex.traceLog(MyApp.DefaultConnection, MyApp.CurrentUser.UID.ToString(), MyApp.SessionID);
                MyApp.DefaultConnection.Close();
                return null;
            }

            // Scompatto 
            _components = _toRet.Split(_separators);
            
            // Inserisco nella lista univoca
            if(MyList!=null)
                MyList.Clear();
            foreach (String _tmp in _components)
            {
                addWithoutDuplicate(_tmp.Trim());
            }


            // Estraggo la lista univoca di eMail concatenate
            _toRet=null;
            foreach (String _tmp in MyList)
                if (_toRet == null)
                    _toRet = _tmp;
                else
                    _toRet = _toRet + ";" + _tmp;
            
            //fine
            MyApp.DefaultConnection.Close();
            return _toRet;
        }

        /// <summary>
        /// <P>
        /// Invia un messaggio di notifica precedentemente composto.
        /// </P>
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 14/07/215
        /// </pre>
        /// </summary>
        public void sendNotification()
        {
            String[] _addrs=null;
            char[] _seps={';'};
            System.Net.Mail.SmtpClient _cl  = new SmtpClient(MyServer);
            MailMessage _mmex = new MailMessage();

            // DESTINATARI
            _addrs = MyDestinations.Split(_seps);
            foreach (String _t in _addrs)
            {
                _mmex.To.Add(new MailAddress(_t.Trim()));
            }
            _mmex.Subject = MySubject;
            _mmex.Body = MyMex;
            _mmex.From = new MailAddress(MyAddress);
            _mmex.Sender = new MailAddress("gtel@palazzochigi.it");
            

            // iscrivo il gestore all'evento callback in quanto la spedizione è 
            // asincrona.

            _cl.SendCompleted += new SendCompletedEventHandler(On_SendCompleted);
            _cl.Port = 25;
            _cl.Credentials = CredentialCache.DefaultNetworkCredentials;
            
            //_cl.SendAsync(_mmex, "Mail OK");
            try
            {
                _cl.Send(_mmex);
            }
            catch (Exception E)
            {
                simplex_ORM.Spx_ORMMessage _mx = new simplex_ORM.Spx_ORMMessage(E);
                _mx.traceLog(MyApp.DefaultConnection, MyApp.CurrentUser.UID.ToString(), MyApp.SessionID);
                throw _mx;
            }
            _mmex.Dispose();
        }


        #region GESTORI
        protected virtual void On_SendCompleted(object sender, System.ComponentModel.AsyncCompletedEventArgs e)
        {
            //throw new NotImplementedException(); //NOP
            simplex_ORM.Spx_ORMMessage _msg = new simplex_ORM.Spx_ORMMessage(sender.ToString());
            _msg.traceLog(MyApp.DefaultConnection, MyApp.CurrentUser.UID.ToString(), "Mail sent");
        }
        #endregion

        #region METODI PRIVATI

        /// <summary>
        /// <p>
        /// Aggiunge una stringa ad una lista di strignghe senza duplicati.
        /// </p>
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 15/07/2015
        /// </pre>
        /// </summary>
        /// <param name="p_Mail">Stringa che rappresenta un indirizzo di eMail.</param>
        /// <returns>Il numero di elementi che compongono la stringa.</returns>
        public int addWithoutDuplicate(String p_Mail)
        {
            if (MyList == null)
                MyList = new System.Collections.Generic.List<string>();
            if (p_Mail == null)
                return MyList.Count;

            //Ciclo: se trovo un duplicato esco.
            foreach (String _s in MyList)
                if (_s.CompareTo(p_Mail) == 0)
                    return MyList.Count;

            //fine ciclo: altrimenti accodo.
            MyList.Add(p_Mail);
            return MyList.Count;
        }


        #endregion




    }
}
