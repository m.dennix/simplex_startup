using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using GeneralApplication;
using WebSimplex;

namespace SIMPLEX_STARTUP
{
    /// <summary>
    /// <p>
    /// Esegue il cambio forzato delle credenziali.
    /// Si aspetta che il chiamante passi un oggetto di tipo UNIF_user dalla variabile di sessione "USR_CHANGE_PWD".
    /// </p>
    /// <p>
    /// Creata da una compia di GenericSpxForm.
    /// Derivata da SimplexForm.
    /// 
    /// Ha attributi protetti necessari per il funzionamento dell'applicazione.
    /// Esegue Override dei seguenti metodi:
    /// </p>
    /// <pre>
    /// ------
    /// @MdN 
    /// Crtd: 25/05/2016
    /// </pre>
    /// </summary>
    public partial class CambiaPassword : simplex_FORM.SimplexForm
    {

        #region ATTRIBUTI
        /// <summary>
        /// Oggetto applicazione corrente.
        /// </summary>
        protected GeneralApplication.GeneralApplication MyApp = null;
        /// <summary>
        /// Oggetto connessione corrente al database SIMPLEX_STARTUP
        /// DEVE essere sempre chiusa.
        /// </summary>
        protected System.Data.Odbc.OdbcConnection MyConn = null;
        /// <summary>
        /// Nome della pagina come visualizzata nel menu.
        /// </summary>
        protected String MyName = null;
        /// <summary>
        /// Riferimento all' oggetto di classe WebSimplexBreadCrumbs.
        /// </summary>
        protected WebSimplexBreadCrumbs MySbc = null;
        /// <summary>
        /// Nome del file della pagina corrente. Viene valorizzato automaticamente 
        /// dal metodo preloadEnvironment().
        /// </summary>
        protected String MyAspxFileName = null;
        /// <summary>
        /// URL della pagina chiamante.
        /// </summary>
        protected String MyBackURL = null;
        /// <summary>
        /// Indica se il riquadro dei messaggi deve essere visibile o meno.
        /// Per default NON è visibile.
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 08/03/2015
        /// </pre>
        /// </summary>
        protected Boolean MyMessageBoxVisible = false;
        /// <summary>
        /// Menu verticale associato alla maschera
        /// </summary>
        protected System.Web.UI.WebControls.Menu MyMainMenu = null;
        /// <summary>
        /// Username.
        /// </summary>
        protected String MyUsername = null;
        /// <summary>
        /// Nuova Password.
        /// </summary>
        protected String MyPwd1 = null;
        /// <summary>
        /// Password di controllo
        /// </summary>
        protected String MyPwd2 = null;
        /// <summary>
        /// Utente da controllare (vedi preloadEnvironment())
        /// </summary>
        protected UNIF_User MyUsr = null;

        #endregion

        #region METODI PROTETTI
        /// <summary>
        /// <p>
        /// Esegue la validazione dei campi
        /// <pre>
        /// ----
        /// @MdN 
        /// Crtd: 25/05/2016 16:52
        /// </pre>
        /// </p>
        /// </summary>
        /// <returns>TRUE se la validazione è avvenuta senza problemi, FALSE altrimenti.</returns>
        protected virtual Boolean validateForm()
        {
            if (this.R_Pwd1.Text == null)
                return false;

            if (this.R_Pwd2.Text == null)
                return false;

            if (this.R_Pwd1.Text.CompareTo(R_Pwd2.Text) != 0)
                return false;

            return true;
        }



        /// <summary>
        /// 
        /// </summary>
        /// <param name="p_str"></param>
        /// <param name="p_format"></param>
        /// <returns></returns>
        protected bool isAValidDateString(String p_str, String p_format)
        {
            //casi banali
            if (p_str == null)
                return false;

            if (p_format == null)
                return false;

            Boolean _toRet = true;
            int _len = p_format.Length;
            String _teststr = null;
            if (p_str.Length > _len)
                _teststr = p_str.Substring(0, _len);
            else
                _teststr = p_str;

            try
            {
                /* verifica la sintassi */
                _toRet = simplex_ORM.Column.isDateTime(p_str, p_format);
                if (_toRet == true)
                {
                    /* verifica la semantica */
                    simplex_ORM.Column.parseDateTimeString(p_str, p_format);
                }
            }
            catch (simplex_ORM.Spx_ORMMessage ormm)
            {
                _toRet = false;
            }
            return _toRet;
        }


        /// <summary>
        /// Aggiunge la pagina corrente alla catena delle briciole di pane (SimplexBreadCrumbs)
        /// della maschera corrente.
        /// @MdN
        /// ----
        /// Crtd: 18/01/2015
        /// Mdfd: 25/01/2015
        /// </summary>
        /// <remarks>La proprietà MySbc DEVE essere valorizzata.</remarks>
        protected virtual void AddToSimplexBreadCrumbs()
        {
            int _lvl;
            // controllo
            if (MySbc == null)
                return;
            // AGGIUNTA DI UNA VOCE NELLA CATENA BREADCRUMBS
            if (MyMainMenu != null)
            {
                // Se esiste un menu mi pongo il problema della coerenza tra il menu 
                // e la catena di BreadCrumbs.
                _lvl = MasterPCM.getMenuVoiceLevel(MyConn, MyName);
                if (_lvl == -1)
                    MySbc.Add(MyName, MyAspxFileName);
                else
                {
                    if (_lvl > MySbc.Count)
                        MySbc.Add(MyName, MyAspxFileName);                      //@MdN 25/01/2015
                    else
                        MySbc.InsertAndBreak(MyName, MyAspxFileName, _lvl);     //@MdN 25/01/2015
                }
            }
            else
            {
                // Se il menu non esiste non mi pongo problemi ed accodo all'ultimo anello della catena.
                if (MySbc != null)
                    MySbc.Add(MyName, MyAspxFileName);
            }
        }//fine

        /// <summary>
        /// <p>
        /// Imposta gli attributi necessari al funzionamento.
        /// Si attende in Session["APPLICATION"] l'oggetto GeneralApplication corrente.
        /// Se non è presente, lancia un eccezione del tipo generalApplication TracedMessage.
        /// Valorizza:
        /// </p>
        /// <p>
        /// Valorizza i seguenti attributi:
        /// </p>
        /// <ul>
        /// <li>MyApp               :   APPLICAZIONE CORRENTE;</li>
        /// <li>MyConn              :   CONNESSIONE DI DEFAULT;</li>
        /// <li>MyMainMenu          :   CONTROLLO MENU SINISTRO;</li>
        /// <li>MySbc               :   CONTROLLO BREADCRUMBS;</li>
        /// <li>MyAspxFileName      :   NOME DELLA PAGINA WEB/WEB FORM.</li>
        /// <li>MyBackURL           :   URL DELLA PAGINAWEB/WEB FORM CHIAMANTE.</li>
        /// </ul>
        /// <pre>
        /// ------
        /// @MdN
        /// Crtd: 06/12/2014
        /// Mdfd: 30/01/2015
        /// </pre>
        /// </summary>
        protected override void preloadEnvironment()
        {
            // Preleva le informazioni utente
            // * PARAMETRI DI CONNESSIONE ODBC
            OdbcConfiguration l_config = null;
            l_config = (OdbcConfiguration)System.Configuration.ConfigurationSettings.GetConfig("Connections/OdbcConfig");
            if (l_config == null)
            {
                simplex_ORM.Spx_ORMMessage Mex = new simplex_ORM.Spx_ORMMessage("Errore nel caricamento delle impostazioni di connessione col database", 1009);
                throw Mex;
            }

            MyConn = new System.Data.Odbc.OdbcConnection(l_config.ToString());
            if (MyConn == null)
            {
                simplex_ORM.Spx_ORMMessage Mex = new simplex_ORM.Spx_ORMMessage("Errore nell'apertura della connessione col database", 2);
                throw Mex;
            }

            if (Session["USR_CHANGE_PWD"] == null)
            {
                simplex_ORM.Spx_ORMMessage Mex = new simplex_ORM.Spx_ORMMessage("Utente non noto.", 0);
                throw Mex;
            }

            // * Utente che deve cambiare la password.
            MyUsr = (UNIF_User)Session["USR_CHANGE_PWD"];
            if (Session["APPLICATION"] != null)
                MyApp = (UNIF_Application)Session["APPLICATION"];
            else
            {
                MyApp = new UNIF_Application(MyConn);
                MyApp.CurrentUser = MyUsr;
                Session.Add("APPLICATION", MyApp);
            }

        }//fine preloadEnvironment

        /// <summary>
        /// <p>EN: Show the errors occurred in the last operation. At the end, clears the list of errors. 
        /// </p>
        /// <p>IT: Mostra gli errori verificatisi nell'ultima operazione. Al termine, cancella la lista degli errori.
        /// </p>
        /// <pre>
        /// @MdN
        /// ----
        /// Mdfd: 18/04/2015
        /// </pre>
        /// </summary>
        protected virtual void showErrors()
        {
            if (MyErrors.Count > 0)
            {
                Boolean l_pari = false;
                Literal l_ErrArea = null;
                System.Text.StringBuilder l_ErrString = new System.Text.StringBuilder("<table class=\"ErrTab\">");
                l_ErrArea = (Literal)this.Master.FindControl("ListOfErrors");

                l_ErrString.Append("<tr class=\"ErrHeaders\"><th class=\"HeadCodCol\">Codice</th><th class=\"HeadDescCol\">Descrizione</th><tr>");

                foreach (simplex_ORM.Spx_ORMMessage ORMM in MyErrors)
                {
                    if (l_pari == true)
                        l_ErrString.Append("<tr class=\"ErrRowPari\">");
                    else
                        l_ErrString.Append("<tr class=\"ErrRowDisPari\">");
                    l_pari = !l_pari;

                    l_ErrString.Append("<td class=\"ErrCodCol\">");
                    l_ErrString.Append(ORMM.MessageCode.ToString());
                    l_ErrString.Append("</td><td class=\"ErrDescCol\">");
                    l_ErrString.Append(ORMM.Message);
                    l_ErrString.Append("</td></tr>");
                }
                l_ErrString.Append("</table>");
                l_ErrArea.Text = l_ErrString.ToString();
                MyErrors.Clear(); //<-- ATTENZIONE
            }
        }//fine
        #endregion

        #region GESTORI
        protected void Page_Load(object sender, EventArgs e)
        {
            #region [WRITE YOUR CODE FROM HERE]
            // <%WRITE YOUR CODE FROM HERE%>

            // @MdN 20/06/2015
            try
            {
                preloadEnvironment();
            }
            catch (simplex_ORM.Spx_ORMMessage MyMex)
            {
                MyErrors.Add(MyMex);
                showErrors();
                return;
            }

            if (IsPostBack == true)
            {
                R_MessageBox.OkMessageButtonText = "Continua";
                //if (ViewState["OK_MESSAGE"] != null)
                //{
                //    R_MessageBox.OkMessageText = (String)ViewState["OK_MESSAGE"];
                //    ViewState["OK_MESSAGE"] = null;
                //}
            }


            // Il messaggio di errore deve essere inizialmente oscurato.
            if (IsPostBack == false)
                Session["GenericSpxForm1.Panel_MessageBox.Visible"] = null;

            if (Session["GenericSpxForm1.Panel_MessageBox.Visible"] == null)
                MyMessageBoxVisible = false;
            else
                MyMessageBoxVisible = (Boolean)Session["GenericSpxForm1.Panel_MessageBox.Visible"];
            Panel_MessageBox.Visible = MyMessageBoxVisible;

            // <%WRITE YOUR CODE TO HERE%>
            #endregion

            // Evento del BOX dei messaggi
            if (Session["GenericSpxForm1.R_Conferma.Click"] != null)
            {
                /* Parte del codice necessaria ad impostare il più opportuno gestore dell'evento
                 * Click del tasto R_Conferma del MessageBox.
                 * Vedere la documentazione a corredo.
                 */
                // <%WRITE YOUR CODE FROM HERE%>

                // <%WRITE YOUR CODE TO HERE%>
            }

            //@MdN 15/03/215
            if (MyErrors.Count > 0)
                showErrors();
            else
                ((Literal)this.Master.FindControl("ListOfErrors")).Text = "";
        }

        /// <summary>
        /// Mostra l'oggetto SQLTable che supporta la maschera.
        /// ---
        /// @MdN
        /// Crtd: 01/01/2015
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected virtual void Page_SaveStateComplete(object sender, EventArgs e)
        {
            if (TBL_UNDER_FORM != null)
                this.show(TBL_UNDER_FORM, "ContentPlaceHolder1", "System.Web.UI.WebControls.ContentPlaceHolder");
        }

        /// <summary>
        /// Gestore dell'evento Click sul tasto "Annulla" della MessageBox
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 08/03/2015
        /// </pre>
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void R_Annulla_Click(object sender, EventArgs e)
        {
            MyMessageBoxVisible = false;
            Session["GenericSpxForm1.Panel_MessageBox.Visible"] = MyMessageBoxVisible;
            Panel_MessageBox.Visible = MyMessageBoxVisible;
            Session["GenericSpxForm1.R_Conferma.Click"] = null;
        }

        /// <summary>
        /// Esegue materialmente l'autenticazione.
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 22/06/2015
        /// Mdfd: 24-26/05/2016 15:35
        /// </pre>
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void R_Login_Click(object sender, EventArgs e)
        {
            if (this.validateForm() == false)
            {
                // <added @MdN 24/05/2016>
                simplex_ORM.Spx_ORMMessage _mex = new simplex_ORM.Spx_ORMMessage("ALMENO UNA DELLE CHIAVI MANCA OPPURE LE CHIAVI IMMESSE NON CORRISPONDONO.");
                MyErrors.Add(_mex);
                showErrors();
                return;
            }            

            this.MyPwd1 = this.R_Pwd1.Text;
            this.MyPwd2 = this.R_Pwd2.Text;

            // CONTROLLO UTENZA
            if (MyConn == null)
            {
                simplex_ORM.Spx_ORMMessage _mex = new simplex_ORM.Spx_ORMMessage("PROBLEMI NELL'APERTURA DELLA CONNESSIONE CON IL DATABASE.");
                MyErrors.Add(_mex);
                showErrors();
                return;
            }

            if (MyUsr == null)
            {
                simplex_ORM.Spx_ORMMessage _mex = new simplex_ORM.Spx_ORMMessage("PROBLEMI DI INDENTIFICAZIONE DELL'UTENTE CHE DEVE CAMBIARE LA PASSWORD.");
                MyErrors.Add(_mex);
                showErrors();
                return;
            }


            // CARICAMENTO DELLE INFORMAZIONI SULLA PASSWORD
            simplex_ORM.SQLSrv.SQLTable _tbl = simplex_ORM.SQLSrv.SQLTable.createSQLTable(MyConn, "ADMN_PASSWORD_INTERNE");
            try
            {
                _tbl.setValue("USR_ID", MyUsr.UID.ToString());
                _tbl.load();
                _tbl.setValue("FLAG_RESET", "0");
                _tbl.setValue("PWDMD5", MyUsr.getHash_MD5(MyPwd1));
                _tbl.setValue("USR_UM", MyUsr.UID.ToString());
                _tbl.save(true);
            }
            catch (simplex_ORM.Spx_ORMMessage _mex)
            {
                MyErrors.Add(_mex);
                showErrors();
                return;
            }

            // redirezione verso il logout
            MyApp.CurrentUser = MyUsr;
            //ViewState["OK_MESSAGE"] = "Password sostituita con successo. Prego premere OK e rieffettuare l'autenticazione per accedere al sistema.";
            //R_MessageBox.OkMessageText = "Password sostituita con successo. Prego premere OK e rieffettuare l'autenticazione per accedere al sistema.";
            R_MessageBox.show(0);
            R_MessageBox.OkMessageButtonText = "Continua";
            Session["Evt_OkButton"] = new EventHandler(R_MessageBox_Evt_OkButton);

            // disabilitazione dei controlli
            R_Pwd1.Enabled = false;
            R_Pwd2.Enabled = false;
            R_Login.Enabled = false;

            //Response.Redirect("~/Logout.aspx");
        }

        protected void R_MessageBox_Evt_OkButton(object sender, EventArgs e)
        {
            HttpContext.Current.Response.Redirect("~/Logout.aspx");
            //Response.Redirect("~/Logout.aspx");
        }

        #endregion

        #region PROPERTIES
        /// <summary>
        /// Nome della pagina visualizzato nei menu e nelle briciole di pane.
        /// @MdN
        /// ----
        /// Crtd: 18/01/2015
        /// </summary>
        public virtual String Nome
        {
            get
            {
                return MyName;
            }
            set
            {
                if (value == null)
                    MyName = "### DA DEFINIRE";
                else
                    MyName = value;
            }
        }

        #endregion

    }
}
